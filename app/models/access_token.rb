# == Schema Information
#
# Table name: access_tokens
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  token      :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AccessToken < ApplicationRecord

	# maintain user visited urls
  is_impressionable

	# ------------------- Associations --------------------- #
  belongs_to :user
end
