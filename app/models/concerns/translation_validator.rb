# This class for title_translations custom validation to make it uniqueness
class TranslationValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    record.errors.add(attribute, :name_taken) if record.class.
      where("name_translations->>'en' = ? OR name_translations->>'ar' = ?", value["en"], value["ar"]).
      where.not(id: record.id).exists?
  end
end
