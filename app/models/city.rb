# == Schema Information
#
# Table name: cities
#
#  id                :integer          not null, primary key
#  name_translations :jsonb            not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  published         :boolean          default(FALSE)
#  soft_delete       :boolean          default(FALSE)
#

class City < ApplicationRecord

	# maintain user visited urls
  is_impressionable

	#-------------------- Associations -------------------------------------- #
	has_many :units
	has_one :picture, as: :imageable, dependent: :destroy
	has_one :top_destination, dependent: :destroy
	accepts_nested_attributes_for :picture

	translates :name

	# ---------------------------- Scope--------------------------- #
  scope :published, lambda{ where(published: true)}
	scope :not_deleted, lambda{ where(soft_delete: false)}


	# -------------------------- validations---------------------- #
  # validates :name_translations, presence: true, uniqueness: true, on: :create
	validates :name_translations, presence: true, translation: true

	# ------------------------ Sunspot search------------------ #
  searchable do

  # ---------------------- Same Model Fields ----------------- #
  integer :id
  time    :created_at
  boolean :soft_delete
  boolean :published

  # ------------------ added with admin v3 ---------------------- #

  text :name_translations, :id

	# ------------------ added with admin v6 ---------------------- #

	integer :most_searched
  end


  # ------------------- Mobile Api response --------------------- #
  api_accessible :city do |t|
		t.add :id
  	t.add :name
		t.add :most_searched
  end

	api_accessible :cities_list do |t|
  	t.add :id
  	t.add :name
		t.add :most_searched
	end


  # ------------------- Mobile Cites List --------------------- #
  api_accessible :v3_cities_list do |t|
  	t.add :id
  	t.add :name
		t.add :units_no
    t.add lambda{|city| city.name_en }, :as => :name_en
    t.add lambda{|city| city.name_ar }, :as => :name_ar
	end

	api_accessible :v3_city_pictures, extend: :v3_cities_list do |t|
		# Here as: 'images' is used as alias for pictures
		# Show default image url if unit has no image
		t.add :picture, as: 'image', :template => :images
	end


 # ------------------- Admin Cites List --------------------- #
	api_accessible :v3_cities_list_admin do |t|
  	t.add :id
  	t.add :name
		t.add lambda{|city| city.name_en }, :as => :name_en
		t.add lambda{|city| city.name_ar }, :as => :name_ar
		t.add :no_of_units, :as => :units_no
		t.add :published
		t.add :soft_delete
		t.add :most_searched
		t.add :default_image
	end

	api_accessible :v3_city_pictures_admin, extend: :v3_cities_list_admin do |t|
		# Here as: 'images' is used as alias for pictures
		# Show default image url if unit has no image
		t.add lambda{|city| city.top_destination.present? ? true : false}, as: 'flag'
		t.add :picture, as: 'image', :template => :images
	end

	api_accessible :v6_top_destination_admin, extend: :v3_city_pictures_admin do |t|
		# Here as: 'images' is used as alias for pictures
		# Show default image url if unit has no image

		t.add :top_destination, as: 'top_destination', :template => :top_destination
	end

	# ------------------- published and unpublished cities --------------------- #
  api_accessible :v3_published_and_unpublished do |t|
  	t.add :id
		t.add :name
		t.add lambda{|city| city.name_en }, :as => :name_en
		t.add lambda{|city| city.name_ar }, :as => :name_ar
		t.add :published
		t.add :picture, as: 'image', :template => :images
	end




  # to list number of units in the city for user in the mobile app
	def units_no
		units.where(soft_delete: false).where(unit_status: true).where(unit_enabled: true).size
	end

	# to list number of units in the city for user in the mobile app
	def no_of_units
		units.where(soft_delete: false).size
	end


	def default_image
		CONSTANT[:cdn_cloudy_url] + CONSTANT[:default_flat_image]
	end
end
