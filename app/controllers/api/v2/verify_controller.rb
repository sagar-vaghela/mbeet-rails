class Api::V2::VerifyController < Api::V2::BaseController
	before_action :login_required, :set_user, except: [:email]
	before_action :check_phone_already_verified, only: [:phone]

	# GET /v2/verify/resend/email
	def resend
		@user = User.find(@current_user)
		if @user.present?
			@user.resend_verify_email
			render_data({}, I18n.t('user.email_verify_resend'))
		end
	end

	# GET /v2/verify/email
	def email
		user = User.find_by(confirm_email_token: params[:token])
		if user.present?
			user.skip_password_validation = true
			render_data({ users: user.as_api_response(:user_pictures) }, I18n.t('user.email_verified_success')) if user.update!(email_verified: true)
		end
	rescue StandardError => e
		error_valid_data(e)
	end

	# PUT /v2/verify/:user_id/phone
	def phone
		if @user.present? && @user.phone.present?
			@user.skip_password_validation = true
			@user.update(phone_verified: params[:phone_verified], verify_phone_sent_at: Time.zone.now)
			render_data({ users: @user.as_api_response(:user_pictures) }, I18n.t('user.data_updated')) if @user.save!
		else
			error_valid_data(I18n.t('user.phone_is_null'))
		end
	rescue StandardError => e
		error_valid_data(e)
	end

	private
	def set_user
		@user = User.find(params[:id])
	end

	def check_phone_already_verified
		render_data({}, I18n.t('user.phone_already_verified')) if @user.phone_verified == true
	end
end
