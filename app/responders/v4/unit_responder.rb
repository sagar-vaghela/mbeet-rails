module V4::UnitResponder
  extend ActiveSupport::Concern

  included do
    # ---------------------- Version 2 Api's ------------------------- #
    api_accessible :v4_unit_only do |t|
      t.add :id
      t.add :title
      t.add :body
      t.add lambda{|unit| unit.title_en }, :as => :title_en
      t.add lambda{|unit| unit.title_ar }, :as => :title_ar
      t.add lambda{|unit| unit.body_en }, :as => :body_en
      t.add lambda{|unit| unit.body_ar }, :as => :body_ar
      t.add :bed_type
      t.add :number_of_guests
      t.add :number_of_rooms
      t.add :number_of_beds
      t.add :number_of_baths
      t.add lambda{|unit| unit.number_of_subunits == 0 ? 1 : unit.number_of_subunits }, :as => :number_of_subunits
      t.add :available_as
      t.add :city, :template => :city
      t.add :address
      t.add :price
      t.add :latitude
      t.add :longitude
      t.add :unit_class
      t.add :unit_type
      t.add :total_area
      t.add :created_at
      t.add lambda{ |unit|
      	avg_array = unit.feedbacks.map(&:star)
      	if avg_array.present?
      		avg_array.inject(0.0) { |sum, el| sum + el } / avg_array.size
      	else
      		0
      	end
      } , :as => :avg_star
      t.add lambda{ |unit| unit.feedbacks.size }, :as => :feedback_count
      t.add :liked_by_user
      t.add :unit_status
      t.add :unit_enabled
      t.add :amenities, :template => :amenities_and_units
      t.add :rules, :template => :rules_and_units
      t.add :user_id
      t.add lambda { |unit|
        # unit.unit_disabled_dates.where("day_disabled >= '#{Time.now.to_date}'").order(day_disabled: :asc)
        disabled_dates_array = unit.unit_disabled_dates
        disabled_dates_array.select do |a|
          a if a[:day_disabled].to_date >= Time.now.utc.to_date
        end
      }, :as => :units_disabled , :template => :disabled_dates
    end

    api_accessible :v4_user, extend: :v4_unit_only do |t|
      t.add :user, :template => :user_for_unit, as: 'owner'
    end

    api_accessible :v4_unit_with_feedbacks, extend: :v4_user do |t|
      t.add :feedbacks, :template => :user_with_feedback
    end

    api_accessible :v4_amenities_and_units, extend: :v4_unit_with_feedbacks do |t|
      t.add :amenities, :template => :amenities_and_units
    end

    api_accessible :v4_rules_and_units, extend: :v4_amenities_and_units do |t|
      t.add :rules, :template => :rules_and_units
    end

    api_accessible :v4_unit_pictures, extend: :v4_rules_and_units do |t|
      # Here as: 'images' is used as alias for pictures
      # Show default image url if unit has no image
      t.add :pictures, as: 'images', :template => :images
      t.add :default_image
    end

    api_accessible :v4_unit_with_pictures, extend: :v4_unit_only do |t|
      # Here as: 'images' is used as alias for pictures
      # Show default image url if unit has no image
      t.add :pictures, as: 'images', :template => :images
      t.add :default_image
    end

    # get count of bookings for unit of owners
    api_accessible :dashbord_owners, extend: :v4_unit_only do |t|
      t.add lambda{ |unit|
        unit.bookings.count
      } , :as => :life_time_booking

      t.add lambda{ |unit|
        unit.bookings.where('DATE(check_in) = ?', Date.today()).count
        } , :as => :total_booking_of_today

      t.add lambda{ |unit|
        unit.bookings.where(check_in: Time.now.beginning_of_month..Time.now.end_of_month).count
      } , :as => :total_booking_of_month
    end

    def self.liked_by_user(user_id, unit_id)
      UserLike.exists?(user_id: user_id, unit_id: unit_id) ? 1 : 0
    end
  end
end
