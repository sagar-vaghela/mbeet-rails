class RemoveCityColumnFromUnit < ActiveRecord::Migration[5.0]
  def change
  	remove_column :units, :city
  end
end
