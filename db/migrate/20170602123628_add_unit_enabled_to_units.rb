class AddUnitEnabledToUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :unit_enabled, :boolean, default: false
  end
end
