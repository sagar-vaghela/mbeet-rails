FactoryBot.define do
  factory :coupon do
    association :user, factory: :admin_user
    value "10"
    starting_date "Fri, 01 Dec 2017"
    expiration_date "Mon, 01 Jan 2018"
    status "disable"
  end
end
