class Api::Admin::V6::CitiesController < Api::Admin::V5::CitiesController
  before_action :admin_login_required
  before_action :find_city, only: [:show, :update, :destroy]
  before_action :find_current_user

  # GET: '/admin/v6/cities/top_destinations'
  def top_destinations
    impressionist(@user, request.path) if @user

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]

    search = City.search do
      fulltext params[:search]
      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      if params[:published].present?
        status = params[:published] == 'true' ? true : false
        with(:published, status)
      end

      order_by(:most_searched, :desc)
      paginate(:offset => params[:offset], :per_page => limit)
    end

    render_data({ total: City.not_deleted.size, result_total: search.total, top_destinations: search.results.as_api_response(:v3_city_pictures_admin)}, I18n.t('city.cities_list'))
  end

  # GET: '/admin/v6/cities/city_list_with_unit'
  def city_list_with_unit
    impressionist(@user, request.path) if @user

    city_ids = Unit.pluck(:city_id)
    city_list = City.where('id IN(?)', city_ids)

    render_data({ total: city_list.size, result_total: city_list.size, cities: city_list.as_api_response(:v3_city_pictures_admin)}, I18n.t('city.cities_list'))
  end

  private

  def find_city
    @city = City.find(params[:id])
  end

  def find_current_user
    @user = User.find_by(id: @current_user, role_id: [1, 2])
  end

end
