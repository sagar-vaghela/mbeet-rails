class AddColumnsToBookings < ActiveRecord::Migration[5.0]
  def change
  	add_column :bookings, :guests, :integer, null: false, default: 0
  	add_column :bookings, :for_days, :integer, null: false, default: 0
  	add_column :bookings, :sub_total_amount, :decimal, precision: 10, scale: 2, null: false, default: '0'
  	add_column :bookings, :total_amount, :decimal, precision: 10, scale: 2, null: false, default: '0'
  end
end
