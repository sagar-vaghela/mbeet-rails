class Api::Admin::V3::BookingsController < Api::Admin::V3::BaseController
  before_action :admin_login_required

  before_action :check_bookings, only: :index
  before_action :find_booking, only: [:show, :cancel]
  # GET: admin/v3/bookings
  def index

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = Booking.search do
      fulltext params[:search]
      if params[:booking_cancelled].present?
        status = params[:booking_cancelled] == 'true' ? true : false
        with(:booking_cancelled, status)
      end
      with(:payment_status, params[:payment_status]) if params[:payment_status].present?
      with(:payment_method, params[:payment_method] )   if params[:payment_method].present?
      with(:lease_status, params[:lease_status] )   if params[:lease_status].present?
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end

      paginate(:offset => params[:offset], :per_page => limit)

    end

    render_data({ total: Booking.all.size, result_total: search.total, booking_list: search.results.as_api_response(:v3_booking_with_unit_and_user) }, I18n.t('admin.booking_list') )
  end

  # GET: admin/v3/bookings/:id
  def show
    render_data({ booking: @booking.as_api_response(:v3_booking_with_unit_and_user) }, I18n.t('admin.booking_detail')) if @booking.present?
  end

  # DELETE: admin/v3/bookings/:id/cancel
  def cancel
    if @booking.update(payment_status: 2, booking_cancelled: true)
      render_updated ({
        message: I18n.t('admin.booking_cancelled'),
        data: { Booking: @booking.as_api_response(:v3_booking_with_unit_and_user) }
      })
    else
      render_unprocessable_entity( message: @booking.errors.full_messages.join(', '))
    end
  end

  private
  def check_bookings
    @booking_list = Booking.all
                            .order(id: :desc)
                            .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                            .offset(params[:offset])
    render_data({}, I18n.t('no_data')) unless @booking_list.present?
  end

  def find_booking
    @booking = Booking.find(params[:id])
  end
end
