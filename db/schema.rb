# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180323064046) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_tokens", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.text     "token",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_access_tokens_on_user_id", using: :btree
  end

  create_table "amenities", force: :cascade do |t|
    t.jsonb    "name_translations",                null: false
    t.boolean  "is_active",         default: true, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "amenities_units", id: false, force: :cascade do |t|
    t.integer "unit_id",    null: false
    t.integer "amenity_id", null: false
    t.index ["amenity_id"], name: "index_amenities_units_on_amenity_id", using: :btree
    t.index ["unit_id"], name: "index_amenities_units_on_unit_id", using: :btree
  end

  create_table "booked_subunits", force: :cascade do |t|
    t.integer  "unit_id"
    t.datetime "day_booked"
    t.integer  "count",      default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["day_booked", "count"], name: "index_booked_subunits_on_day_booked_and_count", using: :btree
    t.index ["unit_id"], name: "index_booked_subunits_on_unit_id", using: :btree
  end

  create_table "bookings", force: :cascade do |t|
    t.integer  "user_id",                                                    null: false
    t.integer  "unit_id",                                                    null: false
    t.datetime "check_in",                                                   null: false
    t.datetime "check_out",                                                  null: false
    t.integer  "payment_method",                             default: 1,     null: false
    t.integer  "payment_status",                             default: 1,     null: false
    t.integer  "lease_status",                               default: 2,     null: false
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.integer  "guests",                                     default: 0,     null: false
    t.integer  "for_days",                                   default: 0,     null: false
    t.decimal  "sub_total_amount",  precision: 10, scale: 2, default: "0.0", null: false
    t.decimal  "total_amount",      precision: 10, scale: 2, default: "0.0", null: false
    t.boolean  "booking_cancelled",                          default: false, null: false
    t.decimal  "mbeet_fees",        precision: 10, scale: 2
    t.decimal  "advance_payment",   precision: 10, scale: 2, default: "0.0"
    t.integer  "coupon_id"
    t.index ["coupon_id"], name: "index_bookings_on_coupon_id", using: :btree
    t.index ["unit_id"], name: "index_bookings_on_unit_id", using: :btree
    t.index ["user_id"], name: "index_bookings_on_user_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.jsonb    "name_translations",                 null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "published",         default: false
    t.boolean  "soft_delete",       default: false
    t.integer  "most_searched",     default: 0
  end

  create_table "coupons", force: :cascade do |t|
    t.integer  "creator_id",      null: false
    t.integer  "value",           null: false
    t.datetime "starting_date",   null: false
    t.datetime "expiration_date", null: false
    t.integer  "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.text     "code"
    t.index ["creator_id"], name: "index_coupons_on_creator_id", using: :btree
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "user_id",                                            null: false
    t.integer  "unit_id",                                            null: false
    t.decimal  "star",       precision: 3, scale: 1, default: "0.0", null: false
    t.text     "feedback",                                           null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "booking_id",                                         null: false
    t.index ["booking_id"], name: "index_feedbacks_on_booking_id", using: :btree
    t.index ["unit_id"], name: "index_feedbacks_on_unit_id", using: :btree
    t.index ["user_id"], name: "index_feedbacks_on_user_id", using: :btree
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.text     "params"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
    t.index ["user_id"], name: "index_impressions_on_user_id", using: :btree
  end

  create_table "login_attempts", force: :cascade do |t|
    t.boolean  "success"
    t.integer  "user_id",    null: false
    t.text     "ip"
    t.text     "user_agent"
    t.text     "os"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_login_attempts_on_user_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id",                    null: false
    t.integer  "booking_id"
    t.jsonb    "body",                       null: false
    t.integer  "push_type",                  null: false
    t.boolean  "read",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["booking_id"], name: "index_notifications_on_booking_id", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "payment_txns", force: :cascade do |t|
    t.integer  "booking_id",                    null: false
    t.integer  "payment_status",    default: 0, null: false
    t.text     "invoice_unique_id",             null: false
    t.datetime "modified_on",                   null: false
    t.text     "transaction_id"
    t.index ["booking_id"], name: "index_payment_txns_on_booking_id", using: :btree
  end

  create_table "periods", force: :cascade do |t|
    t.integer  "unit_id"
    t.float    "price"
    t.date     "period_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.string   "name",                           null: false
    t.string   "imageable_type",                 null: false
    t.integer  "imageable_id",                   null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "locale",         default: false
    t.index ["imageable_type", "imageable_id"], name: "index_pictures_on_imageable_type_and_imageable_id", using: :btree
  end

  create_table "reasons", force: :cascade do |t|
    t.jsonb    "title_translations", null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "request_to_list_units", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.string   "name",       null: false
    t.string   "email",      null: false
    t.string   "phone",      null: false
    t.text     "message",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_request_to_list_units_on_user_id", using: :btree
  end

  create_table "role_permissions", force: :cascade do |t|
    t.text     "permitted_actions"
    t.integer  "role_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["role_id"], name: "index_role_permissions_on_role_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rules", force: :cascade do |t|
    t.jsonb    "name_translations",                null: false
    t.boolean  "status",            default: true, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "rules_units", id: false, force: :cascade do |t|
    t.integer "unit_id", null: false
    t.integer "rule_id", null: false
    t.index ["rule_id"], name: "index_rules_units_on_rule_id", using: :btree
    t.index ["unit_id"], name: "index_rules_units_on_unit_id", using: :btree
  end

  create_table "top_destinations", force: :cascade do |t|
    t.integer  "city_id"
    t.jsonb    "description_translations", null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["city_id"], name: "index_top_destinations_on_city_id", using: :btree
  end

  create_table "trending_cities", force: :cascade do |t|
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "unit_disabled_dates", force: :cascade do |t|
    t.integer  "unit_id"
    t.datetime "day_disabled"
    t.integer  "number_of_disabled_units", default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["unit_id"], name: "index_unit_disabled_dates_on_unit_id", using: :btree
  end

  create_table "units", force: :cascade do |t|
    t.integer  "user_id",                                                               null: false
    t.jsonb    "title_translations",                                                    null: false
    t.jsonb    "body_translations",                                                     null: false
    t.integer  "number_of_guests",   limit: 2,                          default: 0,     null: false
    t.integer  "number_of_rooms",    limit: 2,                          default: 1,     null: false
    t.integer  "number_of_beds",     limit: 2,                          default: 0,     null: false
    t.integer  "number_of_baths",    limit: 2,                          default: 0,     null: false
    t.integer  "available_as",       limit: 2,                          default: 1,     null: false
    t.text     "address",                                                               null: false
    t.decimal  "price",                        precision: 10, scale: 2, default: "0.0", null: false
    t.decimal  "service_charge",               precision: 6,  scale: 2, default: "0.0", null: false
    t.decimal  "latitude",                     precision: 20, scale: 8
    t.decimal  "longitude",                    precision: 20, scale: 8
    t.string   "unit_class",                                            default: "a",   null: false
    t.boolean  "soft_delete",                                           default: false, null: false
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.integer  "room_type",                                             default: 1,     null: false
    t.integer  "city_id",                                               default: 1,     null: false
    t.integer  "unit_type",          limit: 2,                          default: 1,     null: false
    t.decimal  "total_area",                   precision: 20, scale: 3, default: "0.0", null: false
    t.boolean  "unit_enabled",                                          default: false
    t.boolean  "unit_status",                                           default: false
    t.integer  "mbeet_percentage",                                      default: 10
    t.integer  "bed_type"
    t.integer  "number_of_subunits",                                    default: 0
    t.text     "message"
    t.index ["city_id"], name: "index_units_on_city_id", using: :btree
    t.index ["number_of_baths"], name: "index_units_on_number_of_baths", using: :btree
    t.index ["number_of_beds"], name: "index_units_on_number_of_beds", using: :btree
    t.index ["number_of_guests"], name: "index_units_on_number_of_guests", using: :btree
    t.index ["number_of_rooms"], name: "index_units_on_number_of_rooms", using: :btree
    t.index ["price"], name: "index_units_on_price", using: :btree
    t.index ["user_id"], name: "index_units_on_user_id", using: :btree
  end

  create_table "user_booking_cancel_requests", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "reason_id",  null: false
    t.text     "message",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "booking_id", null: false
    t.index ["booking_id"], name: "index_user_booking_cancel_requests_on_booking_id", using: :btree
    t.index ["user_id"], name: "index_user_booking_cancel_requests_on_user_id", using: :btree
  end

  create_table "user_lang_settings", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.string   "language",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_lang_settings_on_user_id", using: :btree
  end

  create_table "user_likes", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "unit_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unit_id"], name: "index_user_likes_on_unit_id", using: :btree
    t.index ["user_id"], name: "index_user_likes_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.integer  "role_id",                              null: false
    t.string   "name",                                 null: false
    t.string   "email",                                null: false
    t.string   "phone"
    t.boolean  "is_owner",             default: false, null: false
    t.string   "facebook_uid"
    t.string   "twitter_uid"
    t.string   "gmail_uid"
    t.string   "password_digest"
    t.string   "reset_digest"
    t.datetime "reset_sent_on"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "gender"
    t.date     "date_of_birth"
    t.datetime "verify_email_sent_at"
    t.boolean  "email_verified",       default: false, null: false
    t.datetime "verify_phone_sent_at"
    t.boolean  "phone_verified",       default: false, null: false
    t.string   "confirm_email_token"
    t.string   "country_code"
    t.boolean  "soft_delete",          default: false
    t.integer  "otp"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
  end

  add_foreign_key "access_tokens", "users"
  add_foreign_key "booked_subunits", "units"
  add_foreign_key "bookings", "coupons"
  add_foreign_key "bookings", "units"
  add_foreign_key "bookings", "users"
  add_foreign_key "feedbacks", "bookings"
  add_foreign_key "feedbacks", "units"
  add_foreign_key "feedbacks", "users"
  add_foreign_key "login_attempts", "users"
  add_foreign_key "notifications", "bookings"
  add_foreign_key "notifications", "users"
  add_foreign_key "payment_txns", "bookings"
  add_foreign_key "request_to_list_units", "users"
  add_foreign_key "role_permissions", "roles"
  add_foreign_key "top_destinations", "cities"
  add_foreign_key "unit_disabled_dates", "units"
  add_foreign_key "units", "cities"
  add_foreign_key "units", "users"
  add_foreign_key "user_booking_cancel_requests", "bookings"
  add_foreign_key "user_booking_cancel_requests", "reasons", on_delete: :cascade
  add_foreign_key "user_booking_cancel_requests", "users"
  add_foreign_key "user_lang_settings", "users"
  add_foreign_key "user_likes", "units"
  add_foreign_key "user_likes", "users"
  add_foreign_key "users", "roles"
end
