module Units
  module Associations
    extend ActiveSupport::Concern

    included do
      attr_accessor :liked_by_user
      attr_accessor :special_prices

      translates :title, :body

      # ---------------------- Associations ---------------------- #
      belongs_to :user
      belongs_to :city

      has_many :bookings, dependent: :destroy
      has_many :users, through: :bookings

      has_many :user_likes, dependent: :destroy
      has_many :feedbacks, dependent: :destroy

      has_many :pictures, as: :imageable, dependent: :destroy
      accepts_nested_attributes_for :pictures, allow_destroy: true

      has_many :user_booking_cancel_requests, dependent: :destroy

      has_and_belongs_to_many :amenities, dependent: :destroy
      has_and_belongs_to_many :rules, dependent: :destroy

      has_many :booked_subunits, dependent: :destroy
      has_many :unit_disabled_dates, dependent: :destroy
      has_many :unit_disabled_dates, dependent: :destroy
      has_many :periods, dependent: :destroy
      scope :is_deleted, -> { where(soft_delete: false) }
    end
  end
end
