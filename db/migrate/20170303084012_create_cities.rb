class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
    	t.column :name_translations, 'jsonb', null: false
      t.timestamps
    end
  end
end
