class Api::Admin::V1::UnitsController < Api::Admin::V1::BaseController
  before_action :admin_login_required
  before_action :check_unit, except: [:index, :create]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy

  # GET /admin/v1/units
  def index
    @units = Unit.order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
    render_data({ total: Unit.count, units: @units.as_api_response(:admin_unit_only)}, I18n.t('unit.list')) if @units
  end

  # GET /admin/v1/units/:id
  def show
    render_data({ units: @unit.as_api_response(:admin_unit_only) }, I18n.t('unit.detail')) if @unit
  end

  # POST /admin/v1/units
  def create
    @unit = Unit.new(unit_params)
    @unit.title_translations = {en: @unit.title_en, ar: @unit.title_ar}
    @unit.body_translations = {en: @unit.body_en, ar: @unit.body_ar}
    render_data({ units: {}}, I18n.t('admin.unit.created')) if  @unit.save!
  end

  # PUT /admin/v1/users/:id
  def update
    @unit.title_translations = {en: @unit.title_en, ar: @unit.title_ar}
    @unit.body_translations = {en: @unit.body_en, ar: @unit.body_ar}

    render_data({ units: @unit.as_api_response(:admin_unit_only) }, I18n.t('admin.unit.updated')) if @unit.update(unit_params)
  end

  # DELETE /admin/v1/units/:id
  def destroy
    render_data({ units: {} }, I18n.t('admin.unit.deleted')) if @unit.update(soft_delete: true)
  end

  # PUT /admin/v1/units/:id/restore
  def restore
    render_data({ units: {} }, I18n.t('admin.unit.restored')) if @unit.update(soft_delete: false)
  end

  private
  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar, :room_type, :number_of_guests,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id,
                  :address, :price, :service_charge, :latitude, :longitude, :unit_class, :unit_type,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @unit = Unit.find(params[:id])
  end

  def rescue_response
  rescue StandardError => e
    error_valid_data(e)
  end

  def delete_liked_unit
    @unit_likes = UserLike.where(unit_id: params[:id])
    @unit_likes.delete_all if @unit_likes.present?
  end

end
