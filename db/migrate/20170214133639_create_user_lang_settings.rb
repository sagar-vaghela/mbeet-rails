class CreateUserLangSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :user_lang_settings do |t|
      t.references :user, foreign_key: true, null: false
      t.string     :language, null: false

      t.timestamps
    end
  end
end
