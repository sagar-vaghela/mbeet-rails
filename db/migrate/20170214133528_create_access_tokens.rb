class CreateAccessTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :access_tokens do |t|
      t.references :user, foreign_key: true, null: false
      t.text :token, null: false

      t.timestamps
    end
  end
end
