module V6::Admin::UserResponder
	extend ActiveSupport::Concern
	extend V3::Admin::UserResponder

	included do
		# -------------------- Admin User API Response ------------------ #

		api_accessible :v6_user_with_permissions, extend: :v3_user_only do |t|
      t.add lambda{|user| user.role.role_permission.permitted_actions}, as: :user_permissions
	  end

		api_accessible :v6_logs, extend: :v6_user_with_permissions do |t|
			t.add lambda{|user| user.impressions }, as: :user_visits
		end

	end
end
