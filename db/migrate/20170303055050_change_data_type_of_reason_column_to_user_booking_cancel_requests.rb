class ChangeDataTypeOfReasonColumnToUserBookingCancelRequests < ActiveRecord::Migration[5.0]
  def self.up
  	change_column :user_booking_cancel_requests, :reason, 'integer USING CAST(reason AS integer)'
  	rename_column :user_booking_cancel_requests, :reason, :reason_id
  	add_foreign_key :user_booking_cancel_requests, :reasons, on_delete: :cascade, null: false
  	add_column :user_booking_cancel_requests, :check_in, :timestamp, null: false 
  	add_column :user_booking_cancel_requests, :check_out, :timestamp, null: false 
  end

  def self.down
  	remove_foreign_key :user_booking_cancel_requests, :reasons
  	remove_column :user_booking_cancel_requests, :check_in
  	remove_column :user_booking_cancel_requests, :check_out
  end
end
