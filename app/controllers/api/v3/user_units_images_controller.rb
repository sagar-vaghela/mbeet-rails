class Api::V3::UserUnitsImagesController < Api::V3::BaseController
  before_action :login_required
  before_action :find_image, only: :destroy

  def destroy
    if  @image.destroy
     render_deleted ({
       message: I18n.t('image.deleted'),
       data: { city:  @image.as_api_response(:images) }
     })
    else
     render_unprocessable_entity( message: @image.errors.full_messages.join(', '))
    end
	end

  ##----------------------------- Private Methods ----------------------------##

  private

  def find_image
    @image = Picture.find(params[:id])
  end
  ##--------------------------------------- end ------------------------------##
end
