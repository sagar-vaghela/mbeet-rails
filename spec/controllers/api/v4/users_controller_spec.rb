require 'rails_helper'
require 'jwt'

RSpec.describe Api::V4::UsersController, type: :controller do

  before(:each) do
    @role = FactoryBot.create(:role_with_id)
    @role.users << FactoryBot.create(:owner_user)
  end

  describe '#dashbord_owners' do

    context '/v4/dashbord_owners' do
      it 'should display units statistics with total bookings' do

        token = JsonWebToken.encode(user_id: @role.users.first.id) if @role.users

        get :dashbord_owners, params: {access_token: token, owner: @role.users.first, format: :json}

        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status 200

      end

    end
  end

end
