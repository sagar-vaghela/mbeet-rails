# == Schema Information
#
# Table name: user_lang_settings
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  language   :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserLangSetting < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
	# ------------------- Associations ------------------- #
  belongs_to :user

  # ------------------- Validation --------------------- #
  validates :user, presence: { message: "is not a valid ID" }
end
