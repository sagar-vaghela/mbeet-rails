# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  role_id              :integer          not null
#  name                 :string           not null
#  email                :string           not null
#  phone                :string
#  is_owner             :boolean          default(FALSE), not null
#  facebook_uid         :string
#  twitter_uid          :string
#  gmail_uid            :string
#  password_digest      :string
#  reset_digest         :string
#  reset_sent_on        :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  gender               :integer
#  date_of_birth        :date
#  verify_email_sent_at :datetime
#  email_verified       :boolean          default(FALSE), not null
#  verify_phone_sent_at :datetime
#  phone_verified       :boolean          default(FALSE), not null
#  confirm_email_token  :string
#  country_code         :string
#  soft_delete          :boolean          default(FALSE)
#

class User < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  include OtpAuth
  # ------------------------ Associations --------------------- #
  include Users::Associations

  # ------------------------ Validations ---------------------- #
  include Users::Validations

  # ------------------------ Model Methods -------------------- #
  include Users::Methods

  # ------------------------ Mobile Api Response -------------- #
  include V1::UserResponder
  include V2::UserResponder
  include V3::UserResponder

  # ------------------------ Admin Api Response --------------- #
  include V1::Admin::UserResponder
  include V2::Admin::UserResponder
  include V3::Admin::UserResponder
  include V4::Admin::UserResponder
  include V6::Admin::UserResponder


  # ---------------------- Same Model Fields ----------------- #

  searchable do
    text :name, :email, :phone, :gender, :id
    integer :id
    string :name
    string :email
    string :phone
    boolean :email_verified
    boolean :phone_verified
    boolean :is_owner
    boolean :soft_delete
    time    :created_at
    string :role do
      role.name
    end
  end

  api_accessible :user do |t|
    t.add :id
    t.add :name
    t.add :email
  end

  api_accessible :user_permissions do |t|
    t.add lambda{|user| user.role.role_permission.permitted_actions if user.role.role_permission.present?}, :as => :user_permission
  end

  # ------------------------ Api Methods ---------------------- #
  ## acts_as_api Json Response methods

  api_accessible :customer_phone_no_name do |t|
    t.add :email
  	t.add :phone
    t.add :name
  end

  def access_token
  	self.access_tokens.last.token
  end

  def default_image
    CONSTANT[:cdn_cloudy_url] + CONSTANT[:default_user_image]
  end

  def type
    if facebook_uid.present?
      "facebook"
    elsif twitter_uid.present?
      "twitter"
    elsif gmail_uid.present?
      "gmail"
    else
      "normal"
    end
  end
end
