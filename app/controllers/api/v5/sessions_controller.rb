class Api::V5::SessionsController < Api::V4::SessionsController
  before_action :check_null_password, only: [ :create_owner, :create ]

  # POST /v2/login #login for owner

  def create_owner
    if @user && @user.authenticate(params[:password])
      if @user.units.count > 0
        if !@user.soft_delete
          auth_token = JsonWebToken.encode({user_id: @user.id})
          render_data( { auth_token: auth_token, users: @user.as_api_response(:v2_user_only) }, I18n.t('user.login_success') )
        else
          render_unprocessable_entity( message: I18n.t('.errors.account_deleted'))
        end
      else
        error_data( I18n.t('user.owner_login_error') )
      end
    else
      error_data( I18n.t('user.login_error') )
    end
  end

  def contact_us
    UserMailer.contact_us_email(params).deliver
    render_data( { }, I18n.t('user.contact_us') )
  end

  private

  def check_null_password
    @user = User.find_by(email: params[:email].downcase)

    if @user.present?
      error_valid_data(I18n.t('user.wrong_signin_way')) if @user.password_digest.nil?
    end
  end

end
