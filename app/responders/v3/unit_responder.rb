module V3::UnitResponder
	extend ActiveSupport::Concern

	included do
		# ---------------------- Version 2 Api's ------------------------- #
	  api_accessible :v3_unit_only do |t|
	    t.add :id
	    t.add :title
	    t.add :body
			t.add lambda{|unit| unit.title_en }, :as => :title_en
	    t.add lambda{|unit| unit.title_ar }, :as => :title_ar
	    t.add lambda{|unit| unit.body_en }, :as => :body_en
	    t.add lambda{|unit| unit.body_ar }, :as => :body_ar

	    # t.add lambda{|unit| case unit.room_type; when 1 then "Entire Place"; when 2 then "Private Room"; when 3 then "Shared Room"; end }, :as => :room_type
	    t.add :number_of_guests
	    t.add :number_of_rooms
	    t.add :number_of_beds
	    t.add :number_of_baths
			t.add :available_as
	    # t.add lambda{|unit| case unit.available_as; when 1 then "daily"; when 2 then "weekly"; when 3 then "monthly"; when 4 then "+3 months"; end } , :as => :available_as
	    t.add :city, :template => :city
	    t.add :address
	    t.add :price
	    t.add :service_charge
	    t.add :latitude
	    t.add :longitude
	    t.add :unit_class
	    t.add :unit_type
			# t.add lambda{|unit| case unit.unit_type; when 1 then "Resort"; when 2 then "Villa"; when 3 then "Apartment"; when 4 then "Shaleeh"; when 5 then "Private Room"; when 6 then "Shared Room"; end }, :as => :unit_name
	    t.add :total_area
	    t.add :created_at
	    t.add :avg_star
	    t.add :feedback_count
	    t.add :liked_by_user
			# t.add lambda{|unit| unit.unit_status ?  "Published" : "Waiting for approval" } , :as => :unit_status
			t.add :unit_status
			t.add :unit_enabled
			t.add :amenities, :template => :amenities_and_units
		  t.add :rules, :template => :rules_and_units
			t.add :user_id
	  end

	  api_accessible :v3_user, extend: :v3_unit_only do |t|
	    t.add :user, :template => :user_for_unit, as: 'owner'
	  end

	  api_accessible :v3_unit_with_feedbacks, extend: :v3_user do |t|
	    t.add :feedbacks, :template => :user_with_feedback
	  end

	  api_accessible :v3_amenities_and_units, extend: :v3_unit_with_feedbacks do |t|
	    t.add :amenities, :template => :amenities_and_units
	  end

	  api_accessible :v3_rules_and_units, extend: :v3_amenities_and_units do |t|
	    t.add :rules, :template => :rules_and_units
	  end

	  api_accessible :v3_unit_pictures, extend: :v3_rules_and_units do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end

	  api_accessible :v3_unit_with_pictures, extend: :v3_unit_only do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end
	end
end
