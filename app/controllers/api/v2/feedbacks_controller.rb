class Api::V2::FeedbacksController < Api::V2::BaseController
	before_action :login_required, :already_reviewed, only: :create
	before_action :get_feedbacks, :get_count, only: :index

	def index
		render_data({ total: @count, feedbacks: @feedbacks.as_api_response(:user_with_feedback) }, I18n.t('unit.feedback.list') )
	end

	def create
		@feedback = Feedback.create!(feedback_params)
		render_data({ feedbacks: @feedback.as_api_response(:user_with_feedback) }, I18n.t('unit.feedback.success') ) if @feedback
	rescue StandardError => e
		error_valid_data(e)	
	end

	private
	def feedback_params
		params.permit(:user_id, :unit_id, :booking_id, :star, :feedback) 
	end

	def get_feedbacks
		@feedbacks = Feedback.where(unit_id: params[:unit_id])
		                     .order(created_at: :desc)
		                     .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
  	                     .offset(params[:offset])
  	render_data({}, I18n.t('no_data')) unless @feedbacks.present?                     
	end

	def get_count
    @count = Feedback.where(unit_id: params[:unit_id]).count
  end

  def already_reviewed
  	feedback = Feedback.find_by(booking_id: params[:booking_id], user_id: params[:user_id])
  	error_valid_data(I18n.t('unit.feedback.already_given')) if feedback
  end
end
