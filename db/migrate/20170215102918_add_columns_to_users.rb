class AddColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :gender, :integer
  	add_column :users, :date_of_birth, :date

  	remove_index :users, :phone
  end
end
