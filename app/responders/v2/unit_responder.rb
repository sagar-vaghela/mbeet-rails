module V2::UnitResponder
	extend ActiveSupport::Concern

	included do
		# ---------------------- Version 2 Api's ------------------------- #
	  api_accessible :v2_unit_only do |t|
	    t.add :id
	    t.add :title
	    t.add :body
	    t.add lambda{|unit| case unit.room_type; when 1 then "Entire Place"; when 2 then "Private Room"; when 3 then "Shared Room"; end }, :as => :room_type
	    t.add :number_of_guests
	    t.add :number_of_rooms  
	    t.add :number_of_beds
	    t.add :number_of_baths
	    t.add lambda{|unit| case unit.available_as; when 1 then "daily"; when 2 then "weekly"; when 3 then "monthly"; when 4 then "max 3 months"; end } , :as => :available_as
	    t.add :city, :template => :city
	    t.add :address
	    t.add :price
	    t.add :service_charge
	    t.add :latitude
	    t.add :longitude
	    t.add :unit_class
	    t.add :unit_type
	    t.add :total_area
	    t.add :created_at
	    t.add :avg_star
	    t.add :feedback_count
	    t.add :liked_by_user
	  end

	  api_accessible :v2_user, extend: :v2_unit_only do |t|
	    t.add :user, :template => :user_for_unit, as: 'owner'
	  end

	  api_accessible :v2_unit_with_feedbacks, extend: :v2_user do |t|
	    t.add :feedbacks, :template => :user_with_feedback
	  end

	  api_accessible :v2_amenities_and_units, extend: :v2_unit_with_feedbacks do |t|
	    t.add :amenities, :template => :amenities_and_units
	  end

	  api_accessible :v2_rules_and_units, extend: :v2_amenities_and_units do |t|
	    t.add :rules, :template => :rules_and_units
	  end

	  api_accessible :v2_unit_pictures, extend: :v2_rules_and_units do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end

	  api_accessible :v2_unit_with_pictures, extend: :v2_unit_only do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end
	end
end