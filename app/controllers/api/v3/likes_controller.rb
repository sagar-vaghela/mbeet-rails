class Api::V3::LikesController < Api::V3::BaseController
	before_action :login_required
	before_action :already_liked, only: :create

	def create
		@like = UserLike.create!(user_like_params)
		# Store the user liked list to redis
		$redis.sadd("mbeet_user:#{params[:user_id]}:units:liked", params[:unit_id])
		render_data({ liked: 1, user_like: @like.as_api_response(:likes) }, I18n.t('user.like') )
	rescue StandardError => e
		error_valid_data(e)
	end

	def destroy
		@like = UserLike.find_by(user_id: params[:user_id], unit_id: params[:unit_id])
		if @like.present? && @like.destroy
			# Store the user liked list to redis
			$redis.srem("mbeet_user:#{params[:user_id]}:units:liked", params[:unit_id])
			render_data({ liked: 0, user_like: @like.as_api_response(:likes) }, I18n.t('user.unlike') )
		else
			error_valid_data(I18n.t('user.not_liked_yet'))
		end
	rescue StandardError => e
		error_valid_data(e)
	end

	private
	def user_like_params
		params.permit(:user_id, :unit_id)
	end

	def already_liked
		@liked = UserLike.exists?(user_id: params[:user_id], unit_id: params[:unit_id])
		render_data({ liked: 1, user_like: {} }, I18n.t('user.already_liked') ) if @liked
	end
end
