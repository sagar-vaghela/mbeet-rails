class AutoCancelBooking
	# if booking status is 1 = pending then booking will be cancelled
	def self.perform booking
    booking = Booking.find(booking['id'].to_i)
		if booking.payment_status == 1
			booking.delete_booked_days
    	booking.update(payment_status: 2, booking_cancelled: true)
			save!
		end
	end
end
