# == Schema Information
#
# Table name: bookings
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  unit_id           :integer          not null
#  check_in          :datetime         not null
#  check_out         :datetime         not null
#  payment_method    :integer          default("credit_card"), not null
#  payment_status    :integer          default(1), not null
#  lease_status      :integer          default(2), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  guests            :integer          default(0), not null
#  for_days          :integer          default(0), not null
#  sub_total_amount  :decimal(10, 2)   default(0.0), not null
#  total_amount      :decimal(10, 2)   default(0.0), not null
#  booking_cancelled :boolean          default(FALSE), not null
#  mbeet_fees        :decimal(10, 2)
#  advance_payment   :decimal(10, 2)   default(0.0)
#

class Booking < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  # ------------------ Associations ----------------------- #
  belongs_to :user
  belongs_to :unit
  belongs_to :coupon
  has_one    :payment_txn, dependent: :destroy
  has_many   :notifications, dependent: :destroy
  has_one    :user_booking_cancel_request, dependent: :destroy

  accepts_nested_attributes_for :payment_txn

  # ------------------ Validations ------------------------ #
  validates_associated :payment_txn
  validates :user, presence: { message: "is not a valid ID" }
  validates :unit, presence: { message: "is not a valid ID" }

  # ------------------ CallBacks------------------------------ #

  before_save :calculate_mbeet_fees


  # --------------------- Enums ------------------------------ #
  enum payment_method: {
  	credit_card: 1
  }

  # ------------------------ Mobile Api Response -------------- #
  include V1::BookingResponder
  include V2::BookingResponder
  include V3::BookingResponder
  include V4::BookingResponder
  include V5::BookingResponder
  include V2::Admin::BookingResponder
  include V3::Admin::BookingResponder
  include V4::Admin::BookingResponder
  include V5::Admin::BookingResponder

  # ------------------------ Sunspot search------------------ #
  searchable do
    time :check_in   # full text would be performed on these fields
    time :check_out  # full text would be performed on these fields
    boolean :booking_cancelled

    # newly added with admin v3
    integer :id
    integer :unit_id
    integer :payment_status
    integer :payment_method
    integer :lease_status
    time    :created_at
    text    :id
    integer    :total_amount

    text :unit do
      unit.title_translations
    end

    # Search by transaction_id in booking list
    text :payment_txn do
      payment_txn.try(:transaction_id)
    end

    # Search by username in booking list
    string :user do
      user.name
    end

    # Search by email in booking list
    string :email do
      user.email
    end

    string :phone do
      user.phone
    end
  end

  def feedback
    Feedback.where(user_id: self.user_id, unit_id: self.unit_id).count
  end

  def process booking, payment_status
    case payment_status.to_i
    when 4
      push_type = 5
      booking.update!( payment_status: 3 )
      Resque.enqueue(PaymentSuccessEmailWorker, booking, push_type)
      send_email_to_owner_and_customer(booking.id) # TODO

    when 2
      push_type = 6
      booking.update!( payment_status: payment_status, booking_cancelled: true )

      Resque.enqueue(PaymentFailedEmailWorker, booking, push_type)
    end

    send_push_obj = SendPush.new(booking, push_type)
    send_push_obj.send
    send_push_obj.process_in_background(SaveUserNotificationsWorker, booking, send_push_obj.message.to_json, push_type)

    # Sends owner notification if the unit is booked and payment is done
    send_owner_push_notification(booking, push_type)
  end

  def notify booking, payment_status
    send_push_obj = SendPush.new(booking, payment_status)
    send_push_obj.send

    send_push_obj.process_in_background(SaveUserNotificationsWorker, booking, send_push_obj.message.to_json, payment_status)
    if payment_status == 3
      send_push_obj.process_in_background(OrderEmailWorker, booking, payment_status)
    else
      send_push_obj.process_in_background(OrderCancelEmailWorker, booking, payment_status)
    end
  end

  #  booking will automatically get cancelled after 1hr of booking.
  def automatically_cancel_booking
    # time = Time.parse("#{self.check_in + 1.hour}")
    # time = self.check_in.strftime("%Y-%m-%d %H:%M:%S").to_time + 1.hour
    time =  Time.now + 1.hour
    Resque.enqueue_at_with_queue(
      "auto_cancel_#{self.id}_booking",
      time,
      AutoCancelBooking,
      id: self.id
    )
  end

  def calculate_mbeet_fees
    self.mbeet_fees = self.total_amount.to_f * self.unit.mbeet_percentage/100.to_f
  end

  # Sends owner notification if the unit is booked and payment is done
  def send_owner_push_notification(booking, push_type)
    if push_type.to_i == 5 && booking.unit.user.is_owner
      # this is the push type for owner notification push message [Your flat has been booked]
      push_type = 9
      # send_push_obj = SendPush.new(booking.unit, push_type)
      send_push_obj = SendPush.new(booking, push_type)
			send_push_obj.send_owner_push_notification
		  send_push_obj.process_in_background(SaveOwnerUnitBookedNotificationsWorker, booking.unit, send_push_obj.owner_flat_is_booked.to_json, push_type)
    end
  end

  def delete_booked_days
    # if the booking is cancelld then the count will be decreased in booked_units table
    (self.check_in.to_datetime...self.check_out.to_datetime).to_a.each do |day_booked|
      if self.unit.number_of_subunits == 0
        BookedSubunit.where(unit_id: self.unit_id ).find_by(
          day_booked: day_booked
        ).destroy
      else
        BookedSubunit.where(unit_id: self.unit_id ).find_by(
          day_booked: day_booked
        ).decrement!(:count, by = 1)
      end
    end
  end

  def send_email_to_owner_and_customer(id)

    Resque.enqueue(BookingEmailWorker, id)
  end


  # Get total month bookings for month
  def self.month_booking(date, status)
    from = Date.parse("#{date.year}-#{date.month}-01")
    to   = from.end_of_month
    Booking.where('created_at between ? and ? and payment_status =?', from, to, status).count
  end

  # Get total bookings for a day
  def self.day_booking(date, status)
    Booking.where('DATE(created_at) = ? AND payment_status = ?', date, status).count
  end

  # Get total bookings for a life_time_booking
  def self.life_time_booking(status)
    Booking.where('payment_status = ?', status).count
  end

  # Get total bookings for current month
  def self.current_month_booking
    booking = Booking.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
  end

  # Get total bookings for current month
  def self.this_month_booking(status)
    booking = Booking.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month,payment_status: status).count
  end


end
