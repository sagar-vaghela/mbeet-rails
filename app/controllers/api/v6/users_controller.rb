class Api::V6::UsersController < Api::V6::BaseController
  before_action :login_required, only: [ :update, :dashbord_owners , :owners_dashboard ]
	before_action :check_user, :only => [:show, :update]

  def owners_dashboard
		@user = User.find(@current_user) if @current_user.present?
    @owner = @user if @user.is_owner == true && @user.role_id == 2
		if @owner
			@units = @owner.units
      @booking = @owner.bookings
      if params[:date].present? && params[:type].to_i == 1
        date = Date.parse(params[:date])
        render json: {
          message: I18n.t('admin.booking_today'),
          pending:   @booking.day_booking(date, 1),
          cancelled: @booking.day_booking(date, 2),
          confirmed: @booking.day_booking(date, 3),
          completed: @booking.day_booking(date, 4)}

      elsif params[:date].present? && params[:type].to_i == 2
        date = Date.parse(params[:date])
        render json: {
          message: I18n.t('admin.booking_month'),
          pending:   @booking.month_booking(date, 1),
          cancelled: @booking.month_booking(date, 2),
          confirmed: @booking.month_booking(date, 3),
          completed: @booking.month_booking(date, 4)
      }
      elsif params[:current_all].present?
        render json: {
          message: I18n.t('admin.total_life_time_booking'),
          pending:   @booking.life_time_booking(1),
          cancelled: @booking.life_time_booking(2),
          confirmed: @booking.life_time_booking(3),
          completed: @booking.life_time_booking(4)
      }
      else
          render json: {
            message: I18n.t('admin.booking_month'),
            pending:   @booking.this_month_booking(1),
            cancelled: @booking.this_month_booking(2),
            confirmed: @booking.this_month_booking(3),
            completed: @booking.this_month_booking(4)
        }
      end
		end
  end

  def index
		@users = User.order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
		render_data({ total: User.count, users: @users.as_api_response(:v3_user_pictures)}, I18n.t('user.list'))
	end

	def show
		render_data({ users: @user.as_api_response(:v3_user_pictures) }, I18n.t('user.detail'))
	end

	def update
		@user.skip_password_validation = true
		@user.update(user_params)

		render_data({ users: @user.as_api_response(:v3_user_pictures) }, I18n.t('user.data_updated')) if @user.save!
	rescue StandardError => e
		error_valid_data(e)
	end

	def dashbord_owners
		@user = User.find(@current_user) if @current_user.present?
	  @owner = @user if @user.is_owner == true && @user.role_id == 2
		if @owner
			@units = @owner.units
			render_data({ units: @units.as_api_response(:dashbord_owners) }, I18n.t('Booking units'))
		end
  end

	private

	def user_params
		params.permit(:name, :email, :date_of_birth, :gender, :country_code, :phone, picture_attributes: [:name])
	end

	def check_user
		@user = User.find(params[:id])
	end

end
