class Api::V4::VerifyController < Api::V3::VerifyController

  def send_otp
    user = User.get_otp(params[:phone], params[:country_code])
    if !user
      update_user_phone
    else
      check_phone
    end
  end

  def confirm_otp
    user = User.confirm_otp(params[:phone], params[:otp], params[:country_code])
    return error_data( I18n.t('user.otp_confirm')) unless user

    auth_token = JsonWebToken.encode({user_id: user.id})
    render_data({ users: user.as_api_response(:v3_user_pictures) }, I18n.t('user.otp_verified'))
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def update_user_phone
    @user.skip_password_validation = true
    @user.update!(phone: params[:phone], country_code: params[:country_code])
    @user.generate_and_send_otp!
    render_data( { otp: @user.otp }, I18n.t('user.otp_sent') ) if @user.save
  end

  def check_phone
    user = User.get_otp(params[:phone], params[:country_code])
    # user = User.find_by(phone: params[:phone], country_code: params[:country_code])
    if user && user.id != @user.id
      error_valid_data(I18n.t('user.phone_is_taken'))
    else
      update_user_phone
    end
  end
end
