class AddTransactionIdToPaymentTxns < ActiveRecord::Migration[5.0]
  def change
    add_column :payment_txns, :transaction_id, :text
  end
end
