class CreateTrendingCities < ActiveRecord::Migration[5.0]
  def change
    create_table :trending_cities do |t|
      t.integer :city_id

      t.timestamps
    end
  end
end
