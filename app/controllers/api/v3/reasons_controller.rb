class Api::V3::ReasonsController < Api::V3::BaseController

  # GET /v2/reasons
  def index
    @reasons = Reason.all
    render_data({ reasons: @reasons.as_api_response(:reasons)}, I18n.t('unit.reasons.list')) if @reasons.present?
  end
end
