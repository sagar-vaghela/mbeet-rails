module Users
  module Associations
    extend ActiveSupport::Concern

    included do

      # ActiveRecord Associations
      belongs_to :role

      has_many :bookings, dependent: :destroy

      # has many through association
      has_many :units, through: :bookings, dependent: :destroy

      has_many :user_likes, dependent: :destroy
      has_many :feedbacks, dependent: :destroy
      has_many :access_tokens, dependent: :destroy
      has_one  :user_lang_setting, dependent: :destroy
      has_many :request_to_list_units, dependent: :destroy
      has_many :user_booking_cancel_requests, dependent: :destroy
      has_many :notifications, dependent: :destroy

      # Polymorphic association
      has_one :picture, as: :imageable, dependent: :destroy

      # coupons association
      has_many :coupons, class_name: 'Coupon', foreign_key: "creator_id", dependent: :nullify

      accepts_nested_attributes_for :picture

      enum gender: { male: 1, female: 2 }

      scope :role, -> { where.not(role_id: 1) }
      scope :owners, -> { where(is_owner: true) }

      before_update :phone_updated, :if => :from_profile_update?
    end
  end
end
