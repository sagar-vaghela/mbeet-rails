class Api::Admin::V6::BookingsController < Api::Admin::V5::BookingsController
  before_action :admin_login_required
  before_action :check_bookings, only: :index
  before_action :find_booking, only: [:show, :cancel]
  before_action :find_current_user

  # GET: admin/v6/bookings
  def index
    impressionist(@user, request.path) if @user
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = Booking.search do
      fulltext params[:search]
      if params[:booking_cancelled].present?
        status = params[:booking_cancelled] == 'true' ? true : false
        with(:booking_cancelled, status)
      end

      # Filter by created date
      # with(:created_at).between(params[:from].to_datetime..params[:to].to_datetime) if  params[:form].present? && params[:to].present? && params[:type].present? && params[:type].to_i == 1
      if params[:from].present? && params[:to].present? && params[:type].present? && params[:type].to_i == 1
          with(:created_at).greater_than_or_equal_to(params[:from])
          with(:created_at).less_than_or_equal_to(params[:to])
      end

      # Filter by check in date
      if params[:from].present? && params[:to].present? && params[:type].present? && params[:type].to_i == 2
          with(:check_in).greater_than_or_equal_to(params[:from])
          with(:check_in).less_than_or_equal_to(params[:to])
      end

      with(:payment_status, params[:payment_status]) if params[:payment_status].present?
      with(:payment_method, params[:payment_method] )   if params[:payment_method].present?
      with(:lease_status, params[:lease_status] )   if params[:lease_status].present?
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end

      paginate(:offset => params[:offset], :per_page => limit)

    end

    render_data({ total: Booking.all.size, result_total: search.total, booking_list: search.results.as_api_response(:v4_booking_with_payment_txns) }, I18n.t('admin.booking_list') )
  end

  # GET: admin/v6/bookings/:id
  def show
    impressionist(@user, request.path) if @user
    render_data({ booking: @booking.as_api_response(:v4_booking_with_payment_txns) }, I18n.t('admin.booking_detail')) if @booking.present?
  end

  # DELETE: admin/v6/bookings/:id/cancel
  def cancel
    impressionist(@user, request.path) if @user
    if @booking.booking_cancelled
      error_valid_data(I18n.t('admin.booking_already_cancelled'))
    else
      if @booking.update(payment_status: 2, booking_cancelled: true)
         @booking.delete_booked_days
        render_updated ({
          message: I18n.t('admin.booking_cancelled'),
          data: { Booking: @booking.as_api_response(:v4_booking_with_payment_txns) }
        })
      else
        render_unprocessable_entity( message: @booking.errors.full_messages.join(', '))
      end
    end
  end

  def dashbord_bookings
    impressionist(@user, request.path) if @user
    if params[:date].present? && params[:type].to_i == 1
      date = Date.parse(params[:date])
      render json: {
        message: I18n.t('admin.booking_today'),
        pending:   Booking.day_booking(date, 1),
        cancelled: Booking.day_booking(date, 2),
        confirmed: Booking.day_booking(date, 3),
        completed: Booking.day_booking(date, 4)}

    elsif params[:date].present? && params[:type].to_i == 2
      date = Date.parse(params[:date])
      render json: {
        message: I18n.t('admin.booking_month'),
        pending:   Booking.month_booking(date, 1),
        cancelled: Booking.month_booking(date, 2),
        confirmed: Booking.month_booking(date, 3),
        completed: Booking.month_booking(date, 4)
    }
    elsif params[:current_all].present?
      render json: {
        message: I18n.t('admin.total_life_time_booking'),
        pending:   Booking.life_time_booking(1),
        cancelled: Booking.life_time_booking(2),
        confirmed: Booking.life_time_booking(3),
        completed: Booking.life_time_booking(4)
    }
    else
        render json: {
          message: I18n.t('admin.booking_month'),
          pending:   Booking.this_month_booking(1),
          cancelled: Booking.this_month_booking(2),
          confirmed: Booking.this_month_booking(3),
          completed: Booking.this_month_booking(4)
      }
    end
  end

  private

  def find_current_user
    @user = User.find_by(id: @current_user, role_id: [1, 2])
  end

end
