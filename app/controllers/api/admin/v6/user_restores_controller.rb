class Api::Admin::V6::UserRestoresController < Api::Admin::V5::BaseController
  before_action :admin_login_required
  before_action :find_user, only: :update
  before_action :find_current_user, only: :update

  def update
    impressionist(@cur_user, request.path) if @cur_user
    if @user.nil?
      error_valid_data(I18n.t('record_not_found'))
    else
      @user.skip_password_validation = true
      if @user.update soft_delete: false
       render_updated ({
         message: I18n.t('admin.user.restored'),
         data: { user: @user.as_api_response(:v4_admin_user_only) }
       })
      else
       render_unprocessable_entity( message: @user.errors.full_messages.join(', '))
      end
    end
  end

  private

  def find_user
    @user = User.find_by_id(params[:id])
  end

  def find_current_user
    @cur_user = User.find_by(id: @cur_user, role_id: [1, 2])
  end

end
