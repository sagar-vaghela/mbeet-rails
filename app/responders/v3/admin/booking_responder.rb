module V3::Admin::BookingResponder
	extend ActiveSupport::Concern

	included do
		api_accessible :v3_booking_list do |t|
	    t.add :id
	    t.add :check_in
	    t.add :check_out
	    t.add :payment_status
	    t.add :lease_status
	    t.add :total_amount
	    t.add :booking_cancelled
	    t.add :created_at
	    t.add :feedback
	  end

	  api_accessible :v3_unit_with_bookings, extend: :v3_booking_list do |t|
	    t.add :unit, :template => :unit_with_pictures_admin_booking_list
	  end

		api_accessible :v3_booking_with_unit_and_user, extend: :v3_unit_with_bookings do |t|
	    t.add :user, :template => :user_with_booking
	  end

	  api_accessible :v3_booking_detail, extend: :v3_booking_list do |t|
	    t.add :guests
	    t.add :for_days, as: :rented_for_days
	    t.add :sub_total_amount
	  end

	  api_accessible :v3_unit_with_bookings_extra, extend: :v3_booking_detail do |t|
	    t.add :unit, :template => :unit_with_pictures_admin_booking_list
	  end

	  api_accessible :v3_booking_with_payment_txns, extend: :v3_unit_with_bookings_extra do |t|
	    t.add :payment_txn, :template => :payment_transaction
	  end

	end
end
