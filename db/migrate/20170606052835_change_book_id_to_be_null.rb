class ChangeBookIdToBeNull < ActiveRecord::Migration[5.0]
  def change
    change_column_null :notifications, :booking_id, true, default: ''
  end
end
