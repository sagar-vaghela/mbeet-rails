class Api::V3::SearchesController < Api::V3::BaseController
  before_action :login_required

  def search
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]

    if params[:date_start].present? && params[:date_start].present?
      booking_search = Booking.where(booking_cancelled: false).where(
        "check_in <= '#{params[:date_end]}' AND check_out >= '#{params[:date_start]}'"
      ).or(
      Booking.where(
         "check_in >= '#{params[:date_start]}' AND check_out <= '#{params[:date_end]}'"
        )
      )
      taken_units = booking_search.pluck(:unit_id)
    end

    #------ This is Sunspot Solr Based search on Parent and its associated tables ------#
    search = Unit.search do

      # any do
      #   fulltext(params[:city], :fields => :city)
      #   fulltext(params[:city], :fields => :address)
      # end

        # Only display enabled and published units
        # ----------------------------#
        with :unit_enabled, true
        with :unit_status, true
        # ----------------------------#

        with :city_id, params[:city] if params[:city].present?
        with :number_of_guests, params[:number_of_guests] if params[:number_of_guests].present?

        if params[:date_start].present? && params[:date_start].present?
          taken_units.each do |unit_id|
            without(:id, unit_id)
          end
        end


      all_of do

        # Greater than 4+ condition
        if params[:number_of_rooms].present?
          if params[:number_of_rooms].to_i >= 4
            with(:number_of_rooms).greater_than_or_equal_to( params[:number_of_rooms] )
          else
            with(:number_of_rooms, params[:number_of_rooms] )
          end
        end

        # Greater than 4+ condition
        if params[:number_of_beds].present?
          if params[:number_of_beds].to_i >= 4
            with(:number_of_beds).greater_than_or_equal_to( params[:number_of_beds] )
          else
            with(:number_of_beds, params[:number_of_beds] )
          end
        end

        # Greater than 4+ condition
        if params[:number_of_baths].present?
          if params[:number_of_baths].to_i >= 4
            with(:number_of_baths).greater_than_or_equal_to( params[:number_of_baths] )
          else
            with( :number_of_baths,  params[:number_of_baths] )
          end
        end

        # with(:room_type,    params[:room_type])    if params[:room_type].present?
        with(:unit_class,   params[:unit_class])   if params[:unit_class].present?
        with(:unit_type,    params[:unit_type])    if params[:unit_type].present?
        with(:available_as, params[:available_as]) if params[:available_as].present?

        #------ search for price kind also ------ #
        with(:price).greater_than_or_equal_to(params[:starting_price]) if params[:starting_price].present?
        with(:price).less_than_or_equal_to(params[:closing_price]) if params[:starting_price].present?

        #------ search with amenities multiple ------ #
        with(:amenities).any_of([params[:amenities]]) if params[:amenities].present?

        #------ search with date ------ #
        # with(:check_in).between(params[:date_start]..params[:date_end])
        # with(:check_out).between(Date.new(2011,3,1)..Date.new(2011,4,1))

        # with( :check_in ).greater_than_or_equal_to(params[:date_start]) if params[:date_start].present?
        # with( :check_out ).less_than_or_equal_to(params[:date_end]) if params[:date_end].present?

        # without( :check_in ).greater_than_or_equal_to(params[:date_start]) if params[:date_start].present?
        # without( :check_out ).less_than_or_equal_to(params[:date_end]) if params[:date_end].present?

      end

      with :soft_delete, false
      order_by :created_at, :desc
      paginate(:offset => params[:offset], :per_page => limit)
    end

    @units = search.results
    total = Unit.where(unit_enabled: true, unit_status: true).where(soft_delete: false).size

    # #---------------- If dates are there then fetch from booking table -----------------#
    # if params[:date_start].present? && params[:date_start].present? && search.results.present?
    #   booking_search = Booking.search do
    #     any_of do
    #       all_of do
    #         with(:check_in).less_than_or_equal_to(Date.parse(params[:date_end]).strftime('%Y-%m-%d').to_date + 1.day) if params[:date_end].present?
    #         with(:check_out).greater_than_or_equal_to(Date.parse(params[:date_start]).strftime('%Y-%m-%d').to_date) if params[:date_start].present?
    #       end
    #       all_of do
    #         with(:check_in).greater_than_or_equal_to(Date.parse(params[:date_end]).strftime('%Y-%m-%d').to_date + 1.day) if params[:date_start].present?
    #         with(:check_out).less_than_or_equal_to(Date.parse(params[:date_start]).strftime('%Y-%m-%d').to_date) if params[:date_end].present?
    #       end
    #     end
    #
    #     # with(:check_out).less_than_or_equal_to(params[:date_end].to_date.strftime('%Y-%m-%d 12:00:00')) if params[:date_end].present?
    #     # with(:check_in).greater_than_or_equal_to(params[:date_start].to_date.strftime('%Y-%m-%d 12:00:00')) if params[:date_start].present?
    #     # any_of do
    #     #   with(:check_in).between((params[:date_start].to_date.strftime('%Y-%m-%d'))..params[:date_end].to_date.strftime('%Y-%m-%d'))
    #     #   with(:check_out).between(params[:date_start].to_date.strftime('%Y-%m-%d')..params[:date_end].to_date.strftime('%Y-%m-%d'))
    #     # end
    #
    #     with(:unit_id, search.results.pluck(:id)) if params[:city].present?
    #     with(:booking_cancelled, 0)
    #   end
    #   debugger
    #   taken_units = booking_search.results.map(&:unit) || []
    #
    #   # Now get search result based upon city and other attribute of units table
    #   units_in_city = search.results
    #
    #   # Minus the taken unit with city units to get available units
    #   @units = units_in_city - taken_units
    # end

    @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
    render_data({ total: total , units: @units.as_api_response(:v3_unit_with_pictures)}, I18n.t('unit.list'))
    # -------------------------------------------------------------------------------------- #
    # Now what ever units are obtained from booking table we have to minus them from Unit
    # table search.
    # -------------------------------------------------------------------------------------- #

    # if params[:date_start].present? && params[:date_end].present? && booking_search.total > 0
    #   debugger
    #   booking_unit_ids = booking_search.results.map(&:unit_id).uniq
    #
    #   to_delete_units = Unit.search do
    #     with(:id, booking_unit_ids)
    #   end
    #
    #   to_delete_elements = to_delete_units.results.map(&:id)
    #
    #   @units = search.results - to_delete_units.results
    #
    #   if booking_unit_ids.sort == to_delete_elements.sort
    #     total = @units.size
    #   else
    #     total = if search.total == 0
    #               0
    #             elsif search.total == 1
    #               1
    #             else
    #               if search.total > to_delete_units.total
    #                 search.total - to_delete_units.total
    #               elsif search.total < to_delete_units.total
    #                 to_delete_units.total - search.total
    #               end
    #
    #             end
    #   end
    #
    #   if total > 0
    #     @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
    #     render_data({ total: @units.size, units: @units.as_api_response(:v3_unit_with_pictures)}, I18n.t('unit.list'))
    #   else
    #     error_data( I18n.t('record_not_found') )
    #   end
    #
    # # elsif params[:date_start].present? && params[:date_start].present? && booking_search.total == 0
    # #   error_data( I18n.t('record_not_found') )
    # else
    #    @units = search.results
    #   if search.total > 0
    #     @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
    #     render_data({ total: search.total, units: @units.as_api_response(:v3_unit_with_pictures)}, I18n.t('unit.list'))
    #   else
    #     error_data( I18n.t('record_not_found') )
    #   end
    # end

  end

end
