class AddRoomTypeColumnToUnits < ActiveRecord::Migration[5.0]
  def change
  	add_column :units, :room_type, :integer, null: false, default: 1
  end
end
