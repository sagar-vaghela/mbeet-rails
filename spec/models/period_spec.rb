require 'rails_helper'

RSpec.describe Period, type: :model do
	it "is valid with valid attributes" do
		period = Period.new(unit_id:54, price: 1000, period_date: "2016-05-29")
  		expect(period).to be_valid
	end

	it "is not valid without price" do
		period = Period.new(unit_id:54, period_date: "2016-05-29")
		expect(period).to_not be_valid
	end
	it "is not valid without date" do
		period = Period.new(unit_id:54)
		expect(period).to_not be_valid
	end
end
