class Api::Admin::V3::RolesController < Api::Admin::V3::BaseController

	# GET /admin/v3/roles
	def index
		@roles = Role.order(id: :asc)
		render_data({ roles: @roles.as_api_response(:for_admin_show_roles)}, I18n.t('admin.roles.list')) if @roles
	end
end
