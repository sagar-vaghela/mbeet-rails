class Api::Admin::V6::UnitsController < Api::Admin::V5::UnitsController
  before_action :admin_login_required
  before_action :check_unit, except: [:index, :create]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy
  before_action :check_booking, only: :disable
  before_action :find_current_user

  # GET /admin/v6/units
  def index
    impressionist(@user, request.path) if @user
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = Unit.search do
      data_accessor_for(Unit).include = [:city, :user]
      fulltext params[:search]
      with(:city_id, params[:city_id]) if params[:city_id].present?
      if params[:unit_status].present?
        status = params[:unit_status] == 'true' ? true : false
        with(:unit_status, status)
        with(:soft_delete, false)
      end

      if params[:unit_enabled].present?
        status = params[:unit_enabled] == 'true' ? true : false
        with(:unit_enabled, status)
        with(:soft_delete, false)
      end

      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      with(:number_of_guests, params[:number_of_guests]) if params[:number_of_guests].present?
      with(:number_of_rooms, params[:number_of_rooms] ) if params[:number_of_rooms].present?
      with(:number_of_beds, params[:number_of_beds] )   if params[:number_of_beds].present?
      with(:number_of_baths,  params[:number_of_baths] )  if params[:number_of_baths].present?
      with(:number_of_subunits, params[:number_of_subunits]) if params[:number_of_subunits].present?
      with(:unit_class,   params[:unit_class])   if params[:unit_class].present?
      with(:unit_type,    params[:unit_type])    if params[:unit_type].present?
      with(:bed_type,    params[:bed_type])    if params[:bed_type].present?
      with(:available_as, params[:available_as]) if params[:available_as].present?
      with(:price).greater_than_or_equal_to(params[:starting_price]) if params[:starting_price].present?
      with(:price).less_than_or_equal_to(params[:closing_price]) if params[:starting_price].present?
      with(:amenities).any_of([params[:amenities]]) if params[:amenities].present?
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end

      paginate(:offset => params[:offset], :per_page => limit)

    end
    render_data({ total: Unit.all.size, result_total: search.total, units: search.results.as_api_response(:v5_admin_unit_only)}, I18n.t('unit.list'))
  end

  # POST /admin/v6/units
  def create
    impressionist(@user, request.path) if @user

    @unit = Unit.new(unit_params)
    admin = User.find(@current_user)
    @unit.title_translations = {en: @unit.title_en, ar: @unit.title_ar}
    @unit.body_translations = {en: @unit.body_en, ar: @unit.body_ar}
    # @unit.unit_status = true
    # @unit.unit_enabled =true
    if @unit.save
     render_data({ units: @unit.as_api_response(:v5_admin_unit_only) }, I18n.t('admin.unit.created'))

    else
     render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  # PUT /admin/v6/units/:id
  def update
    impressionist(@user, request.path) if @user
    user = User.find(@current_user)

    if !@unit.price_changed?
      @unit.update_attributes(unit_status: false, message: params[:message]) if params[:message].present?
    end

    if @unit.update(unit_params)
      render_data({ units: @unit.as_api_response(:v6_admin_unit_only) }, I18n.t('unit.updated'))
      if @unit.unit_status_previously_changed? && @unit.unit_status == true
        user.send_email_to_user(@unit)
      end
    else
      render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  # GET /admin/v6/units/:id
  def show
    impressionist(@user, request.path) if @user
    render_data({ units: @unit.as_api_response(:v6_admin_unit_only) }, I18n.t('unit.detail')) if @unit
  end

  # DELETE /admin/v6/units/:id
  def destroy
    impressionist(@user, request.path) if @user
    render_data({ units: {} }, I18n.t('admin.unit.deleted')) if @unit.update(soft_delete: true)
  end

  # PUT: admin/v6/units/:id/enable
  def enable
    impressionist(@user, request.path) if @user
    if @unit.number_of_subunits >= 0
      if params[:start_date].present? && params[:end_date].present? && params[:number_of_disabled_units].present?
        (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
          @res = @unit.unit_disabled_dates.find_by(day_disabled: day_disabled)
          @res.destroy if @res
        end
        if @unit.unit_disabled_dates.count == 0
          @unit.update(unit_enabled: true)
        end
        render_updated ({ message: I18n.t('unit.enabled'),data: { unit: @unit.as_api_response(:v5_unit_pictures) } })
      end
    else
      render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  # PUT: admin/v6/units/:id/disable
  def disable
    impressionist(@user, request.path) if @user
    number_of_disabled_units = @unit.number_of_subunits > 0 ? params[:number_of_disabled_units] : 0
    (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
      @unit.unit_disabled_dates.find_or_create_by(
            day_disabled: day_disabled
      ).increment!(:number_of_disabled_units, by = number_of_disabled_units.to_i)
    end
    render_updated ({
      message: I18n.t('unit.disabled'),
      data: { unit: @unit.as_api_response(:v5_unit_pictures) }
    })
  end

  # PUT: admin/v6/units/:id/publish
  def publish
    impressionist(@user, request.path) if @user
    unit = Unit.find(params[:id])
    admin = User.find(@current_user)
    # if unit_status is true, the flat is published to the app
    if unit.update(unit_status: true, unit_enabled: true)
      render_updated ({
        message: I18n.t('unit.published'),
        data: { unit: unit.as_api_response(:v5_admin_unit_only) }
      })

      admin.send_email_to_user(unit)
    else
      render_unprocessable_entity( message: unit.errors.full_messages.join(', '))
    end
  end

  # PUT /admin/v6/units/:id/restore
  def restore
    impressionist(@user, request.path) if @user
    render_data({ units: {} }, I18n.t('admin.unit.restored')) if @unit.update(soft_delete: false)
  end

  private

  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar,:number_of_guests, :unit_status, :unit_enabled,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id, :mbeet_percentage, :bed_type,
                  :address, :price, :latitude, :longitude, :unit_class, :unit_type, :number_of_subunits,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @unit = Unit.find(params[:id])
  end

  def find_current_user
    @user = User.find_by(id: @current_user, role_id: [1, 2])
  end
end
