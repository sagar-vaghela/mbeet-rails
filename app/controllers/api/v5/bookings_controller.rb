class Api::V5::BookingsController < Api::V4::BookingsController

  before_action :get_coupon, only: [:create, :validate_coupon]

  before_action :login_required
	before_action :check_user_verification, :check_unit_available_as, :check_booking, only: :create

	before_action :first_verify_booking, only: :cancel
	before_action :check_payment_status, :get_payment, only: :payment_update
	after_action  :update_booking_cancelled, only: :cancel


  # POST /v2/book_now
	def create
		if @booking = Booking.create!(booking_params)
      if @coupon
        @booking.coupon_id = @coupon.id
        @booking.save
      end
			@booking.notify @booking, 3
			@booking.automatically_cancel_booking
      (params[:check_in].to_datetime...params[:check_out].to_datetime).to_a.each do |day_booked|
        BookedSubunit.where(unit_id: params[:unit_id]).create_with(unit_id: params[:unit_id])
        .find_or_create_by(
            day_booked: day_booked
        ).increment!(:count, by = 1)
      end
			render_data({ bookings: @booking.as_api_response(:v5_booking_with_customer_info) }, I18n.t('user.booking.success') )
		end
	rescue StandardError => e
		error_valid_data(e)

  # count_number_of_booked_days
	end

  # it will validates the coupon for booking
  def validate_coupon
    if @coupon
      if @coupon.status == "enable"
        if @coupon.starting_date <= Date.today && @coupon.expiration_date >= Date.today
          render_data({ coupon: @coupon.as_api_response(:coupons_list) }, I18n.t('user.coupon.success_valid') )
        else
          render_unprocessable_entity( message: I18n.t('user.coupon.fail_valid'))
        end
      else
        render_unprocessable_entity( message: I18n.t('user.coupon.fail_valid'))
      end
    else
      render_unprocessable_entity( message: I18n.t('user.coupon.fail_valid'))
    end
    # if @coupon
    #   render_data({ coupon: @coupon.as_api_response(:coupons_list) }, I18n.t('success') )
    # else
    #   render_unprocessable_entity( message: "Coupon is not valid.")
    # end
  end


  private

  def get_coupon
    @coupon = Coupon.find_by(code: params[:coupon]) if params[:coupon]
  end



end
