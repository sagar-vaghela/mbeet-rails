# == Schema Information
#
# Table name: booked_subunits
#
#  id         :integer          not null, primary key
#  unit_id    :integer
#  day_booked :datetime
#  count      :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BookedSubunit < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  belongs_to :unit
end
