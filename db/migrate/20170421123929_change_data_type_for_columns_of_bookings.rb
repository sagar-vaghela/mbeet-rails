class ChangeDataTypeForColumnsOfBookings < ActiveRecord::Migration[5.0]
  def change
  	change_column :bookings, :payment_method, :integer, default: 1
  	change_column :bookings, :payment_status, :integer, default: 1
  	change_column :bookings, :lease_status, :integer, default: 2
  end
end
