class Api::V1::UnitsController < Api::V1::BaseController
  before_action :login_required, only: :check_if_free
  before_action :login_required, only: [:index, :show], :if => :user_is_present?
  #before_action :validate_limit_offset, only: :index

  # GET /v1/units
	def index
    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
    	@units = Unit.order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = 0 }
    	render_data({ total: Unit.count, units: @units.as_api_response(:unit_with_pictures)}, I18n.t('unit.list')) if @units
    else
      @units = Unit.order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      render_data({ total: Unit.count, units: @units.as_api_response(:unit_with_pictures)}, I18n.t('unit.list')) if @current_user.present? && @units
    end
	end

  # GET /v1/units/:id
	def show
    @unit = Unit.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
     	render_data({ units: @unit.as_api_response(:unit_pictures) }, I18n.t('unit.detail')) if @unit
    else
      @unit.liked_by_user = (UserLike.if_liked_by_user @current_user, @unit.id).present? ? 1 : 0
      render_data({ units: @unit.as_api_response(:unit_pictures) }, I18n.t('unit.detail')) if @unit
    end
	end

  # GET /v1/units/check_if_free
  def check_if_free
  	@available_dates = Booking.where(unit_id: params[:unit_id])
  	                          .where("check_in <= ? AND check_out >= ?", params[:current_date_time], params[:current_date_time])
  	                          .pluck(:check_in, :check_out)

  	if @available_dates.present?
  		first_date = @available_dates.flatten.first.strftime("%Y-%m-%d")
  		second_date = @available_dates.flatten.last.strftime("%Y-%m-%d")

  		range = (first_date..second_date).map(&:to_s)

  		render_data({ taken_dates: range, units: {id: params[:unit_id]} }, I18n.t('unit.availability'))
  	else
  		render_data({ taken_dates: [], units: {} }, I18n.t('unit.availability'))
  	end
  end

  private
  def user_is_present?
    (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
  end

end
