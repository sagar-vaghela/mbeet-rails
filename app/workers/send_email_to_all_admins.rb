class SendEmailToAllAdmins
	@queue = 'send_email_to_all_admins'

	def self.perform(id)
		@user = User.find_by_id(id)
		admin_user_emails = User.where(role_id: 1).pluck(:email)
    UserMailer.user_add_new_flat(admin_user_emails, @user).deliver_now
		# admin_user_emails.each do |admin_email|
		# 	UserMailer.user_add_new_flat(admin_email, @user).deliver_now
		# end
	end
end
