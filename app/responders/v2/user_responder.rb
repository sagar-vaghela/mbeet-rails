module V2::UserResponder
	extend ActiveSupport::Concern

	included do
		# Format api json response using acts_as_api gem
		# will render json: { "user": { "name": "John", "facebook_uid":  .., "twitter_uid": .., "gmail_uid": .., "created_at": .., "updated_at": .. } }
		api_accessible :v2_user_only do |t|
			t.add :id
	    t.add :name
	    t.add :email
	    t.add :phone
	    t.add :type
	    t.add :email_verified
	    t.add :phone_verified
	  end

	  api_accessible :v2_user_details, extend: :v2_user_only do |t|
	    t.add :gender
	    t.add :date_of_birth
	  end

	  api_accessible :v2_user_pictures, extend: :v2_user_details do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :picture, as: "image", :template => :images
	    t.add :default_image
	  end

	  api_accessible :v2_user_with_token_data, extend: :v2_user_only do |t|
	    t.add :access_token
	  end

	  # To display owner name in unit detail response
	  api_accessible :v2_user_for_unit do |t|
	    t.add :id, as: "user_id"
	    t.add :name
	  end

	  api_accessible :v2_only_user_email do |t|
	    t.add :email
	  end

	  api_accessible :v2_get_user_for_feedback do |t|
	    t.add :id
	    t.add :name
	  end

	  api_accessible :v2_get_user_for_feedback_with_image, extend: :v2_get_user_for_feedback do |t|
	    t.add :picture, as: "image", :template => :images
	    t.add :default_image
	  end

	  # Owners List for Admin Api
	  api_accessible :v2_owners do |t|
	    t.add :id
	    t.add :name
	  end

	end
end
