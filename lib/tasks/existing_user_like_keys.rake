namespace :existing_user_like_keys do

  desc "Generate Redis Keys for User liked Units for old record"
  task create: :environment do
    
    list = UserLike.select("user_id, unit_id")

    puts "Note: User liked units generating key process started"
    list.each do |value|
      # save user selected leagues id's redis key
      $redis.sadd("mbeet_user:#{value.user_id}:units:liked", value.unit_id)
    end
    puts "Note:  User liked units created successfully into redis"
  end
end