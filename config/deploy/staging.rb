set :branch, 'staging'
set :keep_releases, 3


server '52.76.100.147',
  user: 'ubuntu',
  roles: %w{web app db},
ssh_options: {
  user: 'ubuntu', # overrides user setting above
  keys: %w(~/.ssh/id_rsa),
  forward_agent: false,
  auth_methods: %w(publickey)
  # password: 'please use keys'
}


role :resque_worker, "52.76.100.147"

role :resque_scheduler, "52.76.100.147"

set :resque_rails_env, 'staging'
