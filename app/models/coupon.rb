class Coupon < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  enum status: [ :enable, :disable ]

  belongs_to :user, class_name: 'User', foreign_key: "creator_id"
  has_many :bookings

  # -------------------------- validations---------------------- #
  # validates :value, :starting_date, :expiration_date, :status, presence: true, on: :create
  validates_uniqueness_of :code
  validates_presence_of :value, :starting_date, :expiration_date, :status, translation: true

  # ---------------------- Same Model Fields ----------------- #

  searchable do
    text :value, :starting_date, :expiration_date, :status, :id, :code
    integer :id
    integer :value
    time :starting_date
    time :expiration_date
    integer :status
    string :code
    time :created_at
    string :user do
      user.name
    end
  end

  api_accessible :coupons_list do |t|
  	t.add :id
  	t.add :value
  	t.add :starting_date
  	t.add :expiration_date
  	t.add :status
    t.add :code
	end

  # get count of bookings for unit of owners
  api_accessible :coupon_statastics, extend: :coupons_list do |t|
    t.add lambda{ |coupon|
      @total_charged_amount = coupon.bookings.where(payment_status: 3).sum(:total_amount)
    } , :as => :total_charged_amount

    t.add lambda{ |coupon|
      discount = coupon.value
      @total_discounted_amount = (@total_charged_amount / 100) * discount
     } , :as => :total_discounted_amount

    t.add lambda{ |coupon|
      @count = coupon.bookings.count
      @total_charged_amount / @count
     } , :as => :average_charged_amount

    t.add lambda{ |coupon|
      @total_discounted_amount / @count
    } , :as => :average_discounted_amount

    t.add lambda{ |coupon|
      @count
    } , :as => :used_coupon_count

  end

end
