module V1::Admin::UserResponder
	extend ActiveSupport::Concern

	included do
		# -------------------- Admin User API Response ------------------ #
		api_accessible :admin_user_only do |t|
	    t.add :id
	    t.add :role, :template => :for_admin_show_roles
	    t.add :name
	    t.add :email
	    t.add :phone
	    t.add :email_verified
	    t.add :phone_verified
	    t.add :gender
	    t.add :date_of_birth
	    t.add :is_owner
	    t.add :facebook_uid
	    t.add :twitter_uid
	    t.add :gmail_uid
	    t.add :picture, as: "picture_attributes", :template => :admin_images
	    t.add :default_image
	    t.add :created_at
	  end
	end
end