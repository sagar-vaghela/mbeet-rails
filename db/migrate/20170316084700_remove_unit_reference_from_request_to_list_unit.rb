class RemoveUnitReferenceFromRequestToListUnit < ActiveRecord::Migration[5.0]
  def change
  	remove_reference :request_to_list_units, :unit
  end
end
