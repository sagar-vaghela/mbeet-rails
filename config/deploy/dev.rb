set :branch, 'dev'
set :keep_releases, 3


server '13.228.110.8',
  user: 'ubuntu',
  roles: %w{web app db},
ssh_options: {
  user: 'ubuntu', # overrides user setting above
  keys: %w(~/.ssh/id_rsa),
  forward_agent: false,
  auth_methods: %w(publickey)
  # password: 'please use keys'
}

role :resque_worker, "13.228.110.8"

role :resque_scheduler, "13.228.110.8"

set :resque_rails_env, 'dev'
