class Api::V3::FavoritesController < Api::V3::BaseController
	before_action :login_required, :check_favourites, :get_count #,:validate_limit_offset

	# GET: /v2/favourites
  def index
    @list = @favourites.user_likes.order(created_at: :desc)
                                  .limit(params[:limit] == -1 ? 100 : params[:limit])
                                  .offset(params[:offset])
  	render_data({ total: @count, favourites: @list.as_api_response(:v2_favourites_list) }, I18n.t('favourite.list'))
  end

  private
  def check_favourites
    @favourites = User.includes(:user_likes)
                      .find_by("users.id = #{params[:user_id]}")

    render_data({}, I18n.t('no_data')) unless @favourites
  end

  def get_count
    @count = UserLike.where(user_id: params[:user_id]).count
  end
end
