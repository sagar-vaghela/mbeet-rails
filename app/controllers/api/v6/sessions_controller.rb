class Api::V6::SessionsController < Api::V5::SessionsController
  before_action :check_null_password, only: [ :create_owner, :create ]

  # POST /v2/login #login for owner

  def create_owner
    if @user && @user.authenticate(params[:password])
      if @user.is_owner
        if !@user.soft_delete
          auth_token = JsonWebToken.encode({user_id: @user.id})
          render_data( { auth_token: auth_token, users: @user.as_api_response(:v2_user_only) }, I18n.t('user.login_success') )
        else
          render_unprocessable_entity( message: I18n.t('.errors.account_deleted'))
        end
      else
        error_data( I18n.t('user.owner_login_error') )
      end
    else
      error_data( I18n.t('user.login_error') )
    end
  end

  def contact_us
    UserMailer.contact_us_email(params).deliver
    render_data( { }, I18n.t('user.contact_us') )
  end


    # POST /v6/owner_signup
    def owner_signup
      user = User.new(user_params)

      user[:role_id] = 2
      user[:is_owner] = true

      if user.save!
        # user.refreshed_token
        auth_token = JsonWebToken.encode({user_id: user.id})
        render_data( { auth_token: auth_token, users: user.as_api_response(:v2_user_only) }, I18n.t('user.signup_success') )
      end
    rescue ActiveModel::StrictValidationFailed => e
      error_valid_data(e)
    rescue ActiveRecord::RecordNotUnique => e
      error_valid_data(e)
    rescue StandardError => e
      err = user.errors.full_messages.try(:first).present? ? user.errors.full_messages.try(:first) : e
      error_valid_data(err)
    end


  private

  def check_null_password
    @user = User.find_by(email: params[:email].downcase)

    if @user.present?
      error_valid_data(I18n.t('user.wrong_signin_way')) if @user.password_digest.nil?
    end
  end

end
