class Api::V5::MyOrdersController < Api::V4::MyOrdersController
  before_action :login_required
  before_action :check_my_orders, only: :index

  def index
    render_data({ total: @my_order_list.size, result_total:  @my_order_list.count, my_orders: @my_order_list.as_api_response(:v5_booking_with_customer_info) }, I18n.t('user.my_orders') )
  end

  def show
    @my_order_detail = Booking.find(params[:id])
    render_data({ my_orders: @my_order_detail.as_api_response(:v5_booking_with_customer_info) }, I18n.t('user.my_order_detail')) if @my_order_detail.present?
  end

  private
  def check_my_orders
    unit_ids = []
    unit_ids = Unit.where(user_id: @current_user).ids
    result = Booking.where.not(user_id: params[:user_id]).where('unit_id IN(?)', unit_ids)
    @my_order_list =  result.order(id: :desc)
                      .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                      .offset(params[:offset])
    render_data({}, I18n.t('no_data')) unless @my_order_list.present?
  end
end
