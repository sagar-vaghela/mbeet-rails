module V5::Admin::UnitResponder
	extend ActiveSupport::Concern

	included do
		# --------------------- Admin Api Response --------------------- #
	  api_accessible :v5_admin_unit_only do |t|
	    t.add :id
	    t.add lambda{|unit| unit.title_en }, :as => :title_en
	    t.add lambda{|unit| unit.title_ar }, :as => :title_ar
	    t.add lambda{|unit| unit.body_en }, :as => :body_en
	    t.add lambda{|unit| unit.body_ar }, :as => :body_ar
	    t.add :bed_type
	    t.add :number_of_guests
	    t.add :number_of_rooms
	    t.add :number_of_beds
	    t.add :number_of_baths
			t.add lambda{|unit| unit.number_of_subunits == 0 ? 1 : unit.number_of_subunits }, :as => :number_of_subunits
	    t.add :available_as
	    t.add :city, :template => :cities_list
	    t.add :address
	    t.add :price
	    t.add :latitude
	    t.add :longitude
	    t.add :unit_class
	    t.add :unit_type
	    t.add :total_area
	    t.add :created_at
      t.add lambda{ |unit|
      	avg_array = unit.feedbacks.map(&:star)
      	if avg_array.present?
      		avg_array.inject(0.0) { |sum, el| sum + el } / avg_array.size
      	else
      		0
      	end
      } , :as => :avg_star
	    t.add :soft_delete
			t.add :unit_status
			t.add :unit_enabled
      t.add lambda{ |unit| unit.feedbacks.size }, :as => :feedback_count
	    t.add :liked_by_user
	    t.add :user, :template => :user_for_unit, as: 'owner'
	    t.add :default_image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :amenities, :template => :amenities_and_units
	    t.add :rules, :template => :rules_and_units
			t.add :mbeet_percentage
      t.add lambda { |unit|
        disabled_dates_array = unit.unit_disabled_dates
        disabled_dates_array.select do |a|
          a if a[:day_disabled].to_date >= Time.now.utc.to_date
        end
      }, :as => :units_disabled , :template => :disabled_dates
	  end

		# Used for booking listing and detail
	  # Used as template under Booking model
	  api_accessible :unit_for_bookings do |t|
	    t.add :id
	    t.add :title
	    t.add :address
	    t.add :latitude
	    t.add :longitude
	    t.add :price
	  end

		api_accessible :unit_with_pictures_admin_booking_list, extend: :unit_for_bookings do |t|
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end
	end
end
