class Api::V4::PeriodsController < Api::V3::BaseController
  before_action :login_required
  before_action :find_user_units
  before_action :find_target_user_unit, except: [:all_periods, :show, :destroy, :update]
  before_action :set_period, except: [:index, :create, :multiple_create, :all_periods, :get_total_price]

  def index
    all_periods = []
      @unit.periods.each do |period|
        period_hash = {}
        period_hash["id"] = period.id
        period_hash["unit_id"] = period.unit.id
        period_hash["period_date"] = period.period_date
        period_hash["price"] = period.price
        all_periods.push(period_hash)
      end
    render json: { all_periods: all_periods}
  end

  def all_periods
    all_periods = []
    @user_units.each do |unit|
      unit.periods.each do |period|
        period_hash = {}
        period_hash["id"] = period.id
        period_hash["unit_id"] = period.unit.id
        period_hash["period_date"] = period.period_date
        period_hash["price"] = period.price
        all_periods.push(period_hash)
      end
    end
    render json: { all_periods: all_periods}
  end

  def show
    render json:{
          period: {
                period_date: @period.period_date,
                price: @period.price,
            }
    }
  end

  def multiple_create
    message = []
    error = []
    @period_from = params[:from_date]
    @period_to = params[:to_date]
    @period_price = params[:price]
    if (@period_from > @period_to)
      render json: { message: I18n.t('errors.e_422'),
              error: { error: I18n.t('period_errors.invalid_date')} }
      return false
    end
    (@period_from..@period_to).each do |date|
      @exsisting_period = @unit.periods.find_by(period_date: date)
      if @exsisting_period.present?
        to_send_hash = _update_period
        message.push(to_send_hash)
      else
        @created_period = _create_period(date)
        message.push(@created_period[:message])
        error.push(@created_period[:error])
      end
    end
      render json: {message: message,
                    error: error }
  end

  def create
    @exsisting_period = @unit.periods.find_by(period_date: params[:period_date])
    if @exsisting_period.present?
      to_send_hash = _update_period
      messages = to_send_hash
    else
      @created_period = _create_period(params[:period_date])
      messages = @created_period[:message]
      errors = @created_period[:error]
    end
    render json: {
      message: messages,
      error: errors
    }
  end

  def _create_period(date)
    @user_period = @unit.periods.new()
    @user_period.period_date = date
    @user_period.price = params[:price]
    if @user_period.save!
      message = {id: @user_period.id,
                 period_date: @user_period.period_date.to_s,
                 price: @user_period.price, message: I18n.t('admin.period.created')}
    else
      error = {period_date: params[:period_date].to_s,
               message: I18n.t('errors.e_422')}
    end
    return {message: message, error: error}
  end

  def _update_period
    @exsisting_period.update_column(:price,params[:price])
    to_send_hash ={id: @exsisting_period.id,
                      period_date: @exsisting_period.period_date,
                      price: @exsisting_period.price, message: I18n.t('admin.period.updated') }
    return to_send_hash
  end


  def update
    @period.period_date = params[:period_date] if params[:period_date].present?
    @period.price = params[:price] if params[:price].present?
    if @period.save!
      render json: {
          message: I18n.t('admin.period.updated')
      }
    else
      render json: {
            message: I18n.t('errors.e_422'),
            error: {error: @period.errors.details}
        }
    end
  end

  def destroy
    if @period.destroy
      render json: {
          message: I18n.t('admin.period.deleted')
      }
    else
      render json: {
          message: I18n.t('errors.e_422')
      }
    end
  end

  def get_total_price
    sum_array = []
    all_message = []
    (params[:from_date]..params[:to_date]).each do |date|
      valid_period = @unit.periods.find_by(period_date: date)
      if valid_period.present?
        price = valid_period.price
        message = "Special price " + price.to_s + " for " + date
      else
        price = @unit.price
        message = "Unit price " + @unit.price.to_s + " for " + date
      end
      sum_array.push(price)
      all_message.push(message)
    end
    grand_total = sum_array.sum
    render json: {
                  total_price: grand_total.to_f,
                  message: all_message
      }
  end

  private

  def find_user_units
    @user_units = Unit.where(user_id: @current_user)
    if @current_user.blank?
      render json: {
          message: I18n.t('period_errors.invalid_user')
      }
    end
  end

  def find_target_user_unit
    @unit = @user_units.find_by(id: params[:unit_id])
    if @unit.blank?
      render json: {
          message: I18n.t('period_errors.invalid_unit')
      }
    end
  end

  def set_period
    @period = Period.find_by(id: params[:id])
    if @period.blank?
      render json: {
          message: I18n.t('period_errors.invalid_period')
      }
    end
  end

end
