class AddMessageToUnit < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :message, :text
  end
end
