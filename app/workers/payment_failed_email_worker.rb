class PaymentFailedEmailWorker
	@queue = 'order_payment_failed_email'

	def self.perform data, push_type
		user = User.where(id: data["user_id"]).pluck(:email, :name).first
		invoice_order_id = PaymentTxn.where(booking_id: data["id"]).pluck(:invoice_unique_id).first
		
		# Send email to user related to his booking order cancellation
		OrderMailer.payment_failed(user.first, user.second, invoice_order_id).deliver
		OrderMailer.payment_failed_to_admin(user.first, user.second, invoice_order_id).deliver
	end
end