require 'rails_helper'

RSpec.describe Coupon, type: :model do

  it 'has a valid factory' do
    expect(FactoryBot.create(:coupon)).to be_valid
  end

  context 'association' do
    it { should belong_to(:user) }
  end

  describe "validations" do

    it { is_expected.to validate_presence_of(:value) }
    it { is_expected.to validate_presence_of(:starting_date) }
    it { is_expected.to validate_presence_of(:expiration_date) }
    it { is_expected.to validate_presence_of(:status) }

  end
end
