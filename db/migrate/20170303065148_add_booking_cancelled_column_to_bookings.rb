class AddBookingCancelledColumnToBookings < ActiveRecord::Migration[5.0]
  def change
  	add_column :bookings, :booking_cancelled, :boolean, null: false, default: false
  end
end
