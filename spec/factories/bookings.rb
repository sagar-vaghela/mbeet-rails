FactoryBot.define do
  factory :booking do
    association :user, factory: :normal_user
    association :unit
    check_in Date.today
    check_out Date.today + 5
    payment_method 1
    payment_status 0
    lease_status 0
  end
end
