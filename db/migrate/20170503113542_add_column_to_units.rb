class AddColumnToUnits < ActiveRecord::Migration[5.0]
  def change
  	add_column :units, :unit_type, :integer, :limit => 2, null: false, default: 1
  	add_column :units, :total_area, :decimal, precision: 20, scale: 3, null: false, default: 0
  end
end
