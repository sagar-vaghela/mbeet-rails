class AddFieldIntoPicture < ActiveRecord::Migration[5.0]
  def change
    add_column :pictures, :locale, :boolean, default: false
  end
end
