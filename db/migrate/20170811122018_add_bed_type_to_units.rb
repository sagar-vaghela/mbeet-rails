class AddBedTypeToUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :bed_type, :integer
  end
end
