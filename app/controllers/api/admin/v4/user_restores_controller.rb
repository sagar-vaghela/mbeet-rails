class Api::Admin::V4::UserRestoresController < Api::Admin::V4::BaseController
  before_action :admin_login_required
	before_action :find_user, only: :update

  def update
    if @user.nil?
      error_valid_data(I18n.t('record_not_found'))
    else
      @user.skip_password_validation = true
      if @user.update soft_delete: false
       render_updated ({
         message: I18n.t('admin.user.restored'),
         data: { user: @user.as_api_response(:v4_admin_user_only) }
       })
      else
       render_unprocessable_entity( message: @user.errors.full_messages.join(', '))
      end
    end
	end

  private

  def find_user
    @user = User.find_by_id(params[:id])
  end
end
