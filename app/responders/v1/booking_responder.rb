module V1::BookingResponder
  extend ActiveSupport::Concern

  included do
    # Api Response using acts_as_api
    api_accessible :my_orders do |t|
      t.add :id
      t.add :check_in
      t.add :check_out
      t.add :payment_status
      t.add :lease_status
      t.add :total_amount
      t.add :booking_cancelled
      t.add :created_at
    end

    api_accessible :unit_with_bookings, extend: :my_orders do |t|
      t.add :unit, :template => :unit_with_pictures_for_myorders
    end

    api_accessible :my_order_detail, extend: :my_orders do |t|
      t.add :guests
      t.add :for_days, as: :rented_for_days
      t.add :sub_total_amount
    end

    api_accessible :unit_with_bookings_extra, extend: :my_order_detail do |t|
      t.add :unit, :template => :unit_with_pictures_for_myorders
    end

    api_accessible :booking_with_payment_txns, extend: :unit_with_bookings_extra do |t|
      t.add :payment_txns, :template => :payment_transaction
    end
  end
end
