class CreatePaymentTxns < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_txns do |t|
      t.references :booking, foreign_key: true, null: false

      # 0 = Pending, 1 = Processing, 2 = Done, 3 = Failed
      t.integer :payment_status, null: false, default: 0

      t.text :invoice_unique_id, null: false
      t.timestamp :modified_on, null: false
    end
  end
end
