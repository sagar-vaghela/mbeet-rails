source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end
gem 'rails', '~> 5.0.1' # Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'pg', '~> 0.18' # Use postgresql as the database for Active Record

# To use this gem you need PostgreSQL 9.4 and higher!
# This gem create DB-based solution for caching relation collections
# It faster than memcache+dalli combination. In some cases three times faster, but with all DB facilities!
#gem 'nitro_pg_cache'

gem 'activerecord-jdbcpostgresql-adapter', :platform => :jruby
gem 'json_translate'
gem 'puma', '~> 3.0' # Use Puma as the app server

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
gem 'redis', '~> 3.2' # Use Redis adapter to run Action Cable in production
gem 'bcrypt', '~> 3.1.7' # Use ActiveModel has_secure_password
gem 'cloudinary' # CDN for pictures
gem 'jwt' # A pure ruby implementation of the RFC 7519 OAuth JSON Web Token (JWT) standard.
gem 'activerecord-reset-pk-sequence' # To reindex primary key

gem 'capistrano-rails'# Use Capistrano for deployment
gem 'capistrano-rvm'
gem 'capistrano3-puma', group: :development
gem 'capistrano3-nginx'
gem 'capistrano-upload-config'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'
gem 'acts_as_api'
gem 'rack-attack'

# To send emails from the system
gem 'sendgrid-rails', '~> 2.0'

# To do search
gem 'sunspot_rails'
gem 'progress_bar'

# For Push Notifications
gem 'pubnub', '~> 4.0.20'

# Background processing for Ruby
gem 'resque'

# Resque-scheduler is an extension to Resque that adds support for queueing items in the future
gem 'resque-scheduler'

# # # Web Interface for Resque
gem 'resque-web', require: 'resque_web'

# # # Tabs in the Resque Web gem for the Resque Scheduler plugin
# gem 'resque-scheduler-web'

gem "capistrano-resque", "~> 0.2.2", require: false

# Adds tyling to HTML emails with CSS without having to do the hard work yourself
gem 'premailer-rails'

# Wrapper around Daniel Veillard’s excellent HTML/XML parsing library written
# libxml2. It simply wraps and builds upon this already existing library
gem 'nokogiri'

gem 'rack-attack'
# for testing analytics
gem 'newrelic_rpm'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'rspec-rails', '~> 3.5'
  gem 'rspec-activemodel-mocks', '= 1.0.1'
  gem 'rspec-collection_matchers'
  gem 'rails-controller-testing'
  gem 'shoulda-matchers', '= 2.8.0'
end


group :development, :dev, :staging do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # gem 'pry-rails'
  gem 'faker'
  gem 'listen', '~> 3.0.5'

  # To find out sql injection, cross site scripting
  gem 'brakeman'

  gem 'sunspot_solr'
  gem 'annotate', require: false # annotate model fields
  gem 'rails-erd'
end

group :development do
  # This gem can be used to find list of database columns that can be Indexed.
  gem 'lol_dba'
  # This gem can be used to find instances where
  # eager loading is required (to avoid N+1 query problem)
  # eager loading is used but not necessary to have it
  # counter cache should be used
  gem 'bullet'
end

group :test do
  gem "factory_bot_rails"
  gem 'database_cleaner'
end

gem 'goldiloader'
gem 'scout_apm', '>= 2.3.0.pre2', '< 3.0.pre'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Log maintaince
gem 'impressionist'
gem 'rails-erd'
