# == Schema Information
#
# Table name: user_booking_cancel_requests
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  reason_id  :integer          not null
#  message    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  booking_id :integer          not null
#

class UserBookingCancelRequest < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  # ------------------- Requirment ------------------- #
  include UserBookingCancelRequestResponder

  # ------------------- Associations ------------------- #
  belongs_to :user
  belongs_to :unit
  belongs_to :booking
  belongs_to :reason

  # ------------------- Validations ------------------- #
  validates :user, presence: true
  validates :unit, presence: true, allow_blank: true
  validates :booking, presence: true
  validates :reason,  presence: true
  validates :message, presence: true
end
