class TrendingCity < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  has_many :cities
end
