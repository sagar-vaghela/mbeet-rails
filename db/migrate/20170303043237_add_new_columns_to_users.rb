class AddNewColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :verify_email_sent_at, :timestamp
  	add_column :users, :email_verified, :boolean, null: false, default: false
  	add_column :users, :verify_phone_sent_at, :timestamp
  	add_column :users, :phone_verified, :boolean, null: false, default: false
  end
end
