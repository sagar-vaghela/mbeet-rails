class SaveNewFlatAddedByUserAdminNotificationsWorker
	@queue = "save_admins_notifications"

	def self.perform data, push_message, push_type
		User.where(role_id: 1).map do |user|
			Notification.create(user_id: user.id, body: JSON.parse(push_message), push_type: push_type)
		end
	end
end
