class Api::V6::TopDestinationsController < Api::V6::BaseController
  before_action :find_current_user

  # GET: '/v6/top_destinations'
  def index
    # impressionist(@current_user, request.path) if @current_user

    # limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    # search = TopDestination.search do
    #   data_accessor_for(TopDestination).include = [:city]
    #   fulltext params[:search]
    #
    #   order_by(:created_at, :desc)
    #   paginate(:offset => params[:offset], :per_page => limit)
    # end

    render_data({ total: TopDestination.all.size, result_total: TopDestination.all.count, top_destination: TopDestination.all.as_api_response(:v6_top_destination_pictures_admin)}, I18n.t('top_destination.top_destination'))
  end

  private

  def find_current_user
    @current_user = User.find_by(id: @current_user, role_id: [2])
  end
end
