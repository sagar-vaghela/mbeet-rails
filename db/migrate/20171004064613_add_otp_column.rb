class AddOtpColumn < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :otp, :integer
  end
end
