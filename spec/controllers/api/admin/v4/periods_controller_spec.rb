require 'rails_helper'

RSpec.describe Api::Admin::V4::PeriodsController, type: :controller do
  # let(:valid_attributes) {
  #   {id: 5,
  #     price: 500,
  #   period_date: "2015-09-02",
  #   unit_id: 54}
  # }
  #
  # let(:unit) {
  #     {id: "54",
  #     user_id: "5"}
  # }
  #
  # let(:valid_attributes_create) {{
  #   price: 500,
  #   period_date: "2015-09-02",
  #   unit_id: 54,
  #   user_id: 5
  #   }}
  #
  # let(:invalid_attributes) {
  #   skip("Add a hash of attributes invalid for your model")
  # }
  #
  # let(:headers) {
  #   {access_token: "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJpYXQiOjE1MTI0MDMxNTksImV4cCI6MTUxNTAzMTE1OX0.MsL8XAPGSwhzSHaq0_cRM_QZ-qKklBZ7vASOz_2aeWc"}
  # }
  #
  #
  # let(:valid_session) { {} }
  #
  #
  # describe "GET #index" do
  #   it "returns a success response" do
  #     period = Period.create! valid_attributes
  #     get :index, params: {
  #       unit_id: 54,
  #       access_token: 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJpYXQiOjE1MTI0MDMxNTksImV4cCI6MTUxNTAzMTE1OX0.MsL8XAPGSwhzSHaq0_cRM_QZ-qKklBZ7vASOz_2aeWc'},
  #       :format => 'json',session: valid_session
  #     expect(response).to be_success
  #   end
  # end
  #
  # describe "GET #all periods index" do
  #   it "returns a success response" do
  #       period = Period.create! valid_attributes
  #       get :all_periods, params: {
  #         unit_id: 54,
  #         user_id: 5,
  #         access_token: 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJpYXQiOjE1MTI0MDMxNTksImV4cCI6MTUxNTAzMTE1OX0.MsL8XAPGSwhzSHaq0_cRM_QZ-qKklBZ7vASOz_2aeWc'},
  #         :format => 'json',session: valid_session
  #       expect(response).to be_success
  #   end
  # end
  #
  # describe "GET #show" do
  #   it "returns a success response" do
  #     period = Period.create! valid_attributes
  #     get :show, params: {id: valid_attributes[:id], access_token: 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJpYXQiOjE1MTI0MDMxNTksImV4cCI6MTUxNTAzMTE1OX0.MsL8XAPGSwhzSHaq0_cRM_QZ-qKklBZ7vASOz_2aeWc'}
  #     expect(response).to be_success
  #   end
  # end
  #
  # describe "POST #create" do
  #   context "with valid params" do
  #     it "renders a JSON response with the new api_admin" do
  #
  #       post :create, params: {period_date: valid_attributes_create[:period_date], price: valid_attributes_create[:price],
  #         unit_id: valid_attributes_create[:unit_id] , access_token: headers[:access_token]}
  #       expect(response).to have_http_status(:ok)
  #       expect(response.content_type).to eq('application/json')
  #       expect(response.location).to eq(Period.last)
  #     end
  #   end
  #
  #   context "with invalid params" do
  #     it "renders a JSON response with errors for the new api_admin" do
  #
  #       post :create, params: {period_date: valid_attributes_create[:period_date],
  #         access_token: headers[:access_token]}
  #       expect(response).to have_http_status(:ok)
  #       expect(response.content_type).to eq('application/json')
  #       end
  #     end
  # end
  #
  # describe "PUT #update" do
  #   context "with valid params" do
  #     let(:new_attributes) {
  #       skip("Add a hash of attributes valid for your model")
  #     }
  #
  #     it "updates the requested api_admin" do
  #       period = Period.create! valid_attributes
  #       put :update, params: {id: period.to_param, price: 700,access_token: headers[:access_token]}
  #       period.reload
  #       skip("Add assertions for updated state")
  #     end
  #
  #     it "renders a JSON response with the api_admin" do
  #       period = Period.create! valid_attributes
  #
  #       put :update, params: {id: period.to_param, period_date: "2017-06-01",access_token: headers[:access_token]}, session: valid_session
  #       expect(response).to have_http_status(:ok)
  #       expect(response.content_type).to eq('application/json')
  #     end
  #   end
  #
  # end
  #
  # describe "DELETE #destroy" do
  #   it "destroys the requested api_admin" do
  #     period = Period.create! valid_attributes
  #     expect {
  #       delete :destroy, params: {id: period.to_param, access_token: headers[:access_token]}
  #     }.to change(Period.all, :count).by(-1)
  #   end
  # end
  #
  # describe "get #get_total_price" do
  #   it "get all get_total_price" do
  #     get :all_periods, params: {
  #       unit_id: 54,
  #       user_id: 5,
  #       access_token: headers[:access_token]},
  #       :format => 'json',session: valid_session
  #     expect(response).to be_success
  #   end
  # end
  #
  # describe "post #get_total_price" do
  #   it "post create multiple periods" do
  #     post :multiple_create, params: {
  #       from_date: "2017-12-02",
  #       to_date: "2017-12-06",
  #       price: 300,
  #       unit_id: 54,
  #       access_token: headers[:access_token]},
  #       :format => 'json',session: valid_session
  #     expect(response).to be_success
  #   end
  # end
  #
  # describe "post #get_total_price" do
  #     it "post create multiple periods" do
  #       post :multiple_create, params: {
  #         from_date: "2017-12-02",
  #         to_date: "2017-12-06",
  #         price: 300,
  #         unit_id: 54,
  #         access_token: headers[:access_token]},
  #         :format => 'json',session: valid_session
  #       expect(response).to be_success
  #     end
  #   end

end
