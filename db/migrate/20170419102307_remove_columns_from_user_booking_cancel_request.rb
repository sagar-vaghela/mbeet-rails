class RemoveColumnsFromUserBookingCancelRequest < ActiveRecord::Migration[5.0]
  def self.up
  	remove_column :user_booking_cancel_requests, :unit_id

  	remove_column :user_booking_cancel_requests, :check_in
  	remove_column :user_booking_cancel_requests, :check_out
  	
  	add_reference :user_booking_cancel_requests, :booking, index: true, foreign_key: true, null: false
  end

  def self.down
  	remove_reference :user_booking_cancel_requests, :booking
  	add_reference :user_booking_cancel_requests, :unit, index: true, foreign_key: true, null: false
  	
  	add_column :user_booking_cancel_requests, :check_in, :timestamp, null: false 
  	add_column :user_booking_cancel_requests, :check_out, :timestamp, null: false 
  end
end
