class Api::Admin::V3::CitiesController < Api::Admin::V3::BaseController
  before_action :admin_login_required
  before_action :find_city, only: [:show, :update, :destroy]

  # GET: 'admin/v3/cities'
  def index

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = City.search do
      fulltext params[:search]
      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      if params[:published].present?
        status = params[:published] == 'true' ? true : false
        with(:published, status)
      end

      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end
      paginate(:offset => params[:offset], :per_page => limit)

    end

    render_data({ total: City.not_deleted.size, result_total: search.total, cities: search.results.as_api_response(:v3_city_pictures_admin)}, I18n.t('city.cities_list'))
  end

  # GET: 'admin/v3/cities/:id'
  def show
		render_data(
      { cities: @city.as_api_response(:v3_city_pictures_admin) },
      I18n.t('city.city_detail')
    ) if @city.present?
	end

  # POST: 'admin/v3/cities'
  def create
    check_required_params params, [:name_en, :name_ar]
    city = City.new city_params
    city.name_translations = {en: city.name_en, ar: city.name_ar}
    if city.save
     render_created ({
       message: I18n.t('city.created'),
       data: { city: city.as_api_response(:v3_city_pictures_admin) }
     })
    else
     render_unprocessable_entity( message: city.errors.full_messages.join(', '))
    end
  end

  # PUT: 'admin/v3/cities/:id'
  def update
    @city.name_translations = {en: @city.name_en, ar: @city.name_ar}
    if  @city.update city_params
     render_updated ({
       message: I18n.t('city.updated'),
       data: { city: @city.as_api_response(:v3_city_pictures_admin) }
     })
   else
     render_unprocessable_entity( message: @city.errors.full_messages.join(', '))
   end
  end

  # DELETE: 'admin/v3/cities/:id'
  def destroy
		if @city.units.any?
			if @city.units.all?{ |unit| unit.soft_delete == true }
        if  @city.update(soft_delete: true)
         render_deleted ({
           message: I18n.t('city.deleted'),
           data: { city: @city.as_api_response(:v3_city_pictures_admin) }
         })
        else
         render_unprocessable_entity( message: @city.errors.full_messages.join(', '))
        end
			else
				render_unprocessable_entity( message: I18n.t('admin.city.has_some_units'))
			end
		else
      if  @city.update(soft_delete: true)
       render_deleted ({
         message: I18n.t('city.deleted'),
         data: { city: @city.as_api_response(:v3_city_pictures_admin) }
       })
      else
       render_unprocessable_entity( message: @city.errors.full_messages.join(', '))
      end
		end
  end


  ##----------------------------- Private Methods ----------------------------##

  private
  # Order cities ASC based on the name
  def get_order
  	"name_translations->>'#{I18n.locale}' ASC"
  end

  # Find a specific city
  def find_city
    @city = City.where(soft_delete: false).find( params[:id] )
  end

  # Define which city parameters are allowed from external applications/PL
  # to perform creation /updation
  def city_params
    params.permit(:name_en, :name_ar, :published, :soft_delete, :picture_attributes => [:id, :name, :_destroy])
    # params.permit(:name_ar, :published, :soft_delete, :picture_attributes => [:id, :name, :_destroy])
  end

  ##--------------------------------------- end ------------------------------##
end
