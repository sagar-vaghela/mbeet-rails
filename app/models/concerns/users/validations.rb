module Users
	module Validations
		extend ActiveSupport::Concern

		included do
			attr_accessor :skip_password_validation, :password_confirmation, :skip_email_validation
		  attr_accessor :random_password, :password_update
		  attr_accessor :type, :uuid, :role_id_exist

		  #	ActiveRecord Callback
			before_validation :downcase_email
			before_validation :downcase_name

			VALIDATE_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

			# ActiveRecord Validations
			validates :name, presence: true, length: { minimum: 6 }, strict: true
			validates :email, presence: true,
			          length: { maximum: 255 },
			          uniqueness: { case_sensitive: false },
			          format: { with: VALIDATE_EMAIL_REGEX },
			          strict: true,
		            unless: :skip_email_validation

			validates :phone, presence: true, length: { maximum: 20, minimum: 4 }, uniqueness: true, if: :phone_exist?

			has_secure_password(validations: false)
			validates :password, presence: true, length: { minimum: 6 }, unless: :skip_password_validation

		  validate :validatpassword, if: :compare_password

		  validates :phone_verified, :presence => true, :on => :update, :if => :phone_verified_exist?

		  validates :role, :presence => true, :on => :create, :if => :role_id_exist
		end
	end
end
