# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Role < ApplicationRecord

	# maintain user visited urls
  is_impressionable

	# ------------------ Associations ----------------------- #
	has_many :users, dependent: :destroy
	has_one :role_permission, dependent: :destroy

	searchable do
		text :name, :id
    integer :id
    string :name
  end

	# ------------------ v6 Api Methods ------------------------ #
	api_accessible :for_admin_show_roles do |t|
		t.add :id
		t.add :name
		t.add :role_permission, :template => :v6_role_permissions
	end

end
