require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mbeet
  class Application < Rails::Application

    require 'resque'

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    
    # Don't generate system test files.
    config.generators.system_tests = nil
    config.api_only = true

    config.i18n.fallbacks = true

    # config.active_record.raise_in_transactional_callbacks = true
    config.active_job.queue_adapter = :resque

    # Load RakeAttack middleware for api throttling
    config.middleware.use Rack::Attack
  end
end
