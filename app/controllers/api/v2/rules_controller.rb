class Api::V2::RulesController < Api::V2::BaseController
	def index
		@rules = Rule.order(id: :ASC)
		render_data({ rules: @rules.as_api_response(:rule_list)}, I18n.t('rules.list')) if @rules
	end
end
