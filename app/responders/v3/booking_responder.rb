module V3::BookingResponder
	extend ActiveSupport::Concern

	included do
		api_accessible :v3_my_orders do |t|
	    t.add :id
	    t.add :check_in
	    t.add :check_out
	    t.add :payment_status
	    t.add :lease_status
	    t.add :total_amount
			t.add lambda{|booking| (booking.payment_status == 3 && booking.check_out < Time.now) ?  1 : 0 } , :as => :hide_cancel
	    t.add :booking_cancelled
	    t.add :created_at
	    t.add :feedback
	  end

	  api_accessible :v3_unit_with_bookings, extend: :v3_my_orders do |t|
	    t.add :unit, :template => :unit_with_pictures_for_myorders
	  end

	  api_accessible :v3_my_order_detail, extend: :v3_my_orders do |t|
	    t.add :guests
	    t.add :for_days, as: :rented_for_days
	    t.add :sub_total_amount
	  end

	  api_accessible :v3_unit_with_bookings_extra, extend: :v3_my_order_detail do |t|
	    t.add :unit, :template => :unit_with_pictures_for_myorders
	  end

	  api_accessible :v3_booking_with_payment_txns, extend: :v3_unit_with_bookings_extra do |t|
	    t.add :payment_txn, :template => :payment_transaction
	  end
	end
end
