# == Schema Information
#
# Table name: bookings
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  unit_id           :integer          not null
#  check_in          :datetime         not null
#  check_out         :datetime         not null
#  payment_method    :integer          default("credit_card"), not null
#  payment_status    :integer          default(1), not null
#  lease_status      :integer          default(2), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  guests            :integer          default(0), not null
#  for_days          :integer          default(0), not null
#  sub_total_amount  :decimal(10, 2)   default(0.0), not null
#  total_amount      :decimal(10, 2)   default(0.0), not null
#  booking_cancelled :boolean          default(FALSE), not null
#  mbeet_fees        :decimal(10, 2)
#  advance_payment   :decimal(10, 2)   default(0.0)
#

require 'rails_helper'

RSpec.describe Booking, type: :model do
  let(:booking) { FactoryBot.create(:booking) }

  it 'has a valid factory of city' do
    expect(booking).to be_valid
  end

  context 'association' do
    it { should belong_to(:user) }
    it { should belong_to(:unit) }
    it { expect(booking).to have_many :notifications }
    it { expect(booking).to have_one :user_booking_cancel_request }
    it { expect(booking).to have_one :payment_txn }
  end
end
