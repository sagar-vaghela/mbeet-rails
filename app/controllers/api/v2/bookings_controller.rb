class Api::V2::BookingsController < Api::V2::BaseController
	before_action :login_required
	before_action :check_user_verification, :check_booking, only: :create

	before_action :first_verify_booking, only: :cancel
	before_action :check_payment_status, :get_payment, only: :payment_update

	after_action  :update_booking_cancelled, only: :cancel

	# POST /v2/book_now
	def create
		if @booking = Booking.create!(booking_params)
			@booking.notify @booking, 3
		
			render_data({ bookings: @booking.as_api_response(:v2_booking_with_payment_txns) }, I18n.t('user.booking.success') )
		end
	rescue StandardError => e
		error_valid_data(e)
	end

	# POST /v2/cancel/booking
	def cancel
		if @cancelled = UserBookingCancelRequest.create!(booking_cancel_params)
			@booking = Booking.find(@cancelled.booking_id)
			@booking.notify @booking, 4
		
			render_data({ bookings: {} }, I18n.t('user.booking.cancelled') )
		end
	rescue StandardError => e
		error_valid_data(e)
	end

	# PUT /v2/payment/update
	def payment_update
		return error_data I18n.t('record_not_found') unless @payment.present?

		if @payment.update(payment_status: params[:payment_status], modified_on: params[:modified_on])
			@booking = Booking.find(params[:booking_id])
			@booking.process @booking, params[:payment_status]

			render_data({ payments: {} }, I18n.t('user.booking.payment.updated') )
		end
	end

	private
	# Methods to do a booking
	def booking_params
		params.permit(:user_id, :unit_id, :check_in, :check_out, :guests, :for_days, :sub_total_amount, :total_amount,
			            payment_txn_attributes: [:modified_on]
			           )
	end

	def check_booking
		booking = Booking.where(unit_id: params[:unit_id])
		                 .where("check_in <= '#{params[:check_in]}' AND check_out > '#{params[:check_in]}'")
		                 .where(booking_cancelled: false)

		if booking.present?
			error_valid_data(I18n.t('user.booking.not_available'))
		elsif params[:check_in].to_s == params[:check_out].to_s
			error_valid_data(I18n.t('user.booking.same_dates'))
		end
	end

	# Methods for Cancel a booking
	def booking_cancel_params
		params.permit(:user_id, :booking_id, :reason_id, :message)
	end

	def first_verify_booking
		booking = Booking.find(params[:booking_id])
		if booking.present?
			error_valid_data(I18n.t('user.booking.already_cancelled')) if booking.booking_cancelled == true
		else
			error_valid_data(I18n.t('user.booking.verify'))
		end
	end

	def update_booking_cancelled
		booking = Booking.find(@cancelled.booking_id)
		booking.update!(booking_cancelled: true)
	end

	def check_user_verification
		verify = User.find_by(email_verified: true, phone_verified: true, id: params[:user_id])
		error_valid_data(I18n.t('user.booking.contact_details')) if verify.nil?
	end

	def get_payment
		@payment = PaymentTxn.find_by(booking_id: params[:booking_id])
	end

	def check_payment_status
		error_valid_data(I18n.t('user.booking.payment.status_not_valid')) unless params[:payment_status].to_i == 4 || params[:payment_status].to_i == 2
	end
end
