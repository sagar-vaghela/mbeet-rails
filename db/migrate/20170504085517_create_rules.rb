class CreateRules < ActiveRecord::Migration[5.0]
  def change
    create_table :rules do |t|
    	t.column  :name_translations,  'jsonb', null: false
    	t.boolean :status, null: false, default: true
      t.timestamps
    end

    # Create HABTM Table
    create_table :rules_units, id: false do |t|
      t.belongs_to :unit, index: true, null: false
      t.belongs_to :rule, index: true, null: false
    end
  end
end
