class AddSoftDeleteToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :soft_delete, :boolean, default: false
  end
end
