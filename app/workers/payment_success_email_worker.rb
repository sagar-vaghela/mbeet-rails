class PaymentSuccessEmailWorker
	@queue = 'order_payment_success_email'

	def self.perform data, push_type
		user = User.where(id: data["user_id"]).pluck(:email, :name).first
		invoice_order_id = PaymentTxn.where(booking_id: data["id"]).pluck(:invoice_unique_id).first
		
		# Send email to user related to his booking order cancellation
		OrderMailer.payment_success(user.first, user.second, invoice_order_id).deliver
		OrderMailer.payment_success_to_admin(user.first, user.second, invoice_order_id).deliver
	end
end