class Api::Admin::V2::UsersController < Api::Admin::V2::BaseController
	before_action :admin_login_required
	before_action :check_user, except: [:index, :create, :owners]
	after_action  :rescue_response

	# GET
	def index
		@users = User.order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
		render_data({ total: User.count, users: @users.as_api_response(:v2_admin_user_only)}, I18n.t('user.list'))
	end

	# GET
	def show
		render_data({ users: @user.as_api_response(:v2_admin_user_only) }, I18n.t('user.detail'))
	end

	# POST
	def create
		@user = User.new(user_params)
		@user.role_id_exist = params[:role_id].empty?
		@user.skip_password_validation = true
		@user.is_owner = true if @user.role_id == 2

		if @user.save!
			# Uncomment this to send email to user
			@user.send_login_detail_to_user
			render_data({ users: @user.as_api_response(:v2_admin_user_only) }, I18n.t('admin.user.created'))
		end
	rescue ActiveModel::StrictValidationFailed => e
    error_valid_data(e)
  rescue ActiveRecord::RecordNotUnique => e
  	err = @user.errors.full_messages.try(:first).present? ? @user.errors.full_messages.try(:first) : e
    error_valid_data(err)
  rescue ActiveRecord::RecordInvalid => e
  	err = @user.errors.full_messages.try(:first).present? ? @user.errors.full_messages.try(:first) : e
    error_valid_data(err)
	end

	# PUT
	def update
		@user.skip_password_validation = true
		@user.update(user_params)

		if @user.role_id != 2
			@user.is_owner = false
		else
			@user.is_owner = true
		end

		render_data({ users: @user.as_api_response(:v2_admin_user_only) }, I18n.t('admin.user.updated')) if @user.save!
	end

	# DELETE
	def destroy
		render_data({ users: {} }, I18n.t('admin.user.deleted')) if @user.destroy!
	end

	# GET
	def owners
		@owners = User.owners.order(name: :asc)
		render_data({ owners: @owners.as_api_response(:owners)}, I18n.t('admin.user.owners')) if @owners.present?
	end

	def units
		@units = Unit.where(user_id: params[:id]).order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @units.present?
			render_data({ total: @units.size, units: @units.as_api_response(:v2_admin_unit_only)}, I18n.t('unit.list'))
		else
			render_no_data
		end
	end

	private
	def user_params
		params.permit(:role_id,
								  :name,
								  :email,
									:country_code,
								  :phone,
								  :date_of_birth,
								  :gender,
								  :is_owner,
								  :email_verified,
									:phone_verified,
									:picture_attributes => [
										:id,
										:name,
										:_destroy
									]
								)
	end

	def check_user
		@user = User.find(params[:id])
	end

	def rescue_response
	rescue StandardError => e
    error_valid_data(e)
  end
end
