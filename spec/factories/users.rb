FactoryBot.define do
  factory :admin_user, class: User do
    association :role, factory: :role
    name "admin1"
    email "admin@example.com"
    phone "00918289067"
    is_owner false
    facebook_uid nil
    twitter_uid nil
    gmail_uid nil
    password "password"
    password_confirmation "password"
    created_at "Wed, 13 Dec 2017"
    updated_at "Wed, 13 Dec 2017"
    gender nil
    date_of_birth nil
    verify_email_sent_at nil
    email_verified false
    verify_phone_sent_at nil
    phone_verified false
    confirm_email_token nil
    country_code nil
    soft_delete false
    otp nil
  end

  factory :owner_user, class: User do
    association :role, factory: :owner_role
    name "Danial"
    email "DW@example.com"
    phone "00818289077"
    is_owner true
    facebook_uid nil
    twitter_uid nil
    gmail_uid nil
    password "password"
    password_confirmation "password"
    created_at "Wed, 15 Dec 2017"
    updated_at "Wed, 15 Dec 2017"
    gender nil
    date_of_birth nil
    verify_email_sent_at nil
    email_verified false
    verify_phone_sent_at nil
    phone_verified false
    confirm_email_token nil
    country_code nil
    soft_delete false
    otp nil
  end

  factory :normal_user, class: User do
    association :role, factory: :normal_role
    name "Kwellia"
    email "KW@example.com"
    phone "00788289077"
    is_owner false
    facebook_uid nil
    twitter_uid nil
    gmail_uid nil
    password "password"
    password_confirmation "password"
    created_at "Wed, 18 Dec 2017"
    updated_at "Wed, 20 Dec 2017"
    gender nil
    date_of_birth nil
    verify_email_sent_at nil
    email_verified false
    verify_phone_sent_at nil
    phone_verified false
    confirm_email_token nil
    country_code nil
    soft_delete false
    otp nil
  end
end
