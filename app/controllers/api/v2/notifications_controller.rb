class Api::V2::NotificationsController < Api::V2::BaseController
	before_action :login_required
	before_action :get_notification, only: [:update, :destroy]

	# GET /v2/notifications
	def index
		@notifications = Notification.where("user_id = ? AND created_at >= ?", params[:user_id], Date.today - 1.day)
										             .order(id: :desc)
                   					     .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                                 .offset(params[:offset])				
		if @notifications.present?
			render_data({ total: Notification.count, 
								  notifications: @notifications.as_api_response(:notifications)}, I18n.t('notification.list')) 
		else
			error_valid_data(I18n.t('no_data'))		
		end	
	end

	# POST /v2/notifications
	def create
		@notification = Notification.new(notification_params)
		@notification["body"] = JSON.parse(params[:body]) if params[:body].present?
		render_data({ notifications: @notification.as_api_response(:notifications) }, I18n.t('notification.created') ) if @notification.save!		
	end

	# PUT /v2/notifications
	def update
		if @notification.update(read: true)
			render_data({ notifications: @notification.as_api_response(:notifications)}, I18n.t('notification.updated')) 
		end
	end

	# DELETE /v2/notifications
	# def destroy
	# end

	private
	def notification_params
		params.permit(:user_id, :booking_id, :push_type, :body, :read)
	end

	def get_notification
		@notification = Notification.find(params[:id])
	end

end
