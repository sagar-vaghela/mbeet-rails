require 'resque/tasks'
require 'resque/scheduler/tasks'

namespace :user_notifications do
  task send: :environment do
    # For testing (single user)
    # you can add your user's id here
    # users = User.where(id: 1)

    # Please comment out below line when you test with single(your) user
    # for multiple(all) users

    users = User.all

    users.each do |user|
      $pubnub.publish(channel: "#{CONSTANT[:prefix]}_user_#{user.id}", message: "Hello World!" ) do |envelope|
        puts envelope.status
      end
    end
  end

end
