module ReasonResponder
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :reasons do |t|
      t.add :id
    	t.add :title
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :reasons

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :reasons

    api_accessible :v1_admin_show, extend: :v1_admin_index
  end
end
