class Api::V4::UserUnitsController < Api::V3::UserUnitsController
  before_action :login_required
  before_action :check_unit, except: [:index, :create]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy
  before_action :find_user_units
  before_action :check_booking, only: [:disable]
  before_action :previous_association_ids, only: :update

  # GET /v4/user_units
  def index
    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
      user_units = @user_units.is_deleted   #.unscope(:where)
                              .order(id: :desc)
                              .limit(params[:limit] == -1 ? 100 : params[:limit])
                              .offset(params[:offset])

      user_units.map { |unit| unit.liked_by_user = 0 }
      render_data({ total: user_units.count, units: user_units.as_api_response(:v4_unit_pictures)}, I18n.t('unit.list')) if user_units
    else
      user_units = @user_units.is_deleted     #.unscope(:where)
                              .order(id: :desc)
                              .limit(params[:limit] == -1 ? 100 : params[:limit])
                              .offset(params[:offset])

      # user_units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      units = user_units.includes(:user, :user_likes, :city, :feedbacks, :unit_disabled_dates)
                        .references(:user, :user_likes, :city, :feedbacks, :unit_disabled_dates)
      # units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }

      units.map do |unit|
        # unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0
        unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", unit.id).present? ? 1 : 0
      end

      render_data({ total:units.count, units: units.as_api_response(:v4_unit_only)}, I18n.t('unit.list')) if @current_user.present? && user_units
    end
  end

  # GET /v4/user_units/:id
  def show
    user_unit = @user_units.is_deleted.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
      render_data({ units: user_unit.as_api_response(:v4_unit_pictures) }, I18n.t('unit.detail')) if user_unit
    else
      # user_unit.liked_by_user = (UserLike.if_liked_by_user @current_user, user_unit.id).present? ? 1 : 0
      user_unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", user_unit.id).present? ? 1 : 0
      render_data({ units: user_unit.as_api_response(:v4_unit_pictures) }, I18n.t('unit.detail')) if user_unit
    end
  end

  # POST /v4/user_units
  def create
    user_unit = @user_units.new(unit_params)
    user =  User.find(@current_user)
    if user_unit.save
      render_data({ units: user_unit.as_api_response(:v4_unit_pictures)}, I18n.t('unit.created'))
      user.change_user_role_to_owner
      user.send_email_to_admin
      # 7 is the push_type for notification 7 = ["New flat has been added", "تم إضافة شقة جديدة"]
      # user.send_notification_to_admin user_unit, 7
    else
      render_unprocessable_entity( message: user_unit.errors.full_messages.join(', '))
    end
  end

  # PUT /v4/user_units/:id
  def update
    if @user_unit.update(unit_params)
      if attributes_changed?(@user_unit)
        unless @user_unit.previous_changes.keys == ["price", "updated_at"]
          @user_unit.send_admin_email_by_user_flat_changes
        end
      end
      render_data({ units: @user_unit.reload.as_api_response(:v4_unit_pictures) }, I18n.t('unit.updated'))
    else
      render_unprocessable_entity( message: @user_unit.errors.full_messages.join(', '))
    end
  end

  # DELETE /v4/user_units/:id
  def destroy
    if find_bookings.any?
      render_error(message: I18n.t('errors.cant_be_deleted') )
    else
      render_data({ units: {} }, I18n.t('unit.deleted')) if @user_unit.update(soft_delete: true)
    end
  end

  # GET /v4/user_units/enable (start_date, end_date, number_of_subunits, id are required params)
  def enable
    if @user_unit.update(unit_enabled: true)
      # if @user_unit.number_of_subunits > 0
      #   if params[:start_date].present? && params[:end_date].present? && params[:number_of_disabled_units].present?
      #     (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
      #       @user_unit.unit_disabled_dates.find_by(
      #         day_disabled: day_disabled
      #       ).decrement!(:number_of_disabled_units, by = params[:number_of_disabled_units].to_i)
      #     end
      #   else
      #     @user_unit.unit_disabled_dates.destroy_all
      #   end

      # else
      #   if params[:start_date].present? && params[:end_date].present?
      #     (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
      #       @user_unit.unit_disabled_dates.find_by(
      #         day_disabled: day_disabled
      #       ).destroy
      #     end
      #   else
      #     @user_unit.unit_disabled_dates.destroy_all
      #   end
      # end
      # @user_unit.unit_disabled_dates.delete_all

      @user_unit.unit_disabled_dates.each do |udd|
        if (params[:start_date].to_datetime..params[:end_date].to_datetime).include?(udd.day_disabled.to_date)
          udd.destroy
        end
      end

      # Check if user has liked this unit or not
      @user_unit.liked_by_user = $redis.sismember(
        "mbeet_user:#{@current_user}:units:liked",
        @user_unit.id
      ).present? ? 1 : 0

      render_updated ({
                        message: I18n.t('unit.enabled'),
                        data: { unit: @user_unit.as_api_response(:v4_unit_pictures) }
      })
    else
      render_unprocessable_entity( message: @user_unit.errors.full_messages.join(', '))
    end
  end

  # GET /v4/user_units/disable
  def disable
    number_of_disabled_units = @user_unit.number_of_subunits > 0 ? params[:number_of_disabled_units] : 0

    (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
      @user_unit.unit_disabled_dates.find_or_create_by(
        day_disabled: day_disabled
      ).increment!(:number_of_disabled_units, by = number_of_disabled_units.to_i)
    end

    # Check if user has liked this unit or not
    @user_unit.liked_by_user = $redis.sismember(
      "mbeet_user:#{@current_user}:units:liked",
      @user_unit.id
    ).present? ? 1 : 0

    render_updated ({
                      message: I18n.t('unit.disabled'),
                      data: { unit: @user_unit.as_api_response(:v4_unit_pictures) }
    })
  end

  # GET /v4/user_units/list_disable_units (id required)
  def list_disable_units
    @unit = Unit.find(params[:id])
    render_data({
      data:{unit: @unit.as_api_response(:v4_unit_only)},
      message: "Success"
      }, status: 200)
  end

  ##----------------------------- Private Methods ----------------------------##

  private

  def user_is_present?
    (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
  end

  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar, :number_of_guests, :bed_type,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id, :number_of_subunits,
                  :address, :price, :latitude, :longitude, :unit_class, :unit_type,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @user_unit = Unit.includes(:user, :user_likes, :city, :feedbacks, :unit_disabled_dates, :booked_subunits)
                     .where(user_id: @current_user)
                     .references(:user, :user_likes, :city, :feedbacks, :unit_disabled_dates, :booked_subunits)
                     .find(params[:id])
  end

  def rescue_response
  rescue StandardError => e
    error_valid_data(e)
  end

  def delete_liked_unit
    @unit_likes = UserLike.where(unit_id: params[:id])
    @unit_likes.delete_all if @unit_likes.present?
  end

  # Find user units
  def find_user_units
    @user_units = Unit.where(user_id: @current_user)
  end


  # find the number of bookings this month (for owners)

  # def number_of_bookings_this_month
  #   @units = find_user_units
  #   @bookings = @units.bookings.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
  #   render_data data: @bookings, message: "success"
  # end

  # find amenity_ids, rule_ids and picture_ids
  def previous_association_ids
    @previous_amenity_ids = @user_unit.amenity_ids
    @previous_rule_ids = @user_unit.rule_ids
    @previous_picture_ids = @user_unit.picture_ids
  end

  # check whether the unit is booked or not
  def check_booking
    # ---------------------- check bookings --------------------- #
    check_required_params params, [:id, :start_date, :end_date, :number_of_disabled_units]
    count_booked_days = count_number_of_booked_days
    count_disabled_units = count_number_of_disabled_units
    booking = Booking.where(booking_cancelled: false)
                     .where(unit_id: params[:id])
                     .where(
                       "check_in <= '#{params[:end_date]}' AND check_out > '#{params[:start_date]}'"
                     ).or(
                       Booking.where(
                         "check_in >= '#{params[:end_date]}' AND check_out < '#{params[:start_date]}'"
                       )
                     )


    # ------------------------------------------------------- #
    if params[:number_of_disabled_units].to_i > @user_unit.number_of_subunits
      error_valid_data(I18n.t('errors.wrong_number_of_disabled_units'))
    else
      if @user_unit.number_of_subunits == 0
        if booking.present? || count_disabled_units.any?
          error_valid_data(I18n.t('errors.disabled_or_booked'))
        end
      elsif booking.present? || @user_unit.number_of_subunits > 0
        number_of_booked_days = count_booked_days.nil? ||
                                  count_booked_days.empty? ? 0 : count_booked_days.compact.max

        number_of_disabled_days = count_disabled_units.nil? ||
                                    count_disabled_units.empty? ? 0 : count_disabled_units.compact.max

        remaining_subunits = (@user_unit.number_of_subunits - number_of_booked_days - number_of_disabled_days)

        if count_booked_days.any? || count_disabled_units.any?
          if remaining_subunits == 0
            error_valid_data(I18n.t('errors.disabled_or_booked'))
          elsif remaining_subunits > 0
            unless remaining_subunits >= params[:number_of_disabled_units].to_i
              error_valid_data(I18n.t('errors.subunits_disabled', subunits: remaining_subunits))
            end
          end
        end
      elsif params[:start_date].to_s == params[:end_date].to_s
        error_valid_data(I18n.t('user.booking.same_dates'))
      end
    end
    # ------------------------------------------------------- #
  end

  def count_number_of_booked_days
    @user_unit.booked_subunits
              .where("day_booked >= '#{params[:start_date].to_datetime}' AND day_booked <= '#{params[:end_date].to_datetime}'")
              .pluck(:count)
  end

  def count_number_of_disabled_units
    @user_unit.unit_disabled_dates
              .where("day_disabled >= '#{params[:start_date].to_datetime}' AND day_disabled <= '#{params[:end_date].to_datetime}'")
              .pluck(:number_of_disabled_units)
  end

  def find_bookings
    @user_unit.booked_subunits.where(
      "day_booked >= '#{Time.now.to_date}'"
    )
  end

  def attributes_changed?(user_unit)
    !user_unit.previous_changes.empty? || @previous_amenity_ids != user_unit.amenity_ids || @previous_rule_ids != user_unit.rule_ids || @previous_picture_ids != user_unit.pictures.pluck(:id)
  end

  ##--------------------------------------- end ------------------------------##
end
