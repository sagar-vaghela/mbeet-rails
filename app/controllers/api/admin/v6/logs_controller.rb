class Api::Admin::V6::LogsController < Api::Admin::V6::BaseController
	before_action :admin_login_required

	# GET: '/admin/v6/logs'
  def index
    search = Impression.order(created_at: :desc)
		.limit(params[:limit].to_i == -1 ? 100 : params[:limit])
		.offset(params[:offset])

		logs = []

		search.each do |imp|
			log = {}
			log["id"] = imp.id
			log["user_name"] = User.find(imp.user_id).name
			log["role"] = User.find(imp.user_id).role.name
			log["date_time"] = imp.created_at
			log["site_url"]= imp.referrer
			log["request_path"]= imp.message
			log["controller_name"]= imp.controller_name
			log["action_name"]= imp.action_name
			logs.push(log)
		end

		render_data({ total: search.count, result_total: Impression.all.count, logs: logs.as_api_response(:v6_logs)}, I18n.t('user_visits.impressions'))
  end

end
