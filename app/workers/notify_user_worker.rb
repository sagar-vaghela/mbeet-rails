class NotifyUserWorker

	@queue = :notify_user_of_booking

	def self.perform
		before_two_days
		before_on_day
		before_on_day_checkout
	end

	#---------------------------------------------------------------------------------------#
	#  This will get the bookings of users for which only two days are left to do check in.
	#---------------------------------------------------------------------------------------#
	def self.before_two_days
		@users = User.includes(:bookings)
		             .select("users.id, users.email, bookings.check_in, bookings.unit_id")
		             .where("bookings.check_in IS NOT NULL")
		             .where("bookings.check_in BETWEEN ? AND ?", Date.today + 2.day, Date.today + 3.days)
		             .references(:bookings)

		@users.each do |user|
			user.bookings.each do |booking|
				## Notify client by sending push notification
				SendPush.new(booking, 1).send
			end
		end  if @users.present?

	end

	#-------------------------------------------------------------------------------------#
	#  This will get the bookings of users for which only one day is left to do check in.
	#-------------------------------------------------------------------------------------#
	def self.before_on_day
		@users = User.includes(:bookings)
		             .select("users.id, users.email, bookings.check_in, bookings.unit_id")
		             .where("bookings.check_in IS NOT NULL")
		             .where("bookings.check_in BETWEEN ? AND ?", Date.today + 1.day, Date.today + 2.days)
		             .references(:bookings)

		@users.each do |user|
			user.bookings.each do |booking|
				## Notify client by sending push notification
				SendPush.new(booking, 2).send
			end
		end  if @users.present?

	end

	#---------------------------------------------------------------------------------------#
	#  This will get the bookings of users for which only one day before checkout.
	#---------------------------------------------------------------------------------------#
	def self.before_on_day_checkout
		@users = User.includes(:bookings)
						 .select("users.id, users.email, bookings.check_out, bookings.unit_id, bookings.unit_id")
						 .where("bookings.check_out IS NOT NULL")
						 .where("bookings.check_out IN(?)", Date.today)
						 .references(:bookings)

		@users.each do |user|
			user.bookings.each do |booking|
				## Notify client by sending push notification
				SendPush.new(booking, 10).send
			end
		end  if @users.present?

	end

end
