class AddTransactionalToDescription < ActiveRecord::Migration[5.0]
  def change
    drop_table :top_destinations, if_exists: true
    create_table :top_destinations do |t|
      t.references :city, foreign_key: true
      t.column :description_translations, 'jsonb', null: false

      t.timestamps
    end
  end
end
