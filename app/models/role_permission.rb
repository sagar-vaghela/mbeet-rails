class RolePermission < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  belongs_to :role
  serialize :permitted_actions, Hash

  # ------------------ v6 Api Methods ------------------------ #
  api_accessible :v6_role_permissions do |t|
  	t.add :permitted_actions
  end
end
