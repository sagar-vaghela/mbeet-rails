class Api::V4::FavoritesController < Api::V3::FavoritesController
  before_action :login_required, :check_favourites, :get_count #,:validate_limit_offset

	# GET: /v4/favourites
  def index
    @list = @favourites.user_likes.order(created_at: :desc)
                                  .limit(params[:limit] == -1 ? 100 : params[:limit])
                                  .offset(params[:offset])
    @list.includes(:unit => :city).map do |data|
      data.unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", data.unit.id).present? ? 1 : 0 
    end
  	render_data({ total: @count, favourites: @list.as_api_response(:v4_favourites_list) }, I18n.t('favourite.list'))
  end

  private
  def check_favourites
    @favourites = User.find_by("users.id = #{params[:user_id]}")

    render_data({}, I18n.t('no_data')) unless @favourites
  end

  def get_count
    @count = UserLike.where(user_id: params[:user_id]).size
  end
end
