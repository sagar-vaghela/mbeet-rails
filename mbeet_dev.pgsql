--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_tokens; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE access_tokens (
    id integer NOT NULL,
    user_id integer NOT NULL,
    token text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE access_tokens OWNER TO c240;

--
-- Name: access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE access_tokens_id_seq OWNER TO c240;

--
-- Name: access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE access_tokens_id_seq OWNED BY access_tokens.id;


--
-- Name: amenities; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE amenities (
    id integer NOT NULL,
    name_translations jsonb NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE amenities OWNER TO c240;

--
-- Name: amenities_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE amenities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE amenities_id_seq OWNER TO c240;

--
-- Name: amenities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE amenities_id_seq OWNED BY amenities.id;


--
-- Name: amenities_units; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE amenities_units (
    unit_id integer NOT NULL,
    amenity_id integer NOT NULL
);


ALTER TABLE amenities_units OWNER TO c240;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ar_internal_metadata OWNER TO c240;

--
-- Name: booked_subunits; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE booked_subunits (
    id integer NOT NULL,
    unit_id integer,
    day_booked timestamp without time zone,
    count integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE booked_subunits OWNER TO c240;

--
-- Name: booked_subunits_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE booked_subunits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE booked_subunits_id_seq OWNER TO c240;

--
-- Name: booked_subunits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE booked_subunits_id_seq OWNED BY booked_subunits.id;


--
-- Name: bookings; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE bookings (
    id integer NOT NULL,
    user_id integer NOT NULL,
    unit_id integer NOT NULL,
    check_in timestamp without time zone NOT NULL,
    check_out timestamp without time zone NOT NULL,
    payment_method integer DEFAULT 1 NOT NULL,
    payment_status integer DEFAULT 1 NOT NULL,
    lease_status integer DEFAULT 2 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    guests integer DEFAULT 0 NOT NULL,
    for_days integer DEFAULT 0 NOT NULL,
    sub_total_amount numeric(10,2) DEFAULT '0'::numeric NOT NULL,
    total_amount numeric(10,2) DEFAULT '0'::numeric NOT NULL,
    booking_cancelled boolean DEFAULT false NOT NULL,
    mbeet_fees numeric(10,2),
    advance_payment numeric(10,2) DEFAULT 0.0,
    coupon_id integer
);


ALTER TABLE bookings OWNER TO c240;

--
-- Name: bookings_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE bookings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bookings_id_seq OWNER TO c240;

--
-- Name: bookings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE bookings_id_seq OWNED BY bookings.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE cities (
    id integer NOT NULL,
    name_translations jsonb NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published boolean DEFAULT false,
    soft_delete boolean DEFAULT false,
    most_searched integer DEFAULT 0
);


ALTER TABLE cities OWNER TO c240;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cities_id_seq OWNER TO c240;

--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE cities_id_seq OWNED BY cities.id;


--
-- Name: coupons; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE coupons (
    id integer NOT NULL,
    creator_id integer NOT NULL,
    value integer NOT NULL,
    starting_date timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone NOT NULL,
    status integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    code text
);


ALTER TABLE coupons OWNER TO c240;

--
-- Name: coupons_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE coupons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupons_id_seq OWNER TO c240;

--
-- Name: coupons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE coupons_id_seq OWNED BY coupons.id;


--
-- Name: feedbacks; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE feedbacks (
    id integer NOT NULL,
    user_id integer NOT NULL,
    unit_id integer NOT NULL,
    star numeric(3,1) DEFAULT 0.0 NOT NULL,
    feedback text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    booking_id integer NOT NULL
);


ALTER TABLE feedbacks OWNER TO c240;

--
-- Name: feedbacks_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE feedbacks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feedbacks_id_seq OWNER TO c240;

--
-- Name: feedbacks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE feedbacks_id_seq OWNED BY feedbacks.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE notifications (
    id integer NOT NULL,
    user_id integer NOT NULL,
    booking_id integer,
    body jsonb NOT NULL,
    push_type integer NOT NULL,
    read boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE notifications OWNER TO c240;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notifications_id_seq OWNER TO c240;

--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: payment_txns; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE payment_txns (
    id integer NOT NULL,
    booking_id integer NOT NULL,
    payment_status integer DEFAULT 0 NOT NULL,
    invoice_unique_id text NOT NULL,
    modified_on timestamp without time zone NOT NULL,
    transaction_id text
);


ALTER TABLE payment_txns OWNER TO c240;

--
-- Name: payment_txns_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE payment_txns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_txns_id_seq OWNER TO c240;

--
-- Name: payment_txns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE payment_txns_id_seq OWNED BY payment_txns.id;


--
-- Name: periods; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE periods (
    id integer NOT NULL,
    unit_id integer,
    price double precision,
    period_date date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE periods OWNER TO c240;

--
-- Name: periods_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE periods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE periods_id_seq OWNER TO c240;

--
-- Name: periods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE periods_id_seq OWNED BY periods.id;


--
-- Name: pictures; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE pictures (
    id integer NOT NULL,
    name character varying NOT NULL,
    imageable_type character varying NOT NULL,
    imageable_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE pictures OWNER TO c240;

--
-- Name: pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pictures_id_seq OWNER TO c240;

--
-- Name: pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE pictures_id_seq OWNED BY pictures.id;


--
-- Name: reasons; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE reasons (
    id integer NOT NULL,
    title_translations jsonb NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE reasons OWNER TO c240;

--
-- Name: reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reasons_id_seq OWNER TO c240;

--
-- Name: reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE reasons_id_seq OWNED BY reasons.id;


--
-- Name: request_to_list_units; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE request_to_list_units (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    phone character varying NOT NULL,
    message text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE request_to_list_units OWNER TO c240;

--
-- Name: request_to_list_units_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE request_to_list_units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE request_to_list_units_id_seq OWNER TO c240;

--
-- Name: request_to_list_units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE request_to_list_units_id_seq OWNED BY request_to_list_units.id;


--
-- Name: role_permissions; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE role_permissions (
    id integer NOT NULL,
    permitted_actions text,
    role_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE role_permissions OWNER TO c240;

--
-- Name: role_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE role_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_permissions_id_seq OWNER TO c240;

--
-- Name: role_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE role_permissions_id_seq OWNED BY role_permissions.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE roles OWNER TO c240;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO c240;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: rules; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE rules (
    id integer NOT NULL,
    name_translations jsonb NOT NULL,
    status boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE rules OWNER TO c240;

--
-- Name: rules_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE rules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rules_id_seq OWNER TO c240;

--
-- Name: rules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE rules_id_seq OWNED BY rules.id;


--
-- Name: rules_units; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE rules_units (
    unit_id integer NOT NULL,
    rule_id integer NOT NULL
);


ALTER TABLE rules_units OWNER TO c240;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO c240;

--
-- Name: trending_cities; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE trending_cities (
    id integer NOT NULL,
    city_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE trending_cities OWNER TO c240;

--
-- Name: trending_cities_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE trending_cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE trending_cities_id_seq OWNER TO c240;

--
-- Name: trending_cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE trending_cities_id_seq OWNED BY trending_cities.id;


--
-- Name: unit_disabled_dates; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE unit_disabled_dates (
    id integer NOT NULL,
    unit_id integer,
    day_disabled timestamp without time zone,
    number_of_disabled_units integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE unit_disabled_dates OWNER TO c240;

--
-- Name: unit_disabled_dates_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE unit_disabled_dates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE unit_disabled_dates_id_seq OWNER TO c240;

--
-- Name: unit_disabled_dates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE unit_disabled_dates_id_seq OWNED BY unit_disabled_dates.id;


--
-- Name: units; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE units (
    id integer NOT NULL,
    user_id integer NOT NULL,
    title_translations jsonb NOT NULL,
    body_translations jsonb NOT NULL,
    number_of_guests smallint DEFAULT 0 NOT NULL,
    number_of_rooms smallint DEFAULT 1 NOT NULL,
    number_of_beds smallint DEFAULT 0 NOT NULL,
    number_of_baths smallint DEFAULT 0 NOT NULL,
    available_as smallint DEFAULT 1 NOT NULL,
    address text NOT NULL,
    price numeric(10,2) DEFAULT '0'::numeric NOT NULL,
    service_charge numeric(6,2) DEFAULT '0'::numeric NOT NULL,
    latitude numeric(20,8),
    longitude numeric(20,8),
    unit_class character varying DEFAULT 'a'::character varying NOT NULL,
    soft_delete boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    room_type integer DEFAULT 1 NOT NULL,
    city_id integer DEFAULT 1 NOT NULL,
    unit_type smallint DEFAULT 1 NOT NULL,
    total_area numeric(20,3) DEFAULT 0 NOT NULL,
    unit_enabled boolean DEFAULT false,
    unit_status boolean DEFAULT false,
    mbeet_percentage integer DEFAULT 10,
    bed_type integer,
    number_of_subunits integer DEFAULT 0,
    message text
);


ALTER TABLE units OWNER TO c240;

--
-- Name: units_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_id_seq OWNER TO c240;

--
-- Name: units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE units_id_seq OWNED BY units.id;


--
-- Name: user_booking_cancel_requests; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE user_booking_cancel_requests (
    id integer NOT NULL,
    user_id integer NOT NULL,
    reason_id integer NOT NULL,
    message text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    booking_id integer NOT NULL
);


ALTER TABLE user_booking_cancel_requests OWNER TO c240;

--
-- Name: user_booking_cancel_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE user_booking_cancel_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_booking_cancel_requests_id_seq OWNER TO c240;

--
-- Name: user_booking_cancel_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE user_booking_cancel_requests_id_seq OWNED BY user_booking_cancel_requests.id;


--
-- Name: user_lang_settings; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE user_lang_settings (
    id integer NOT NULL,
    user_id integer NOT NULL,
    language character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE user_lang_settings OWNER TO c240;

--
-- Name: user_lang_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE user_lang_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_lang_settings_id_seq OWNER TO c240;

--
-- Name: user_lang_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE user_lang_settings_id_seq OWNED BY user_lang_settings.id;


--
-- Name: user_likes; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE user_likes (
    id integer NOT NULL,
    user_id integer NOT NULL,
    unit_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE user_likes OWNER TO c240;

--
-- Name: user_likes_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE user_likes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_likes_id_seq OWNER TO c240;

--
-- Name: user_likes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE user_likes_id_seq OWNED BY user_likes.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: c240
--

CREATE TABLE users (
    id integer NOT NULL,
    role_id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    phone character varying,
    is_owner boolean DEFAULT false NOT NULL,
    facebook_uid character varying,
    twitter_uid character varying,
    gmail_uid character varying,
    password_digest character varying,
    reset_digest character varying,
    reset_sent_on timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    gender integer,
    date_of_birth date,
    verify_email_sent_at timestamp without time zone,
    email_verified boolean DEFAULT false NOT NULL,
    verify_phone_sent_at timestamp without time zone,
    phone_verified boolean DEFAULT false NOT NULL,
    confirm_email_token character varying,
    country_code character varying,
    soft_delete boolean DEFAULT false,
    otp integer
);


ALTER TABLE users OWNER TO c240;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: c240
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO c240;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: c240
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: access_tokens id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY access_tokens ALTER COLUMN id SET DEFAULT nextval('access_tokens_id_seq'::regclass);


--
-- Name: amenities id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY amenities ALTER COLUMN id SET DEFAULT nextval('amenities_id_seq'::regclass);


--
-- Name: booked_subunits id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY booked_subunits ALTER COLUMN id SET DEFAULT nextval('booked_subunits_id_seq'::regclass);


--
-- Name: bookings id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY bookings ALTER COLUMN id SET DEFAULT nextval('bookings_id_seq'::regclass);


--
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY cities ALTER COLUMN id SET DEFAULT nextval('cities_id_seq'::regclass);


--
-- Name: coupons id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY coupons ALTER COLUMN id SET DEFAULT nextval('coupons_id_seq'::regclass);


--
-- Name: feedbacks id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY feedbacks ALTER COLUMN id SET DEFAULT nextval('feedbacks_id_seq'::regclass);


--
-- Name: notifications id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: payment_txns id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY payment_txns ALTER COLUMN id SET DEFAULT nextval('payment_txns_id_seq'::regclass);


--
-- Name: periods id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY periods ALTER COLUMN id SET DEFAULT nextval('periods_id_seq'::regclass);


--
-- Name: pictures id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY pictures ALTER COLUMN id SET DEFAULT nextval('pictures_id_seq'::regclass);


--
-- Name: reasons id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY reasons ALTER COLUMN id SET DEFAULT nextval('reasons_id_seq'::regclass);


--
-- Name: request_to_list_units id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY request_to_list_units ALTER COLUMN id SET DEFAULT nextval('request_to_list_units_id_seq'::regclass);


--
-- Name: role_permissions id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY role_permissions ALTER COLUMN id SET DEFAULT nextval('role_permissions_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: rules id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY rules ALTER COLUMN id SET DEFAULT nextval('rules_id_seq'::regclass);


--
-- Name: trending_cities id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY trending_cities ALTER COLUMN id SET DEFAULT nextval('trending_cities_id_seq'::regclass);


--
-- Name: unit_disabled_dates id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY unit_disabled_dates ALTER COLUMN id SET DEFAULT nextval('unit_disabled_dates_id_seq'::regclass);


--
-- Name: units id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY units ALTER COLUMN id SET DEFAULT nextval('units_id_seq'::regclass);


--
-- Name: user_booking_cancel_requests id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_booking_cancel_requests ALTER COLUMN id SET DEFAULT nextval('user_booking_cancel_requests_id_seq'::regclass);


--
-- Name: user_lang_settings id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_lang_settings ALTER COLUMN id SET DEFAULT nextval('user_lang_settings_id_seq'::regclass);


--
-- Name: user_likes id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_likes ALTER COLUMN id SET DEFAULT nextval('user_likes_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: c240
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: access_tokens; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY access_tokens (id, user_id, token, created_at, updated_at) FROM stdin;
\.


--
-- Name: access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('access_tokens_id_seq', 1, false);


--
-- Data for Name: amenities; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY amenities (id, name_translations, is_active, created_at, updated_at) FROM stdin;
1	"test"	t	2017-12-12 11:29:53.814399	2017-12-12 11:29:53.814399
2	"test1"	t	2017-12-12 11:30:00.303639	2017-12-12 11:30:00.303639
3	"test2"	t	2017-12-12 11:30:04.135509	2017-12-12 11:30:04.135509
4	{"ar": "واي فاي", "en": "wi-fi"}	t	2017-12-19 10:44:34.685946	2017-12-19 10:44:34.685946
\.


--
-- Name: amenities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('amenities_id_seq', 4, true);


--
-- Data for Name: amenities_units; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY amenities_units (unit_id, amenity_id) FROM stdin;
22	1
23	2
23	1
24	2
24	1
\.


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2017-12-11 07:28:03.241048	2017-12-11 07:28:03.241048
\.


--
-- Data for Name: booked_subunits; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY booked_subunits (id, unit_id, day_booked, count, created_at, updated_at) FROM stdin;
1	9	2017-12-01 07:00:00	3	2017-12-16 05:14:30.880922	2017-12-16 05:14:30.880922
2	9	2017-12-02 07:00:00	3	2017-12-16 05:14:30.949085	2017-12-16 05:14:30.949085
3	9	2017-12-03 07:00:00	3	2017-12-16 05:14:30.957272	2017-12-16 05:14:30.957272
4	9	2017-12-04 07:00:00	3	2017-12-16 05:14:30.96463	2017-12-16 05:14:30.96463
5	9	2017-12-05 07:00:00	3	2017-12-16 05:14:30.971109	2017-12-16 05:14:30.971109
6	9	2017-12-06 07:00:00	3	2017-12-16 05:14:30.976417	2017-12-16 05:14:30.976417
7	9	2017-12-07 07:00:00	3	2017-12-16 05:14:30.981295	2017-12-16 05:14:30.981295
8	18	2017-12-01 07:00:00	1	2017-12-16 05:17:56.706454	2017-12-16 05:17:56.706454
9	18	2017-12-02 07:00:00	1	2017-12-16 05:17:56.713023	2017-12-16 05:17:56.713023
10	18	2017-12-03 07:00:00	1	2017-12-16 05:17:56.719032	2017-12-16 05:17:56.719032
11	18	2017-12-04 07:00:00	1	2017-12-16 05:17:56.724732	2017-12-16 05:17:56.724732
12	18	2017-12-05 07:00:00	1	2017-12-16 05:17:56.731429	2017-12-16 05:17:56.731429
13	18	2017-12-06 07:00:00	1	2017-12-16 05:17:56.743993	2017-12-16 05:17:56.743993
14	18	2017-12-07 07:00:00	1	2017-12-16 05:17:56.750931	2017-12-16 05:17:56.750931
15	9	2018-02-01 00:00:00	26	2018-01-22 09:39:33.03351	2018-01-22 09:39:33.03351
16	9	2018-02-02 00:00:00	26	2018-01-22 09:39:33.110099	2018-01-22 09:39:33.110099
17	9	2018-02-03 00:00:00	26	2018-01-22 09:39:33.116069	2018-01-22 09:39:33.116069
18	9	2018-02-04 00:00:00	26	2018-01-22 09:39:33.122712	2018-01-22 09:39:33.122712
19	9	2018-02-05 00:00:00	26	2018-01-22 09:39:33.129016	2018-01-22 09:39:33.129016
20	9	2018-02-06 00:00:00	26	2018-01-22 09:39:33.147982	2018-01-22 09:39:33.147982
21	9	2018-02-07 00:00:00	26	2018-01-22 09:39:33.154242	2018-01-22 09:39:33.154242
22	9	2018-02-08 00:00:00	26	2018-01-22 09:39:33.160942	2018-01-22 09:39:33.160942
23	9	2018-02-09 00:00:00	26	2018-01-22 09:39:33.168043	2018-01-22 09:39:33.168043
24	9	2018-02-15 12:00:00	0	2018-02-14 10:50:40.264061	2018-02-14 10:50:40.264061
25	9	2018-02-16 12:00:00	0	2018-02-14 10:50:40.486477	2018-02-14 10:50:40.486477
26	9	2018-02-17 12:00:00	0	2018-02-14 10:50:40.499213	2018-02-14 10:50:40.499213
27	9	2018-02-18 12:00:00	0	2018-02-14 10:50:40.510242	2018-02-14 10:50:40.510242
28	9	2018-02-19 12:00:00	0	2018-02-14 10:50:40.521604	2018-02-14 10:50:40.521604
29	9	2018-02-20 12:00:00	0	2018-02-14 10:50:40.529136	2018-02-14 10:50:40.529136
30	9	2018-02-21 12:00:00	0	2018-02-14 10:50:40.537436	2018-02-14 10:50:40.537436
\.


--
-- Name: booked_subunits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('booked_subunits_id_seq', 30, true);


--
-- Data for Name: bookings; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY bookings (id, user_id, unit_id, check_in, check_out, payment_method, payment_status, lease_status, created_at, updated_at, guests, for_days, sub_total_amount, total_amount, booking_cancelled, mbeet_fees, advance_payment, coupon_id) FROM stdin;
90	3	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-20 12:14:28.789118	2018-02-22 05:26:30.050771	2	10	200.00	1000.00	t	100.00	200.00	\N
91	3	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-20 12:16:40.452853	2018-02-22 05:26:30.360067	2	10	200.00	1000.00	t	100.00	200.00	\N
92	3	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-20 12:17:35.72619	2018-02-22 05:26:30.672306	2	10	200.00	1000.00	t	100.00	200.00	\N
85	3	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	3	2	2018-02-01 10:34:05.68397	2018-02-14 06:16:02.279466	2	10	200.00	1000.00	t	100.00	0.00	\N
86	7	9	2018-02-15 12:00:00	2018-02-22 12:00:00	1	2	2	2018-02-14 10:50:39.12558	2018-02-20 12:16:21.520009	1	7	3194.24	3194.24	t	319.42	319.42	\N
87	7	9	2018-02-15 12:00:00	2018-02-22 12:00:00	1	2	2	2018-02-14 11:21:34.652046	2018-02-20 12:16:22.284286	1	7	3194.24	3194.24	t	319.42	319.42	\N
88	3	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-20 12:09:07.876863	2018-02-22 05:26:28.793041	2	10	200.00	1000.00	t	100.00	200.00	\N
89	3	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-20 12:12:47.075027	2018-02-22 05:26:29.741586	2	10	200.00	1000.00	t	100.00	200.00	\N
93	9	9	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-21 06:18:53.768846	2018-02-23 06:04:42.455517	2	10	200.00	1000.00	t	100.00	200.00	\N
94	4	22	2018-02-01 00:00:00	2018-02-10 00:00:00	1	2	2	2018-02-23 06:05:24.245604	2018-02-27 09:44:24.551404	2	10	200.00	1000.00	t	100.00	200.00	\N
\.


--
-- Name: bookings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('bookings_id_seq', 94, true);


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY cities (id, name_translations, created_at, updated_at, published, soft_delete, most_searched) FROM stdin;
1	{"ar": "surat", "en": "surat"}	2017-12-12 11:19:23.896407	2018-02-28 09:14:14.896248	t	f	0
2	{"ar": "demo ar", "en": "demo"}	2017-12-12 11:19:27.768975	2018-02-28 09:14:29.030949	t	f	2
3	{"ar": "tyefg", "en": "tresyt"}	2017-12-12 11:19:30.940782	2018-02-28 09:14:44.679607	t	f	0
4	{"ar": "dsgfvdgd", "en": "test"}	2017-12-12 11:19:34.460124	2018-02-28 09:15:09.562396	t	f	7
5	{"ar": "das", "en": "ads"}	2018-03-07 05:28:53.237173	2018-03-07 05:28:53.237173	t	f	0
\.


--
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('cities_id_seq', 5, true);


--
-- Data for Name: coupons; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY coupons (id, creator_id, value, starting_date, expiration_date, status, created_at, updated_at, code) FROM stdin;
14	3	200	2017-12-22 00:00:00	2017-12-24 00:00:00	0	2017-12-26 04:41:29.885025	2017-12-26 07:19:28.783149	mbeet1
5	3	12	2017-12-01 00:00:00	2018-01-01 00:00:00	0	2017-12-13 07:33:00.865974	2017-12-26 07:20:31.684115	mbeet2
7	4	32	2017-12-22 00:00:00	2017-12-24 00:00:00	0	2017-12-22 11:21:06.576551	2017-12-26 07:20:42.737856	mbeet3
4	3	12	2017-12-01 00:00:00	2018-01-01 00:00:00	1	2017-12-13 07:32:23.498727	2017-12-26 07:20:58.136164	mbeet4
17	3	20	2017-12-22 00:00:00	2017-12-24 00:00:00	0	2017-12-26 06:34:48.333691	2017-12-26 10:34:26.468856	mbeet123
16	3	10	2017-12-22 00:00:00	2017-12-24 00:00:00	0	2017-12-26 06:34:16.43122	2017-12-26 11:13:09.408856	mbeet123
\.


--
-- Name: coupons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('coupons_id_seq', 17, true);


--
-- Data for Name: feedbacks; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY feedbacks (id, user_id, unit_id, star, feedback, created_at, updated_at, booking_id) FROM stdin;
\.


--
-- Name: feedbacks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('feedbacks_id_seq', 1, false);


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY notifications (id, user_id, booking_id, body, push_type, read, created_at, updated_at) FROM stdin;
29	3	85	{"id": 85, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 85, "created_at": "2018-02-01T10:34:05.683Z", "modified_on": "2017-12-01T07:00:00.000Z", "invoice_unique_id": "12057368269411"}, "push_type": 3}	3	f	2018-02-01 10:34:08.258643	2018-02-01 10:34:08.258643
30	3	85	{"id": 85, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["You have only two days left for your check in.", "لم يتبق أمامك سوى يومين."]}, "sound": "default"}, "my_orders": {"id": 85, "created_at": "2018-02-01T10:34:05.683Z", "modified_on": "2017-12-01T07:00:00.000Z", "invoice_unique_id": "12057368269411"}, "push_type": 1}	1	f	2018-02-01 11:07:47.276449	2018-02-01 11:07:47.276449
31	7	86	{"id": 86, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 86, "created_at": "2018-02-14T10:50:39.125Z", "modified_on": "2018-02-15T12:00:00.000Z", "invoice_unique_id": "58547951343733"}, "push_type": 3}	3	f	2018-02-20 12:16:22.623189	2018-02-20 12:16:22.623189
32	7	87	{"id": 87, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 87, "created_at": "2018-02-14T11:21:34.652Z", "modified_on": "2018-02-15T12:00:00.000Z", "invoice_unique_id": "21137700986178"}, "push_type": 3}	3	f	2018-02-20 12:16:22.963194	2018-02-20 12:16:22.963194
33	3	88	{"id": 88, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 88, "created_at": "2018-02-20T12:09:07.876Z", "modified_on": "2017-12-01T07:00:00.000Z", "invoice_unique_id": "48759553527750"}, "push_type": 3}	3	f	2018-02-20 12:16:23.251662	2018-02-20 12:16:23.251662
34	3	89	{"id": 89, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 89, "created_at": "2018-02-20T12:12:47.075Z", "modified_on": "2017-12-01T07:00:00.000Z", "invoice_unique_id": "172164342489199"}, "push_type": 3}	3	f	2018-02-20 12:16:23.618574	2018-02-20 12:16:23.618574
35	3	93	{"id": 93, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 93, "created_at": "2018-02-21T06:18:53.768Z", "modified_on": "2017-12-01T07:00:00.000Z", "invoice_unique_id": "97904548213661"}, "push_type": 3}	3	f	2018-02-22 05:26:31.22046	2018-02-22 05:26:31.22046
36	3	\N	{"aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["New unit is published successfully", "تم  نشر شقة جديده"]}, "sound": "default"}, "flat": {"id": 9, "title": "", "created_at": "2017-12-12T11:39:42.403Z", "updated_at": "2017-12-16T05:14:10.371Z"}, "user_id": 3, "push_type": 8}	8	f	2018-02-22 05:40:45.489949	2018-02-22 05:40:45.489949
37	3	\N	{"aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["New unit is published successfully", "تم  نشر شقة جديده"]}, "sound": "default"}, "flat": {"id": 9, "title": "", "created_at": "2017-12-12T11:39:42.403Z", "updated_at": "2017-12-16T05:14:10.371Z"}, "user_id": 3, "push_type": 8}	8	f	2018-02-22 05:41:05.944679	2018-02-22 05:41:05.944679
38	3	\N	{"aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["New unit is published successfully", "تم  نشر شقة جديده"]}, "sound": "default"}, "flat": {"id": 9, "title": "", "created_at": "2017-12-12T11:39:42.403Z", "updated_at": "2017-12-16T05:14:10.371Z"}, "user_id": 3, "push_type": 8}	8	f	2018-02-22 05:41:26.402774	2018-02-22 05:41:26.402774
39	3	94	{"id": 94, "aps": {"alert": {"loc-key": "REQUEST_FORMAT", "loc-args": ["Order placed successfully.", "تم وضع الطلب بنجاح."]}, "sound": "default"}, "my_orders": {"id": 94, "created_at": "2018-02-23T06:05:24.245Z", "modified_on": "2017-12-01T07:00:00.000Z", "invoice_unique_id": "42170806188302"}, "push_type": 3}	3	f	2018-02-26 10:08:04.158151	2018-02-26 10:08:04.158151
\.


--
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('notifications_id_seq', 39, true);


--
-- Data for Name: payment_txns; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY payment_txns (id, booking_id, payment_status, invoice_unique_id, modified_on, transaction_id) FROM stdin;
72	85	0	12057368269411	2017-12-01 07:00:00	\N
73	86	0	58547951343733	2018-02-15 12:00:00	\N
74	87	0	21137700986178	2018-02-15 12:00:00	\N
75	88	0	48759553527750	2017-12-01 07:00:00	\N
76	89	0	172164342489199	2017-12-01 07:00:00	\N
77	90	0	129958266644161	2017-12-01 07:00:00	\N
78	91	0	16154681007060	2017-12-01 07:00:00	\N
79	92	0	129003207979497	2017-12-01 07:00:00	\N
80	93	0	97904548213661	2017-12-01 07:00:00	\N
81	94	0	42170806188302	2017-12-01 07:00:00	\N
\.


--
-- Name: payment_txns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('payment_txns_id_seq', 81, true);


--
-- Data for Name: periods; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY periods (id, unit_id, price, period_date, created_at, updated_at) FROM stdin;
1323	22	12	2018-02-26	2018-02-26 05:37:29.355171	2018-02-26 05:37:29.355171
1324	22	12	2018-02-27	2018-02-26 05:37:29.383873	2018-02-26 05:37:29.383873
1325	22	12	2018-02-28	2018-02-26 05:37:29.420793	2018-02-26 05:37:29.420793
1326	19	20	0001-01-04 BC	2018-03-01 09:28:56.453344	2018-03-01 09:28:56.453344
1327	19	20	0001-01-11 BC	2018-03-01 09:28:56.557245	2018-03-01 09:28:56.557245
1328	19	20	0001-01-18 BC	2018-03-01 09:28:56.578832	2018-03-01 09:28:56.578832
1329	19	20	0001-01-25 BC	2018-03-01 09:28:56.592388	2018-03-01 09:28:56.592388
1330	19	20	0001-02-01 BC	2018-03-01 09:28:56.604672	2018-03-01 09:28:56.604672
1331	19	20	0001-02-08 BC	2018-03-01 09:28:56.623449	2018-03-01 09:28:56.623449
1332	19	20	0001-02-15 BC	2018-03-01 09:28:56.636513	2018-03-01 09:28:56.636513
1333	19	20	0001-02-22 BC	2018-03-01 09:28:56.650875	2018-03-01 09:28:56.650875
1334	19	20	0001-02-29 BC	2018-03-01 09:28:56.665894	2018-03-01 09:28:56.665894
1335	19	20	0001-03-07 BC	2018-03-01 09:28:56.679237	2018-03-01 09:28:56.679237
1336	19	20	0001-03-14 BC	2018-03-01 09:28:56.692769	2018-03-01 09:28:56.692769
1337	19	20	0001-03-21 BC	2018-03-01 09:28:56.70527	2018-03-01 09:28:56.70527
1338	19	20	0001-03-28 BC	2018-03-01 09:28:56.719831	2018-03-01 09:28:56.719831
1339	19	20	0001-04-04 BC	2018-03-01 09:28:56.733373	2018-03-01 09:28:56.733373
1340	19	20	0001-04-11 BC	2018-03-01 09:28:56.748215	2018-03-01 09:28:56.748215
1341	19	20	0001-04-18 BC	2018-03-01 09:28:56.765788	2018-03-01 09:28:56.765788
1342	19	20	0001-04-25 BC	2018-03-01 09:28:56.781017	2018-03-01 09:28:56.781017
1343	19	20	0001-05-02 BC	2018-03-01 09:28:56.795454	2018-03-01 09:28:56.795454
1344	19	20	0001-05-09 BC	2018-03-01 09:28:56.810558	2018-03-01 09:28:56.810558
1345	19	20	0001-05-16 BC	2018-03-01 09:28:56.824822	2018-03-01 09:28:56.824822
1346	19	20	0001-05-23 BC	2018-03-01 09:28:56.882274	2018-03-01 09:28:56.882274
1347	19	20	0001-05-30 BC	2018-03-01 09:28:56.901262	2018-03-01 09:28:56.901262
1348	19	20	0001-06-06 BC	2018-03-01 09:28:56.919137	2018-03-01 09:28:56.919137
1349	19	20	0001-06-13 BC	2018-03-01 09:28:56.936868	2018-03-01 09:28:56.936868
1350	19	20	0001-06-20 BC	2018-03-01 09:28:56.955787	2018-03-01 09:28:56.955787
1351	19	20	0001-06-27 BC	2018-03-01 09:28:56.973318	2018-03-01 09:28:56.973318
1352	19	20	0001-07-04 BC	2018-03-01 09:28:56.989819	2018-03-01 09:28:56.989819
1353	19	20	0001-07-11 BC	2018-03-01 09:28:57.276116	2018-03-01 09:28:57.276116
1354	19	20	0001-07-18 BC	2018-03-01 09:28:57.332361	2018-03-01 09:28:57.332361
1355	19	20	0001-07-25 BC	2018-03-01 09:28:57.352315	2018-03-01 09:28:57.352315
1356	19	20	0001-08-01 BC	2018-03-01 09:28:57.376602	2018-03-01 09:28:57.376602
1357	19	20	0001-08-08 BC	2018-03-01 09:28:57.404007	2018-03-01 09:28:57.404007
1358	19	20	0001-08-15 BC	2018-03-01 09:28:57.421073	2018-03-01 09:28:57.421073
1359	19	20	0001-08-22 BC	2018-03-01 09:28:57.437887	2018-03-01 09:28:57.437887
1360	19	20	0001-08-29 BC	2018-03-01 09:28:57.470518	2018-03-01 09:28:57.470518
1361	19	20	0001-09-05 BC	2018-03-01 09:28:57.48413	2018-03-01 09:28:57.48413
1362	19	20	0001-09-12 BC	2018-03-01 09:28:57.496646	2018-03-01 09:28:57.496646
1363	19	20	0001-09-19 BC	2018-03-01 09:28:57.508388	2018-03-01 09:28:57.508388
1364	19	20	0001-09-26 BC	2018-03-01 09:28:57.51969	2018-03-01 09:28:57.51969
1365	19	20	0001-10-03 BC	2018-03-01 09:28:57.531914	2018-03-01 09:28:57.531914
1366	19	20	0001-10-10 BC	2018-03-01 09:28:57.543447	2018-03-01 09:28:57.543447
1367	19	20	0001-10-17 BC	2018-03-01 09:28:57.555004	2018-03-01 09:28:57.555004
1368	19	20	0001-10-24 BC	2018-03-01 09:28:57.568652	2018-03-01 09:28:57.568652
1369	19	20	0001-10-31 BC	2018-03-01 09:28:57.580941	2018-03-01 09:28:57.580941
1370	19	20	0001-11-07 BC	2018-03-01 09:28:57.592366	2018-03-01 09:28:57.592366
1371	19	20	0001-11-14 BC	2018-03-01 09:28:57.60335	2018-03-01 09:28:57.60335
1372	19	20	0001-11-21 BC	2018-03-01 09:28:57.615848	2018-03-01 09:28:57.615848
1373	19	20	0001-11-28 BC	2018-03-01 09:28:57.628647	2018-03-01 09:28:57.628647
1374	19	20	0001-12-05 BC	2018-03-01 09:28:57.640111	2018-03-01 09:28:57.640111
1375	19	20	0001-12-12 BC	2018-03-01 09:28:57.653262	2018-03-01 09:28:57.653262
1376	19	20	0001-12-19 BC	2018-03-01 09:28:57.667606	2018-03-01 09:28:57.667606
1377	19	20	0001-12-26 BC	2018-03-01 09:28:57.686481	2018-03-01 09:28:57.686481
1378	19	20	2018-03-01	2018-03-01 09:28:57.69945	2018-03-01 09:28:57.69945
1379	24	200	2019-01-04	2018-03-05 10:15:54.162459	2018-03-05 10:15:54.162459
1380	24	200	2019-01-05	2018-03-05 10:15:54.283989	2018-03-05 10:15:54.283989
1381	24	200	2019-01-06	2018-03-05 10:15:54.29834	2018-03-05 10:15:54.29834
1382	24	200	2019-01-07	2018-03-05 10:15:54.31155	2018-03-05 10:15:54.31155
1383	24	200	2019-01-11	2018-03-05 10:15:54.323479	2018-03-05 10:15:54.323479
1384	24	200	2019-01-12	2018-03-05 10:15:54.380253	2018-03-05 10:15:54.380253
1385	24	200	2019-01-13	2018-03-05 10:15:54.393828	2018-03-05 10:15:54.393828
1386	24	200	2019-01-14	2018-03-05 10:15:54.409565	2018-03-05 10:15:54.409565
1387	24	200	2019-01-18	2018-03-05 10:15:54.423638	2018-03-05 10:15:54.423638
1388	24	200	2019-01-19	2018-03-05 10:15:54.438392	2018-03-05 10:15:54.438392
1389	24	200	2019-01-20	2018-03-05 10:15:54.514065	2018-03-05 10:15:54.514065
1390	24	200	2019-01-21	2018-03-05 10:15:54.528299	2018-03-05 10:15:54.528299
1391	24	200	2019-01-25	2018-03-05 10:15:54.54682	2018-03-05 10:15:54.54682
1392	24	200	2019-01-26	2018-03-05 10:15:54.558943	2018-03-05 10:15:54.558943
1393	24	200	2019-01-27	2018-03-05 10:15:54.57393	2018-03-05 10:15:54.57393
1394	24	200	2019-01-28	2018-03-05 10:15:54.587244	2018-03-05 10:15:54.587244
1395	24	200	2019-02-01	2018-03-05 10:15:54.598944	2018-03-05 10:15:54.598944
1396	24	200	2019-02-02	2018-03-05 10:15:54.611208	2018-03-05 10:15:54.611208
1397	24	200	2019-02-03	2018-03-05 10:15:54.623344	2018-03-05 10:15:54.623344
1398	24	200	2019-02-04	2018-03-05 10:15:54.636266	2018-03-05 10:15:54.636266
1399	24	200	2019-02-08	2018-03-05 10:15:54.649794	2018-03-05 10:15:54.649794
1400	24	200	2019-02-09	2018-03-05 10:15:54.664699	2018-03-05 10:15:54.664699
1401	24	200	2019-02-10	2018-03-05 10:15:54.675788	2018-03-05 10:15:54.675788
1402	24	200	2019-02-11	2018-03-05 10:15:54.690363	2018-03-05 10:15:54.690363
1403	24	200	2019-02-15	2018-03-05 10:15:54.701818	2018-03-05 10:15:54.701818
1404	24	200	2019-02-16	2018-03-05 10:15:54.715109	2018-03-05 10:15:54.715109
1405	24	200	2019-02-17	2018-03-05 10:15:54.726562	2018-03-05 10:15:54.726562
1406	24	200	2019-02-18	2018-03-05 10:15:54.740312	2018-03-05 10:15:54.740312
1407	24	200	2019-02-22	2018-03-05 10:15:54.753882	2018-03-05 10:15:54.753882
1408	24	200	2019-02-23	2018-03-05 10:15:54.765716	2018-03-05 10:15:54.765716
1409	24	200	2019-02-24	2018-03-05 10:15:54.78173	2018-03-05 10:15:54.78173
1410	24	200	2019-02-25	2018-03-05 10:15:54.7941	2018-03-05 10:15:54.7941
1411	24	200	2019-03-01	2018-03-05 10:15:54.808142	2018-03-05 10:15:54.808142
1412	24	200	2019-03-02	2018-03-05 10:15:54.821113	2018-03-05 10:15:54.821113
1413	24	200	2019-03-03	2018-03-05 10:15:54.834349	2018-03-05 10:15:54.834349
1414	24	200	2019-03-04	2018-03-05 10:15:54.850398	2018-03-05 10:15:54.850398
1415	24	200	2019-03-08	2018-03-05 10:15:54.872925	2018-03-05 10:15:54.872925
1416	24	200	2019-03-09	2018-03-05 10:15:54.89164	2018-03-05 10:15:54.89164
1417	24	200	2019-03-10	2018-03-05 10:15:54.923799	2018-03-05 10:15:54.923799
1418	24	200	2019-03-11	2018-03-05 10:15:54.939725	2018-03-05 10:15:54.939725
1419	24	200	2019-03-15	2018-03-05 10:15:54.966134	2018-03-05 10:15:54.966134
1420	24	200	2019-03-16	2018-03-05 10:15:54.982453	2018-03-05 10:15:54.982453
1421	24	200	2019-03-17	2018-03-05 10:15:55.001279	2018-03-05 10:15:55.001279
1422	24	200	2019-03-18	2018-03-05 10:15:55.033034	2018-03-05 10:15:55.033034
1423	24	200	2019-03-22	2018-03-05 10:15:55.051027	2018-03-05 10:15:55.051027
1424	24	200	2019-03-23	2018-03-05 10:15:55.125416	2018-03-05 10:15:55.125416
1425	24	200	2019-03-24	2018-03-05 10:15:55.147419	2018-03-05 10:15:55.147419
1426	24	200	2019-03-25	2018-03-05 10:15:55.165024	2018-03-05 10:15:55.165024
1427	24	200	2019-03-29	2018-03-05 10:15:55.178101	2018-03-05 10:15:55.178101
1428	24	200	2019-03-30	2018-03-05 10:15:55.190692	2018-03-05 10:15:55.190692
1429	24	200	2019-03-31	2018-03-05 10:15:55.204154	2018-03-05 10:15:55.204154
1430	24	200	2019-04-01	2018-03-05 10:15:55.21647	2018-03-05 10:15:55.21647
1431	24	200	2019-04-05	2018-03-05 10:15:55.232312	2018-03-05 10:15:55.232312
1432	24	200	2019-04-06	2018-03-05 10:15:55.247207	2018-03-05 10:15:55.247207
1433	24	200	2019-04-07	2018-03-05 10:15:55.261184	2018-03-05 10:15:55.261184
1434	24	200	2019-04-08	2018-03-05 10:15:55.273559	2018-03-05 10:15:55.273559
1435	24	200	2019-04-12	2018-03-05 10:15:55.29489	2018-03-05 10:15:55.29489
1436	24	200	2019-04-13	2018-03-05 10:15:55.307946	2018-03-05 10:15:55.307946
1437	24	200	2019-04-14	2018-03-05 10:15:55.319167	2018-03-05 10:15:55.319167
1438	24	200	2019-04-15	2018-03-05 10:15:55.332255	2018-03-05 10:15:55.332255
1439	24	200	2019-04-19	2018-03-05 10:15:55.348746	2018-03-05 10:15:55.348746
1440	24	200	2019-04-20	2018-03-05 10:15:55.363889	2018-03-05 10:15:55.363889
1441	24	200	2019-04-21	2018-03-05 10:15:55.374476	2018-03-05 10:15:55.374476
1442	24	200	2019-04-22	2018-03-05 10:15:55.390564	2018-03-05 10:15:55.390564
1443	24	200	2019-04-26	2018-03-05 10:15:55.40739	2018-03-05 10:15:55.40739
1444	24	200	2019-04-27	2018-03-05 10:15:55.463174	2018-03-05 10:15:55.463174
1445	24	200	2019-04-28	2018-03-05 10:15:55.474521	2018-03-05 10:15:55.474521
1446	24	200	2019-04-29	2018-03-05 10:15:55.489863	2018-03-05 10:15:55.489863
1447	24	200	2019-05-03	2018-03-05 10:15:55.501378	2018-03-05 10:15:55.501378
1448	24	200	2019-05-04	2018-03-05 10:15:55.515003	2018-03-05 10:15:55.515003
1449	24	200	2019-05-05	2018-03-05 10:15:55.526128	2018-03-05 10:15:55.526128
1450	24	200	2019-05-06	2018-03-05 10:15:55.540232	2018-03-05 10:15:55.540232
1451	24	200	2019-05-10	2018-03-05 10:15:55.552077	2018-03-05 10:15:55.552077
1452	24	200	2019-05-11	2018-03-05 10:15:55.567238	2018-03-05 10:15:55.567238
1453	24	200	2019-05-12	2018-03-05 10:15:55.581834	2018-03-05 10:15:55.581834
1454	24	200	2019-05-13	2018-03-05 10:15:55.592299	2018-03-05 10:15:55.592299
1455	24	200	2019-05-17	2018-03-05 10:15:55.606696	2018-03-05 10:15:55.606696
1456	24	200	2019-05-18	2018-03-05 10:15:55.618875	2018-03-05 10:15:55.618875
1457	24	200	2019-05-19	2018-03-05 10:15:55.632535	2018-03-05 10:15:55.632535
1458	24	200	2019-05-20	2018-03-05 10:15:55.643012	2018-03-05 10:15:55.643012
1459	24	200	2019-05-24	2018-03-05 10:15:55.658689	2018-03-05 10:15:55.658689
1460	24	200	2019-05-25	2018-03-05 10:15:55.723855	2018-03-05 10:15:55.723855
1461	24	200	2019-05-26	2018-03-05 10:15:55.735962	2018-03-05 10:15:55.735962
1462	24	200	2019-05-27	2018-03-05 10:15:55.749006	2018-03-05 10:15:55.749006
1463	24	200	2019-05-31	2018-03-05 10:15:55.76078	2018-03-05 10:15:55.76078
1464	24	200	2019-06-01	2018-03-05 10:15:55.773274	2018-03-05 10:15:55.773274
1465	24	200	2019-06-02	2018-03-05 10:15:55.784307	2018-03-05 10:15:55.784307
1466	24	200	2019-06-03	2018-03-05 10:15:55.799611	2018-03-05 10:15:55.799611
1467	24	200	2019-06-07	2018-03-05 10:15:55.811969	2018-03-05 10:15:55.811969
1468	24	200	2019-06-08	2018-03-05 10:15:55.824704	2018-03-05 10:15:55.824704
1469	24	200	2019-06-09	2018-03-05 10:15:55.839629	2018-03-05 10:15:55.839629
1470	24	200	2019-06-10	2018-03-05 10:15:55.851091	2018-03-05 10:15:55.851091
1471	24	200	2019-06-14	2018-03-05 10:15:55.866196	2018-03-05 10:15:55.866196
1472	24	200	2019-06-15	2018-03-05 10:15:55.876506	2018-03-05 10:15:55.876506
1473	24	200	2019-06-16	2018-03-05 10:15:55.890049	2018-03-05 10:15:55.890049
1474	24	200	2019-06-17	2018-03-05 10:15:55.901304	2018-03-05 10:15:55.901304
1475	24	200	2019-06-21	2018-03-05 10:15:55.91539	2018-03-05 10:15:55.91539
1476	24	200	2019-06-22	2018-03-05 10:15:55.925553	2018-03-05 10:15:55.925553
1477	24	200	2019-06-23	2018-03-05 10:15:55.939966	2018-03-05 10:15:55.939966
1478	24	200	2019-06-24	2018-03-05 10:15:55.951183	2018-03-05 10:15:55.951183
1479	24	200	2019-06-28	2018-03-05 10:15:55.966576	2018-03-05 10:15:55.966576
1480	24	200	2019-06-29	2018-03-05 10:15:55.976922	2018-03-05 10:15:55.976922
1481	24	200	2019-06-30	2018-03-05 10:15:55.990004	2018-03-05 10:15:55.990004
1482	24	200	2019-07-01	2018-03-05 10:15:56.001151	2018-03-05 10:15:56.001151
1483	24	200	2019-07-05	2018-03-05 10:15:56.015927	2018-03-05 10:15:56.015927
1484	24	200	2019-07-06	2018-03-05 10:15:56.026572	2018-03-05 10:15:56.026572
1485	24	200	2019-07-07	2018-03-05 10:15:56.040384	2018-03-05 10:15:56.040384
1486	24	200	2019-07-08	2018-03-05 10:15:56.054235	2018-03-05 10:15:56.054235
1487	24	200	2019-07-12	2018-03-05 10:15:56.067697	2018-03-05 10:15:56.067697
1488	24	200	2019-07-13	2018-03-05 10:15:56.082784	2018-03-05 10:15:56.082784
1489	24	200	2019-07-14	2018-03-05 10:15:56.094916	2018-03-05 10:15:56.094916
1490	24	200	2019-07-15	2018-03-05 10:15:56.108176	2018-03-05 10:15:56.108176
1491	24	200	2019-07-19	2018-03-05 10:15:56.119873	2018-03-05 10:15:56.119873
1492	24	200	2019-07-20	2018-03-05 10:15:56.140538	2018-03-05 10:15:56.140538
1493	24	200	2019-07-21	2018-03-05 10:15:56.156727	2018-03-05 10:15:56.156727
1494	24	200	2019-07-22	2018-03-05 10:15:56.170337	2018-03-05 10:15:56.170337
1495	24	200	2019-07-26	2018-03-05 10:15:56.182689	2018-03-05 10:15:56.182689
1496	24	200	2019-07-27	2018-03-05 10:15:56.195077	2018-03-05 10:15:56.195077
1497	24	200	2019-07-28	2018-03-05 10:15:56.249214	2018-03-05 10:15:56.249214
1498	24	200	2019-07-29	2018-03-05 10:15:56.272076	2018-03-05 10:15:56.272076
1499	24	200	2019-08-02	2018-03-05 10:15:56.282812	2018-03-05 10:15:56.282812
1500	24	200	2019-08-03	2018-03-05 10:15:56.294452	2018-03-05 10:15:56.294452
1501	24	200	2019-08-04	2018-03-05 10:15:56.307523	2018-03-05 10:15:56.307523
1502	24	200	2019-08-05	2018-03-05 10:15:56.31829	2018-03-05 10:15:56.31829
1503	24	200	2019-08-09	2018-03-05 10:15:56.33148	2018-03-05 10:15:56.33148
1504	24	200	2019-08-10	2018-03-05 10:15:56.343217	2018-03-05 10:15:56.343217
1505	24	200	2019-08-11	2018-03-05 10:15:56.356971	2018-03-05 10:15:56.356971
1506	24	200	2019-08-12	2018-03-05 10:15:56.369411	2018-03-05 10:15:56.369411
1507	24	200	2019-08-16	2018-03-05 10:15:56.381946	2018-03-05 10:15:56.381946
1508	24	200	2019-08-17	2018-03-05 10:15:56.395775	2018-03-05 10:15:56.395775
1509	24	200	2019-08-18	2018-03-05 10:15:56.407346	2018-03-05 10:15:56.407346
1510	24	200	2019-08-19	2018-03-05 10:15:56.421955	2018-03-05 10:15:56.421955
1511	24	200	2019-08-23	2018-03-05 10:15:56.436621	2018-03-05 10:15:56.436621
1512	24	200	2019-08-24	2018-03-05 10:15:56.449746	2018-03-05 10:15:56.449746
1513	24	200	2019-08-25	2018-03-05 10:15:56.467446	2018-03-05 10:15:56.467446
1514	24	200	2019-08-26	2018-03-05 10:15:56.480821	2018-03-05 10:15:56.480821
1515	24	200	2019-08-30	2018-03-05 10:15:56.493212	2018-03-05 10:15:56.493212
1516	24	200	2019-08-31	2018-03-05 10:15:56.506791	2018-03-05 10:15:56.506791
1517	24	200	2019-09-01	2018-03-05 10:15:56.520988	2018-03-05 10:15:56.520988
1518	24	200	2019-09-02	2018-03-05 10:15:56.532986	2018-03-05 10:15:56.532986
1519	24	200	2019-09-06	2018-03-05 10:15:56.543686	2018-03-05 10:15:56.543686
1520	24	200	2019-09-07	2018-03-05 10:15:56.556465	2018-03-05 10:15:56.556465
1521	24	200	2019-09-08	2018-03-05 10:15:56.56648	2018-03-05 10:15:56.56648
1522	24	200	2019-09-09	2018-03-05 10:15:56.581794	2018-03-05 10:15:56.581794
1523	24	200	2019-09-13	2018-03-05 10:15:56.59249	2018-03-05 10:15:56.59249
1524	24	200	2019-09-14	2018-03-05 10:15:56.608059	2018-03-05 10:15:56.608059
1525	24	200	2019-09-15	2018-03-05 10:15:56.620577	2018-03-05 10:15:56.620577
1526	24	200	2019-09-16	2018-03-05 10:15:56.632067	2018-03-05 10:15:56.632067
1527	24	200	2019-09-20	2018-03-05 10:15:56.642705	2018-03-05 10:15:56.642705
1528	24	200	2019-09-21	2018-03-05 10:15:56.656636	2018-03-05 10:15:56.656636
1529	24	200	2019-09-22	2018-03-05 10:15:56.668555	2018-03-05 10:15:56.668555
1530	24	200	2019-09-23	2018-03-05 10:15:56.681963	2018-03-05 10:15:56.681963
1531	24	200	2019-09-27	2018-03-05 10:15:56.692176	2018-03-05 10:15:56.692176
1532	24	200	2019-09-28	2018-03-05 10:15:56.706155	2018-03-05 10:15:56.706155
1533	24	200	2019-09-29	2018-03-05 10:15:56.717586	2018-03-05 10:15:56.717586
1534	24	200	2019-09-30	2018-03-05 10:15:56.732407	2018-03-05 10:15:56.732407
1535	24	200	2019-10-04	2018-03-05 10:15:56.748427	2018-03-05 10:15:56.748427
1536	24	200	2019-10-05	2018-03-05 10:15:56.75883	2018-03-05 10:15:56.75883
1537	24	200	2019-10-06	2018-03-05 10:15:56.775029	2018-03-05 10:15:56.775029
1538	24	200	2019-10-07	2018-03-05 10:15:56.785543	2018-03-05 10:15:56.785543
1539	24	200	2019-10-11	2018-03-05 10:15:56.798338	2018-03-05 10:15:56.798338
1540	24	200	2019-10-12	2018-03-05 10:15:56.808904	2018-03-05 10:15:56.808904
1541	24	200	2019-10-13	2018-03-05 10:15:56.823981	2018-03-05 10:15:56.823981
1542	24	200	2019-10-14	2018-03-05 10:15:56.835606	2018-03-05 10:15:56.835606
1543	24	200	2019-10-18	2018-03-05 10:15:56.848567	2018-03-05 10:15:56.848567
1544	24	200	2019-10-19	2018-03-05 10:15:56.858034	2018-03-05 10:15:56.858034
1545	24	200	2019-10-20	2018-03-05 10:15:56.873481	2018-03-05 10:15:56.873481
1546	24	200	2019-10-21	2018-03-05 10:15:56.885198	2018-03-05 10:15:56.885198
1547	24	200	2019-10-25	2018-03-05 10:15:56.899508	2018-03-05 10:15:56.899508
1548	24	200	2019-10-26	2018-03-05 10:15:56.909383	2018-03-05 10:15:56.909383
1549	24	200	2019-10-27	2018-03-05 10:15:56.922673	2018-03-05 10:15:56.922673
1550	24	200	2019-10-28	2018-03-05 10:15:56.935858	2018-03-05 10:15:56.935858
1551	24	200	2019-11-01	2018-03-05 10:15:56.949222	2018-03-05 10:15:56.949222
1552	24	200	2019-11-02	2018-03-05 10:15:56.96111	2018-03-05 10:15:56.96111
1553	24	200	2019-11-03	2018-03-05 10:15:56.977977	2018-03-05 10:15:56.977977
1554	24	200	2019-11-04	2018-03-05 10:15:56.991539	2018-03-05 10:15:56.991539
1555	24	200	2019-11-08	2018-03-05 10:15:57.005829	2018-03-05 10:15:57.005829
1556	24	200	2019-11-09	2018-03-05 10:15:57.018452	2018-03-05 10:15:57.018452
1557	24	200	2019-11-10	2018-03-05 10:15:57.032245	2018-03-05 10:15:57.032245
1558	24	200	2019-11-11	2018-03-05 10:15:57.042902	2018-03-05 10:15:57.042902
1559	24	200	2019-11-15	2018-03-05 10:15:57.056675	2018-03-05 10:15:57.056675
1560	24	200	2019-11-16	2018-03-05 10:15:57.075757	2018-03-05 10:15:57.075757
1561	24	200	2019-11-17	2018-03-05 10:15:57.088338	2018-03-05 10:15:57.088338
1562	24	200	2019-11-18	2018-03-05 10:15:57.098579	2018-03-05 10:15:57.098579
1563	24	200	2019-11-22	2018-03-05 10:15:57.109415	2018-03-05 10:15:57.109415
1564	24	200	2019-11-23	2018-03-05 10:15:57.123193	2018-03-05 10:15:57.123193
1565	24	200	2019-11-24	2018-03-05 10:15:57.13384	2018-03-05 10:15:57.13384
1566	24	200	2019-11-25	2018-03-05 10:15:57.148879	2018-03-05 10:15:57.148879
1567	24	200	2019-11-29	2018-03-05 10:15:57.159606	2018-03-05 10:15:57.159606
1568	24	200	2019-11-30	2018-03-05 10:15:57.177366	2018-03-05 10:15:57.177366
1569	24	200	2019-12-01	2018-03-05 10:15:57.188154	2018-03-05 10:15:57.188154
1570	24	200	2019-12-02	2018-03-05 10:15:57.199806	2018-03-05 10:15:57.199806
1571	24	200	2019-12-06	2018-03-05 10:15:57.216096	2018-03-05 10:15:57.216096
1572	24	200	2019-12-07	2018-03-05 10:15:57.236156	2018-03-05 10:15:57.236156
1573	24	200	2019-12-08	2018-03-05 10:15:57.249742	2018-03-05 10:15:57.249742
1574	24	200	2019-12-09	2018-03-05 10:15:57.26174	2018-03-05 10:15:57.26174
1575	24	200	2019-12-13	2018-03-05 10:15:57.276008	2018-03-05 10:15:57.276008
1576	24	200	2019-12-14	2018-03-05 10:15:57.331011	2018-03-05 10:15:57.331011
1577	24	200	2019-12-15	2018-03-05 10:15:57.344547	2018-03-05 10:15:57.344547
1578	24	200	2019-12-16	2018-03-05 10:15:57.358852	2018-03-05 10:15:57.358852
1579	24	200	2019-12-20	2018-03-05 10:15:57.374821	2018-03-05 10:15:57.374821
1580	24	200	2019-12-21	2018-03-05 10:15:57.391358	2018-03-05 10:15:57.391358
1581	24	200	2019-12-22	2018-03-05 10:15:57.405144	2018-03-05 10:15:57.405144
1582	24	200	2019-12-23	2018-03-05 10:15:57.42522	2018-03-05 10:15:57.42522
1583	24	200	2019-12-27	2018-03-05 10:15:57.437744	2018-03-05 10:15:57.437744
1584	24	200	2019-12-28	2018-03-05 10:15:57.451654	2018-03-05 10:15:57.451654
1585	24	200	2019-12-29	2018-03-05 10:15:57.466623	2018-03-05 10:15:57.466623
1586	24	200	2019-12-30	2018-03-05 10:15:57.479202	2018-03-05 10:15:57.479202
\.


--
-- Name: periods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('periods_id_seq', 1586, true);


--
-- Data for Name: pictures; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY pictures (id, name, imageable_type, imageable_id, created_at, updated_at) FROM stdin;
1	v1519722904/mbeet/dev/plwpjyb5q8xc2dyavjd6.jpg	Unit	23	2018-02-27 09:15:05.07412	2018-02-27 09:15:05.07412
2	v1519723200/mbeet/dev/noiylfmwqgyjsvhwqkfg.jpg	Unit	24	2018-02-27 09:20:01.139672	2018-02-27 09:20:01.139672
3	v1519809254/mbeet/dev/frxi3cfwbwp4ke4xhqn5.jpg	City	1	2018-02-28 09:14:14.899521	2018-02-28 09:14:14.899521
4	v1519809268/mbeet/dev/n6xlviyvblbi8zvvpuyg.jpg	City	2	2018-02-28 09:14:29.033016	2018-02-28 09:14:29.033016
5	v1519809284/mbeet/dev/lkz31v33pclnblpppjxj.jpg	City	3	2018-02-28 09:14:44.681676	2018-02-28 09:14:44.681676
6	v1519809309/mbeet/dev/be3tcnuxki7895vvcztc.jpg	City	4	2018-02-28 09:15:09.564694	2018-02-28 09:15:09.564694
7		User	9	2018-03-05 04:16:28.825098	2018-03-05 04:16:28.825098
8	v1520400532/mbeet/dev/cwezrdfk42u31ok5uqbf.jpg	City	5	2018-03-07 05:28:53.315393	2018-03-07 05:28:53.315393
\.


--
-- Name: pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('pictures_id_seq', 8, true);


--
-- Data for Name: reasons; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY reasons (id, title_translations, created_at, updated_at) FROM stdin;
\.


--
-- Name: reasons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('reasons_id_seq', 1, false);


--
-- Data for Name: request_to_list_units; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY request_to_list_units (id, user_id, name, email, phone, message, created_at, updated_at) FROM stdin;
\.


--
-- Name: request_to_list_units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('request_to_list_units_id_seq', 1, false);


--
-- Data for Name: role_permissions; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY role_permissions (id, permitted_actions, role_id, created_at, updated_at) FROM stdin;
10	---\n:dashboard_permissions:\n  :view: true\n:users_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n  :show: false\n  :restore: false\n:booking_permissions:\n  :view: false\n  :create: true\n  :edit: true\n  :delete: true\n  :cancel: false\n  :show: false\n:unit_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :show: false\n  :trash: false\n  :period: false\n  :restore: false\n  :disableSubUnit: false\n  :enableSubUnit: false\n:special_price_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:coupon_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:city_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:role_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :show: false\n:top_destination_permissions:\n  :view: false\n  :update: false\n	2	2018-02-28 08:46:01.487495	2018-03-03 04:20:07.981695
8	---\n:dashboard_permissions:\n  :view: true\n:users_permissions:\n  :view: true\n  :create: false\n  :edit: false\n  :delete: false\n:booking_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n  :cancel: false\n  :show: false\n:unit_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :show: true\n  :trash: false\n  :period: false\n  :restore: false\n  :disableSubUnit: true\n  :enableSubUnit: false\n:special_price_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:coupon_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: true\n:city_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:role_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :show: false\n:top_destination_permissions:\n  :view: false\n  :update: false\n	36	2018-02-27 08:42:39.806399	2018-02-28 07:23:39.185107
9	---\n:dashboard_permissions:\n  :view: true\n:users_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n  :show: true\n  :restore: true\n:booking_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n  :cancel: true\n  :show: true\n:unit_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :show: true\n  :trash: true\n  :period: true\n  :restore: true\n  :disableSubUnit: true\n  :enableSubUnit: true\n:special_price_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n:coupon_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n:city_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n:role_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :show: true\n:top_destination_permissions:\n  :view: true\n  :update: true\n	1	2018-02-28 08:45:56.335288	2018-03-07 05:55:30.896325
17	---\n:dashboard_permissions:\n  :view: true\n:users_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n  :show: false\n  :restore: false\n:booking_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n  :cancel: false\n  :show: false\n:unit_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :show: false\n  :trash: false\n  :period: false\n  :restore: false\n  :disableSubUnit: false\n  :enableSubUnit: false\n:special_price_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:coupon_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:city_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :delete: false\n:role_permissions:\n  :view: false\n  :create: false\n  :edit: false\n  :show: false\n:top_destination_permissions:\n  :view: false\n  :update: false\n	53	2018-03-05 12:03:46.275472	2018-03-05 12:03:46.275472
18	---\n:dashboard_permissions:\n  :view: true\n:users_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n  :show: true\n  :restore: true\n:booking_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n  :cancel: true\n  :show: true\n:unit_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :show: true\n  :trash: true\n  :period: true\n  :restore: true\n  :disableSubUnit: true\n  :enableSubUnit: true\n:special_price_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n:coupon_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n:city_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :delete: true\n:role_permissions:\n  :view: true\n  :create: true\n  :edit: true\n  :show: true\n:top_destination_permissions:\n  :view: true\n  :update: true\n	54	2018-03-05 12:37:16.085182	2018-03-05 12:37:16.085182
\.


--
-- Name: role_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('role_permissions_id_seq', 18, true);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY roles (id, name, created_at, updated_at) FROM stdin;
1	admin	2017-12-12 10:17:20.706291	2017-12-12 10:17:20.706291
2	owner	2017-12-12 10:17:25.055148	2017-12-12 10:17:25.055148
3	normal_user	2017-12-12 10:17:31.960084	2017-12-12 10:17:31.960084
36	Demosagar	2018-02-27 08:42:39.76553	2018-02-28 05:42:06.321832
53	fegrdh	2018-03-05 12:03:46.264121	2018-03-05 12:03:46.264121
54	demo test	2018-03-05 12:37:15.985378	2018-03-05 12:37:15.985378
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('roles_id_seq', 54, true);


--
-- Data for Name: rules; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY rules (id, name_translations, status, created_at, updated_at) FROM stdin;
1	"Rule1"	t	2017-12-12 11:30:41.480234	2017-12-12 11:30:41.480234
2	"Rule2"	t	2017-12-12 11:30:45.927518	2017-12-12 11:30:45.927518
\.


--
-- Name: rules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('rules_id_seq', 2, true);


--
-- Data for Name: rules_units; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY rules_units (unit_id, rule_id) FROM stdin;
22	1
23	1
24	1
23	2
24	2
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY schema_migrations (version) FROM stdin;
20170214131154
20170214131803
20170214132133
20170214132702
20170214132932
20170214133212
20170214133341
20170214133438
20170214133528
20170214133639
20170214133829
20170214133915
20170214133958
20170215101037
20170215102918
20170215113950
20170217122131
20170301065707
20170303043237
20170303055048
20170303055050
20170303065148
20170303084012
20170303103516
20170306111347
20170316084700
20170418093531
20170419102307
20170421123929
20170421132712
20170501041605
20170502053015
20170503113542
20170504085517
20170529115109
20170531080350
20170602123628
20170606052835
20170606122042
20170615104128
20170616043055
20170616065600
20170712091828
20170811122018
20170816055530
20170817071510
20170822063713
20170824065712
20170828054848
20170901055109
20171004064613
20171127072711
20171202115159
20171206080716
20171212120025
20171226043314
20171226061538
20171226063259
20171226110129
20180223114333
20180224051327
20180303060347
\.


--
-- Data for Name: trending_cities; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY trending_cities (id, city_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: trending_cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('trending_cities_id_seq', 1, false);


--
-- Data for Name: unit_disabled_dates; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY unit_disabled_dates (id, unit_id, day_disabled, number_of_disabled_units, created_at, updated_at) FROM stdin;
82	9	2017-12-29 12:00:00	1	2017-12-27 07:17:12.160107	2017-12-27 07:17:12.160107
83	9	2017-12-30 12:00:00	1	2017-12-27 07:17:12.165413	2017-12-27 07:17:12.165413
84	9	2017-12-31 12:00:00	1	2017-12-27 07:17:12.171053	2017-12-27 07:17:12.171053
\.


--
-- Name: unit_disabled_dates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('unit_disabled_dates_id_seq', 87, true);


--
-- Data for Name: units; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY units (id, user_id, title_translations, body_translations, number_of_guests, number_of_rooms, number_of_beds, number_of_baths, available_as, address, price, service_charge, latitude, longitude, unit_class, soft_delete, created_at, updated_at, room_type, city_id, unit_type, total_area, unit_enabled, unit_status, mbeet_percentage, bed_type, number_of_subunits, message) FROM stdin;
18	4	{"ar": "آسيا آسيا آسيا آسيا آسيا", "en": "4bhk home available"}	{"ar": "آسيا آسيا آسيا آسيا آسيا", "en": "Lorem ipsum Lorem ipsum Lorem ipsum"}	5	2	2	2	1	50775 Skye Mews, Luigibury, Micronesia	6357.06	19.24	-2.75210822	-136.20998045	a	f	2017-12-16 05:17:10.88959	2017-12-26 08:29:15.188149	1	1	1	0.000	t	f	10	\N	2	\N
9	3	"TestingUnit"	"Test"	2	10	5	3	1	sector 91 golf view	456.32	8.53	45.00000000	56.36521456	a	f	2017-12-12 11:39:42.40377	2018-02-23 04:40:19.956323	1	1	1	0.000	t	f	10	\N	3	\N
19	7	{"ar": "آسيا آسيا آسيا آسيا آسيا", "en": "4bhk home available"}	{"ar": "آسيا آسيا آسيا آسيا آسيا", "en": "Lorem ipsum Lorem ipsum Lorem ipsum"}	5	2	2	2	3	7261 Jarred Ranch, Moseport, Slovenia	9074.32	81.83	-87.86205826	-143.66633540	a	f	2017-12-19 10:44:07.2994	2018-02-23 04:48:54.99192	1	1	1	0.000	f	f	10	\N	0	\N
23	3	{"ar": "demo", "en": "demo"}	{"ar": "demo", "en": "demo"}	1	3	3	3	2		100.00	0.00	24.71355170	46.67529570	b	f	2018-02-27 09:15:04.988087	2018-02-27 09:15:04.988087	1	1	2	222.000	f	f	10	1	3	\N
22	3	{"ar": "jkh", "en": "cdfsgdfasdasdd"}	{"ar": "jk", "en": "jkh"}	2	2	2	2	2	Title change	123.00	0.00	24.71355170	46.67529570	a	f	2018-02-20 09:48:34.280193	2018-03-05 10:07:31.591138	1	1	2	32134.000	f	f	10	1	0	\N
24	3	{"ar": "dfggvd", "en": "dewf"}	{"ar": "gdfgdfdasdsada", "en": "gdfgd"}	1	1	2	2	4	set 3 mon	231.00	0.00	24.71355170	46.67529570	a	f	2018-02-27 09:20:01.137067	2018-03-05 11:09:02.707972	1	4	3	213.000	f	f	10	1	3	dec addasadfsafdsfsdf265914589458
\.


--
-- Name: units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('units_id_seq', 24, true);


--
-- Data for Name: user_booking_cancel_requests; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY user_booking_cancel_requests (id, user_id, reason_id, message, created_at, updated_at, booking_id) FROM stdin;
\.


--
-- Name: user_booking_cancel_requests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('user_booking_cancel_requests_id_seq', 1, false);


--
-- Data for Name: user_lang_settings; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY user_lang_settings (id, user_id, language, created_at, updated_at) FROM stdin;
\.


--
-- Name: user_lang_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('user_lang_settings_id_seq', 1, false);


--
-- Data for Name: user_likes; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY user_likes (id, user_id, unit_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: user_likes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('user_likes_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: c240
--

COPY users (id, role_id, name, email, phone, is_owner, facebook_uid, twitter_uid, gmail_uid, password_digest, reset_digest, reset_sent_on, created_at, updated_at, gender, date_of_birth, verify_email_sent_at, email_verified, verify_phone_sent_at, phone_verified, confirm_email_token, country_code, soft_delete, otp) FROM stdin;
5	3	mobily_test	mobily_test@clickapps.co	00918289067322	f	\N	\N	\N	$2a$10$.IjFkKP3HTIB25y2It3sf.HBa73uVpUmpES.JMtKBsHU5dERO4Vtm	\N	\N	2017-12-13 09:53:12.557458	2017-12-13 09:53:12.557458	\N	\N	\N	f	\N	f	\N	\N	f	\N
7	1	admin1	admin@example.com	00918289067	f	\N	\N	\N	$2a$10$I7JuGWagSyKjABSehYOcXOdmXZAfpGJz7ybHpu0Zlq/tEumSOkaNO	\N	\N	2017-12-13 10:03:22.724442	2017-12-13 10:03:22.724442	\N	\N	\N	f	\N	f	\N	\N	f	\N
3	2	amit abc	akl@narola.email	\N	t	\N	\N	\N	$2a$10$TRmflmYkz3iUMWZCbNLfbeJRZPzX3XBuE.dmFFhT.CQEtgzDDOC0i	\N	\N	2017-12-12 10:20:12.846991	2018-02-20 12:12:40.169984	\N	\N	\N	t	\N	t	\N	\N	f	\N
9	1	sagar vaghela	sav@narola.email	9725790019	f	\N	\N	\N	$2a$10$yBVlFpaONPpsQzVmcYvGGefeKsWi.tk2OL2ImQDZO8KEzMqPI7XYi	8c49ef475fad491099d0564590d05d4312194304	2018-02-02 08:31:07.268387	2018-02-02 08:30:19.8673	2018-03-05 04:17:55.614399	\N	1970-01-01	\N	f	\N	f	\N	+376	f	\N
11	54	dhruv narola	dhm@narola.email	9725790020	f	\N	\N	\N	$2a$10$AuhLe4OekCbyQTatifT/A.JlSBfDo/Cmz7QBYcnxXm9WBmyePgSqK	\N	\N	2018-03-06 05:42:45.79487	2018-03-06 05:42:46.906988	1	2018-03-21	\N	t	\N	t	\N	+374	f	\N
4	36	john blue	john@mailinator.com	0000000024	f	\N	\N	\N	$2a$10$s2MquG22BC/g10/qCqneEODMYW3JCNbiiRtZ84TdPMuhKBFuaPDVW	\N	\N	2017-12-12 10:30:34.874759	2018-03-06 05:42:51.195339	\N	\N	\N	t	\N	t	\N	\N	f	\N
12	53	sdasddsad	dem@narola.email	978654646	f	\N	\N	\N	$2a$10$vvxOxvfcKLtr8Lfw8stdD.Y2vWHYHsw0fcsG8MpY4cEC9Wihga7Oy	\N	\N	2018-03-06 05:54:50.100375	2018-03-06 05:54:50.271358	\N	2018-03-20	\N	f	\N	f	\N	+244	f	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: c240
--

SELECT pg_catalog.setval('users_id_seq', 12, true);


--
-- Name: access_tokens access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY access_tokens
    ADD CONSTRAINT access_tokens_pkey PRIMARY KEY (id);


--
-- Name: amenities amenities_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY amenities
    ADD CONSTRAINT amenities_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: booked_subunits booked_subunits_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY booked_subunits
    ADD CONSTRAINT booked_subunits_pkey PRIMARY KEY (id);


--
-- Name: bookings bookings_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT bookings_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: coupons coupons_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY coupons
    ADD CONSTRAINT coupons_pkey PRIMARY KEY (id);


--
-- Name: feedbacks feedbacks_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT feedbacks_pkey PRIMARY KEY (id);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: payment_txns payment_txns_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY payment_txns
    ADD CONSTRAINT payment_txns_pkey PRIMARY KEY (id);


--
-- Name: periods periods_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY periods
    ADD CONSTRAINT periods_pkey PRIMARY KEY (id);


--
-- Name: pictures pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- Name: reasons reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY reasons
    ADD CONSTRAINT reasons_pkey PRIMARY KEY (id);


--
-- Name: request_to_list_units request_to_list_units_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY request_to_list_units
    ADD CONSTRAINT request_to_list_units_pkey PRIMARY KEY (id);


--
-- Name: role_permissions role_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT role_permissions_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: rules rules_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY rules
    ADD CONSTRAINT rules_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: trending_cities trending_cities_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY trending_cities
    ADD CONSTRAINT trending_cities_pkey PRIMARY KEY (id);


--
-- Name: unit_disabled_dates unit_disabled_dates_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY unit_disabled_dates
    ADD CONSTRAINT unit_disabled_dates_pkey PRIMARY KEY (id);


--
-- Name: units units_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: user_booking_cancel_requests user_booking_cancel_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_booking_cancel_requests
    ADD CONSTRAINT user_booking_cancel_requests_pkey PRIMARY KEY (id);


--
-- Name: user_lang_settings user_lang_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_lang_settings
    ADD CONSTRAINT user_lang_settings_pkey PRIMARY KEY (id);


--
-- Name: user_likes user_likes_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_likes
    ADD CONSTRAINT user_likes_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_access_tokens_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_access_tokens_on_user_id ON access_tokens USING btree (user_id);


--
-- Name: index_amenities_units_on_amenity_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_amenities_units_on_amenity_id ON amenities_units USING btree (amenity_id);


--
-- Name: index_amenities_units_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_amenities_units_on_unit_id ON amenities_units USING btree (unit_id);


--
-- Name: index_booked_subunits_on_day_booked_and_count; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_booked_subunits_on_day_booked_and_count ON booked_subunits USING btree (day_booked, count);


--
-- Name: index_booked_subunits_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_booked_subunits_on_unit_id ON booked_subunits USING btree (unit_id);


--
-- Name: index_bookings_on_coupon_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_bookings_on_coupon_id ON bookings USING btree (coupon_id);


--
-- Name: index_bookings_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_bookings_on_unit_id ON bookings USING btree (unit_id);


--
-- Name: index_bookings_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_bookings_on_user_id ON bookings USING btree (user_id);


--
-- Name: index_coupons_on_creator_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_coupons_on_creator_id ON coupons USING btree (creator_id);


--
-- Name: index_feedbacks_on_booking_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_feedbacks_on_booking_id ON feedbacks USING btree (booking_id);


--
-- Name: index_feedbacks_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_feedbacks_on_unit_id ON feedbacks USING btree (unit_id);


--
-- Name: index_feedbacks_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_feedbacks_on_user_id ON feedbacks USING btree (user_id);


--
-- Name: index_notifications_on_booking_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_notifications_on_booking_id ON notifications USING btree (booking_id);


--
-- Name: index_notifications_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_notifications_on_user_id ON notifications USING btree (user_id);


--
-- Name: index_payment_txns_on_booking_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_payment_txns_on_booking_id ON payment_txns USING btree (booking_id);


--
-- Name: index_pictures_on_imageable_type_and_imageable_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_pictures_on_imageable_type_and_imageable_id ON pictures USING btree (imageable_type, imageable_id);


--
-- Name: index_request_to_list_units_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_request_to_list_units_on_user_id ON request_to_list_units USING btree (user_id);


--
-- Name: index_role_permissions_on_role_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_role_permissions_on_role_id ON role_permissions USING btree (role_id);


--
-- Name: index_rules_units_on_rule_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_rules_units_on_rule_id ON rules_units USING btree (rule_id);


--
-- Name: index_rules_units_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_rules_units_on_unit_id ON rules_units USING btree (unit_id);


--
-- Name: index_unit_disabled_dates_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_unit_disabled_dates_on_unit_id ON unit_disabled_dates USING btree (unit_id);


--
-- Name: index_units_on_city_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_city_id ON units USING btree (city_id);


--
-- Name: index_units_on_number_of_baths; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_number_of_baths ON units USING btree (number_of_baths);


--
-- Name: index_units_on_number_of_beds; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_number_of_beds ON units USING btree (number_of_beds);


--
-- Name: index_units_on_number_of_guests; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_number_of_guests ON units USING btree (number_of_guests);


--
-- Name: index_units_on_number_of_rooms; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_number_of_rooms ON units USING btree (number_of_rooms);


--
-- Name: index_units_on_price; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_price ON units USING btree (price);


--
-- Name: index_units_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_units_on_user_id ON units USING btree (user_id);


--
-- Name: index_user_booking_cancel_requests_on_booking_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_user_booking_cancel_requests_on_booking_id ON user_booking_cancel_requests USING btree (booking_id);


--
-- Name: index_user_booking_cancel_requests_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_user_booking_cancel_requests_on_user_id ON user_booking_cancel_requests USING btree (user_id);


--
-- Name: index_user_lang_settings_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_user_lang_settings_on_user_id ON user_lang_settings USING btree (user_id);


--
-- Name: index_user_likes_on_unit_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_user_likes_on_unit_id ON user_likes USING btree (unit_id);


--
-- Name: index_user_likes_on_user_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_user_likes_on_user_id ON user_likes USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: c240
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_role_id; Type: INDEX; Schema: public; Owner: c240
--

CREATE INDEX index_users_on_role_id ON users USING btree (role_id);


--
-- Name: payment_txns fk_rails_00ec3bd36c; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY payment_txns
    ADD CONSTRAINT fk_rails_00ec3bd36c FOREIGN KEY (booking_id) REFERENCES bookings(id);


--
-- Name: feedbacks fk_rails_08259dafa6; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT fk_rails_08259dafa6 FOREIGN KEY (booking_id) REFERENCES bookings(id);


--
-- Name: feedbacks fk_rails_0a355a3bc8; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT fk_rails_0a355a3bc8 FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: user_booking_cancel_requests fk_rails_0b5d98e389; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_booking_cancel_requests
    ADD CONSTRAINT fk_rails_0b5d98e389 FOREIGN KEY (reason_id) REFERENCES reasons(id) ON DELETE CASCADE;


--
-- Name: units fk_rails_11b7bf6ab1; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY units
    ADD CONSTRAINT fk_rails_11b7bf6ab1 FOREIGN KEY (city_id) REFERENCES cities(id);


--
-- Name: booked_subunits fk_rails_11ccda113b; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY booked_subunits
    ADD CONSTRAINT fk_rails_11ccda113b FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: user_lang_settings fk_rails_5a1a387069; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_lang_settings
    ADD CONSTRAINT fk_rails_5a1a387069 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: units fk_rails_5b28abdd45; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY units
    ADD CONSTRAINT fk_rails_5b28abdd45 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: bookings fk_rails_5c56412d7d; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT fk_rails_5c56412d7d FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: role_permissions fk_rails_60126080bd; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT fk_rails_60126080bd FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: user_likes fk_rails_611368df04; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_likes
    ADD CONSTRAINT fk_rails_611368df04 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: users fk_rails_642f17018b; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_rails_642f17018b FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: user_likes fk_rails_7d9adf2c99; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_likes
    ADD CONSTRAINT fk_rails_7d9adf2c99 FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: access_tokens fk_rails_96fc070778; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY access_tokens
    ADD CONSTRAINT fk_rails_96fc070778 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: unit_disabled_dates fk_rails_9b5c6ab02d; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY unit_disabled_dates
    ADD CONSTRAINT fk_rails_9b5c6ab02d FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: notifications fk_rails_b080fb4855; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT fk_rails_b080fb4855 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: notifications fk_rails_c000af975d; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT fk_rails_c000af975d FOREIGN KEY (booking_id) REFERENCES bookings(id);


--
-- Name: feedbacks fk_rails_c57bb6cf28; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT fk_rails_c57bb6cf28 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: bookings fk_rails_c75bbbfb3f; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT fk_rails_c75bbbfb3f FOREIGN KEY (coupon_id) REFERENCES coupons(id);


--
-- Name: user_booking_cancel_requests fk_rails_dc84c3973f; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_booking_cancel_requests
    ADD CONSTRAINT fk_rails_dc84c3973f FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: request_to_list_units fk_rails_e182e073e8; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY request_to_list_units
    ADD CONSTRAINT fk_rails_e182e073e8 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: bookings fk_rails_ef0571f117; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT fk_rails_ef0571f117 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: user_booking_cancel_requests fk_rails_f6fb4abe5f; Type: FK CONSTRAINT; Schema: public; Owner: c240
--

ALTER TABLE ONLY user_booking_cancel_requests
    ADD CONSTRAINT fk_rails_f6fb4abe5f FOREIGN KEY (booking_id) REFERENCES bookings(id);


--
-- PostgreSQL database dump complete
--

