# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  role_id              :integer          not null
#  name                 :string           not null
#  email                :string           not null
#  phone                :string
#  is_owner             :boolean          default(FALSE), not null
#  facebook_uid         :string
#  twitter_uid          :string
#  gmail_uid            :string
#  password_digest      :string
#  reset_digest         :string
#  reset_sent_on        :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  gender               :integer
#  date_of_birth        :date
#  verify_email_sent_at :datetime
#  email_verified       :boolean          default(FALSE), not null
#  verify_phone_sent_at :datetime
#  phone_verified       :boolean          default(FALSE), not null
#  confirm_email_token  :string
#  country_code         :string
#  soft_delete          :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryBot.create(:admin_user) }
  let(:owner_user) { FactoryBot.create(:owner_user) }

  it 'has a valid factory of owner user' do
    expect(FactoryBot.create(:owner_user)).to be_valid
  end

  it "should be valid from factory" do

    expect(user).to be_valid

  end

  context 'association' do
    it { should belong_to(:role) }
    it { expect(user).to have_many :bookings }
    it { expect(user).to have_many :units }
    it { expect(user).to have_many :user_likes }
    it { expect(user).to have_many :feedbacks }
    it { expect(user).to have_many :access_tokens }
    it { expect(user).to have_one :user_lang_setting }
    it { expect(user).to have_many :request_to_list_units }
    it { expect(user).to have_many :user_booking_cancel_requests }
    it { expect(user).to have_many :notifications }
    it { expect(user).to have_many :coupons }
    it { expect(user).to have_one :picture }
  end

  describe "validations" do

    it { is_expected.to validate_presence_of(:name).strict }

  end
end
