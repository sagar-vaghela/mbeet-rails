module V4::Admin::UserResponder
	extend ActiveSupport::Concern

	included do
		# -------------------- Admin User API Response ------------------ #
		api_accessible :v4_admin_user_only do |t|
      t.add :id
			t.add lambda{|user| user.user_likes.count}, as: :unit_likes_count
			t.add lambda{|user| Unit.where(user_id: user.id).count }, as: :units_count
			t.add lambda{|user| user.bookings.count }, as: :orders_count
			t.add :bookings, as: :orders, :template => :v4_booking_list
	    t.add :role, :template => :for_admin_show_roles
	    t.add :name
	    t.add :email
			t.add :country_code
	    t.add :phone
	    t.add :email_verified
	    t.add :phone_verified
	    t.add :gender
	    t.add :date_of_birth
	    t.add :is_owner
	    t.add :facebook_uid
	    t.add :twitter_uid
	    t.add :gmail_uid
	    t.add :picture, as: "picture_attributes", :template => :admin_images
	    t.add :default_image
	    t.add :created_at
			t.add :soft_delete
	  end

		api_accessible :v4_user_with_booking do |t|
	    t.add :id
	    t.add :name
	    t.add :picture, as: "image", :template => :user_image
	  end

		api_accessible :v4_user_only do |t|
			t.add :id
	    t.add :name
	    t.add :email
			t.add :country_code
	    t.add :phone
	    t.add :type
	    t.add :email_verified
	    t.add :phone_verified
			t.add :picture, as: "image", :template => :admin_images
	  end

	end
end
