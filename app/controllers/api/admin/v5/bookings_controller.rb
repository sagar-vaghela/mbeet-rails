class Api::Admin::V5::BookingsController < Api::Admin::V4::BookingsController
  before_action :admin_login_required
  before_action :check_bookings, only: :index
  before_action :find_booking, only: [:show, :cancel]
  
  def dashbord_bookings
    if params[:date].present? && params[:type].to_i == 1
      date = Date.parse(params[:date])
      render json: {
        message: I18n.t('admin.booking_today'),
        pending:   Booking.day_booking(date, 1),
        cancelled: Booking.day_booking(date, 2),
        confirmed: Booking.day_booking(date, 3),
        completed: Booking.day_booking(date, 4)}

    elsif params[:date].present? && params[:type].to_i == 2
      date = Date.parse(params[:date])
      render json: {
        message: I18n.t('admin.booking_month'),
        pending:   Booking.month_booking(date, 1),
        cancelled: Booking.month_booking(date, 2),
        confirmed: Booking.month_booking(date, 3),
        completed: Booking.month_booking(date, 4)
    }
    elsif params[:current_all].present?
      render json: {
        message: I18n.t('admin.total_life_time_booking'),
        pending:   Booking.life_time_booking(1),
        cancelled: Booking.life_time_booking(2),
        confirmed: Booking.life_time_booking(3),
        completed: Booking.life_time_booking(4)
    }
    else
        render json: {
          message: I18n.t('admin.booking_month'),
          pending:   Booking.this_month_booking(1),
          cancelled: Booking.this_month_booking(2),
          confirmed: Booking.this_month_booking(3),
          completed: Booking.this_month_booking(4)
      }
    end
  end
end
