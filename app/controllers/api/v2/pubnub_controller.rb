class Api::V2::PubnubController < Api::V2::BaseController
	
	# POST /v2/pubnub
	def create
		SendPush.new(params[:user_id], nil, params[:msg]).test
		render json: { success: true, message: "Message Sent", data: {} }, status: 200
	end	
end
