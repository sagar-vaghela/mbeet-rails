# == Schema Information
#
# Table name: notifications
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  booking_id :integer
#  body       :json             not null
#  push_type  :integer          not null
#  read       :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Notification < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
	# ------------------- Associations --------------------- #
  belongs_to :user
	belongs_to :booking, optional: true

  # ----------------------- Validations -------------------- #
  validates :user, presence: { message: "is not a valid ID" }
  # validates :booking, presence: { message: "is not a valid ID" }
  validates :push_type, presence: true, :numericality => true
  validate 	:body_data

  # ------------------ Mobile Api Response --------------- #
  api_accessible :notifications do |t|
  	t.add :body
  	t.add :user_id
  	t.add :booking_id
  	t.add :read
  	t.add :push_type
  end

  def body_data
  	 errors.add(:body, 'not a valid json') unless body.present? && body.to_json.match(/{.+}/)[0]
  end

end
