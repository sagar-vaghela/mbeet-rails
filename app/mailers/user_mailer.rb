class UserMailer < ApplicationMailer
	default :from => 'no-reply@mbeet.com'

	def password_reset(user, passwd)
		category 'SendGridRocks'
		@user = user
		@password = passwd
		#@url  = ENV_CONSTANTS[:url] + '?token=' + @user.reset_digest
		mail to: @user.email, subject: 'Mbeet - Password reset'
	end

	def verify_email(user, token)
		category 'SendGridRocks'
		@user = user
		@token = token
		# this url would change later to web app link
		# @url = CONSTANT[:request_url] + '/v2/verify/email?token=' + @token
		@url = CONSTANT[:base_url] + '/verify_email.html?token=' + @token

		mail to: @user.email, subject: 'Mbeet - Verify your email resend'
	end

	# This function is used by admin side api
	def new_user_login_email(user, passwd)
		category 'SendGridRocks'
		@user = user
		@password = passwd
		#@url  = ENV_CONSTANTS[:url] + '?token=' + @user.reset_digest
		mail to: @user.email, subject: 'Mbeet - Welome and login details email'
	end

	# send email to admin when user adds new flat
	def user_add_new_flat(admin_user_emails, user)
    @user = user

    # Used to send emails in bulk
    headers["X-SMTPAPI"] = { :to => admin_user_emails }.to_json
 	  mail(to: '', from: @user.email ,subject: "New Flat Is Added")
  end

	# send email to admin to notify him that owner has changed his flat
	def owner_update_flat(admin_email, user, unit)
 	  @user = user
	  @unit = unit
 	 mail(to: admin_email, subject: "Owner has Updated flat (#{@unit.title})")
  end


	# send email to all users when admin adds new flat
  def admin_add_new_unit(unit)
	  @unit = unit
	  email_to_all_users = User.all.pluck(:email)
	  mail to: email_to_all_users, subject: "New Flat Is Added"
  end

 # send email to user when flat is published
  def user_flat_published(unit)
	  @user = unit.user
	  @unit = unit
	  mail to: @user.email, subject: "Your Flat Is Published"
  end

  # Send email to owner After booking payment finish
  def booking_email(booking)
    email = booking.unit.user.email
    @unit = booking.unit
    @user = booking.user
    @booking = booking
    mail to: email, subject: "Your Flat Is Booked"
  end

	def booking_email_to_customer(booking)
    email = booking.user.email
    @unit = booking.unit
    @user = booking.unit.user
    @booking = booking
    mail to: email, subject: "Your Flat Owner details"
  end

	# Send email to admin for contact_us
	def contact_us_email(contact_us)
		email = "info@mbeetapp.com"
		@contact_us = contact_us
		mail to: email, subject: "Contact Us"
	end
end
