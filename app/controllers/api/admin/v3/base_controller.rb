class Api::Admin::V3::BaseController < ApplicationController
	private
	def render_data data, message
		render json: { success: true, message: message, data: data }, status: 200
	end

	def error_data message
		render json: { success: false, message: message, data: {} }, status: 404
	end

	def error_valid_data message
		render json: { success: false, message: message, data: {} }, status: 400
	end

	##--------------------------- Render Success Methods ---------------------- ##

	def render_success ( message: 'OK', data: {}, status_code: 200 )

    render json: {
      success: true,
      message: message,
      data: data,
      status_code: status_code }, status: status_code
  end

	# Render created message with data if any
	  def render_created ( message: I18n.t('crud.created'), data: nil, status_code: 201 )
	    render_success message: message, data: data, status_code: status_code
	  end

  # Render updated message with data if any
  def render_updated( message: I18n.t('crud.updated'), data: nil, status_code: 200 )
   render_success message: message, data: data, status_code: status_code
  end

	# Render deleted message with data if any
  def render_deleted( message: I18n.t('crud.deleted'), data: nil, status_code: 200 )
    render_success message: message, data: data, status_code: status_code
  end

	# Render no data found with default message otherwise given message
  def render_no_data( message: I18n.t('crud.no_data_found'), data: {}, status_code: 200 )
    render_error message: message, data: data, status_code: status_code
  end



	##--------------------------- Render Error Methods ------------------------ ##


  # Render error / fail message
  def render_error( message: 'Error!', data: {}, status_code: 400 )

    render json: {
      success: false,
      message: message,
      data: data,
      status_code: status_code }, status: status_code
  end

	##----------------------------------- end ----------------------------------##


	# This method will return JSON error with 422 status code when there is
  # unprocessed entity error
  def render_unprocessable_entity( message: I18n.t('errors.e_422'), data: {}, status_code: 422 )
    render_error message: message, data: data, status_code: status_code
  end


  # This method will return JSON error message with 400 status code when rails app
  # throw bad request error of missing_params_error.
  def render_missing_params_error(names: [], data: {}, status_code: 400)
    error_message = I18n.t('errors.missing_params', names: names.join(', '))
    render_error message: error_message, data: data, status_code: status_code
  end
  ##----------------------------------- end ----------------------------------##


	protected
	def admin_login_required
		token = params[:access_token]
    token ||= request.headers["Access-Token"]

    decode_token = JsonWebToken.decode(token.gsub('Bearer','').gsub(/\s+/, ""))

    error_valid_data(decode_token[:error]) and return if decode_token[:error].present?
    error_valid_data(decode_token[:error]) and return if decode_token.blank?

    @current_user = decode_token["user_id"].present? ? decode_token["user_id"] : nil
  end
end
