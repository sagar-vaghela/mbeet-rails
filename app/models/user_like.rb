# == Schema Information
#
# Table name: user_likes
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  unit_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserLike < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  # ------------------- Associations ------------------- #
  belongs_to :user
  belongs_to :unit

  searchable do
    integer :user_id
    integer :unit_id
  end

  # ------------------- Validation --------------------- #
  validates :user, presence: { message: "is not a valid ID" }
  validates :unit, presence: { message: "is not a valid ID" }

  # ------------------- Mobile Api Response ------------ #
  api_accessible :likes do |t|
  	t.add :user_id
  	t.add :unit_id
  end

  api_accessible :favourites_list do |t|
  	t.add :unit, :template => :unit_with_pictures
  end

  api_accessible :v2_favourites_list do |t|
    t.add :unit, :template => :v2_unit_with_pictures
  end

  api_accessible :v4_favourites_list do |t|
    t.add :unit, :template => :v4_unit_with_pictures
  end

  # def self.if_liked_by_user(user_id, unit_id)
  #   UserLike.exists?(user_id: user_id, unit_id: unit_id)
  # end

end
