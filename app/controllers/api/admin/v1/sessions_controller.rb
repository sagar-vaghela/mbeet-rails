class Api::Admin::V1::SessionsController < Api::Admin::V1::BaseController
	before_action :check_null_password

	# POST /admin/v1/login
	def create
		if @user && @user.authenticate(params[:password])
			auth_token = JsonWebToken.encode({user_id: @user.id})
			render_data( { auth_token: auth_token, users: @user.as_api_response(:user_only) }, I18n.t('admin.user.success') )
		else
			error_data( I18n.t('admin.user.error') )
		end
	end

	private
	def check_null_password
		return error_valid_data( I18n.t('admin.user.error') ) if params[:email].empty? || params[:password].empty?

		@user = User.find_by(email: params[:email].downcase)
		error_valid_data( I18n.t('admin.user.wrong') ) if (@user.present?) && @user["role_id"] != 1
	end

end
