class CreateBookedSubunits < ActiveRecord::Migration[5.0]
  def change
    create_table :booked_subunits do |t|
      t.references :unit, foreign_key: true
      t.datetime :day_booked
      t.integer :count, default: 1
      t.timestamps
    end
    add_index :booked_subunits, [:day_booked, :count]
  end
end
