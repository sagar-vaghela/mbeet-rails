class Api::V3::UnitsController < Api::V3::BaseController
  before_action :login_required

  # GET /v2/units
  def index
    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
      @units = Unit.is_deleted
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = 0 }
      render_data({ total: Unit.is_deleted.count, units: @units.as_api_response(:v3_unit_pictures)}, I18n.t('unit.list')) if @units
    else
      @units = Unit.is_deleted
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      render_data({ total: Unit.is_deleted.count, units: @units.as_api_response(:v3_unit_pictures)}, I18n.t('unit.list')) if @current_user.present? && @units
    end
  end

  # GET /v2/units/:id
  def show
    @unit = Unit.is_deleted.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
      render_data({ units: @unit.as_api_response(:v3_unit_pictures) }, I18n.t('unit.detail')) if @unit
    else
      @unit.liked_by_user = (UserLike.if_liked_by_user @current_user, @unit.id).present? ? 1 : 0
      render_data({ units: @unit.as_api_response(:v3_unit_pictures) }, I18n.t('unit.detail')) if @unit
    end
  end

  # GET /v2/units/check_if_free
  def check_if_free
    # .where("check_in <= ? AND check_out >= ?", params[:current_date_time], params[:current_date_time])
                              # .where("check_out > ?", params[:current_date_time])
    @available_dates = Booking.where(unit_id: params[:unit_id])
                              .where('? BETWEEN check_in AND check_out OR check_in > ?', params[:current_date_time],  params[:current_date_time])
                              .where(booking_cancelled: false)
                              .order(check_in: :asc)
                              .pluck(:check_in, :check_out)


    if @available_dates.present?
      taken = []
      @available_dates.map do |date|
        # taken <<  ((date.first.strftime("%Y-%m-%d"))...(date.second.strftime("%Y-%m-%d"))).map(&:to_s)
        taken << (Date.parse(date.first.strftime("%Y-%m-%d"))..Date.parse(date.second.strftime("%Y-%m-%d"))).to_a
      end

      render_data({ taken_dates: taken.flatten, units: {id: params[:unit_id]} }, I18n.t('unit.availability'))
    else
      render_data({ taken_dates: [], units: {} }, I18n.t('unit.availability'))
    end
  end

  private

  def user_is_present?
    (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
  end

  def login_required
    if %w(index show).include?(params[:action]) &&  user_is_present?
      super
    elsif params[:action] == 'check_if_free'
      super
    end
  end

end
