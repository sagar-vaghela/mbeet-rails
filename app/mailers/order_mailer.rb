class OrderMailer < ApplicationMailer
	default :from => 'booking@mbeetapp.com'

	def invoice(email, uname, invoice_order_id)
		category 'SendGridRocks'

		@name = uname
		@invoice_order_id = invoice_order_id
		mail to: email, subject: 'Mbeet - Order placed successfully'
	end

	def invoice_to_admin(email, uname, invoice_order_id)
		category 'SendGridRocks'

		@name = uname
		@invoice_order_id = invoice_order_id

		@email = email
		@recipients = User.where(:role_id => 1)
		@email_address  = @recipients.collect(&:email)

		mail to: @email_address, subject: "Mbeet - User #{@name.capitalize}, placed an order successfully"
	end

	def cancel(email, uname, invoice_order_id)
		category 'SendGridRocks'

		@name = uname
		@invoice_order_id = invoice_order_id
		mail to: email, subject: 'Mbeet - Order cancelled successfully'
	end

	def order_cancel_to_admin(email, uname, invoice_order_id)
		category 'SendGridRocks'

		@name = uname
		@invoice_order_id = invoice_order_id

		@email = email

		@recipients = User.where(:role_id => 1)
		@email_address  = @recipients.collect(&:email)

		mail to: @email_address, subject: "Mbeet - User #{@name.capitalize}, cancelled an order successfully"
	end

	def payment_failed(email, uname, invoice_order_id)
		category 'SendGridRocks'
		
		@name = uname
		@invoice_order_id = invoice_order_id
		mail to: email, subject: 'Mbeet - Order payment failed'
	end

	def payment_failed_to_admin(email, uname, invoice_order_id)
		category 'SendGridRocks'
		
		@name = uname
		@invoice_order_id = invoice_order_id
		
		@email = email

		@recipients = User.where(:role_id => 1)
		@email_address  = @recipients.collect(&:email)
		
		mail to: @email_address, subject: "Mbeet - User #{@name.capitalize}, order payment failed"
	end

	def payment_success(email, uname, invoice_order_id)
		category 'SendGridRocks'
		
		@name = uname
		@invoice_order_id = invoice_order_id
		mail to: email, subject: 'Mbeet - Order payment successfull'
	end

	def payment_success_to_admin(email, uname, invoice_order_id)
		category 'SendGridRocks'
		
		@name = uname
		@invoice_order_id = invoice_order_id
		
		@email = email

		@recipients = User.where(:role_id => 1)
		@email_address  = @recipients.collect(&:email)
		
		mail to: @email_address, subject: "Mbeet - User #{@name.capitalize}, order payment successfull"
	end
end
