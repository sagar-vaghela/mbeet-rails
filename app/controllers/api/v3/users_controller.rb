class Api::V3::UsersController < Api::V3::BaseController
	before_action :login_required, only: [:update, :dashbord_owners]
	before_action :check_user, :only => [:show, :update]
	# before_action :validate_limit_offset, :only => [:index]

	def index
		@users = User.order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
		render_data({ total: User.count, users: @users.as_api_response(:v3_user_pictures)}, I18n.t('user.list'))
	end

	def show
		render_data({ users: @user.as_api_response(:v3_user_pictures) }, I18n.t('user.detail'))
	end

	def update
		@user.skip_password_validation = true
		@user.update(user_params)

		render_data({ users: @user.as_api_response(:v3_user_pictures) }, I18n.t('user.data_updated')) if @user.save!
	rescue StandardError => e
		error_valid_data(e)
	end

	def dashbord_owners
		@user = User.find(@current_user) if @current_user.present?
	  @owner = @user if @user.is_owner == true && @user.role_id == 2
		if @owner
			@units = @owner.units
			render_data({ units: @units.as_api_response(:dashbord_owners) }, I18n.t('Booking units'))
		end
  end

	private

	def user_params
		params.permit(:name, :email, :date_of_birth, :gender, :country_code, :phone, picture_attributes: [:name])
	end

	def check_user
		@user = User.find(params[:id])
	end

end
