class Api::V1::AmenitiesController < Api::V1::BaseController
	def index
		@amenities = Amenity.order(id: :ASC)
		render_data({ amenities: @amenities.as_api_response(:list_amenities)}, I18n.t('amenity.list')) if @amenities
	end
	#edit
end
