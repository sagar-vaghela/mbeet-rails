class CreateUnitDisabledDates < ActiveRecord::Migration[5.0]
  def change
    create_table :unit_disabled_dates do |t|
      t.references :unit, foreign_key: true
      t.datetime :day_disabled
      t.integer :number_of_disabled_units, default: 0

      t.timestamps
    end
  end
end
