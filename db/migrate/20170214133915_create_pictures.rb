class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.string :name, null: false
      t.references :imageable, polymorphic: true, index: true, null: false

      t.timestamps
    end
  end
end
