class BookingEmailWorker
  @queue = 'booking_email'

  def self.perform(id)
		booking = Booking.find_by_id(id)
    UserMailer.booking_email(booking).deliver
    UserMailer.booking_email_to_customer(booking).deliver
	end
end
