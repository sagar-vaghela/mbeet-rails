# == Schema Information
#
# Table name: request_to_list_units
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  name       :string           not null
#  email      :string           not null
#  phone      :string           not null
#  message    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RequestToListUnit < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  # ----------------------- Associations ------------------- #
  belongs_to :user

  # ----------------------- Validations -------------------- #
  #validates :unit, presence: { message: "is not a valid ID" }
  validates :user, presence: { message: "is not a valid ID" }
  validates :name, presence: true,length: { minimum: 3 }

  VALIDATE_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,
			          length: { maximum: 255 },
			          format: { with: VALIDATE_EMAIL_REGEX }

  validates :phone, presence: true
  validates :message, presence: true

  ## Api Response using acts_as_api
  # api_accessible :request_data do |t|
  #   t.add :name
  #   t.add :email
  #   t.add :phone
  #   t.add :message
  # end
end
