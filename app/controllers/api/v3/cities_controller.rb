class Api::V3::CitiesController < Api::V3::BaseController
  before_action :login_required
	before_action :find_city, only: :show

	def index
		@cities = City.published.not_deleted.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @cities.present?
			render_data({ cities: @cities.as_api_response(:v3_city_pictures) }, I18n.t('city.cities_list') ) if @cities.present?
		else
			render_no_data
		end
	end

	def show
		render_data({ cities: @city.as_api_response(:v3_city_pictures) }, I18n.t('city.city_detail') ) if @city.present?
	end

  # display all published and unpublished cities
  def published_and_unpublished
    @cities = City.not_deleted.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @cities.present?
			render_data({ cities: @cities.as_api_response(:v3_published_and_unpublished) }, I18n.t('city.cities_list') ) if @cities.present?
		else
			render_no_data
		end
  end


	##----------------------------- Private Methods ----------------------------##

	private
	def get_order
		"name_translations->>'#{I18n.locale}' ASC"
	end

  # Find a specific city
  def find_city
    @city = City.published.not_deleted.find( params[:id] )
  end


	##--------------------------------------- end ------------------------------##
end
