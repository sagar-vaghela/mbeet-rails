class Api::V6::UnitsController < Api::V5::UnitsController
  before_action :check_unit, only: :update


  # GET /v6/units
  def index
    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
      @units = Unit.is_deleted
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = 0 }
      render_data({ total: Unit.is_deleted.count, units: @units.as_api_response(:v6_unit_pictures)}, I18n.t('unit.list')) if @units
    else
      @units = Unit.is_deleted
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      render_data({ total: Unit.is_deleted.count, units: @units.as_api_response(:v6_unit_pictures)}, I18n.t('unit.list')) if @current_user.present? && @units
    end
  end


  # PUT /v6/units/:id
  def update
    user = @unit.user if @unit.user.is_owner?

    if !@unit.price_changed?
      @unit.update_attributes(unit_status: false, message: params[:message]) if params[:message].present?
    end

    if @unit.update(unit_params)
      render_data({ units: @unit.as_api_response(:v6_unit_only) }, I18n.t('unit.updated'))
      if @unit.unit_status_previously_changed? && @unit.unit_status == true
        user.send_email_to_user(@unit)
      end
    else
      render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  private

  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar,:number_of_guests, :unit_status, :unit_enabled,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id, :mbeet_percentage, :bed_type,
                  :address, :price, :latitude, :longitude, :unit_class, :unit_type, :number_of_subunits,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @unit = Unit.find(params[:unit_id])
  end

end
