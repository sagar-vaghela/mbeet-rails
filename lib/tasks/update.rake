namespace :update do

	desc 'Update City ids as per name from City table to Unit table'
	task unit_city_ids: :environment do
		Unit.all.map do |data|
			puts "Update Start"
			
  		get_city = City.with_name_translation(data["city"]).first

  		unit = Unit.where("city = '#{data["city"]}'")
  		unit.update(city_id: get_city.id)
  		puts "Update Ends"
  	end
	end
end