module V6::Admin::PictureResponder
	extend ActiveSupport::Concern

	included do
		# --------------------- Admin Api Response --------------------- #
		api_accessible :images do |t|
	    t.add :id
	    t.add :url
			t.add :locale
	  end
	end

end
