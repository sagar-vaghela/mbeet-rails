FactoryBot.define do
  factory :role, class: Role do
    name "admin"
  end
  factory :normal_role, class: Role do
    name 'normal_user'
  end
  factory :owner_role, class: Role do
    name 'owner'
  end
  factory :role_with_id, class: Role do
    id 2
    name "owner"
  end
end
