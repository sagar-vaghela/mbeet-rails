class Api::V6::MyOrdersController < Api::V5::MyOrdersController
  before_action :login_required
  before_action :check_my_orders, only: :index

  def owner_order
    @my_order_list = Booking.where(user_id: params[:user_id],payment_status: 3)
                      .order(id: :desc)
                      .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                      .offset(params[:offset])
    render_data({ total: @my_order_list.size, result_total:  @my_order_list.count, my_orders: @my_order_list.as_api_response(:v5_booking_with_customer_info) }, I18n.t('user.my_orders') )
  end

  private
  def check_my_orders
    @my_order_list = Booking.where(user_id: params[:user_id])
                      .order(id: :desc)
                      .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                      .offset(params[:offset])
    # render_data({}, I18n.t('no_data')) unless @my_order_list.present?
  end
end
