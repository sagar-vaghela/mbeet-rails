# == Schema Information
#
# Table name: feedbacks
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  unit_id    :integer          not null
#  star       :decimal(3, 1)    default(0.0), not null
#  feedback   :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  booking_id :integer          not null
#

require 'rails_helper'

RSpec.describe Feedback, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
end
