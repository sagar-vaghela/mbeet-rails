class OwnerUpdateFlatEmailWorker
	@queue = 'owner_update_flat'

	def self.perform(id)
		@unit = Unit.find_by_id(id)
		admin_user_emails = User.where(role_id: 1).pluck(:email)
		admin_user_emails.each do |admin_email|
			UserMailer.owner_update_flat(admin_email,@unit.user, @unit).deliver_now
		end
	end
end
