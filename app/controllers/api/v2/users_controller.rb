class Api::V2::UsersController < Api::V2::BaseController
	before_action :login_required, only: :update
	before_action :check_user, :only => [:show, :update]
	# before_action :validate_limit_offset, :only => [:index]
	
	def index
		@users = User.order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
		render_data({ total: User.count, users: @users.as_api_response(:v2_user_pictures)}, I18n.t('user.list'))
	end

	def show
		render_data({ users: @user.as_api_response(:v2_user_pictures) }, I18n.t('user.detail'))
	end

	def update
		@user.skip_password_validation = true
		@user.update(user_params)
		
		render_data({ users: @user.as_api_response(:v2_user_pictures) }, I18n.t('user.data_updated')) if @user.save!
	rescue StandardError => e
		error_valid_data(e)
	end

	private
	def user_params
		params.permit(:name, :email, :date_of_birth, :gender, :phone, picture_attributes: [:name])
	end

	def check_user
		@user = User.find(params[:id])
	end

end
