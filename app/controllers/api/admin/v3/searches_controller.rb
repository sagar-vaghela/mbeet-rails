class Api::Admin::V3::SearchesController < Api::Admin::V3::BaseController
  # GET admin/v3/search
  def index
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = Unit.search do
      fulltext params[:search]
      with(:city_id, params[:city_id]) if params[:city_id].present?
      if params[:unit_status].present?
        status = params[:unit_status] == 'true' ? true : false
        with(:unit_status, status)
      end

      if params[:unit_enabled].present?
        status = params[:unit_enabled] == 'true' ? true : false
        with(:unit_enabled, status)
      end

      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      with(:number_of_guests, params[:number_of_guests]) if params[:number_of_guests].present?
      with(:number_of_rooms, params[:number_of_rooms] ) if params[:number_of_rooms].present?
      with(:number_of_beds, params[:number_of_beds] )   if params[:number_of_beds].present?
      with( :number_of_baths,  params[:number_of_baths] )  if params[:number_of_baths].present?
      with(:unit_class,   params[:unit_class])   if params[:unit_class].present?
      with(:unit_type,    params[:unit_type])    if params[:unit_type].present?
      with(:available_as, params[:available_as]) if params[:available_as].present?
      with(:price).greater_than_or_equal_to(params[:starting_price]) if params[:starting_price].present?
      with(:price).less_than_or_equal_to(params[:closing_price]) if params[:starting_price].present?
      with(:amenities).any_of([params[:amenities]]) if params[:amenities].present?
      paginate(:offset => params[:offset], :per_page => limit)
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end

    end
    render_data({ total: Unit.all.size, units: search.results.as_api_response(:v3_admin_unit_only)}, I18n.t('unit.list'))
  end
end
