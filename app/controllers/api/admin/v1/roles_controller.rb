class Api::Admin::V1::RolesController < Api::Admin::V1::BaseController
	
	# GET /admin/v1/roles
	def index
		@roles = Role.order(id: :asc)
		render_data({ roles: @roles.as_api_response(:for_admin_show_roles)}, I18n.t('admin.roles.list')) if @roles
	end

	def create
		@role = Role.new(role_params)
		render_data({ roles: {}}, I18n.t('admin.roles.created')) if  @role.save!
	end


	private

		def role_params
			params.permit(:name)
		end
end
