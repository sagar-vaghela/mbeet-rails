class Date

  def self.date_diff(date1, date2)
   	difference = (date2.year - date1.year) * 12 + date2.month - date1.month - (date2.day >= date1.day ? 0 : 1)
    y_n_m = difference.divmod(12)
    date3 = date2 - y_n_m[0].year - y_n_m[1].months
    days = (date3 - date1).to_i
    [y_n_m[0], y_n_m[1], days] 
  end

  def upto(date2)
   	Date.date_diff(self, date2 + 1.day)
  end

  def equal_to?(other_date)
    self == other_date
  end

  def greater_than?(other_date)
    self > other_date
  end

  def less_than?(other_date)
    self < other_date
  end

  def greater_than_or_equal_to?(other_date)
    # greater_than(other_date) || equal_to(other_date)
    self >= other_date
  end

  def less_than_or_equal_to?(other_date)
    # greater_than(other_date) || equal_to(other_date
    self <= other_date
  end
end