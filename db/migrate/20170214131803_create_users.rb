class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.references :role, foreign_key: true, null: false
      t.string :name, null: false
      t.string :email, null: false
      t.string :phone, null: false

      # 0 = False, 1 = True
      t.boolean :is_owner, null: false, default: false

      t.string :facebook_uid
      t.string :twitter_uid
      t.string :gmail_uid

      t.string :password_digest, null: false
      t.string :reset_digest
      t.timestamp :reset_sent_on

      t.timestamps
    end

    add_index :users, :email, unique: true
    add_index :users, :phone, unique: true
  end
end
