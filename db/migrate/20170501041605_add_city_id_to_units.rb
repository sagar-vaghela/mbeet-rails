class AddCityIdToUnits < ActiveRecord::Migration[5.0]
  def self.up
  	add_reference :units, :city, index: true, foreign_key: true, null: false, default: 1
  	change_column_null :units, :city_id, false, default: nil

    Unit.all.map do |data|
      puts "Update Start"
      
      get_city = City.with_name_translation(data["city"]).first

      unit = Unit.where("city = '#{data["city"]}'")
      unit.update(city_id: get_city.id)
      puts "Update Ends"
    end
  end

  def self.down
		remove_reference :units, :city
  end
end
