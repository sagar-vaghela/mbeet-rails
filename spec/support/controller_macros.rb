module ControllerMacros
  extend ActiveSupport::Concern

  module ClassMethods
  end
end

RSpec.configure do |config|
  config.include ControllerMacros, type: :controller
end
