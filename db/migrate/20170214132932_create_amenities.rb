class CreateAmenities < ActiveRecord::Migration[5.0]
  def change
    create_table :amenities do |t|
    	t.column  :name_translations, 'jsonb', null: false
      t.boolean :is_active, null: false, default: true
      t.timestamps
    end
    # Create HABTM Table
    create_table :amenities_units, id: false do |t|
      t.belongs_to :unit, index: true, null: false
      t.belongs_to :amenity, index: true, null: false
    end
  end
end
