Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # scope module was used to not include api in url

  require "resque_web"

  mount ResqueWeb::Engine => "/resque"

  scope module: 'api' do

    # From here API'S routes for Admin start
    namespace :admin do
      namespace :v1 do

        resources :users, only: [:index, :show, :create, :update, :destroy] do
          get 'owners', on: :collection

        end

        resources :sessions, only: [], path: '' do
          collection do
            post 'login', to: 'sessions#create'
          end
        end

        resources :units, only: [:index, :show, :create, :update, :destroy] do
          put 'restore', on: :member
        end

        resources :roles, only: [:index, :create]

        resources :pictures, only: [:destroy]
      end
    end

    # From here API'S routes for Admin version 2
    namespace :admin do
      namespace :v2 do

        resources :users, only: [:index, :show, :create, :update, :destroy] do
          get 'owners', on: :collection
          get :units,   on: :member
        end

        resources :sessions, only: [], path: '' do
          collection do
            post 'login', to: 'sessions#create'
          end
        end

        resources :units, only: [:index, :show, :create, :update, :destroy] do
          put 'restore', on: :member
          put :publish, on: :member
        end

        resources :roles, only: [:index]

        resources :pictures, only: [:destroy]
        resources :cities,   only: [:index, :show, :create, :update, :destroy]
        resources :bookings, only: [:index, :show] do
          delete :cancel,    on: :member
        end
      end
    end


    # ---------------------------- Admin v3 -----------------------------------#
    namespace :admin do
      namespace :v3 do

        resources :users, only: [:index, :show, :create, :update, :destroy] do
          get 'owners', on: :collection
          get :units,   on: :member
          get :units_interested_on, on: :member

        end


        resources :sessions, only: [], path: '' do
          collection do
            post 'login', to: 'sessions#create'
          end
        end

        resources :units, only: [:index, :show, :create, :update, :destroy] do
          put 'restore', on: :member
          put :publish, on: :member
        end

        resources :roles, only: [:index]

        resources :pictures, only: [:destroy]
        resources :cities,   only: [:index, :show, :create, :update, :destroy]
        resources :bookings, only: [:index, :show] do
          delete :cancel,    on: :member
        end

        resources :search, only:[:index], controller: :searches
      end
    end


    # ---------------------------- Admin v4 -----------------------------------#
    namespace :admin do
      namespace :v4 do

        resources :users, only: [:index, :show, :create, :update, :destroy] do
          get 'owners', on: :collection
          get :units,   on: :member
          get :units_interested_on, on: :member
          get :dashbord_users, on: :collection
        end

        resources :sessions, only: [], path: '' do
          collection do
            post 'login', to: 'sessions#create'
          end
        end

        resources :units, only: [:index, :show, :create, :update, :destroy] do
          put 'restore', on: :member
          put :publish, on: :member
          put :enable, on: :member
          put :disable, on: :member
        end

        resources :periods do
          collection do
            post 'multiple_create', to: 'periods#multiple_create'
            get 'all_periods', to: 'periods#all_periods'
            get 'get_total_price', to: 'periods#get_total_price'
          end
        end

        resources :roles, only: [:index]

        resources :pictures, only: [:destroy]
        resources :cities,   only: [:index, :show, :create, :update, :destroy]
        resources :bookings, only: [:index, :show] do
          delete :cancel,    on: :member
          get :dashbord_bookings, on: :collection
        end

        resources :search, only:[:index], controller: :searches
        resources :user_restores, only: :update

        # coupons api to make discounts to the users
        resources :coupons, only: [:index, :show, :create, :update, :destroy] do
          get 'get_statistics', to: 'coupons#get_statistics'
        end
      end
    end


    # ---------------------------- Admin v5 -----------------------------------#
    namespace :admin do
      namespace :v5 do

        resources :users, only: [:index, :show, :create, :update, :destroy] do
          get 'owners', on: :collection
          get :units,   on: :member
          get :units_interested_on, on: :member
          get :dashbord_users, on: :collection
        end

        resources :sessions, only: [], path: '' do
          collection do
            post 'login', to: 'sessions#create'
          end
        end

        resources :units, only: [:index, :show, :create, :update, :destroy] do
          put 'restore', on: :member
          put :publish, on: :member
          put :enable, on: :member
          put :disable, on: :member
        end

        resources :periods do
          collection do
            post 'multiple_create', to: 'periods#multiple_create'
            get 'all_periods', to: 'periods#all_periods'
            get 'get_total_price', to: 'periods#get_total_price'
          end
        end

        resources :roles, only: [:index]

        resources :pictures, only: [:destroy]
        resources :cities,   only: [:index, :show, :create, :update, :destroy]
        resources :bookings, only: [:index, :show] do
          delete :cancel,    on: :member
          get :dashbord_bookings, on: :collection
        end

        resources :search, only:[:index], controller: :searches
        resources :user_restores, only: :update

        # coupons api to make discounts to the users
        resources :coupons, only: [:index, :show, :create, :update, :destroy] do
          get 'get_statistics', to: 'coupons#get_statistics'
        end
      end
    end

    # ---------------------------- Admin v6 -----------------------------------#
    namespace :admin do
      namespace :v6 do

        resources :users, only: [:index, :show, :create, :update, :destroy] do
          get 'owners', on: :collection
          get :units,   on: :member
          get :units_interested_on, on: :member
          get :dashbord_users, on: :collection
          get :getCurrentUserPermission, on: :collection
        end

        resources :sessions, only: [], path: '' do
          collection do
            post 'login', to: 'sessions#create'
          end
        end

        resources :units, only: [:index, :show, :create, :update, :destroy] do
          put 'restore', on: :member
          put :publish, on: :member
          put :enable, on: :member
          put :disable, on: :member
        end

        resources :periods do
          collection do
            post 'multiple_create', to: 'periods#multiple_create'
            get 'all_periods', to: 'periods#all_periods'
            get 'get_total_price', to: 'periods#get_total_price'
          end
        end

        resources :roles

        resources :pictures, only: [:destroy]
        resources :cities, only: [:index, :show, :create, :update, :destroy] do
          get :top_destinations, on: :collection
          get :city_list_with_unit, on: :collection
        end

        # routes for manual top destination
        resources :top_destinations, only: [:index, :show, :create, :update, :destroy] do
          get :top_destin_cities, on: :collection
        end

        resources :bookings, only: [:index, :show] do
          delete :cancel,    on: :member
          get :dashbord_bookings, on: :collection
        end

        resources :search, only:[:index], controller: :searches
        resources :user_restores, only: :update

        # coupons api to make discounts to the users
        resources :coupons, only: [:index, :show, :create, :update, :destroy] do
          get 'get_statistics', to: 'coupons#get_statistics'
        end

        # for log maintenance
        resources :logs, only: [:index]
      end
    end

    # From here Api routes for Mobile App starts
    namespace :v1 do

      resources :users, only: [:index, :show, :update]

      resources :favorites, only: [:index]

      # path attribute is empty as we do not want controller name in url
      resources :sessions, only: [], path: '' do
        collection do
          post 'login', to: 'sessions#create' # /v1/login
          post 'signup'                       # /v1/signup
        end
      end

      resources :socials, only: [], path: '' do
        collection do
          post 'signup/social', to: 'socials#create'
          get 'get_user_email'
        end
      end

      resources :passwords, only: [], path: '' do
        collection do
          get 'forgot_password', to: 'passwords#create'   # /v1/forgot_password
          post 'reset_password', to: 'password#update'    # /v1/reset_password

          # not to be used in an api
          get 'change_password', to: 'passwords#edit'
        end
      end

      resources :units, only: [:index, :show] do
        collection do
          get 'check_if_free', to: 'units#check_if_free'
        end
      end

      resources :likes, only: [], path: '' do
        collection do
          post 'like/unit', to: 'likes#create'
          delete 'unlike/unit', to: 'likes#destroy'
        end
      end

      resources :amenities, only: [:index]
    end

    # Routes for api version 2
    namespace :v2 do
      resources :users, only: [:index, :show, :update]

      resources :verify, only: [] do
        member do
          put 'phone'
          get 'resend/email', to: 'verify#resend'
        end

        collection do
          get 'email'
        end
      end

      resources :my_orders, only: [:index, :show]

      resources :favorites, only: [:index]

      # path attribute is empty as we do not want controller name in url
      resources :sessions, only: [], path: '' do
        collection do
          post 'login', to: 'sessions#create' # /v2/login
          post 'signup'                       # /v2/signup
        end
      end

      resources :socials, only: [], path: '' do
        collection do
          post 'signup/social', to: 'socials#create'
          get 'get_user_email'
        end
      end

      resources :passwords, only: [], path: '' do
        collection do
          get 'forgot_password', to: 'passwords#create'   # /v2/forgot_password
          post 'reset_password', to: 'password#update'    # /v2/reset_password
          put 'change_password', to: 'passwords#change'   # /v2/change_password

          # not to be used in an api
          get 'change_password', to: 'passwords#edit'
        end
      end

      resources :units, only: [:index, :show] do
        collection do
          get 'check_if_free', to: 'units#check_if_free'
        end
      end

      resources :likes, only: [], path: '' do
        collection do
          post 'like/unit', to: 'likes#create'
          delete 'unlike/unit', to: 'likes#destroy'
        end
      end

      resources :bookings, only: [], path: '' do
        collection do
          post 'book_now', to: 'bookings#create'
          post 'cancel/booking', to: 'bookings#cancel'
          put  'payment/update', to: 'bookings#payment_update'
        end
      end

      resources :feedbacks, only: [:index, :create]

      resources :searches, only: [], path: '' do
        collection do
          get 'search'
          # get 'filter'
        end
      end

      resources :amenities, only: [:index]

      resources :cities, only: [:index]

      resources :list_my_unit, only: [:create]

      resources :reasons, only: [:index]

      resources :pubnub, only: [:create]

      resources :notifications,  only: [:index, :create, :update]

      resources :rules, only: [:index]
    end

    # Routes for api version 3
    namespace :v3 do
      resources :users, only: [:index, :show, :update]

      resources :verify, only: [] do
        member do
          put 'phone'
          get 'resend/email', to: 'verify#resend'
        end

        collection do
          get 'email'
        end
      end

      resources :my_orders, only: [:index, :show]

      resources :favorites, only: [:index]

      # path attribute is empty as we do not want controller name in url
      resources :sessions, only: [], path: '' do
        collection do
          post 'login', to: 'sessions#create' # /v3/login
          post 'login_owner', to: 'sessions#create_owner' # /v3/login_owner
          post 'signup'                       # /v3/signup
        end
      end

      resources :socials, only: [], path: '' do
        collection do
          post 'signup/social', to: 'socials#create'
          get 'get_user_email'
        end
      end

      resources :passwords, only: [], path: '' do
        collection do
          get 'forgot_password', to: 'passwords#create'   # /v2/forgot_password
          post 'reset_password', to: 'password#update'    # /v2/reset_password
          put 'change_password', to: 'passwords#change'   # /v2/change_password

          # not to be used in an api
          get 'change_password', to: 'passwords#edit'
        end
      end

      resources :units, only: [:index, :show] do
        collection do
          get 'check_if_free', to: 'units#check_if_free'
        end
      end

      resources :user_units, only: [:index, :show, :create, :update, :destroy] do
        get :enable,         on: :collection
        get :disable,         on: :collection
      end

      resources :likes, only: [], path: '' do
        collection do
          post 'like/unit', to: 'likes#create'
          delete 'unlike/unit', to: 'likes#destroy'
        end
      end

      resources :bookings, only: [], path: '' do
        collection do
          post 'book_now', to: 'bookings#create'
          post 'cancel/booking', to: 'bookings#cancel'
          put  'payment/update', to: 'bookings#payment_update'
        end
      end

      resources :feedbacks, only: [:index, :create]

      resources :searches, only: [], path: '' do
        collection do
          get 'search'
          # get 'filter'
        end
      end

      resources :amenities, only: [:index]

      resources :cities, only: [:index, :show] do
        get :published_and_unpublished, on: :collection
      end

      resources :list_my_unit, only: [:create]

      resources :reasons, only: [:index]

      resources :pubnub, only: [:create]

      resources :notifications,  only: [:index, :create, :update]

      resources :rules, only: [:index]

      resources :user_units_images, only: [:destroy]

      resources :coupons, only: [:index]
    end

    # ----------------------- mobile api v4 ---------------------------------#
    namespace :v4 do
      resources :users, only: [:index, :show, :update]

      resources :verify, only: [] do
        member do
          put 'phone'
          get 'resend/email', to: 'verify#resend'
          put 'send_otp'
          put 'confirm_otp'

        end

        collection do
          get 'email'
        end
      end

      resources :my_orders, only: [:index, :show]

      resources :favorites, only: [:index]

      # path attribute is empty as we do not want controller name in url
      resources :sessions, only: [], path: '' do
        collection do
          post 'login', to: 'sessions#create' # /v4/login
          post 'login_owner', to: 'sessions#create_owner' # /v4/login_owner
          post 'signup'                       # /v4/signup
        end
      end

      resources :socials, only: [], path: '' do
        collection do
          post 'signup/social', to: 'socials#create'
          get 'get_user_email'
        end
      end

      resources :passwords, only: [], path: '' do
        collection do
          get 'forgot_password', to: 'passwords#create'   # /v2/forgot_password
          post 'reset_password', to: 'password#update'    # /v2/reset_password
          put 'change_password', to: 'passwords#change'   # /v2/change_password

          # not to be used in an api
          get 'change_password', to: 'passwords#edit'
        end
      end

      resources :units, only: [:index, :show] do
        collection do
          get 'check_if_free', to: 'units#check_if_free'
        end
      end

      resources :periods do
        collection do
          post 'multiple_create', to: 'periods#multiple_create'
          get 'all_periods', to: 'periods#all_periods'
          get 'get_total_price', to: 'periods#get_total_price'
        end
      end

      resources :user_units, only: [:index, :show, :create, :update, :destroy] do
        get :enable,               on: :collection
        get :disable,              on: :collection
        get :list_disable_units,   on: :collection
      end

      resources :likes, only: [], path: '' do
        collection do
          post 'like/unit', to: 'likes#create'
          delete 'unlike/unit', to: 'likes#destroy'
        end
      end

      resources :bookings, only: [], path: '' do
        collection do
          post 'book_now', to: 'bookings#create'
          post 'cancel/booking', to: 'bookings#cancel'
          put  'payment/update', to: 'bookings#payment_update'
          get  'total_bookings_this_month', to: 'bookings#current_month_booking_for_owner'
          get 'validate_coupon', to: 'bookings#validate_coupon'
        end
      end

      resources :feedbacks, only: [:index, :create]

      resources :searches, only: [], path: '' do
        collection do
          get 'search'
          # get 'filter'
        end
      end

      resources :amenities, only: [:index]

      resources :cities, only: [:index, :show] do
        get :published_and_unpublished, on: :collection
      end

      resources :list_my_unit, only: [:create]

      resources :reasons, only: [:index]

      resources :pubnub, only: [:create]

      resources :notifications,  only: [:index, :create, :update]

      resources :rules, only: [:index]

      resources :user_units_images, only: [:destroy]

      # Owner's dashboard
      get "dashbord_owners" => "users#dashbord_owners", as: :dashbord_owners
    end

    # ----------------------- mobile api v5 ---------------------------------#
    namespace :v5 do
      resources :users, only: [:index, :show, :update]

      resources :verify, only: [] do
        member do
          put 'phone'
          get 'resend/email', to: 'verify#resend'
          put 'send_otp'
          put 'confirm_otp'

        end

        collection do
          get 'email'
        end
      end

      resources :my_orders, only: [:index, :show]

      resources :favorites, only: [:index]

      # path attribute is empty as we do not want controller name in url
      resources :sessions, only: [], path: '' do
        collection do
          post 'login', to: 'sessions#create' # /v5/login
          post 'login_owner', to: 'sessions#create_owner' # /v4/login_owner
          post 'signup'                       # /v5/signup
          post 'contact_us' , to: 'sessions#contact_us' # /v5/contact_us
        end
      end

      resources :socials, only: [], path: '' do
        collection do
          post 'signup/social', to: 'socials#create'
          get 'get_user_email'
        end
      end

      resources :passwords, only: [], path: '' do
        collection do
          get 'forgot_password', to: 'passwords#create'   # /v2/forgot_password
          post 'reset_password', to: 'password#update'    # /v2/reset_password
          put 'change_password', to: 'passwords#change'   # /v2/change_password

          # not to be used in an api
          get 'change_password', to: 'passwords#edit'
        end
      end

      resources :units, only: [:index, :show] do
        collection do
          get 'check_if_free', to: 'units#check_if_free'
        end
      end

      resources :periods do
        collection do
          post 'multiple_create', to: 'periods#multiple_create'
          get 'all_periods', to: 'periods#all_periods'
          get 'get_total_price', to: 'periods#get_total_price'
        end
      end

      resources :user_units, only: [:index, :show, :create, :update, :destroy] do
        get :enable,               on: :collection
        get :disable,              on: :collection
        put :enable,               on: :collection
        put :owner_enable,         on: :collection
        put :disable,              on: :collection
        get :list_disable_units,   on: :collection
      end

      resources :likes, only: [], path: '' do
        collection do
          post 'like/unit', to: 'likes#create'
          delete 'unlike/unit', to: 'likes#destroy'
        end
      end

      resources :bookings, only: [], path: '' do
        collection do
          post 'book_now', to: 'bookings#create'
          post 'cancel/booking', to: 'bookings#cancel'
          put  'payment/update', to: 'bookings#payment_update'
          get  'total_bookings_this_month', to: 'bookings#current_month_booking_for_owner'
          get 'validate_coupon', to: 'bookings#validate_coupon'
        end
      end

      resources :feedbacks, only: [:index, :create]

      resources :searches, only: [], path: '' do
        collection do
          get 'search'
          # get 'filter'
        end
      end

      resources :amenities, only: [:index]

      resources :cities, only: [:index, :show] do
        get :published_and_unpublished, on: :collection
      end

      resources :list_my_unit, only: [:create]

      resources :reasons, only: [:index]

      resources :pubnub, only: [:create]

      resources :notifications,  only: [:index, :create, :update]

      resources :rules, only: [:index]

      resources :user_units_images, only: [:destroy]

      # Owner's dashboard
      get "dashbord_owners" => "users#dashbord_owners", as: :dashbord_owners
      get "owners_dashboard" => "users#owners_dashboard", as: :owners_dashboard
    end

    # ----------------------- mobile api v6 ---------------------------------#
    namespace :v6 do
      resources :users, only: [:index, :show, :update]

      resources :verify, only: [] do
        member do
          put 'phone'
          get 'resend/email', to: 'verify#resend'
          put 'send_otp'
          put 'confirm_otp'

        end

        collection do
          get 'email'
        end
      end

      resources :my_orders, only: [:index, :show] do
        get :owner_order,        on: :collection
      end

      resources :favorites, only: [:index]

      # path attribute is empty as we do not want controller name in url
      resources :sessions, only: [], path: '' do
        collection do
          post 'login', to: 'sessions#create' # /v6/login
          post 'login_owner', to: 'sessions#create_owner' # /v4/login_owner
          post 'signup'                       # /v6/signup
          post 'owner_signup'                       # /v6/owner_signup
          post 'contact_us' , to: 'sessions#contact_us' # /v6/contact_us
        end
      end

      resources :socials, only: [], path: '' do
        collection do
          post 'signup/social', to: 'socials#create'
          get 'get_user_email'
        end
      end

      resources :passwords, only: [], path: '' do
        collection do
          get 'forgot_password', to: 'passwords#create'   # /v2/forgot_password
          post 'reset_password', to: 'password#update'    # /v2/reset_password
          put 'change_password', to: 'passwords#change'   # /v2/change_password

          # not to be used in an api
          get 'change_password', to: 'passwords#edit'
        end
      end

      resources :units, only: [:index, :show, :update] do
        collection do
          get 'check_if_free', to: 'units#check_if_free'
        end
      end

      resources :periods do
        collection do
          post 'multiple_create', to: 'periods#multiple_create'
          get 'all_periods', to: 'periods#all_periods'
          get 'get_total_price', to: 'periods#get_total_price'
        end
      end

      resources :user_units, only: [:index, :show, :create, :update, :destroy] do
        get :enable,               on: :collection
        get :disable,              on: :collection
        put :enable,               on: :collection
        put :owner_enable,         on: :collection
        put :disable,              on: :collection
        get :list_disable_units,   on: :collection
      end

      resources :likes, only: [], path: '' do
        collection do
          post 'like/unit', to: 'likes#create'
          delete 'unlike/unit', to: 'likes#destroy'
        end
      end

      resources :bookings, only: [], path: '' do
        collection do
          post 'book_now', to: 'bookings#create'
          post 'cancel/booking', to: 'bookings#cancel'
          put  'payment/update', to: 'bookings#payment_update'
          get  'total_bookings_this_month', to: 'bookings#current_month_booking_for_owner'
          get 'validate_coupon', to: 'bookings#validate_coupon'
        end
      end

      resources :feedbacks, only: [:index, :create]

      resources :searches, only: [], path: '' do
        collection do
          get 'search'
          # get 'filter'
        end
      end

      resources :amenities, only: [:index]

      resources :cities, only: [:index, :show] do
        get :published_and_unpublished, on: :collection
        get :top_destinations, on: :collection
      end

      resources :list_my_unit, only: [:create]

      resources :reasons, only: [:index]

      resources :pubnub, only: [:create]

      resources :notifications, only: [:index, :create, :update]

      resources :rules, only: [:index]

      resources :user_units_images, only: [:destroy]

      # Owner's dashboard
      get "dashbord_owners" => "users#dashbord_owners", as: :dashbord_owners
      get "owners_dashboard" => "users#owners_dashboard", as: :owners_dashboard

      # routes for manual top destination
      resources :top_destinations, only: [:index]

    end
  end
end
