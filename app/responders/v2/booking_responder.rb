module V2::BookingResponder
	extend ActiveSupport::Concern

	included do
		# Api Response using acts_as_api
	  api_accessible :v2_my_orders do |t|
	    t.add :id
	    t.add :check_in
	    t.add :check_out
	    t.add :payment_status
	    t.add :lease_status
	    t.add :total_amount
	    t.add :booking_cancelled
	    t.add :created_at
	    t.add :feedback
	  end

	  api_accessible :v2_unit_with_bookings, extend: :v2_my_orders do |t|
	    t.add :unit, :template => :unit_with_pictures_for_myorders
	  end

	  api_accessible :v2_my_order_detail, extend: :v2_my_orders do |t|
	    t.add :guests
	    t.add :for_days, as: :rented_for_days
	    t.add :sub_total_amount
	  end

	  api_accessible :v2_unit_with_bookings_extra, extend: :v2_my_order_detail do |t|
	    t.add :unit, :template => :unit_with_pictures_for_myorders
	  end

	  api_accessible :v2_booking_with_payment_txns, extend: :v2_unit_with_bookings_extra do |t|
	    t.add :payment_txn, :template => :payment_transaction
	  end
	end
end