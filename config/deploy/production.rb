set :branch, 'master'
set :keep_releases, 3


server '52.74.23.102',
  user: 'ubuntu',
  roles: %w{web app db},
ssh_options: {
  user: 'ubuntu', # overrides user setting above
  keys: %w(~/.ssh/id_rsa),
  forward_agent: false,
  auth_methods: %w(publickey)
  # password: 'please use keys'
}

role :resque_worker, "52.74.23.102"
role :resque_scheduler, "52.74.23.102"
set :resque_rails_env, 'production'
