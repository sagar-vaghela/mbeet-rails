# == Schema Information
#
# Table name: amenities
#
#  id                :integer          not null, primary key
#  name_translations :jsonb            not null
#  is_active         :boolean          default(TRUE), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Amenity < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  # ------------------- Associations --------------------- #
  has_and_belongs_to_many :units

  # Without this you can't access jsonb column correctly with locale
  translates :name

  default_scope { where(is_active: true) }

  # ------------------ Mobile Api Response --------------- #
  api_accessible :amenities_and_units do |t|
    t.add :id
  end

  api_accessible :list_amenities do |t|
    t.add :id
    t.add lambda{|amenity| amenity.name.capitalize  }, :as => :name
    t.add lambda{|amenity| amenity.name_translations['en'].capitalize  }, :as => :name_en
    t.add lambda{|amenity| amenity.name_translations['ar'].capitalize  }, :as => :name_ar
  end

end
