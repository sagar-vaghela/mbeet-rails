class Api::Admin::V6::BaseController < Api::Admin::V5::BaseController
  def getCurrentUserPermission
    @user = User.find(@current_user)
    render_data({ user_permissions: @user.as_api_response(:user_permissions) }, "user permission") if @user
  end
end
