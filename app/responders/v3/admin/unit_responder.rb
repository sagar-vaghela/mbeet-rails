module V3::Admin::UnitResponder
	extend ActiveSupport::Concern

	included do
		# --------------------- Admin Api Response --------------------- #
	  api_accessible :v3_admin_unit_only do |t|
	    t.add :id
	    t.add lambda{|unit| unit.title_en }, :as => :title_en
	    t.add lambda{|unit| unit.title_ar }, :as => :title_ar
	    t.add lambda{|unit| unit.body_en }, :as => :body_en
	    t.add lambda{|unit| unit.body_ar }, :as => :body_ar
	    # t.add :room_type
	    t.add :number_of_guests
	    t.add :number_of_rooms
	    t.add :number_of_beds
	    t.add :number_of_baths
	    t.add :available_as
	    t.add :city, :template => :cities_list
	    t.add :address
	    t.add :price
	    t.add :service_charge
	    t.add :latitude
	    t.add :longitude
	    t.add :unit_class
	    t.add :unit_type
			# t.add lambda{|unit| case unit.unit_type; when 1 then "Resort"; when 2 then "Villa"; when 3 then "Apartment"; when 4 then "Shaleeh"; when 5 then "Private Room"; when 6 then "Shared Room"; end }, :as => :unit_name
	    t.add :total_area
	    t.add :created_at
	    t.add :avg_star
	    t.add :soft_delete
			t.add :unit_status
			t.add :unit_enabled
	    t.add :feedback_count
	    t.add :liked_by_user
	    t.add :user, :template => :user_for_unit, as: 'owner'
	    # t.add :user, :template => :user_for_unit
	    t.add :default_image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :amenities, :template => :amenities_and_units
	    t.add :rules, :template => :rules_and_units
			t.add :mbeet_percentage
	  end

		# Used for booking listing and detail
	  # Used as template under Booking model
	  api_accessible :unit_for_bookings do |t|
	    t.add :id
	    t.add :title
	    t.add :address
	    t.add :latitude
	    t.add :longitude
	    t.add :price
	    t.add :service_charge
	  end

		api_accessible :unit_with_pictures_admin_booking_list, extend: :unit_for_bookings do |t|
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end
	end
end
