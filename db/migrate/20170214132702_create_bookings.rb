class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.references :user, foreign_key: true, null: false
      t.references :unit, foreign_key: true, null: false

      t.timestamp :check_in, null: false
      t.timestamp :check_out, null: false

      # 1 = Credit Card
      t.integer :payment_method, :limit => 2, null: false, default: 1

      # 0 = Pending, 1 = Processing, 2 = Done, 3 = Failed
      t.integer :payment_status, :limit => 2, null: false, default: 0

      # 0 = Expired, 1 = Renewed, 2 = Active, 3 = Cancelled
      t.integer :lease_status, :limit => 2, null: false, default: 2

      t.timestamps
    end
  end
end
