class Api::Admin::V5::PeriodsController < Api::Admin::V1::BaseController
  before_action :admin_login_required
  before_action :find_user_units, except: [:destroy, :index, :update, :show, :get_total_price, :multiple_create, :create]
  before_action :find_target_user_unit, except: [:destroy, :update, :show, :all_periods, :show]
  before_action :set_period, except: [:index, :create, :multiple_create, :all_periods, :get_total_price]

	def index
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]

    @periods = @unit.periods

    search = @periods.all.search do
      fulltext params[:search]
      with(:unit_id, params[:unit_id])
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end
      paginate(:offset => params[:offset], :per_page => limit)
    end

    render_data({ totalPeriods: @unit.periods.count, result_total: search.total, all_periods: search.results.as_api_response(:periods_list) }, I18n.t('success') )
	end

	def all_periods
		all_periods = []
		@user_units.each do |unit|
		  unit.periods.each do |period|
		  	period_hash = {}
		    period_hash["id"] = period.id
		    period_hash["unit_id"] = period.unit.id
		    period_hash["period_date"] = period.period_date
		    period_hash["price"] = period.price
		    all_periods.push(period_hash)
		  end
		end
		render json: { all_periods: all_periods}
	end

	def show
  	render json:{
  	      period: {
  	      		period_id: @period.id,
  	            period_date: @period.period_date,
  	            price: @period.price,
  	        }
  	}
	end

  def multiple_create
  	message = []
  	error = []

    if params[:selected_year] && params[:selected_days]

      @selected_year = params[:selected_year].to_i
      @selected_days = []
      params[:selected_days].each do |sd|
        @selected_days << sd.to_i
      end

      if Time.current.year == @selected_year
        start_date = Date.today # your start
      else
        start_date = Date.new(@selected_year) # your start
      end
      end_date = start_date.end_of_year #  end of year

      my_days = @selected_days # day of the week in 0-6. Sunday is day-of-week 0; Saturday is day-of-week 6.
      result = (start_date.to_date..end_date.to_date).to_a.select {|k| my_days.include?(k.wday)}

      result.each do |date|
        @ex_period = @unit.periods.find_by(period_date: date)
    	  if @ex_period.present?
    	    to_send_hash = _update_period
    	    message.push(to_send_hash[:message])
          error.push(to_send_hash[:error])
    	  else
    	    @created_period = _create_period(date)
    	    message.push(@created_period[:message])
    	    error.push(@created_period[:error])
    	  end
      end

    end



  	@period_from = params[:from]
  	@period_to = params[:to].present? ? params[:to] : params[:from]

  	@period_price = params[:price]
  	if (@period_from > @period_to)
  	  render json: { message: I18n.t('errors.e_422'),
  	          error: { error: I18n.t('period_errors.invalid_date')} }
  	  return false
  	end
    if @period_from.present? && @period_to.present?
    	(@period_from.to_date..@period_to.to_date).each do |date|
    	  @exsisting_period = @unit.periods.find_by(period_date: date)
    	  if @exsisting_period.present?
    	    to_send_hash = _update_period
    	    message.push(to_send_hash[:message])
          error.push(to_send_hash[:error])
    	  else
    	    @created_period = _create_period(date)
    	    message.push(@created_period[:message])
    	    error.push(@created_period[:error])
    	  end
    	end
    end

	  render json: {message: message,error: error }
	end

	def create
  	@exsisting_period = @unit.periods.find_by(period_date: params[:period_date])
  	if @exsisting_period.present?
  	  to_send_hash = _update_period
  	  messages = to_send_hash
  	else
  	  @created_period = _create_period(params[:period_date])
  	  messages = @created_period[:message]
  	  errors = @created_period[:error]
  	end
  	render json: {
  	  message: messages,
  	  error: errors
  	}
	end

	def _create_period(date)

    @user_period = @unit.periods.new()
    @user_period.period_date = date
    @user_period.price = params[:price]
    if @user_period.save!
      message = {id: @user_period.id,
                 period_date: @user_period.period_date.to_s,
                 price: @user_period.price, message: I18n.t('admin.period.created')}
    else
      error = {period_date: @user_period.period_date.to_s,
               message: I18n.t('errors.e_422')}
    end
    return {message: message, error: error}
	end

	def _update_period
    if @exsisting_period != nil
      if @exsisting_period.update!(price: params[:price])
        message = {id: @exsisting_period.id, period_date: @exsisting_period.period_date,price: @exsisting_period.price, message: I18n.t('admin.period.updated')}
      else
        error = {period_date: @exsisting_period.period_date.to_s,message: @exsisting_period.errors.full_messages.join(', ')}
        return {message: message, error: error}
      end
    else
      if @ex_period.update!(price: params[:price])
        message = {id: @ex_period.id, period_date: @ex_period.period_date,price: @ex_period.price, message: I18n.t('admin.period.updated')}
      else
        error = {period_date: @ex_period.period_date.to_s,message: @ex_period.errors.full_messages.join(', ')}
        return {message: message, error: error}
      end
    end
	end

	def update
		@period_date = params[:period_date] if params[:period_date].present?
		@price = params[:price] if params[:price].present?
		if @period.update!( price: @price , period_date: @period_date )
		  render json: {
		      message: I18n.t('admin.period.updated')
		  }
		else
		  render json: {
		        message: I18n.t('errors.e_422'),
		        errors: {error: @period.errors.details}
		    }
		end
	end

	def destroy
		if @period.destroy
		  render json: {
		      message: I18n.t('admin.period.deleted')
		  }
		else
		  render json: {
		      message: I18n.t('errors.e_422')
		  }
		end
	end

	def get_total_price
		sum_array = []
		all_message = []
		(params[:from_date].to_datetime..params[:to_date].to_datetime).each do |date|
		  valid_period = @unit.periods.find_by(period_date: date)
		  if valid_period.present?
		    price = valid_period.price
		    message = "Special price " + price.to_s + " for " + date
		  else
		    price = @unit.price
		    message = "Unit price " + @unit.price.to_s + " for " + date
		  end
		  sum_array.push(price)
		  all_message.push(message)
		end
		grand_total = sum_array.sum
		render json: {
		              total_price: grand_total.to_f,
		              message: all_message
		  }
	end

	private
	def find_user_units
		@user_units = Unit.where(user_id: params[:user_id])
		if params[:user_id].blank?
		  render json: {
		      message: I18n.t('period_errors.invalid_user')
		  }
		end
	end

	def find_target_user_unit
		@unit = Unit.find_by(id: params[:unit_id])
		if @unit.blank?
		  render json: {
		      message: I18n.t('period_errors.invalid_unit')
		  }
		end
	end

	def set_period
		@period = Period.find_by(id: params[:id])
		if @period.blank?
	      render json: {
	          message: I18n.t('period_errors.invalid_period')
	    }
    end
	end
end
