# config valid only for current version of Capistrano
lock "3.10.1"

set :application, "mbeet"
# set :repo_url, "git@bitbucket.org:clickapps2015/mbeet_api.git"
set :repo_url, "git@bitbucket.org:mbeet/mbeet_api_test.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default3.9.1 deploy_to directory is /var/www/my_app_name
set :deploy_to, "/var/www/html/mbeet"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, "config/database.yml", "config/secrets.yml", "config/puma.rb"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system", "public/uploads"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

# Define which type of RVM the server is using
set :rvm_type, :user

# Define ruby version to be used for this application alongwith the gemset
set :rvm_ruby_version, '2.4.0@mbeet --create'

set :puma_conf, "#{shared_path}/config/puma.rb"

namespace :deploy do

  desc "Generate Redis Keys for User liked Units for old record"
  task :generate_keys do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'existing_user_like_keys:create'
        end
      end
    end
  end

  before 'check:linked_files', 'puma:config'
  #before 'check:linked_files', 'puma:nginx_config'
  #after 'puma:smart_restart', 'nginx:restart'
  after  'deploy:finished', 'generate_keys'
end

# solr tasks
namespace :solr do

  desc "start solr sunspot server"
  task :start do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:start"
        end
      end
    end
  end

  desc "stop solr sunspot server"
  task :stop do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:stop"
          # execute "rm -rf #{shared_path}/solr/data"
        end
      end
    end
  end

  desc "reindex the whole database"
  task :reindex do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:reindex"
        end
      end
    end
  end

  desc "start & reindex and stop solr and start again"
  task :restart do
    on roles(:app), in: :sequence, wait: 3 do
      %w[stop start reindex].each { |tast| invoke "solr:#{tast}" }
    end
  end

  # automatically stop, start and reindex while deploying
  #before 'deploy:starting', 'solr:stop'
  after  'deploy:finished', 'solr:start'
  after  'deploy:finished', 'solr:reindex'
  after  'deploy:finished', 'puma:restart'
end

set :resque_environment_task, true

#set :workers, { "*" => 1 }

# set :workers, {
#   "emails"              => 2,
#   "notify_user"         => 3,
#   "notifications_push"  => 3,
#   "messages"            => 1
# }

set :interval, "0.1"
set :resque_log_file, "log/resque.log"

after "deploy:restart",  "resque:restart"
after "deploy:finished", "resque:restart"
after "deploy:finished", "resque:scheduler:restart"
