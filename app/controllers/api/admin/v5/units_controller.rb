class Api::Admin::V5::UnitsController < Api::Admin::V4::UnitsController
  before_action :admin_login_required
  before_action :check_unit, except: [:index, :create]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy
  before_action :check_booking, only: :disable

  # GET /admin/v5/units
  def index

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = Unit.search do
      data_accessor_for(Unit).include = [:city, :user]
      fulltext params[:search]
      with(:city_id, params[:city_id]) if params[:city_id].present?
      if params[:unit_status].present?
        status = params[:unit_status] == 'true' ? true : false
        with(:unit_status, status)
      end

      if params[:unit_enabled].present?
        status = params[:unit_enabled] == 'true' ? true : false
        with(:unit_enabled, status)
      end

      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      with(:number_of_guests, params[:number_of_guests]) if params[:number_of_guests].present?
      with(:number_of_rooms, params[:number_of_rooms] ) if params[:number_of_rooms].present?
      with(:number_of_beds, params[:number_of_beds] )   if params[:number_of_beds].present?
      with(:number_of_baths,  params[:number_of_baths] )  if params[:number_of_baths].present?
      with(:number_of_subunits, params[:number_of_subunits]) if params[:number_of_subunits].present?
      with(:unit_class,   params[:unit_class])   if params[:unit_class].present?
      with(:unit_type,    params[:unit_type])    if params[:unit_type].present?
      with(:bed_type,    params[:bed_type])    if params[:bed_type].present?
      with(:available_as, params[:available_as]) if params[:available_as].present?
      with(:price).greater_than_or_equal_to(params[:starting_price]) if params[:starting_price].present?
      with(:price).less_than_or_equal_to(params[:closing_price]) if params[:starting_price].present?
      with(:amenities).any_of([params[:amenities]]) if params[:amenities].present?
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end

      paginate(:offset => params[:offset], :per_page => limit)

    end
    render_data({ total: Unit.all.size, result_total: search.total, units: search.results.as_api_response(:v5_admin_unit_only)}, I18n.t('unit.list'))
  end

  # GET /admin/v5/units/:id
  def show
    render_data({ units: @unit.as_api_response(:v5_admin_unit_only) }, I18n.t('unit.detail')) if @unit
  end

  # POST /admin/v5/units
  def create
    @unit = Unit.new(unit_params)
    admin = User.find(@current_user)
    @unit.title_translations = {en: @unit.title_en, ar: @unit.title_ar}
    @unit.body_translations = {en: @unit.body_en, ar: @unit.body_ar}
    # @unit.unit_status = true
    # @unit.unit_enabled =true
    if @unit.save
     render_data({ units: @unit.as_api_response(:v5_admin_unit_only) }, I18n.t('admin.unit.created'))

    else
     render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  # PUT /admin/v5/users/:id
  def update
    admin = User.find(@current_user)
    if @unit.update(unit_params)
      render_data({ units: @unit.as_api_response(:v5_admin_unit_only) }, I18n.t('admin.unit.updated'))
      if @unit.unit_status_previously_changed? && @unit.unit_status == true
        admin.send_email_to_user(@unit)
      end
    else
      render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  # DELETE /admin/v5/units/:id
  def destroy
    render_data({ units: {} }, I18n.t('admin.unit.deleted')) if @unit.update(soft_delete: true)
  end


  def enable
    if @unit.number_of_subunits >= 0
      if params[:start_date].present? && params[:end_date].present? && params[:number_of_disabled_units].present?
        (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
          @res = @unit.unit_disabled_dates.find_by(day_disabled: day_disabled)
          @res.destroy if @res
        end
        if @unit.unit_disabled_dates.count == 0
          @unit.update(unit_enabled: true)
        end
        render_updated ({ message: I18n.t('unit.enabled'),data: { unit: @unit.as_api_response(:v5_unit_pictures) } })
      end
    else
      render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end


  # PUT: admin/v5/units/:id/enable
  # def enable
  #   if @unit.update(unit_enabled: true)
  #     if @unit.number_of_subunits > 0
  #       if params[:start_date].present? && params[:end_date].present? && params[:number_of_disabled_units].present?
  #         (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
  #           @unit.unit_disabled_dates.find_by(
  #                 day_disabled: day_disabled
  #           ).decrement!(:number_of_disabled_units, by = params[:number_of_disabled_units].to_i)
  #         end
  #       else
  #         @unit.unit_disabled_dates.destroy_all
  #       end
  #
  #     else
  #       if params[:start_date].present? && params[:end_date].present?
  #         (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
  #           @unit.unit_disabled_dates.find_by(
  #                 day_disabled: day_disabled
  #           ).destroy
  #         end
  #       else
  #         @unit.unit_disabled_dates.destroy_all
  #       end
  #     end
  #     render_updated ({
  #       message: I18n.t('unit.enabled'),
  #       data: { unit: @unit.as_api_response(:v5_unit_pictures) }
  #     })
  #   else
  #     render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
  #   end
  # end

  # PUT: admin/v5/units/:id/disable
  def disable
    number_of_disabled_units = @unit.number_of_subunits > 0 ? params[:number_of_disabled_units] : 0
    (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
      @unit.unit_disabled_dates.find_or_create_by(
            day_disabled: day_disabled
      ).increment!(:number_of_disabled_units, by = number_of_disabled_units.to_i)
    end
    render_updated ({
      message: I18n.t('unit.disabled'),
      data: { unit: @unit.as_api_response(:v5_unit_pictures) }
    })
  end

  # PUT: admin/v5/units/:id/publish
  def publish
    unit = Unit.find(params[:id])
    admin = User.find(@current_user)
    # if unit_status is true, the flat is published to the app
    if unit.update(unit_status: true, unit_enabled: true)
      render_updated ({
        message: I18n.t('unit.published'),
        data: { unit: unit.as_api_response(:v5_admin_unit_only) }
      })

      admin.send_email_to_user(unit)
    else
      render_unprocessable_entity( message: unit.errors.full_messages.join(', '))
    end
  end

  # PUT /admin/v1/units/:id/restore
  def restore
    render_data({ units: {} }, I18n.t('admin.unit.restored')) if @unit.update(soft_delete: false)
  end

  private

  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar,:number_of_guests, :unit_status, :unit_enabled,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id, :mbeet_percentage, :bed_type,
                  :address, :price, :latitude, :longitude, :unit_class, :unit_type, :number_of_subunits,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @unit = Unit.find(params[:id])
  end

  def rescue_response
  rescue StandardError => e
    error_valid_data(e)
  end

  def delete_liked_unit
    @unit_likes = UserLike.where(unit_id: params[:id])
    @unit_likes.delete_all if @unit_likes.present?
  end

  # check whether the unit is booked or not
	def check_booking
    # ---------------------- check bookings --------------------- #
     check_required_params params, [:start_date, :end_date, :number_of_disabled_units]

     booking = Booking.where(booking_cancelled: false)
     .where(unit_id: params[:id])
     .where(
       "check_in <= '#{params[:end_date]}' AND check_out > '#{params[:start_date]}'"
     ).or(
     Booking.where(
        "check_in >= '#{params[:end_date]}' AND check_out < '#{params[:start_date]}'"
       )
     )
    # ------------------------------------------------------- #
    if params[:number_of_disabled_units].to_i > @unit.number_of_subunits
      error_valid_data(I18n.t('errors.wrong_number_of_disabled_units'))
    else
      if @unit.number_of_subunits == 0
         if booking.present? || count_number_of_disabled_units.any?
           error_valid_data(I18n.t('errors.disabled_or_booked'))
         end
  		elsif booking.present? || @unit.number_of_subunits > 0
        number_of_booked_days = count_number_of_booked_days.nil? || count_number_of_booked_days.empty? ? 0 : count_number_of_booked_days.compact.max
        number_of_disabled_days = count_number_of_disabled_units.nil? || count_number_of_disabled_units.empty? ? 0 : count_number_of_disabled_units.compact.max
        remaining_subunits = (@unit.number_of_subunits - number_of_booked_days - number_of_disabled_days)
        if count_number_of_booked_days.any? || count_number_of_disabled_units.any?
          if remaining_subunits == 0
            error_valid_data(I18n.t('errors.disabled_or_booked'))
          elsif remaining_subunits > 0
            unless remaining_subunits >= params[:number_of_disabled_units].to_i
              error_valid_data(I18n.t('errors.subunits_disabled', subunits: remaining_subunits))
            end
          end
        end
  		elsif params[:start_date].to_s == params[:end_date].to_s
  			error_valid_data(I18n.t('user.booking.same_dates'))
  		end
    end

    # ------------------------------------------------------- #
	end

  def count_number_of_booked_days
    booked_days = @unit.booked_subunits
    .where("day_booked >= '#{params[:start_date].to_datetime}' AND day_booked <= '#{params[:end_date].to_datetime}'")
    .pluck(:count)
  end

  def count_number_of_disabled_units

     count_number_of_disabled_units = @unit.unit_disabled_dates
    .where("day_disabled >= '#{params[:start_date].to_datetime}' AND day_disabled <= '#{params[:end_date].to_datetime}'")
    .pluck(:number_of_disabled_units)

  end
end
