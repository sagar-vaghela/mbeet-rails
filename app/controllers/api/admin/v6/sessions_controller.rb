class Api::Admin::V6::SessionsController < Api::Admin::V5::SessionsController
  before_action :check_null_password

  # POST /admin/v6/login
  def create
    if @user && @user.authenticate(params[:password])
      auth_token = JsonWebToken.encode({user_id: @user.id})
      render_data( { auth_token: auth_token, users: @user.as_api_response(:v6_user_with_permissions) }, I18n.t('admin.user.success') )
    else
      error_data( I18n.t('admin.user.error') )
    end
  end

  private
  def check_null_password
    check_required_params params, [:email, :password]
    return error_valid_data( I18n.t('admin.user.error') ) if params[:email].blank? || params[:password].blank?

    @user = User.where(soft_delete: false).find_by(email: params[:email].downcase)

    if (@user.present?) && (@user.role.name.include?('owner') || @user.role.name.include?('user') )
      error_valid_data( I18n.t('admin.user.wrong') )
    end

  end

end
