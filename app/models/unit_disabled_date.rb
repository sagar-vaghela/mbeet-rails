# == Schema Information
#
# Table name: unit_disabled_dates
#
#  id                       :integer          not null, primary key
#  unit_id                  :integer
#  day_disabled             :datetime
#  number_of_disabled_units :integer          default(0)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class UnitDisabledDate < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  belongs_to :unit

  api_accessible :disabled_dates do |t|
    t.add lambda { |d_unit|
            d_unit.day_disabled.to_date
          }, :as => :day_disabled

    t.add :number_of_disabled_units
    t.add lambda { |d_unit|
            d_unit.unit.number_of_subunits - d_unit.number_of_disabled_units
          }, :as => :number_of_enabled_units
  end
end
