class Api::Admin::V2::CitiesController < Api::Admin::V2::BaseController
  before_action :admin_login_required
  before_action :find_city, only: [:show, :update, :destroy]

  # GET: 'admin/v2/cities'
  def index
    @cities = City.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @cities.present?
			render_data(
        { cities: @cities.as_api_response(:v3_city_pictures_admin) },
        I18n.t('city.cities_list')
      ) if @cities.present?
		else
			render_no_data
		end
  end

  # GET: 'admin/v2/cities/:id'
  def show
		render_data(
      { cities: @city.as_api_response(:v3_city_pictures_admin) },
      I18n.t('city.city_detail')
    ) if @city.present?
	end

  # POST: 'admin/v2/cities'
  def create
    check_required_params params, [:name_en, :name_ar]
    city = City.new city_params
    city.name_translations = {en: city.name_en, ar: city.name_ar}
    if city.save
     render_created ({
       message: I18n.t('city.created'),
       data: { city: city.as_api_response(:v3_city_pictures_admin) }
     })
    else
     render_unprocessable_entity( message: city.errors.full_messages.join(', '))
    end
  end

  # PUT: 'admin/v2/cities/:id'
  def update
    @city.name_translations = {en: @city.name_en, ar: @city.name_ar}
    if  @city.update city_params
     render_updated ({
       message: I18n.t('city.updated'),
       data: { city: @city.as_api_response(:v3_city_pictures_admin) }
     })
   else
     render_unprocessable_entity( message: @city.errors.full_messages.join(', '))
   end
  end

  # DELETE: 'admin/v2/cities/:id'
  def destroy
    if  @city.update(soft_delete: true)
     render_deleted ({
       message: I18n.t('city.deleted'),
       data: { city: @city.as_api_response(:v3_city_pictures_admin) }
     })
   else
     render_unprocessable_entity( message: @city.errors.full_messages.join(', '))
   end
  end


  ##----------------------------- Private Methods ----------------------------##

  private
  # Order cities ASC based on the name
  def get_order
  	"name_translations->>'#{I18n.locale}' ASC"
  end

  # Find a specific city
  def find_city
    @city = City.find( params[:id] )
  end

  # Define which city parameters are allowed from external applications/PL
  # to perform creation /updation
  def city_params
    params.permit(:name_en, :name_ar, :published, :soft_delete, :picture_attributes => [:id, :name, :_destroy])
  end

  ##--------------------------------------- end ------------------------------##
end
