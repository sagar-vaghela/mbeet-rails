class Api::Admin::V6::RolesController < Api::Admin::V5::RolesController
	before_action :admin_login_required
	before_action :check_role, only: [:show, :destroy, :update]
	before_action :find_current_user

	# GET /admin/v6/roles
	def index
		impressionist(@user, request.path) if @user

		limit = params[:limit].to_i == -1 ? 100 : params[:limit]
		@roles = Role.all

		search = @roles.search do
			fulltext params[:search]
			if params[:sort_by].present? && params[:sort_direction].present?
				order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
			else
				order_by(:name, :desc)
			end
			paginate(:offset => params[:offset], :per_page => limit)
		end
		render_data({ total: @roles.count, result_total: search.total, roles: search.results.as_api_response(:for_admin_show_roles)}, I18n.t('admin.roles.list')) if @roles
	end

	# POST: 'admin/v6/role'
	def create
		impressionist(@user, request.path) if @user

		@role = Role.new role_params
		result = {}

		if params[:selected_dashboard_permissions].present?
			view = params[:selected_dashboard_permissions].include? 'view'
			a1 = {
				dashboard_permissions: {
					view: view
				}
			}
			result = result.merge **a1
		else
			a1 = {
				dashboard_permissions: {
					view: false,
				}
			}
			result = result.merge **a1
		end

		if params[:selected_users_permissions].present?
			view = params[:selected_users_permissions].include? ("view")
			create = params[:selected_users_permissions].include? ("create")
			edit = params[:selected_users_permissions].include? ("edit")
			delete = params[:selected_users_permissions].include? ("delete")
			show = params[:selected_users_permissions].include? ("show")
			restore = params[:selected_users_permissions].include? ("restore")
			a2 = {
				users_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete,
					show: show,
					restore: restore
				}
			}
			result = result.merge **a2
		else
			a2 = {
				users_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false,
					show: false,
					restore: false
				}
			}
			result = result.merge **a2
		end

		if params[:selected_booking_permissions].present?
			view = params[:selected_booking_permissions].include? ("view")
			create = params[:selected_booking_permissions].include? ("create")
			edit = params[:selected_booking_permissions].include? ("edit")
			delete = params[:selected_booking_permissions].include? ("delete")
			cancel = params[:selected_booking_permissions].include? ("cancel")
			show = params[:selected_booking_permissions].include? ("show")

			a3 = {
				booking_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete,
					cancel: cancel,
					show: show
				}
			}
			result = result.merge **a3
		else
			a3 = {
				booking_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false,
					cancel: false,
					show: false
				}
			}
			result = result.merge **a3
		end

		if params[:selected_unit_permissions].present?
			view = params[:selected_unit_permissions].include? ("view")
			create = params[:selected_unit_permissions].include? ("create")
			edit = params[:selected_unit_permissions].include? ("edit")
			show = params[:selected_unit_permissions].include? ("show")
			trash = params[:selected_unit_permissions].include? ("trash")
			period = params[:selected_unit_permissions].include? ("special_price")
			restore = params[:selected_unit_permissions].include? ("restore")
			disableSubUnit = params[:selected_unit_permissions].include? ("disable")
			enableSubUnit = params[:selected_unit_permissions].include? ("enable")
			publish = params[:selected_unit_permissions].include? ("publish")
			a4 = {
				unit_permissions: {
					view: view,
					create: create,
					edit: edit,
					show: show,
					trash: trash,
					period: period,
					restore: restore,
					disableSubUnit: disableSubUnit,
					enableSubUnit: enableSubUnit,
					publish: publish
				}
			}
			result = result.merge **a4
		else
			a4 = {
				unit_permissions: {
					view: false,
					create: false,
					edit: false,
					show: false,
					trash: false,
					period: false,
					restore: false,
					disableSubUnit: false,
					enableSubUnit: false,
					publish: false
				}
			}
			result = result.merge **a4
		end

		if params[:selected_special_price_permissions].present?
			view = params[:selected_special_price_permissions].include? ("view")
			create = params[:selected_special_price_permissions].include? ("create")
			edit = params[:selected_special_price_permissions].include? ("edit")
			delete = params[:selected_special_price_permissions].include? ("delete")
			a5 = {
				special_price_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a5
		else
			a5 = {
				special_price_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a5
		end

		if params[:selected_coupon_permissions].present?
			view = params[:selected_coupon_permissions].include? ('view')
			create = params[:selected_coupon_permissions].include? ('create')
			edit = params[:selected_coupon_permissions].include? ('edit')
			delete = params[:selected_coupon_permissions].include? ('delete')
			a6 = {
				coupon_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a6
		else
			a6 = {
				coupon_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a6
		end

		if params[:selected_city_permissions].present?
			view = params[:selected_city_permissions].include? ('view')
			create = params[:selected_city_permissions].include? ('create')
			edit = params[:selected_city_permissions].include? ('edit')
			delete = params[:selected_city_permissions].include? ('delete')

			a7 = {
				city_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a7
		else
			a7 = {
				city_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a7
		end

		if params[:selected_role_permissions].present?
			view = params[:selected_role_permissions].include? ('view')
			create = params[:selected_role_permissions].include? ('create')
			edit = params[:selected_role_permissions].include? ('edit')
			show = params[:selected_role_permissions].include? ('show')

			a8 = {
				role_permissions: {
					view: view,
					create: create,
					edit: edit,
					show: show
				}
			}
			result = result.merge **a8
		else
			a8 = {
				role_permissions: {
					view: false,
					create: false,
					edit: false,
					show: false
				}
			}
			result = result.merge **a8
		end

		if params[:selected_topDestination_permissions].present?
			view = params[:selected_topDestination_permissions].include? ('view')
			create = params[:selected_topDestination_permissions].include? ('create')
			edit = params[:selected_topDestination_permissions].include? ('edit')
			delete = params[:selected_topDestination_permissions].include? ('delete')

			a9 = {
				top_destination_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a9
		else
			a9 = {
				top_destination_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a9
		end

		# @rp.permitted_actions = result
		# @rp.role_id = @role.id

		if @role.save
			@rp = RolePermission.new(permitted_actions: result, role_id: @role.id)
			@rp.save
			render_created ({
				message: I18n.t('admin.roles.created'),
				data: { roles: @role.as_api_response(:for_admin_show_roles) }
				})
		else
			render_unprocessable_entity( message: @role.errors.full_messages.join(', '))
		end
	end

	# GET /admin/v6/roles/:id
	def show
		impressionist(@user, request.path) if @user

		render_data({ roles: @role.as_api_response(:for_admin_show_roles) }, I18n.t('role.detail')) if @role
	end

	# PUT /admin/v6/roles/:id
	def update
		impressionist(@user, request.path) if @user

		@role.update(name: params[:name]) if params[:name]
		result = {}

		if params[:selected_dashboard_permissions].present?
			view = params[:selected_dashboard_permissions].include? 'view'
			a1 = {
				dashboard_permissions: {
					view: view
				}
			}
			result = result.merge **a1
		else
			a1 = {
				dashboard_permissions: {
					view: false,
				}
			}
			result = result.merge **a1
		end

		if params[:selected_users_permissions].present?
			view = params[:selected_users_permissions].include? ("view")
			create = params[:selected_users_permissions].include? ("create")
			edit = params[:selected_users_permissions].include? ("edit")
			delete = params[:selected_users_permissions].include? ("delete")
			show = params[:selected_users_permissions].include? ("show")
			restore = params[:selected_users_permissions].include? ("restore")
			a2 = {
				users_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete,
					show: show,
					restore: restore
				}
			}
			result = result.merge **a2
		else
			a2 = {
				users_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false,
					show: false,
					restore: false
				}
			}
			result = result.merge **a2
		end

		if params[:selected_booking_permissions].present?
			view = params[:selected_booking_permissions].include? ("view")
			create = params[:selected_booking_permissions].include? ("create")
			edit = params[:selected_booking_permissions].include? ("edit")
			delete = params[:selected_booking_permissions].include? ("delete")
			cancel = params[:selected_booking_permissions].include? ("cancel")
			show = params[:selected_booking_permissions].include? ("show")

			a3 = {
				booking_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete,
					cancel: cancel,
					show: show
				}
			}
			result = result.merge **a3
		else
			a3 = {
				booking_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false,
					cancel: false,
					show: false
				}
			}
			result = result.merge **a3
		end

		if params[:selected_unit_permissions].present?
			view = params[:selected_unit_permissions].include? ("view")
			create = params[:selected_unit_permissions].include? ("create")
			edit = params[:selected_unit_permissions].include? ("edit")
			show = params[:selected_unit_permissions].include? ("show")
			trash = params[:selected_unit_permissions].include? ("trash")
			period = params[:selected_unit_permissions].include? ("special_price")
			restore = params[:selected_unit_permissions].include? ("restore")
			disableSubUnit = params[:selected_unit_permissions].include? ("disable")
			enableSubUnit = params[:selected_unit_permissions].include? ("enable")
			publish = params[:selected_unit_permissions].include? ("publish")
			a4 = {
				unit_permissions: {
					view: view,
					create: create,
					edit: edit,
					show: show,
					trash: trash,
					period: period,
					restore: restore,
					disableSubUnit: disableSubUnit,
					enableSubUnit: enableSubUnit,
					publish: publish
				}
			}
			result = result.merge **a4
		else
			a4 = {
				unit_permissions: {
					view: false,
					create: false,
					edit: false,
					show: false,
					trash: false,
					period: false,
					restore: false,
					disableSubUnit: false,
					enableSubUnit: false,
					publish: false
				}
			}
			result = result.merge **a4
		end

		if params[:selected_special_price_permissions].present?
			view = params[:selected_special_price_permissions].include? ("view")
			create = params[:selected_special_price_permissions].include? ("create")
			edit = params[:selected_special_price_permissions].include? ("edit")
			delete = params[:selected_special_price_permissions].include? ("delete")
			a5 = {
				special_price_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a5
		else
			a5 = {
				special_price_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a5
		end

		if params[:selected_coupon_permissions].present?
			view = params[:selected_coupon_permissions].include? ('view')
			create = params[:selected_coupon_permissions].include? ('create')
			edit = params[:selected_coupon_permissions].include? ('edit')
			delete = params[:selected_coupon_permissions].include? ('delete')
			a6 = {
				coupon_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a6
		else
			a6 = {
				coupon_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a6
		end

		if params[:selected_city_permissions].present?
			view = params[:selected_city_permissions].include? ('view')
			create = params[:selected_city_permissions].include? ('create')
			edit = params[:selected_city_permissions].include? ('edit')
			delete = params[:selected_city_permissions].include? ('delete')

			a7 = {
				city_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a7
		else
			a7 = {
				city_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a7
		end

		if params[:selected_role_permissions].present?
			view = params[:selected_role_permissions].include? ('view')
			create = params[:selected_role_permissions].include? ('create')
			edit = params[:selected_role_permissions].include? ('edit')
			show = params[:selected_role_permissions].include? ('show')

			a8 = {
				role_permissions: {
					view: view,
					create: create,
					edit: edit,
					show: show
				}
			}
			result = result.merge **a8
		else
			a8 = {
				role_permissions: {
					view: false,
					create: false,
					edit: false,
					show: false
				}
			}
			result = result.merge **a8
		end

		if params[:selected_topDestination_permissions].present?
			view = params[:selected_topDestination_permissions].include? ('view')
			create = params[:selected_topDestination_permissions].include? ('create')
			edit = params[:selected_topDestination_permissions].include? ('edit')
			delete = params[:selected_topDestination_permissions].include? ('delete')

			a9 = {
				top_destination_permissions: {
					view: view,
					create: create,
					edit: edit,
					delete: delete
				}
			}
			result = result.merge **a9
		else
			a9 = {
				top_destination_permissions: {
					view: false,
					create: false,
					edit: false,
					delete: false
				}
			}
			result = result.merge **a9
		end

		if @role.role_permission.present?
			if @role.role_permission.update(permitted_actions: result)
				render_data({ roles: @role.as_api_response(:for_admin_show_roles) }, I18n.t('admin.role.updated'))
			else
				render_unprocessable_entity( message: @role.errors.full_messages.join(', '))
			end
		else
			if RolePermission.create(permitted_actions: result, role_id: @role.id)
				render_data({ roles: @role.as_api_response(:for_admin_show_roles) }, I18n.t('admin.role.updated'))
			else
				render_unprocessable_entity( message: @role.errors.full_messages.join(', '))
			end
		end
	end

	# DELETE: 'admin/v6/roles/1'
	# def destroy
	# 	render_data({ roles: {} }, I18n.t('admin.role.deleted')) if @role.destroy!
	# end

	private
	def role_params
		params.permit(:name)
	end

	def check_role
		@role = Role.find(params[:id])
	end

	def find_current_user
    @user = User.find_by(id: @current_user, role_id: [1, 2])
  end
end
