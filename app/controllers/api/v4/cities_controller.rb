class Api::V4::CitiesController < Api::V3::CitiesController
  before_action :login_required
	before_action :find_city, only: :show

	def index
		@cities = City.published.not_deleted.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @cities.present?
			render_data({ cities: @cities.as_api_response(:v3_city_pictures) }, I18n.t('city.cities_list') ) if @cities.present?
		else
			render_no_data
		end
	end

	def show
		render_data({ cities: @city.as_api_response(:v3_city_pictures) }, I18n.t('city.city_detail') ) if @city.present?
	end

  # display all published and unpublished cities
  def published_and_unpublished
    @cities = City.not_deleted.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @cities.present?
			render_data({ cities: @cities.as_api_response(:v3_published_and_unpublished) }, I18n.t('city.cities_list') ) if @cities.present?
		else
			render_no_data
		end
  end


	##----------------------------- Private Methods ----------------------------##

	private
	def get_order
		"name_translations->>'#{I18n.locale}' ASC"
	end

  # Find a specific city
  def find_city
    @city = City.published.not_deleted.find( params[:id] )
  end

  def user_is_present?
    (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
  end

  def login_required
    if %w(index show published_and_unpublished).include?(params[:action]) &&  user_is_present?
      super
    end
  end


	##--------------------------------------- end ------------------------------##
end
