# == Schema Information
#
# Table name: cities
#
#  id                :integer          not null, primary key
#  name_translations :jsonb            not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  published         :boolean          default(FALSE)
#  soft_delete       :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe City, type: :model do
  let(:city) { FactoryBot.create(:city) }

  it 'has a valid factory of city' do
    expect(city).to be_valid
  end

  context 'association' do
    it { expect(city).to have_many :units }
    it { expect(city).to have_one :picture }
  end

end
