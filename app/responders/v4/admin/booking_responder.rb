module V4::Admin::BookingResponder
	extend ActiveSupport::Concern

	included do
		api_accessible :v4_booking_list do |t|
	    t.add :id
	    t.add :check_in
	    t.add :check_out
	    t.add :payment_status
	    t.add :lease_status
	    t.add :total_amount
			t.add :advance_payment
	    t.add :booking_cancelled
	    t.add :created_at
	    t.add :feedback
	    t.add :user_booking_cancel_request, :template => :cancel_reason, as: :reason
      # t.add lambda{|booking| booking.user_booking_cancel_request }, :as => :cancellation_reason
	  end

	  api_accessible :v4_unit_with_bookings, extend: :v4_booking_list do |t|
	    t.add :unit, :template => :unit_with_pictures_admin_booking_list
	  end

		api_accessible :v4_booking_with_unit_and_user, extend: :v4_unit_with_bookings do |t|
	    t.add :user, :template => :user_with_booking
	  end

		api_accessible :v4_booking_with_payment_txns, extend: :v4_booking_with_unit_and_user do |t|
		 t.add :payment_txn, :template => :payment_transaction_id
	  end
	end
end
