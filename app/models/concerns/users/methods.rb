module Users
	module Methods
		extend ActiveSupport::Concern

	  # Model methods
	  def refreshed_token
	  	fresh = access_tokens.create token: Digest::SHA1.hexdigest([Time.now, rand].join)
	    fresh.token
	  end

	  # Sets the password reset attributes.
	  def create_reset_digest
	    update_attribute(:reset_digest,  Digest::SHA1.hexdigest([Time.now, rand].join))
	    update_attribute(:reset_sent_on, Time.zone.now)
	  end

	  def send_login_detail_to_user
	  	UserMailer.new_user_login_email(self, generate_random_password).deliver
	  end

	  def send_password_reset_email
	    UserMailer.password_reset(self, generate_random_password).deliver
	  end

	  def resend_verify_email
	  	UserMailer.verify_email(self, generate_confirm_email_token).deliver
	  end

	  def compare_password
	    errors.add(:base, I18n.t('user.password.not_matching')) unless self.password.eql?(self.password_confirmation)
	  end

	  def from_profile_update?
	  	self.phone_changed?
	  end

	  def phone_updated
	  	self.phone_verified = false if self.phone_changed? || self.phone.empty?
	  end

		# send email to tell the admin that user has added new flat
		def send_email_to_admin
			Resque.enqueue(SendEmailToAllAdmins, self.id)
			# UserMailer.user_add_new_flat(self).deliver
		end

		# send nofification to tell the admin that user has added new flat
		def send_notification_to_admin user_unit, new_flat_status
       send_push_obj = SendPush.new(user_unit, new_flat_status)
	     send_push_obj.send_admin_notification
	     send_push_obj.process_in_background(SaveNewFlatAddedByUserAdminNotificationsWorker, user_unit, send_push_obj.new_flat_added.to_json, new_flat_status)
		end

    # changes the user role to be an owner
		def change_user_role_to_owner
			if self.role_id == 1
				update(is_owner: true)
				save! validate: false
			else
				update(is_owner: true, role_id: 2)
				save! validate: false
			end
		end

		# send email to tell the user that new flat has been published to app
		def send_email_to_user(unit)
			UserMailer.user_flat_published(unit).deliver
		end

		# send email to tell all users that new flat has been added by admin
		def send_email_to_all_users(unit)
			UserMailer.admin_add_new_unit(unit).deliver
		end

    # send notification to tell the user that new flat has been published to app
		def send_notification_to_user(unit, push_type)
			send_push_obj = SendPush.new(unit, push_type)
			send_push_obj.user_flat_published_notification
		  send_push_obj.process_in_background(SaveUserNewFlatNotificationsWorker, unit, send_push_obj.flat_published.to_json, push_type)
		end

		# Implement change password functionality
		def change_password password, password_confirmation

			if password == password_confirmation
				update(password: password, password_confirmation: password_confirmation)
			else
				errors.add :base, I18n.t('password.not_matched')
				return false
			end

		end

	  private
	  # This is temporary solution to reset password
	  # later can be removed
	  def generate_random_password
	    random = SecureRandom.hex(3)
	    update_attribute(:password, random)
	    self.random_password = random
	  end

	  def generate_confirm_email_token
	  	token = Digest::SHA1.hexdigest([Time.now, rand].join)
	  	update_attribute(:confirm_email_token, token)
	  	update_attribute(:verify_email_sent_at, Time.zone.now)
	  	self.confirm_email_token = token
	  end

	  def downcase_email
	  	self.email = email.downcase if email.present?
	  end

	  def downcase_name
	  	self.name = name.downcase  if name.present?
	  end

	  def phone_exist?
	  	phone.present?
	  end

	  def phone_verified_exist?
	  	phone_verified.nil?
	  end
	end
end
