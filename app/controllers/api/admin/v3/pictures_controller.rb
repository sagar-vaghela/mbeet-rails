class Api::Admin::V3::PicturesController < Api::Admin::V3::BaseController
	before_action :admin_login_required, :images

	def destroy
		@picture = Picture.find(params[:id])
		render_data({ units: {images: [@unit_pictures.as_api_response(:admin_images)]} }, I18n.t('admin.unit.image_deleted')) if @picture.present? && @picture.destroy
	rescue StandardError => e
		error_valid_data(e)
	end

	def images
		@unit_pictures = Picture.find_by(imageable_id: params[:unit_id])
	end
end
