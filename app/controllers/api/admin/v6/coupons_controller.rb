class Api::Admin::V6::CouponsController < Api::Admin::V5::CouponsController
  before_action :admin_login_required
  before_action :set_coupon, only: [:show, :update, :destroy]
  before_action :find_current_user

  # GET /admin/v6/coupons
  def index
    impressionist(@cur_user, request.path) if @cur_user

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    @coupons = Coupon.all

    search = @coupons.search do
      fulltext params[:search]
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end
      paginate(:offset => params[:offset], :per_page => limit)
    end

    render_data({ total: @coupons.count, result_total: search.total, coupons: search.results.as_api_response(:coupon_statastics) }, I18n.t('success') )
  end

  # GET /admin/v6/coupons/1
  def show
    impressionist(@cur_user, request.path) if @cur_user

    render_data( { coupons: @coupon.as_api_response(:coupon_statastics), creator: @coupon.user.as_api_response(:user) }, I18n.t('success') ) if @coupon
  end

  # POST /admin/v6/coupons
  def create
    impressionist(@cur_user, request.path) if @cur_user

    @coupon = Coupon.new(coupon_params)
    @coupon.creator_id = @current_user.id
    if @coupon.save
     render_created ({
       message: I18n.t('coupon.created'),
       data: { coupon: @coupon.as_api_response(:coupons_list) }
     })
    else
     render_unprocessable_entity( message: @coupon.errors.full_messages.join(', '))
    end
  end

  # PATCH/PUT /admin/v6/coupons/1
  def update
    impressionist(@cur_user, request.path) if @cur_user

    if @coupon.update(coupon_params)
      render_created ({
        message: I18n.t('coupon.updated'),
        data: { coupons: @coupon.as_api_response(:coupons_list) }
      })
     else
      render_unprocessable_entity( message: @coupon.errors.full_messages.join(', '))
     end
  end

  # DELETE /admin/v6/coupons/1
  def destroy
    impressionist(@cur_user, request.path) if @cur_user
    render_data({ coupon: {} }, I18n.t('coupon.deleted')) if @coupon.destroy
  end

  # GET /admin/v5/coupons/1/get_statistics
  def get_statistics
    impressionist(@cur_user, request.path) if @cur_user
    @coupon = Coupon.find(params[:coupon_id]) if params[:coupon_id]
    render_data({ coupon: @coupon.as_api_response(:coupon_statastics) }, I18n.t('coupon.statistics')) if @coupon
  end

  private
  def find_current_user
    @cur_user = User.find_by(id: @current_user, role_id: [1, 2])
  end

end
