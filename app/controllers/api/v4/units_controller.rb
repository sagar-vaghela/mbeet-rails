class Api::V4::UnitsController < Api::V3::UnitsController
  before_action :login_required

  # GET /v4/units
  def index
    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
      @units = Unit.is_deleted
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = 0 }
      render_data({ total: Unit.is_deleted.count, units: @units.as_api_response(:v4_unit_pictures)}, I18n.t('unit.list')) if @units
    else
      @units = Unit.is_deleted
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      @units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      render_data({ total: Unit.is_deleted.count, units: @units.as_api_response(:v4_unit_pictures)}, I18n.t('unit.list')) if @current_user.present? && @units
    end
  end

  # GET /v4/units/:id
  def show
    @unit = Unit.is_deleted.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
      render_data({ units: @unit.as_api_response(:v4_unit_pictures) }, I18n.t('unit.detail')) if @unit
    else
      @unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", @unit.id).present? ? 1 : 0 
      render_data({ units: @unit.as_api_response(:v4_unit_pictures) }, I18n.t('unit.detail')) if @unit
    end
  end

  # GET /v4/units/check_if_free
  def check_if_free

    @available_dates = BookedSubunit.where(unit_id: params[:unit_id])
                      .where("day_booked >= '#{params[:current_date_time].to_date}'")
                      .pluck(:day_booked)
    disable_units = UnitDisabledDate.where(unit_id: params[:unit_id])
                      .where("day_disabled >= '#{params[:current_date_time].to_date}'")

    if @available_dates.present? || disable_units.present?
      taken = []
      number_of_subunits = Unit.find(params[:unit_id]).number_of_subunits
      @available_dates.map do |day|
        units_disabled = Unit.find(params[:unit_id]).unit_disabled_dates.find_by(
              day_disabled: day
        )

        units_or_subunits_booked = Unit.find(params[:unit_id]).booked_subunits.find_by(
              day_booked: day
        )

        if units_disabled.present?
          number_of_disabled_days = units_disabled.number_of_disabled_units
        else
          number_of_disabled_days = 0
        end

        if units_or_subunits_booked.present?
          number_of_booked_days = units_or_subunits_booked.count
        else
          number_of_booked_days = 0
        end
        if ( number_of_disabled_days + number_of_booked_days) == number_of_subunits && number_of_subunits > 0
          taken << Date.parse(day.strftime("%Y-%m-%d"))
        elsif number_of_subunits == 0
          taken << Date.parse(day.strftime("%Y-%m-%d"))
        end
      end
      if disable_units.any? 
        taken = taken + disable_units.where(number_of_disabled_units: number_of_subunits).pluck(:day_disabled).map(&:to_date)
      end
      render_data({ taken_dates: taken.flatten.sort, units: {id: params[:unit_id]} }, I18n.t('unit.availability'))
    else
      render_data({ taken_dates: [], units: {} }, I18n.t('unit.availability'))
    end
  end

  private

  def user_is_present?
    (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
  end

  def login_required
    if %w(index show).include?(params[:action]) &&  user_is_present?
      super
    elsif params[:action] == 'check_if_free'
      super
    end
  end
end
