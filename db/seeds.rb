# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

## Populate roles table
# Role.create name: 'super admin'
# Role.create name: 'owner'
# Role.create name: 'normal'


## Populate users table
# 10.times do
#   User.create role_id: 3, name: Faker::Name.unique.name, email: Faker::Internet.email, password: '123456', password_confirmation: '123456',
#                 phone: Faker::PhoneNumber.cell_phone, is_owner: false
# end


## Populate units table
# Unit.create user_id: 8, title_translations: {en: "4bhk home available", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "Lorem ipsum Lorem ipsum Lorem ipsum", ar: "آسيا آسيا آسيا آسيا آسيا"}, number_of_guests: 5, number_of_rooms: 2, number_of_beds: 2, number_of_baths: 2, available_as: 3,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false

# Unit.create user_id: 8, title_translations: {en: "2bhk home available", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "Lorem ipsum Lorem ipsum Lorem ipsum", ar: "آسيا آسيا آسيا آسيا آسيا"}, number_of_guests: 5, number_of_rooms: 2, number_of_beds: 2, number_of_baths: 2, available_as: 3,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false

# Unit.create user_id: 8, title_translations: {en: "Luxury villa moutain view", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "Lorem ipsum Lorem ipsum Lorem ipsum", ar: "آسيا آسيا آسيا آسيا آسيا"}, number_of_guests: 5, number_of_rooms: 2, number_of_beds: 2, number_of_baths: 2, available_as: 3,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false


# Unit.create user_id: 8, title_translations: {en: "Leccio Apartment", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "Our farmhouse is glad to give you hospitality in the Leccio apartment, it is indipendent and there's space for a couple and a third person.", ar: "آسيا آسيا آسيا آسيا آسيا"},
#             number_of_guests: 2, number_of_rooms: 2, number_of_beds: 2, number_of_baths: 2, available_as: 1,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false

# Unit.create user_id: 8, title_translations: {en: "The Cozy Palace", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "Let this be your Unique experience in Marrakech!", ar: "آسيا آسيا آسيا آسيا آسيا"}, number_of_guests: 4, number_of_rooms: 3, number_of_beds: 3, number_of_baths: 3, available_as: 2,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false

# Unit.create user_id: 4, title_translations: {en: "White Breeze Pool 1BD Apartment", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "The apartments share private pool, so there is no crowd and enough privacy", ar: "آسيا آسيا آسيا آسيا آسيا"}, number_of_guests: 2, number_of_rooms: 2, number_of_beds: 2, number_of_baths: 2, available_as: 1,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false

# Unit.create user_id: 4, title_translations: {en: "A quiet yurt in Savoie - Bauges", ar:"آسيا آسيا آسيا آسيا آسيا"},
#             body_translations: {en: "A lot of activities are nearby as trek, mountain bike, climbing and ski", ar: "آسيا آسيا آسيا آسيا آسيا"}, number_of_guests: 4, number_of_rooms: 3, number_of_beds: 3, number_of_baths: 3, available_as: 2,
#             city: Faker::Address.city, address: Faker::Address.street_address + ", " + Faker::Address.city + ", " + Faker::Address.country,
#             price: Faker::Number.decimal(4, 2), service_charge: Faker::Number.decimal(2) ,
#             latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, unit_class: 'a',
#             soft_delete: false


## Populate amenities table
# Amenity.create name_translations: {en: "wi-fi", ar: "واي فاي"}, is_active: true
# Amenity.create name_translations: {en: "pets allowed", ar: "مسموح بدخول الحيوانات الأليفة"}, is_active: true
# Amenity.create name_translations: {en: "air conditioning", ar: "نظام تكييف الهواء"}, is_active: true
# Amenity.create name_translations: {en: "parking", ar: "موقف سيارات"}, is_active: true
# Amenity.create name_translations: {en: "smoking allowed", ar: "مسموح التدخين"}, is_active: true
# Amenity.create name_translations: {en: "kitchen", ar: "مطبخ"}, is_active: true
# Amenity.create name_translations: {en: "tv cable", ar: "تلفزيون الكابل"}, is_active: true
# Amenity.create name_translations: {en: "heating", ar: "تسخين"}, is_active: true


## Populate amenities_units table
# unit = Unit.find(7)
# ame = Amenity.find(1)
# unit.amenities << ame
# ame = Amenity.find(2)
# unit.amenities << ame
# ame = Amenity.find(3)
# unit.amenities << ame

# unit = Unit.find(8)
# ame = Amenity.find(6)
# unit.amenities << ame
# ame = Amenity.find(7)
# unit.amenities << ame
# ame = Amenity.find(8)
# unit.amenities << ame


# unit = Unit.find(9)
# ame = Amenity.find(2)
# unit.amenities << ame
# ame = Amenity.find(4)
# unit.amenities << ame
# ame = Amenity.find(6)
# unit.amenities << ame

# unit = Unit.find(10)
# ame = Amenity.find(1)
# unit.amenities << ame
# ame = Amenity.find(3)
# unit.amenities << ame
# ame = Amenity.find(5)
# unit.amenities << ame
# ame = Amenity.find(7)
# unit.amenities << ame

# unit = Unit.find(11)
# ame = Amenity.find(1)
# unit.amenities << ame
# ame = Amenity.find(2)
# unit.amenities << ame
# ame = Amenity.find(3)
# unit.amenities << ame
# ame = Amenity.find(4)
# unit.amenities << ame


# unit = Unit.find(12)
# ame = Amenity.find(2)
# unit.amenities << ame
# ame = Amenity.find(3)
# unit.amenities << ame
# ame = Amenity.find(4)
# unit.amenities << ame

# unit = Unit.find(13)
# ame = Amenity.find(3)
# unit.amenities << ame
# ame = Amenity.find(4)
# unit.amenities << ame



## Populate bookings table
# Booking.create user_id: 1, unit_id: 2, check_in: Faker::Time.between(DateTime.now, DateTime.now), check_out: Faker::Date.forward(5), payment_method: 1, payment_status: 0,
#                lease_status: 0
# Booking.create user_id: 1, unit_id: 3, check_in: Faker::Time.between(DateTime.now + 4, DateTime.now), check_out: Faker::Time.forward(23, :morning), payment_method: 1, payment_status: 0,
#               lease_status: 0


## Populate payment_txns table
# PaymentTxn.create booking_id: 1, payment_status: 0, invoice_unique_id: Faker::Bank.swift_bic, modified_on: Faker::Time.between(DateTime.now, DateTime.now)
# PaymentTxn.create booking_id: 2, payment_status: 0, invoice_unique_id: Faker::Bank.swift_bic, modified_on: Faker::Time.between(DateTime.now, DateTime.now)


## Populate feedbacks table
# Feedback.create user_id: 1, unit_id: 2, star: 3.5, feedback: "Nice service but they can improve more"
# Feedback.create user_id: 2, unit_id: 2, star: 5.0, feedback: "Over all nice service and vibes would recommed to all"
# Feedback.create user_id: 3, unit_id: 2, star: 3.0, feedback: "Place was neet and clean"
# Feedback.create user_id: 4, unit_id: 2, star: 0.0, feedback: "No happy with the service"
# Feedback.create user_id: 1, unit_id: 3, star: 3.5, feedback: "Food was excellent"

## Populate user_likes table
# UserLike.create user_id: 1, unit_id: 2
# UserLike.create user_id: 1, unit_id: 3
# UserLike.create user_id: 1, unit_id: 4
# UserLike.create user_id: 1, unit_id: 5
# UserLike.create user_id: 1, unit_id: 6
# UserLike.create user_id: 2, unit_id: 2
# UserLike.create user_id: 2, unit_id: 3
# UserLike.create user_id: 3, unit_id: 2
# UserLike.create user_id: 3, unit_id: 3
# UserLike.create user_id: 5, unit_id: 2
# UserLike.create user_id: 6, unit_id: 2


## Populate user_lang_settings table
# UserLangSetting.create user_id: 1, language: "en"
# UserLangSetting.create user_id: 2, language: "en"
# UserLangSetting.create user_id: 3, language: "en"
# UserLangSetting.create user_id: 4, language: "en"
# UserLangSetting.create user_id: 5, language: "en"
# UserLangSetting.create user_id: 6, language: "en"
# UserLangSetting.create user_id: 7, language: "en"
# UserLangSetting.create user_id: 8, language: "en"
# UserLangSetting.create user_id: 9, language: "en"
# UserLangSetting.create user_id: 10, language: "en"


## Populate pictures table
# unit = Unit.find(3)
# unit.pictures << Picture.new(name: "v1488342364/mbeet/dev/home4.jpg")
# unit.pictures << Picture.new(name: "v1488342369/mbeet/dev/Houses_in_Hail.jpg")
# unit.pictures << Picture.new(name: "v1488342358/mbeet/dev/home3.jpg")

# unit = Unit.find(4)
# unit.pictures << Picture.new(name: "v1488342369/mbeet/dev/Houses_in_Hail.jpg")
# unit.pictures << Picture.new(name: "v1488342358/mbeet/dev/home3.jpg")
# unit.pictures << Picture.new(name: "v1488342364/mbeet/dev/home4.jpg")

# unit = Unit.find(5)
# unit.pictures << Picture.new(name: "v1488342358/mbeet/dev/home3.jpg")
# unit.pictures << Picture.new(name: "v1488342364/mbeet/dev/home4.jpg")
# unit.pictures << Picture.new(name: "v1488342369/mbeet/dev/Houses_in_Hail.jpg")

# unit = Unit.find(6)
# unit.pictures << Picture.new(name: "v1488346295/mbeet/dev/home20.jpg")
# unit.pictures << Picture.new(name: "v1488346295/mbeet/dev/home19.jpg")
# unit.pictures << Picture.new(name: "v1488346294/mbeet/dev/home18.jpg")

# unit = Unit.find(7)
# unit.pictures << Picture.new(name: "v1488346294/mbeet/dev/home17.jpg")
# unit.pictures << Picture.new(name: "v1488346295/mbeet/dev/home16.jpg")
# unit.pictures << Picture.new(name: "v1488346294/mbeet/dev/home15.jpg")

# unit = Unit.find(8)
# unit.pictures << Picture.new(name: "v1488346293/mbeet/dev/home14.jpg")
# unit.pictures << Picture.new(name: "v1488346293/mbeet/dev/home13.jpg")
# unit.pictures << Picture.new(name: "v1488346294/mbeet/dev/home12.jpg")

# unit = Unit.find(9)
# unit.pictures << Picture.new(name: "v1488346292/mbeet/dev/home11.jpg")
# unit.pictures << Picture.new(name: "v1488346292/mbeet/dev/home10.png")
# unit.pictures << Picture.new(name: "v1488346292/mbeet/dev/home9.jpg")

# unit = Unit.find(10)
# unit.pictures << Picture.new(name: "v1488346291/mbeet/dev/home8.webp")
# unit.pictures << Picture.new(name: "v1488346291/mbeet/dev/home7.jpg")
# unit.pictures << Picture.new(name: "v1488346292/mbeet/dev/home6.jpg")

# unit = Unit.find(11)
# unit.pictures << Picture.new(name: "v1488346296/mbeet/dev/home5.png")
# unit.pictures << Picture.new(name: "v1488346293/mbeet/dev/home13.jpg")
# unit.pictures << Picture.new(name: "v1488346292/mbeet/dev/home10.png")

# unit = Unit.find(12)
# unit.pictures << Picture.new(name: "v1488346295/mbeet/dev/home16.jpg")
# unit.pictures << Picture.new(name: "v1488346294/mbeet/dev/home12.jpg")
# unit.pictures << Picture.new(name: "v1488346292/mbeet/dev/home9.jpg")

# unit = Unit.find(13)
# unit.pictures << Picture.new(name: "v1488346294/mbeet/dev/home18.jpg")
# unit.pictures << Picture.new(name: "v1487229077/mbeet/dev/flat3.jpg")
# unit.pictures << Picture.new(name: "v1487229072/mbeet/dev/flat2.jpg")

## Populate reasons table
# Reason.create(title_translations: {en: "I change my mind", ar:"غيرت رأيي"})
# Reason.create(title_translations: {en: "I don't like the flat", ar:"أنا لا أحب الشقة"})
# Reason.create(title_translations: {en: "Price is too high", ar:"السعر مرتفع جدا"})
# Reason.create(title_translations: {en: "Would like to book later", ar:"ترغب في حجز في وقت لاحق"})
# Reason.create(title_translations: {en: "Reviews are not good", ar:"استعراض ليست جيدة"})

## Populate cities table
# City.create(name_translations: {en: "New York", ar: "نيويورك"})
# City.create(name_translations: {en: "Chicago", ar: "شيكاغو"})
# City.create(name_translations: {en: "Tokyo", ar: "طوكيو"})
# City.create(name_translations: {en: "London", ar: "لندن"})
# City.create(name_translations: {en: "Manchester", ar: "مانشستر"})
# City.create(name_translations: {en: "Rome", ar: "روما"})
# City.create(name_translations: {en: "Riyadh", ar: "مدينة الرياض"})
# City.create(name_translations: {en: "Jeddah", ar: "جدة"})
# City.create(name_translations: {en: "Mecca", ar: "مكة المكرمة"})
# City.create(name_translations: {en: "Medina", ar: "المدينة المنورة"})
# City.create(name_translations: {en: "Hofuf", ar: "الهفوف"})
# City.create(name_translations: {en: "Paris", ar: "باريس"})
# City.create(name_translations: {en: "Perth", ar: "بيرث"})
# City.create(name_translations: {en: "Melbourne", ar: "ملبورن"})

## Populate unit_classes table
# Rule.create name_translations: {en: "Not suitable for pets", ar: "غير مناسبة للحيوانات الاليفة"}, status: true
# Rule.create name_translations: {en: "No parties or events", ar: "لا أطراف أو أحداث"}, status: true
# Rule.create name_translations: {en: "Check in is anytime after 4 PM", ar: "يتم تسجيل الوصول في أي وقت بعد الساعة 4 مساء"}, status: true
# Rule.create name_translations: {en: "Please warn the host if something is broken or damaged", ar: "يرجى تحذير المضيف إذا كان هناك شيء مكسورة أو تالفة"}, status: true

## Adding permissions to role
# result = {
#   dashboard_permissions: {
#     view: true,
#     },
#     users_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       delete: true,
#       show: true,
#       restore: true
#     },
#     booking_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       delete: true,
#       cancel: true,
#       show: true
#     },
#     unit_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       show: true,
#       trash: true,
#       period: true,
#       restore: true,
#       disableSubUnit: true,
#       enableSubUnit: true,
#       publish: true,
#     },
#     special_price_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       delete: true
#     },
#     coupon_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       delete: true
#     },
#     city_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       delete: true
#     },
#     role_permissions: {
#       view: true,
#       create: true,
#       edit: true,
#       show: true
#     },
#     top_destination_permissions: {
#       view: true,
#       update: true
#     }
# }
# RolePermission.create(permitted_actions: result, role_id: Role.find_by(name: "admin").id)
