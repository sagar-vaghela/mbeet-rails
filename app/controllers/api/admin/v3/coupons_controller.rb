class Api::Admin::V3::CouponsController < Api::Admin::V3::BaseController
  before_action :admin_login_required
  before_action :set_coupon, only: [:show, :update, :destroy]

  # GET /coupons
  def index
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    @coupons = Coupon.all

    search = @coupons.search do
      fulltext params[:search]
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end
      paginate(:offset => params[:offset], :per_page => limit)
    end

    render_data({ total: @coupons.count, result_total: search.total, coupons: search.results.as_api_response(:coupon_statastics) }, I18n.t('success') )
  end

  # GET /coupons/1
  def show
    render_data( { coupons: @coupon.as_api_response(:coupon_statastics), creator: @coupon.user.as_api_response(:user) }, I18n.t('success') ) if @coupon
  end

  # POST /coupons
  def create
    @coupon = Coupon.new(coupon_params)
    @coupon.creator_id = @current_user
    if @coupon.save
     render_created ({
       message: I18n.t('coupon.created'),
       data: { coupon: @coupon.as_api_response(:coupons_list) }
     })
    else
     render_unprocessable_entity( message: @coupon.errors.full_messages.join(', '))
    end
  end

  # PATCH/PUT /coupons/1
  def update
    if @coupon.update(coupon_params)
      render_created ({
        message: I18n.t('coupon.updated'),
        data: { coupons: @coupon.as_api_response(:coupons_list) }
      })
     else
      render_unprocessable_entity( message: @coupon.errors.full_messages.join(', '))
     end
  end

  # DELETE /coupons/1
  def destroy
    render_data({ coupon: {} }, I18n.t('coupon.deleted')) if @coupon.destroy
  end

  def get_statistics
    @coupon = Coupon.find(params[:coupon_id]) if params[:coupon_id]
    render_data({ coupon: @coupon.as_api_response(:coupon_statastics) }, I18n.t('coupon.statistics')) if @coupon
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coupon
      @coupon = Coupon.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def coupon_params
      params.permit(:id, :creator_id, :value, :starting_date, :expiration_date, :status, :code)
    end
end
