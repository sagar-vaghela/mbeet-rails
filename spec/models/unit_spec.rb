# == Schema Information
#
# Table name: units
#
#  id                 :integer          not null, primary key
#  user_id            :integer          not null
#  title_translations :jsonb            not null
#  body_translations  :jsonb            not null
#  number_of_guests   :integer          default(0), not null
#  number_of_rooms    :integer          default(1), not null
#  number_of_beds     :integer          default(0), not null
#  number_of_baths    :integer          default(0), not null
#  available_as       :integer          default(1), not null
#  address            :text             not null
#  price              :decimal(10, 2)   default(0.0), not null
#  service_charge     :decimal(6, 2)    default(0.0), not null
#  latitude           :decimal(20, 8)
#  longitude          :decimal(20, 8)
#  unit_class         :string           default("a"), not null
#  soft_delete        :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  room_type          :integer          default(1), not null
#  city_id            :integer          default(1), not null
#  unit_type          :integer          default(1), not null
#  total_area         :decimal(20, 3)   default(0.0), not null
#  unit_enabled       :boolean          default(FALSE)
#  unit_status        :boolean          default(FALSE)
#  mbeet_percentage   :integer          default(10)
#  bed_type           :integer
#  number_of_subunits :integer          default(0)
#

require 'rails_helper'

RSpec.describe Unit, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
end
