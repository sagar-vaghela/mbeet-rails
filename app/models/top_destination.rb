class TopDestination < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  belongs_to :city

  # -------------------- Associations -------------------------------------- #
  has_many :pictures, as: :imageable, dependent: :destroy

  translates :description

  # ---------------------------- Scope--------------------------- #
  # scope :top_destin_city, lambda{ where(published: true)}

  # -------------------------- validations---------------------- #
	validates :city, presence: true

  # ------------------------ Sunspot search------------------ #
  searchable do
    integer :id
    time    :created_at
    text :city do
      city.name
    end
    text :description_translations, :id
  end

  # ------------------- Mobile Api response --------------------- #
  api_accessible :top_destination do |t|
		t.add :id
    t.add :description_translations
  	t.add :description
    t.add :default_image
    t.add :city, as: 'city', :template => :city
  end

  api_accessible :v6_top_destination_pictures_admin, extend: :top_destination do |t|
		# Here as: 'images' is used as alias for pictures
		# Show default image url if unit has no image
		t.add :pictures, as: 'image', :template => :images
    t.add lambda{|city| city.pictures.first.url if city.pictures.present? }, :as => :picture_image
	end

  def default_image
    CONSTANT[:cdn_cloudy_url] + CONSTANT[:default_flat_image]
  end
end
