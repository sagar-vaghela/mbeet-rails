class Api::V1::PasswordsController < Api::V1::BaseController

	before_action :get_user, only: [:edit, :update]

	def create
  	if @user = User.find_by(email: params[:email])
  		@user.create_reset_digest
  		@user.send_password_reset_email
  		render_data({}, I18n.t('user.reset_email_sent') )
  	else
  		error_data( I18n.t('user.email_error') )	
  	end
	end

	def update
		# @user.update_attributes(user_params)
		# if @user.save!
		# 	render_data({}, I18n.t('user.password_reset_success') )
		# end
	# rescue Exception => e
	# 	error_valid_data(e)
	end


	# private
	# def get_user
	# 	 @user = User.find_by(reset_digest: params[:token])
	# end

	# def user_params
 #      params.require(:user).permit(:password, :password_confirmation)
 #  end

  # # Checks expiration of reset token.
  # def check_expiration
  #   if @user.password_reset_expired?
  #     error_data( I18n.t('user.reset_email_token_expired') )
  #   end
  # end
end
