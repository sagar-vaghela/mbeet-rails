class Api::V6::SearchesController < Api::V5::SearchesController
  before_action :login_required

  def search
    limit = params[:limit].to_i == -1 ? 999_999 : params[:limit]

    city = City.find(params[:city]) if params[:city].present?

    if city.present?
      counter = city.most_searched
      counter += 1
      most_searched = city.update(most_searched: counter.to_i)
    end

    if params[:date_start].present? && params[:date_end].present?
      booking_search = Booking.where(booking_cancelled: false).where(
        "check_in <= '#{params[:date_end]}' AND check_out >= '#{params[:date_start]}'"
      ).or(
      Booking.where(
          "check_in >= '#{params[:date_start]}' AND check_out <= '#{params[:date_end]}'"
        )
      )
      taken_units = booking_search.pluck(:unit_id)
    end

    current_user = @current_user

    #------ This is Sunspot Solr Based search on Parent and its associated tables ------#
    search = Unit.search do

      data_accessor_for(Unit).include = [:city]

      # Only display enabled and published units
      # ----------------------------#
      with :unit_enabled, true
      with :unit_status, true
      # ----------------------------#

      with :city_id, params[:city] if params[:city].present?
      with :number_of_guests, params[:number_of_guests] if params[:number_of_guests].present?

      if params[:date_start].present? && params[:date_end].present?
        taken_units.each do |unit_id|
          without(:id, unit_id)
        end
      end

      # hide all units that are disabled
      # without(:id, unit_disabled_ids)

      all_of do

        # Greater than 4+ condition
        if params[:number_of_rooms].present?
          if params[:number_of_rooms].to_i >= 4
            with(:number_of_rooms).greater_than_or_equal_to( params[:number_of_rooms] )
          else
            with(:number_of_rooms, params[:number_of_rooms] )
          end
        end

        # Greater than 4+ condition
        if params[:number_of_beds].present?
          if params[:number_of_beds].to_i >= 4
            with(:number_of_beds).greater_than_or_equal_to( params[:number_of_beds] )
          else
            with(:number_of_beds, params[:number_of_beds] )
          end
        end

        # Greater than 4+ condition
        if params[:number_of_baths].present?
          if params[:number_of_baths].to_i >= 4
            with(:number_of_baths).greater_than_or_equal_to( params[:number_of_baths] )
          else
            with( :number_of_baths,  params[:number_of_baths] )
          end
        end

        # with(:room_type,    params[:room_type])    if params[:room_type].present?
        with(:unit_class, params[:unit_class])   if params[:unit_class].present?
        with(:unit_type, params[:unit_type])    if params[:unit_type].present?
        with(:bed_type, params[:bed_type])    if params[:bed_type].present?
        with(:available_as, params[:available_as]) if params[:available_as].present?
        with(:number_of_subunits, params[:number_of_subunits]) if params[:number_of_subunits].present?

        #------ search for price kind also ------ #
        with(:price).greater_than_or_equal_to(params[:starting_price]) if params[:starting_price].present?
        with(:price).less_than_or_equal_to(params[:closing_price]) if params[:starting_price].present?

        #------ search with amenities multiple ------ #
        with(:amenity_ids).all_of(params[:amenities].split(',')) if params[:amenities].present?
      end

      with :soft_delete, false
      order_by :unit_class, :asc
      # order_by :created_at, :desc
      paginate(:offset => params[:offset], :per_page => limit)
    end

    @units = search.results
    @units.map do |unit|
       # Store the user liked list to redis
       $redis.sismember("mbeet_user:#{current_user}:units:liked", unit.id)
       #  unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0
       unit.liked_by_user = $redis.sismember("mbeet_user:#{current_user}:units:liked", unit.id).present? ? 1 : 0
    end

    # p @units.as_api_response(:v5_unit_with_pictures)
    # @units_ans = @units.as_api_response(:v5_unit_with_pictures)

    all_periods = []


    @units.each do |data|
      data.periods.each do |du|
        if params[:date_start].present? && params[:date_end].present?
          (params[:date_start].to_date..params[:date_end].to_date).each do |date|
            if date.to_date == du.period_date
              period_hash = {}
              period_hash["id"] = du.id
              period_hash["unit_id"] = du.unit.id
              period_hash["period_date"] = du.period_date
              period_hash["price"] = du.price
              all_periods.push(period_hash)
            end
          end
        end
      end
    end

    @units.map do |unit|
      unit.special_prices = []
      all_periods.each do |ap|
        if ap['unit_id'] == unit.id
          unit.special_prices.push(ap)
        end
      end
    end

    render_data({ total: search.total, units: @units.as_api_response(:v5_unit_with_pictures)}, I18n.t('unit.list'))
  end

  ##----------------------------- Private Methods ----------------------------##
  private

    def user_is_present?
      (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
    end

    def login_required
      if %w(search).include?(params[:action]) &&  user_is_present?
        super
      end
    end

  ##--------------------------------------- end ------------------------------##

end
