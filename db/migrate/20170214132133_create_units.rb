class CreateUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :units do |t|
      t.references :user, foreign_key: true, null: false, index: true
      t.column  :title_translations, 'jsonb', null: false
      t.column  :body_translations,  'jsonb', null: false
      t.integer :number_of_guests, :limit => 2, null: false, default: 0
      t.integer :number_of_rooms, :limit => 2, null: false, default: 1
      t.integer :number_of_beds, :limit => 2, null: false, default: 0
      t.integer :number_of_baths, :limit => 2, null: false, default: 0

      # 1 = daily, 2 = weekly, 3 = monthly, 4 = max 3 months
      t.integer :available_as, :limit => 2, null: false, default: 1
      t.string :city, null: false
      t.text :address, null: false
     
      t.decimal :price, precision: 10, scale: 2, null: false, default: '0'
      t.decimal :service_charge, precision: 6, scale: 2, null: false, default: '0'
      t.decimal :latitude, precision: 20, scale: 8
      t.decimal :longitude, precision: 20, scale: 8


      t.string :unit_class, null: false, default: 'a'

      # 0 = false, 1 = true
      t.boolean :soft_delete, null: false, default: false

      t.timestamps
    end

    add_index :units, :number_of_guests
    add_index :units, :number_of_rooms
    add_index :units, :number_of_beds
    add_index :units, :number_of_baths
    add_index :units, :price
  end
end
