class AddPopulateCityCounterField < ActiveRecord::Migration[5.0]
  def change
    add_column :cities, :most_searched, :integer, default: 0
  end
end
