class Api::V3::UserUnitsController < Api::V3::BaseController
  before_action :login_required
  before_action :check_unit, except: [:index, :create, :enable, :disable]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy
  before_action :find_user_units


  # GET /v3/user_units
  def index

    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
      user_units = @user_units.is_deleted   #.unscope(:where)
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      user_units.map { |unit| unit.liked_by_user = 0 }
      render_data({ total: user_units.count, units: user_units.as_api_response(:v3_unit_pictures)}, I18n.t('unit.list')) if user_units
    else
      user_units = @user_units.is_deleted     #.unscope(:where)
                   .order(id: :desc)
                   .limit(params[:limit] == -1 ? 100 : params[:limit])
                   .offset(params[:offset])

      user_units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      render_data({ total:user_units.count, units: user_units.as_api_response(:v3_unit_pictures)}, I18n.t('unit.list')) if @current_user.present? && user_units
    end
  end

  # GET /v3/user_units/:id
  def show
    user_unit = @user_units.is_deleted.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
      render_data({ units: user_unit.as_api_response(:v3_unit_pictures) }, I18n.t('unit.detail')) if user_unit
    else
      user_unit.liked_by_user = (UserLike.if_liked_by_user @current_user, user_unit.id).present? ? 1 : 0
      render_data({ units: user_unit.as_api_response(:v3_unit_pictures) }, I18n.t('unit.detail')) if user_unit
    end
  end

  # POST /v3/user_units
  def create
    user_unit = @user_units.new(unit_params)
    user =  User.find(@current_user)
    user_unit.title_translations = {en: user_unit.title_en, ar: user_unit.title_ar}
    user_unit.body_translations = {en: user_unit.body_en, ar: user_unit.body_ar}
    if user_unit.save
      render_data({ units: user_unit.as_api_response(:v3_unit_pictures)}, I18n.t('unit.created'))
      user.change_user_role_to_owner
      user.send_email_to_admin
      # 7 is the push_type for notification 7 = ["New flat has been added", "تم إضافة شقة جديدة"]
      # user.send_notification_to_admin user_unit, 7
    else
        render_unprocessable_entity( message: user_unit.errors.full_messages.join(', '))
    end
  end

  # PUT /v3/user_units/:id
  def update
    @user_unit.title_translations = {en: @user_unit.title_en, ar: @user_unit.title_ar}
    @user_unit.body_translations = {en: @user_unit.body_en, ar: @user_unit.body_ar}

    render_data({ units: @user_unit.as_api_response(:v3_unit_pictures) }, I18n.t('unit.updated')) if @user_unit.update(unit_params)
  end

  # DELETE /v3/user_units/:id
  def destroy
    if @user_unit.bookings.any?
      render_error( data: {check_out_dates: @user_unit.bookings.pluck(:check_out)}, message: I18n.t('errors.cant_be_deleted') )
    else
      render_data({ units: {} }, I18n.t('unit.deleted')) if @user_unit.update(soft_delete: true)
    end
  end

  # GET /v3/user_units/enable
  def enable
    unit = @user_units.find(params[:id])
    if unit.update(unit_enabled: true)
      render_updated ({
        message: I18n.t('unit.enabled'),
        data: { unit: unit.as_api_response(:v3_unit_pictures) }
      })
    else
      render_unprocessable_entity( message:unit.errors.full_messages.join(', '))
    end
  end

  # GET /v3/user_units/disable
  def disable
    unit = @user_units.find(params[:id])
    if unit.bookings.pluck(:payment_status).include?(1) || unit.bookings.pluck(:payment_status).include?(3)
      render_error( data:{check_out_dates: unit.bookings.pluck(:check_out)}, message: I18n.t('errors.cant_be_disabled') )
    else
      if unit.update(unit_enabled: false)
        render_updated ({
          message: I18n.t('unit.disabled'),
          data: { unit: unit.as_api_response(:v3_unit_pictures) }
        })
      else
        render_unprocessable_entity( message: unit.errors.full_messages.join(', '))
      end
    end
  end

  ##----------------------------- Private Methods ----------------------------##

  private

  def user_is_present?
    (params[:user_id].present? && params[:user_id].to_i == 0) ? false : true
  end

  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar, :number_of_guests,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id,
                  :address, :price, :service_charge, :latitude, :longitude, :unit_class, :unit_type,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @user_unit = Unit.where(user_id: @current_user).find(params[:id])
  end

  def rescue_response
  rescue StandardError => e
    error_valid_data(e)
  end

  def delete_liked_unit
    @unit_likes = UserLike.where(unit_id: params[:id])
    @unit_likes.delete_all if @unit_likes.present?
  end

  # Find user units
  def find_user_units
    @user_units = Unit.where(user_id: @current_user)
  end
  ##--------------------------------------- end ------------------------------##
end
