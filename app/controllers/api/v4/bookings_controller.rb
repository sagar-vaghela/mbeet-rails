class Api::V4::BookingsController < Api::V3::BookingsController
  before_action :login_required
	before_action :check_user_verification, :check_unit_available_as, :check_booking, only: :create

	before_action :first_verify_booking, only: :cancel
	before_action :check_payment_status, :get_payment, only: :payment_update
	after_action  :update_booking_cancelled, only: :cancel

	# POST /v2/book_now
	def create
		if @booking = Booking.create!(booking_params)
			@booking.notify @booking, 3
			@booking.automatically_cancel_booking
      (params[:check_in].to_datetime...params[:check_out].to_datetime).to_a.each do |day_booked|
        BookedSubunit.where(unit_id: params[:unit_id]).create_with(unit_id: params[:unit_id])
        .find_or_create_by(
            day_booked: day_booked
        ).increment!(:count, by = 1)
      end
			render_data({ bookings: @booking.as_api_response(:v4_booking_with_customer_info) }, I18n.t('user.booking.success') )
		end
	rescue StandardError => e
		error_valid_data(e)

  # count_number_of_booked_days
	end

	# POST /v2/cancel/booking
	def cancel
		if @cancelled = UserBookingCancelRequest.create!(booking_cancel_params)
			@booking = Booking.find(@cancelled.booking_id)
			@booking.notify @booking, 4
			@booking.update(payment_status: 2)
      @booking.delete_booked_days
			render_data({ bookings: {} }, I18n.t('user.booking.cancelled') )
		end
	rescue StandardError => e
		error_valid_data(e)
	end

	# PUT /v2/payment/update
	def payment_update
		return error_data I18n.t('record_not_found') unless @payment.present?

		if @payment.update(payment_status: params[:payment_status], modified_on: params[:modified_on], transaction_id: params[:transaction_id])
      render_data({ payments: {} }, I18n.t('user.booking.payment.updated') )
    	Booking.new.process @payment.booking, params[:payment_status]
		end
	end

	# Get total bookings for current month (for owners)
	def current_month_booking_for_owner
		booking = Booking.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month, unit_id: @current_user.unit_ids).count
	end

	private
	# Methods to do a booking
	def booking_params
		params.permit(:user_id, :unit_id, :coupon_id, :check_in, :check_out, :guests,
                  :for_days, :sub_total_amount, :total_amount, :advance_payment,
			            payment_txn_attributes: [:modified_on]
			           )
	end

  # check the unit availablity [Daily, weekly and monthly]
  def check_unit_available_as
    check_required_params params, [:unit_id, :check_in, :check_out]
    date = Date.date_diff params[:check_in].to_date, params[:check_out].to_date
    case Unit.find(params[:unit_id]).available_as
      when 1
        # vacant = (date[0] == 0) && (date[1] == 0) && (date[2] == 1)  ? true : false
        vacant = true
        availablity = I18n.t('errors.only_daily')
      when 2
        vacant = (date[0] == 0) && (date[1] == 0) && (date[2] % 7 == 0) && date[2].between?(7, 29) ? true : false
        availablity = I18n.t('errors.only_weekly')
      when 3, 4
        vacant = (params[:check_out].to_date - params[:check_in].to_date) % 30 == 0 ? true : false
        availablity = I18n.t('errors.only_monthly')
    end
    error_valid_data(availablity) unless vacant
  end

	def check_booking
    # ---------------------- check bookings --------------------- #
     unit = Unit.find(params[:unit_id])
     booking = Booking.where(booking_cancelled: false)
     .where(unit_id: params[:unit_id])
     .where(
       "check_in <= '#{params[:check_out]}' AND check_out > '#{params[:check_in]}'"
     ).or(
     Booking.where(
        "check_in >= '#{params[:check_out]}' AND check_out < '#{params[:check_in]}'"
       )
     )
    # ------------------------------------------------------- #

    number_of_disabled_units = count_number_of_disabled_units.empty? ? 0 : count_number_of_disabled_units.max
    if unit.number_of_subunits == number_of_disabled_units && !count_number_of_disabled_units.empty?
      error_valid_data(I18n.t('user.booking.not_available'))
    else
      if booking.present? && unit.number_of_subunits == 0
  			error_valid_data(I18n.t('user.booking.not_available'))
  		elsif booking.present? && unit.number_of_subunits > 0
        error_valid_data(I18n.t('user.booking.not_available')) if count_number_of_booked_days.include?(unit.number_of_subunits - number_of_disabled_units)
  		elsif params[:check_in].to_s == params[:check_out].to_s
  			error_valid_data(I18n.t('user.booking.same_dates'))
  		end
    end
    # ------------------------------------------------------- #
	end

	# Methods for Cancel a booking
	def booking_cancel_params
		params.permit(:user_id, :booking_id, :reason_id, :message)
	end

	def first_verify_booking
		booking = Booking.find(params[:booking_id])
		if booking.present?
			error_valid_data(I18n.t('user.booking.already_cancelled')) if booking.booking_cancelled == true
		else
			error_valid_data(I18n.t('user.booking.verify'))
		end
	end

	def update_booking_cancelled
		booking = Booking.find(@cancelled.booking_id)
		booking.update!(booking_cancelled: true)
	end

	def check_user_verification
		verify = User.find_by(email_verified: true, phone_verified: true, id: params[:user_id])
		error_valid_data(I18n.t('user.booking.contact_details')) if verify.nil?
	end

	def get_payment
		@payment = PaymentTxn.find_by(booking_id: params[:booking_id])
	end

	def check_payment_status
		error_valid_data(I18n.t('user.booking.payment.status_not_valid')) unless params[:payment_status].to_i == 4 || params[:payment_status].to_i == 2
	end

  def count_number_of_booked_days
    booked_days = Unit.find(params[:unit_id]).booked_subunits
    .where("day_booked >= '#{params[:check_in].to_datetime}' AND day_booked <= '#{params[:check_out].to_datetime}'")
    .pluck(:count)
  end

  def count_number_of_disabled_units
     count_number_of_disabled_units = Unit.find(params[:unit_id]).unit_disabled_dates
    .where("day_disabled >= '#{params[:check_in].to_datetime}' AND day_disabled <= '#{params[:check_out].to_datetime}'")
    .pluck(:number_of_disabled_units)
	end
end
