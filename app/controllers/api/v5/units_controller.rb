class Api::V5::UnitsController < Api::V4::UnitsController
  before_action :login_required

  # GET /v5/units/:id
  def show
    @unit = Unit.is_deleted.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
      # render_data({ units: @unit.as_api_response(:v5_unit_pictures) }, I18n.t('unit.detail')) if @unit
    else
      @unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", @unit.id).present? ? 1 : 0
    end
    all_periods = []
    @unit.periods.each do |du|
      if params[:date_start].present? && params[:date_end].present?
        (params[:date_start].to_date..params[:date_end].to_date).each do |date|
          if date.to_date == du.period_date
            period_hash = {}
            period_hash["id"] = du.id
            period_hash["unit_id"] = du.unit.id
            period_hash["period_date"] = du.period_date
            period_hash["price"] = du.price
            all_periods.push(period_hash)
          end
        end
      else
        period_hash = {}
        period_hash["id"] = du.id
        period_hash["unit_id"] = du.unit.id
        period_hash["period_date"] = du.period_date
        period_hash["price"] = du.price
        all_periods.push(period_hash)
      end
    end
    @unit.special_prices = []
    all_periods.each do |ap|
      @unit.special_prices.push(ap)
    end
    render_data({ units: @unit.as_api_response(:v5_unit_pictures)}, I18n.t('unit.detail')) if @unit
  end

end
