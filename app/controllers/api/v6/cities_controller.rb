class Api::V6::CitiesController < Api::V5::CitiesController
  before_action :login_required, except: :top_destinations
	before_action :find_city, only: :show

  def top_destinations
    limit = params[:limit].to_i == -1 ? 100 : params[:limit]

    search = City.search do
      fulltext params[:search]
      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      if params[:published].present?
        status = params[:published] == 'true' ? true : false
        with(:published, status)
      end

      order_by(:most_searched, :desc)
      paginate(:offset => params[:offset], :per_page => limit)
    end

    render_data({ total: City.not_deleted.size, result_total: search.total, top_destinations: search.results.as_api_response(:v3_city_pictures_admin)}, I18n.t('city.cities_list'))
  end


  # display all published and unpublished cities
  def published_and_unpublished
    @cities = City.not_deleted.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
    if @cities.present?
      render_data({ cities: @cities.as_api_response(:v3_published_and_unpublished) }, I18n.t('city.cities_list') ) if @cities.present?
    else
      render_no_data
    end
  end

end
