# == Schema Information
#
# Table name: feedbacks
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  unit_id    :integer          not null
#  star       :decimal(3, 1)    default(0.0), not null
#  feedback   :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  booking_id :integer          not null
#

class Feedback < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  # ------------------------- Associations ----------------------- #
  belongs_to :user
  belongs_to :unit

  default_scope { order(id: :desc) }

  # ------------------------- Validations ------------------------ #
  validates :user, presence: true
  validates :unit, presence: true
  validates :feedback, presence: true

  # Get feedback user detail as well
  # Response for feedback node under GET /v1/unit/2
  api_accessible :unit_with_feedbacks do |t|
    t.add :star
    t.add :feedback
    t.add :created_at
  end

  api_accessible :user_with_feedback, extend: :unit_with_feedbacks do |t|
    t.add :user, :template => :get_user_for_feedback_with_image
  end

end
