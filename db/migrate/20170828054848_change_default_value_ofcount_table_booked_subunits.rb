class ChangeDefaultValueOfcountTableBookedSubunits < ActiveRecord::Migration[5.0]
  def change
    change_column_default(:booked_subunits, :count, 0)
  end
end
