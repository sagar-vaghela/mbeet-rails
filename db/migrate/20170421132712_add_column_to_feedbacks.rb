class AddColumnToFeedbacks < ActiveRecord::Migration[5.0]
  def change
  	add_reference :feedbacks, :booking, index: true, foreign_key: true, null: false
  end
end
