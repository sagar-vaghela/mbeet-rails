class AddReferenceOfCouponToBooking < ActiveRecord::Migration[5.0]
  def change
    add_reference :bookings, :coupon, foreign_key: true, null: true
  end
end
