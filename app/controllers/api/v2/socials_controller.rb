class Api::V2::SocialsController < Api::V2::BaseController
  before_action :check_user, :only => :get_user_email
  before_action :set_user, :only => :create

  # POST /v2/social/signup
  def create
    if @user.present?
      @user.update(name: @user.name.present? ? @user.name : params[:name],
                   facebook_uid: params[:type].downcase == "facebook" ? params[:uuid] : @user.facebook_uid,
                   twitter_uid: params[:type].downcase == "twitter" ? params[:uuid] : @user.twitter_uid,
                   gmail_uid: params[:type].downcase == "gmail" ? params[:uuid] : @user.gmail_uid
                   )

      if @user.save!(:validate => false)
        #@user.refreshed_token
        auth_token = JsonWebToken.encode({user_id: @user.id})
        render_data({ auth_token: auth_token, users: @user.as_api_response(:v2_user_only)}, I18n.t('user.login_success'))
      end

    else
      @user = User.new(input_params)

      @user["role_id"] = 3
      @user["facebook_uid"] = params[:type].downcase == "facebook" ? params[:uuid] : nil
      @user["twitter_uid"] = params[:type].downcase == "twitter" ? params[:uuid] : nil
      @user["gmail_uid"] = params[:type].downcase == "gmail" ? params[:uuid] : nil

      @user.skip_password_validation = true

      if @user.save!
        #@user.refreshed_token
        auth_token = JsonWebToken.encode({user_id: @user.id})
        render_data({ auth_token: auth_token, users: @user.as_api_response(:v2_user_only)}, I18n.t('user.signup_success'))
      end
    end
  rescue StandardError => e
    error_valid_data(e)
  end

  def get_user_email
    render_data({ users: @user.as_api_response(:only_user_email)}, "User email")
  end

  private
  def input_params
    params.permit(:name, :email, :type, :uuid)
  end

  def check_user
    @user = User.find_by("facebook_uid= ? OR twitter_uid= ? OR gmail_uid= ?", params[:uuid], params[:uuid], params[:uuid])
    error_valid_data("#{params[:type]} user id doesn't exist") unless @user
  end

  def set_user
    @user = User.find_by("email= ? OR facebook_uid= ? OR twitter_uid= ? OR gmail_uid= ?", params[:email], params[:uuid], params[:uuid], params[:uuid])
  end

  # def validate_social_signup
  #   @user = User.new(input_params)
  #   @user.custom_form_validation_required = true
  #   @user.skip_password_validation = true

  #   if params[:action] == "update"
  #     @user.skip_email_validation = true
  #   end

  #    unless @user.valid?
  #     empty_params = input_params.reject{|_, v| v.present?}.keys.join(", ")
  #     error_valid_data("#{empty_params} can't be blank")
  #   end
  # rescue StandardError => e
  #   error_valid_data(e)
  # end

end
