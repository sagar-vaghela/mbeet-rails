class AddPublishedTocities < ActiveRecord::Migration[5.0]
  def change
    add_column :cities, :published, :boolean, default:false
  end
end
