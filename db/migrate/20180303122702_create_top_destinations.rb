class CreateTopDestinations < ActiveRecord::Migration[5.0]
  def change
    create_table :top_destinations do |t|
      t.references :city, foreign_key: true
      t.text :description

      t.timestamps
    end
  end
end
