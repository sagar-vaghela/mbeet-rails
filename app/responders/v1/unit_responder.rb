module V1::UnitResponder
	extend ActiveSupport::Concern

	included do
		# --------------------- Api Response using acts_as_api --------------------- #
	  api_accessible :unit_only do |t|
	    t.add :id
	    t.add :title
	    t.add :body
	    t.add :room_type
	    t.add :number_of_guests
	    t.add :number_of_rooms
	    t.add :number_of_beds
	    t.add :number_of_baths
	    t.add :available_as
	    t.add :city
	    t.add :address
	    t.add :price
	    t.add :service_charge
	    t.add :latitude
	    t.add :longitude
	    t.add :unit_class
	    t.add :created_at
	    t.add :avg_star
	    t.add :feedback_count
	    t.add :liked_by_user
	  end

	  api_accessible :user, extend: :unit_only do |t|
	    t.add :user, :template => :user_for_unit, as: 'owner'
	  end

	  api_accessible :unit_with_feedbacks, extend: :user do |t|
	    t.add :feedbacks, :template => :user_with_feedback
	  end

	  api_accessible :amenities_and_units, extend: :unit_with_feedbacks do |t|
	    t.add :amenities, :template => :amenities_and_units
	  end

	  api_accessible :unit_pictures, extend: :amenities_and_units do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end

	  api_accessible :unit_with_pictures, extend: :unit_only do |t|
	    # Here as: 'images' is used as alias for pictures
	    # Show default image url if unit has no image
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end

	  # Used for my orders listing and detail
	  # Used as template under Booking model
	  api_accessible :unit_for_my_orders do |t|
	    t.add :id
	    t.add :title
	    t.add :address
	    t.add :latitude
	    t.add :longitude
	    t.add :price
	    t.add :service_charge
	  end

	  api_accessible :unit_with_pictures_for_myorders, extend: :unit_for_my_orders do |t|
	    t.add :pictures, as: 'images', :template => :images
	    t.add :default_image
	  end
	end
end
