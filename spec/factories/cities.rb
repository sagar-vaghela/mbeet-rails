FactoryBot.define do
  factory :city do
    name "CA"
    soft_delete true
  end
end
