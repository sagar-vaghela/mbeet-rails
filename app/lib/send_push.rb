class SendPush

	def initialize data, push_type, msg = nil
		@data = data
		@push_type = push_type

		# We can remove this on production
		@msg = msg if msg.present?
	end

	def send
		$pubnub.publish(channel: "#{CONSTANT[:prefix]}_user_#{@data.user_id}", message: message) do |envelope|
		    puts envelope.status
		end
	end

  # send nofification to tell the admin that user has added new flat
	def send_admin_notification
		User.where(role_id: 1).map do |user|
			$pubnub.publish(channel: "#{CONSTANT[:prefix]}_user_#{user.id}", message: new_flat_added) do |envelope|
			    puts envelope.status
			end
		end
	end

	def user_flat_published_notification
		channels = []
		User.all.map do |user|
			$pubnub.publish(channel: "#{CONSTANT[:prefix]}_user_#{user.id}", message: flat_published) do |envelope|
			    puts envelope.status
			end
		end
	end

	def send_owner_push_notification
		$pubnub.publish(
        channel: "#{CONSTANT[:prefix]}_user_#{@data.unit.user_id}",
        message: owner_flat_is_booked) do |envelope|
          Rails.logger.debug(" ========================================== ")
          Rails.logger.debug("\e[1mPush Notification to channel\e[22m : \e[31m#{CONSTANT[:prefix]}_user_#{@data.unit.user_id}\e[0m")
          Rails.logger.debug(owner_flat_is_booked.to_json)
          Rails.logger.debug(" ========================================== ")
		      # puts envelope.status
		end
	end

	def test
		$pubnub.publish(channel: "#{CONSTANT[:prefix]}_user_#{@data}", message: JSON.parse(@msg) ) do |envelope|
		    puts envelope.status

		end
	end

	def message
		d_msg = Hash.new

		d_msg["id"] = @data.id
		d_msg["push_type"] = @push_type
		d_msg["my_orders"] = {
			"id": @data.id,
			"created_at": @data.created_at,
			"modified_on": @data.payment_txn.modified_on ||= "",
			"invoice_unique_id": @data.payment_txn.invoice_unique_id ||= ""
		}
		d_msg["aps"] = {
        "alert": {
               "loc-key": "REQUEST_FORMAT",
               "loc-args": loc_arg_data
           },
        "sound": "default"
      }

		return d_msg
	end

  #
	def new_flat_added
		d_msg = Hash.new

		d_msg["id"] = @data.id
		d_msg["push_type"] = @push_type
		d_msg["owner_of_flat"]={
			"name": @data.user.name,
			"email": @data.user.email,
			"phone": @data.user.phone
		}
		d_msg["User_flat"] = {
			"id": @data.id,
			"title": @data.title,
			"created_at": @data.created_at,
			"updated_at": @data.updated_at
		}
		d_msg["aps"] = {
        "alert": {
               "loc-key": "REQUEST_FORMAT",
               "loc-args": loc_arg_data
           },
        "sound": "default"
      }

		return d_msg
	end

	def flat_published
		d_msg = Hash.new

		d_msg["user_id"] = @data.user_id
		d_msg["push_type"] = @push_type
		d_msg["flat"] = {
			"id": @data.id,
			"title": @data.title,
			"created_at": @data.created_at,
			"updated_at": @data.updated_at
		}
		d_msg["aps"] = {
        "alert": {
               "loc-key": "REQUEST_FORMAT",
               "loc-args": loc_arg_data
           },
        "sound": "default"
      }

		return d_msg
	end

	def owner_flat_is_booked
		d_msg = Hash.new
		d_msg["order_id"] = @data.id
		d_msg["push_type"] = @push_type
		d_msg["flat"] = {
			"id": @data.unit.id,
			"title": @data.unit.title,
			"created_at": @data.unit.created_at,
			"updated_at": @data.unit.updated_at
		}
		d_msg["aps"] = {
        "alert": {
               "loc-key": "REQUEST_FORMAT",
               "loc-args": loc_arg_data
           },
        "sound": "default"
      }

		return d_msg
	end

	# -------------------------------------------------------------------------------------- #
	# Function responsible to decide data based upon push type
	# Push Types are:
	# checking_intimation_before_2_days => 1
	# checking_intimation_before_1_day => 2
	# order_placed => 3
	# order_cancelled => 4
	# payment_done => 5
	# payment_failed => 6
	# -------------------------------------------------------------------------------------- #
	def loc_arg_data
		case @push_type
			when 1
				then ["You have only two days left for your check in.", "لم يتبق أمامك سوى يومين."]
			when 2
			 	then ["You have only one day left for your check in.", "لم يتبق سوى يوم واحد حتى تتمكن من تسجيل الوصول"]
			when 3
				then ["Order placed successfully.", "تم وضع الطلب بنجاح."]
			when 4
				then ["Order cancelled", "تم الغاء الأمر او الطلب"]
			when 5
			  then ["Payment done successfully", "تم الدفع بنجاح"]
			when 6
			  then ["Payment failed", "عملية الدفع فشلت"]
			when 7
				then ["New flat has been added by #{@data.user.name}","أ ضاف شقة جديدة #{@data.user.name} "]
			when 8
				then ["New unit is published successfully", "تم  نشر شقة جديده"]
			when 9
				then ["Your unit #{@data.unit.id} has been booked by #{@data.user.name}", " لقد تم حجز شقتك #{@data.unit.id} من قبل #{@data.user.name}"]
			when 10
				then ["You have only one day left for your check out, Please give rate the unit.", "لديك يوم واحد فقط من أجل خروجك ، يرجى إعطاء سعر الوحدة.
وحدة"]
			else
				["testing", "testing"]
			end
	end

	def process_in_background worker, data, message = nil, push_type
		if message.nil?
			Resque.enqueue(worker, data, push_type)
		else
			Resque.enqueue(worker, data, message, push_type)
		end
	end

end
