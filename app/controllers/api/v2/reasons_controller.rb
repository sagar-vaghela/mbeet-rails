class Api::V2::ReasonsController < Api::V2::BaseController

  # GET /v2/reasons
  def index
    @reasons = Reason.all
    render_data({ reasons: @reasons.as_api_response(:reasons)}, I18n.t('unit.reasons.list')) if @reasons.present?
  end

end
