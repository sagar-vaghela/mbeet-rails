class Api::Admin::V3::UnitsController < Api::Admin::V3::BaseController
  before_action :admin_login_required
  before_action :check_unit, except: [:index, :create]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy

  # GET /admin/v3/units
  def index

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = Unit.search do
      fulltext params[:search]
      with(:city_id, params[:city_id]) if params[:city_id].present?
      if params[:unit_status].present?
        status = params[:unit_status] == 'true' ? true : false
        with(:unit_status, status)
      end

      if params[:unit_enabled].present?
        status = params[:unit_enabled] == 'true' ? true : false
        with(:unit_enabled, status)
      end

      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      with(:number_of_guests, params[:number_of_guests]) if params[:number_of_guests].present?
      with(:number_of_rooms, params[:number_of_rooms] ) if params[:number_of_rooms].present?
      with(:number_of_beds, params[:number_of_beds] )   if params[:number_of_beds].present?
      with( :number_of_baths,  params[:number_of_baths] )  if params[:number_of_baths].present?
      with(:unit_class,   params[:unit_class])   if params[:unit_class].present?
      with(:unit_type,    params[:unit_type])    if params[:unit_type].present?
      with(:available_as, params[:available_as]) if params[:available_as].present?
      with(:price).greater_than_or_equal_to(params[:starting_price]) if params[:starting_price].present?
      with(:price).less_than_or_equal_to(params[:closing_price]) if params[:starting_price].present?
      with(:amenities).any_of([params[:amenities]]) if params[:amenities].present?
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end

      paginate(:offset => params[:offset], :per_page => limit)

    end
    render_data({ total: Unit.all.size, result_total: search.total, units: search.results.as_api_response(:v3_admin_unit_only)}, I18n.t('unit.list'))
  end

  # GET /admin/v3/units/:id
  def show
    render_data({ units: @unit.as_api_response(:v3_admin_unit_only) }, I18n.t('unit.detail')) if @unit
  end

  # POST /admin/v3/units
  def create
    @unit = Unit.new(unit_params)
    admin = User.find(@current_user)
    @unit.title_translations = {en: @unit.title_en, ar: @unit.title_ar}
    @unit.body_translations = {en: @unit.body_en, ar: @unit.body_ar}
    # @unit.unit_status = true
    # @unit.unit_enabled =true
    if @unit.save
     render_data({ units: @unit.as_api_response(:v3_admin_unit_only) }, I18n.t('admin.unit.created'))

    else
     render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
    end
  end

  # PUT /admin/v3/users/:id
  def update
    @unit.title_translations = {en: @unit.title_en, ar: @unit.title_ar}
    @unit.body_translations = {en: @unit.body_en, ar: @unit.body_ar}

    render_data({ units: @unit.as_api_response(:v3_admin_unit_only) }, I18n.t('admin.unit.updated')) if @unit.update(unit_params)
  end

  # DELETE /admin/v3/units/:id
  def destroy
    if @unit.bookings.pluck(:payment_status).include?(1) || @unit.bookings.pluck(:payment_status).include?(3)
      render_unprocessable_entity( message: I18n.t('admin.unit.has_some_bookings'))
    else
      render_data({ units: {} }, I18n.t('admin.unit.deleted')) if @unit.update(soft_delete: true)
    end

  end

  # PUT: admin/v3/units/:id/publish
  def publish
    unit = Unit.find(params[:id])
    admin = User.find(@current_user)
    # if unit_status is true, the flat is published to the app
    if unit.update(unit_status: true, unit_enabled: true)
      render_updated ({
        message: I18n.t('unit.published'),
        data: { unit: unit.as_api_response(:v3_admin_unit_only) }
      })

      admin.send_email_to_user(unit)
    else
      render_unprocessable_entity( message: unit.errors.full_messages.join(', '))
    end
  end

  # PUT /admin/v1/units/:id/restore
  def restore
    render_data({ units: {} }, I18n.t('admin.unit.restored')) if @unit.update(soft_delete: false)
  end

  private
  def unit_params
    params.permit(:user_id, :title_en, :title_ar, :body_en, :body_ar, :room_type, :number_of_guests, :unit_status, :unit_enabled,
                  :number_of_rooms, :number_of_beds, :number_of_baths, :available_as, :city_id, :mbeet_percentage,
                  :address, :price, :service_charge, :latitude, :longitude, :unit_class, :unit_type,
                  :total_area, {:rule_ids => []}, {:amenity_ids => []}, :pictures_attributes => [:id, :name, :_destroy])
  end

  def check_unit
    @unit = Unit.find(params[:id])
  end

  def rescue_response
  rescue StandardError => e
    error_valid_data(e)
  end

  def delete_liked_unit
    @unit_likes = UserLike.where(unit_id: params[:id])
    @unit_likes.delete_all if @unit_likes.present?
  end

end
