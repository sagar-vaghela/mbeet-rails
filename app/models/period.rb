class Period < ApplicationRecord

	# maintain user visited urls
  is_impressionable

	include Periods::Associations
	validates :price, presence: true , numericality: true
	validates :period_date, presence: true

	searchable do
		text :unit_id, :period_date, :price , :id
		integer :id
		integer :unit_id
		float :price
		date :period_date
		date :created_at
	end

	api_accessible :periods_list do |t|
  	t.add :id
  	t.add :unit_id
  	t.add :period_date
		t.add :price
  	t.add :created_at
	end

end
