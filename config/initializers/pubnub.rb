 require 'pubnub'

# # Init pubnub client
 $pubnub = Pubnub.new(
   subscribe_key:    Rails.application.secrets.pubnub_subscription_key,
   publish_key:      Rails.application.secrets.pubnub_publish_key,
   secret_key:       Rails.application.secrets.pubnub_secret_key,
   connect_callback: lambda { |msg| pubnub.publish(channel: channel, message: 'Connection established') }
  )
# =======
# require 'pubnub'

# # Init pubnub client
# $pubnub = Pubnub.new(
#  subscribe_key:    Rails.application.secrets.pubnub_subscription_key,
#  publish_key:      Rails.application.secrets.pubnub_publish_key,
#  secret_key:       Rails.application.secrets.pubnub_secret_key,
#  connect_callback: lambda { |msg| pubnub.publish(channel: channel, message: 'Connection established') }
# )
# >>>>>>> remotes/origin/staging
