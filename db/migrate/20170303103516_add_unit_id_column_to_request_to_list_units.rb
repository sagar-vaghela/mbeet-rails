class AddUnitIdColumnToRequestToListUnits < ActiveRecord::Migration[5.0]
  def change
  	add_reference :request_to_list_units, :unit, foreign_key: true, null: false
  end
end
