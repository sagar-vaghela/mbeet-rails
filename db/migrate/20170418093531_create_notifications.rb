class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.references :user, foreign_key: true, null: false
      t.references :booking, foreign_key: true, null: false
      t.column :body, 'jsonb', null: false
      t.integer :push_type, null: false
      t.boolean :read, default: false


      t.timestamps
    end

    # add_index :notifications, :user_id
    # add_index :notifications, :booking_id
  end
end
