class CreatePeriods < ActiveRecord::Migration[5.0]
  def change
    create_table :periods do |t|
      t.integer :unit_id
      t.float :price
      t.date :period_date
      t.timestamps
    end
  end
end
