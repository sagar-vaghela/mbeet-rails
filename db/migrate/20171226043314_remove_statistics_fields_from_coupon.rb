class RemoveStatisticsFieldsFromCoupon < ActiveRecord::Migration[5.0]
  def change
    remove_column :coupons, :total_charged_amount
    remove_column :coupons, :total_discounted_amount
    remove_column :coupons, :average_charged_amount
    remove_column :coupons, :average_discount_amount
    remove_column :coupons, :used_coupon_count
  end
end
