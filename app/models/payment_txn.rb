# == Schema Information
#
# Table name: payment_txns
#
#  id                :integer          not null, primary key
#  booking_id        :integer          not null
#  payment_status    :integer          default(0), not null
#  invoice_unique_id :text             not null
#  modified_on       :datetime         not null
#  transaction_id    :text
#

class PaymentTxn < ApplicationRecord

  # maintain user visited urls
  is_impressionable

  # ------------------------- Associations ----------------------- #
  belongs_to :booking

  # ------------------------- Callbacks -------------------------- #
  before_create :generate_invoice_code , :order_default_payment_status

  # ------------------------- Enums ------------------------------ #
  # enum payment_status: {
  #   pending: 1,
  #   cancelled: 2,
  #   confirmed: 3,
  #   completed: 4
  # }

  # ------------------------- API -------------------------------- #
  api_accessible :payment_transaction do |t|
  	t.add :invoice_unique_id
  end

  # ------------------------- payment with transaction id ------------------- #
  api_accessible :payment_transaction_id do |t|
  	t.add :invoice_unique_id
    t.add :transaction_id
  end

  def generate_invoice_code
    # self.invoice_unique_id = SecureRandom.base64(15).tr('+/=0', '78976546431')
    self.invoice_unique_id = SecureRandom.random_number(178943212311321).to_s
  end

  def order_default_payment_status
    self.payment_status = 'pending'
  end

end
