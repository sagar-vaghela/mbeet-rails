class Api::Admin::V2::BookingsController < Api::Admin::V2::BaseController
  before_action :admin_login_required

  before_action :check_bookings, only: :index
  before_action :find_booking, only: [:show, :cancel]
  # GET: admin/v2/bookings
  def index
    render_data({ total: Booking.all.size, booking_list: @booking_list.as_api_response(:v2_booking_with_unit_and_user) }, I18n.t('admin.booking_list') )
  end

  # GET: admin/v2/bookings/:id
  def show
    render_data({ booking: @booking.as_api_response(:v2_booking_with_unit_and_user) }, I18n.t('admin.booking_detail')) if @booking.present?
  end

  # DELETE: admin/v2/bookings/:id/cancel
  def cancel
    if @booking.update(payment_status: 2, booking_cancelled: true)
      render_updated ({
        message: I18n.t('admin.booking_cancelled'),
        data: { Booking: @booking.as_api_response(:v2_booking_with_unit_and_user) }
      })
    else
      render_unprocessable_entity( message: @booking.errors.full_messages.join(', '))
    end
  end

  private
  def check_bookings
    @booking_list = Booking.all
                            .order(id: :desc)
                            .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                            .offset(params[:offset])
    render_data({}, I18n.t('no_data')) unless @booking_list.present?
  end

  def find_booking
    @booking = Booking.find(params[:id])
  end
end
