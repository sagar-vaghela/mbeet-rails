class AddUnitStatusToUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :unit_status, :boolean, default: false
  end
end
