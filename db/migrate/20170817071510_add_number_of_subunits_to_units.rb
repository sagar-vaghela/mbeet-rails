class AddNumberOfSubunitsToUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :number_of_subunits, :integer, default: 0
  end
end
