# == Schema Information
#
# Table name: reasons
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Reason < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
	# ------------------ Requirment ----------------------- #
  include ReasonResponder
  translates :title

  # ------------------ Associations ----------------------- #
	has_one :user_booking_cancel_request

end
