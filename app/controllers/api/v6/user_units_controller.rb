class Api::V6::UserUnitsController < Api::V5::UserUnitsController
  before_action :login_required
  before_action :check_unit, except: [:index, :create, :owner_enable]
  after_action  :rescue_response
  after_action  :delete_liked_unit, only: :destroy
  before_action :find_user_units
  before_action :check_booking, only: [:disable]
  before_action :previous_association_ids, only: :update

  # GET /v5/user_units
  def index
    if params[:user_id].present? && params[:user_id].to_i == 0 # To check if unit or flat is liked by current user or not
      user_units = @user_units.is_deleted   #.unscope(:where)
                              .order(id: :desc)
                              .limit(params[:limit] == -1 ? 100 : params[:limit])
                              .offset(params[:offset])

      user_units.map { |unit| unit.liked_by_user = 0 }
      render_data({ total: user_units.count, units: user_units.as_api_response(:v5_unit_pictures)}, I18n.t('unit.list')) if user_units
    else
      user_units = @user_units.is_deleted     #.unscope(:where)
                              .order(id: :desc)
                              .limit(params[:limit] == -1 ? 100 : params[:limit])
                              .offset(params[:offset])

      # user_units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }
      units = user_units.includes(:user, :user_likes, :city, :feedbacks, :unit_disabled_dates)
                        .references(:user, :user_likes, :city, :feedbacks, :unit_disabled_dates)
      # units.map { |unit| unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0 }

      units.map do |unit|
        # unit.liked_by_user = (UserLike.if_liked_by_user @current_user, unit.id).present? ? 1 : 0
        unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", unit.id).present? ? 1 : 0
      end

      render_data({ total:units.count, units: units.as_api_response(:v5_unit_pictures)}, I18n.t('unit.list')) if @current_user.present? && user_units
    end
  end

  def owner_enable
    @unit = Unit.find(params[:id])
    if @unit.number_of_subunits >= 0
      if params[:start_date].present? && params[:end_date].present? && params[:number_of_disabled_units].present?
        (params[:start_date].to_datetime..params[:end_date].to_datetime).to_a.each do |day_disabled|
          @res = @unit.unit_disabled_dates.find_by(day_disabled: day_disabled)
          @res.destroy if @res
        end
        if @unit.unit_disabled_dates.count == 0
          @unit.update(unit_enabled: true)
        end
        render_updated ({ message: I18n.t('unit.enabled'),data: { unit: @unit.as_api_response(:v5_unit_pictures) } })
      end
     else
       render_unprocessable_entity( message: @unit.errors.full_messages.join(', '))
     end
  end

  # PUT /v6/user_units/:id
  def update
    user = @user_unit.user if @user_unit.user.is_owner?

    if !@user_unit.price_changed?
      @user_unit.update_attributes(unit_status: false, message: params[:message]) if params[:message].present?
    end

    if @user_unit.update(unit_params)
      render_data({ units: @user_unit.as_api_response(:v6_unit_only) }, I18n.t('unit.updated'))
      if @user_unit.unit_status_previously_changed? && @user_unit.unit_status == true
        user.send_email_to_user(@user_unit)
      end
    else
      render_unprocessable_entity( message: @user_unit.errors.full_messages.join(', '))
    end
  end

  # GET /v6/user_units/:id
  def show
    user_unit = @user_units.is_deleted.find(params[:id])
    if params[:user_id].present? && params[:user_id].to_i == 0
      render_data({ units: user_unit.as_api_response(:v5_unit_pictures) }, I18n.t('unit.detail')) if user_unit
    else
      # user_unit.liked_by_user = (UserLike.if_liked_by_user @current_user, user_unit.id).present? ? 1 : 0
      user_unit.liked_by_user = $redis.sismember("mbeet_user:#{@current_user}:units:liked", user_unit.id).present? ? 1 : 0
      render_data({ units: user_unit.as_api_response(:v5_unit_pictures) }, I18n.t('unit.detail')) if user_unit
    end
  end


  # GET /v4/user_units/enable (start_date, end_date, number_of_subunits, id are required params)
  def enable
    if @user_unit.update(unit_enabled: true)
      @user_unit.unit_disabled_dates.each do |udd|
        if params[:start_date].present? && params[:end_date].present? && params[:number_of_disabled_units].present?
          if (params[:start_date].to_datetime..params[:end_date].to_datetime).include?(udd.day_disabled.to_date)
            udd.destroy
          end
        else
          udd.destroy
        end
      end

      @user_unit.liked_by_user = $redis.sismember(
        "mbeet_user:#{@current_user}:units:liked",
        @user_unit.id
      ).present? ? 1 : 0

      render_updated ({
                        message: I18n.t('unit.enabled'),
                        data: { unit: @user_unit.as_api_response(:v4_unit_pictures) }
      })
    else
      render_unprocessable_entity( message: @user_unit.errors.full_messages.join(', '))
    end
  end


end
