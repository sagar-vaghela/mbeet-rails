class Api::V2::PasswordsController < Api::V2::BaseController

  before_action :get_user, only: [:edit, :update]
  before_action :login_required

  def create
    if @user = User.find_by(email: params[:email])
      @user.create_reset_digest
      @user.send_password_reset_email
      render_data({}, I18n.t('user.reset_email_sent') )
    else
      error_data( I18n.t('user.email_error') )
    end
  end

  def update
    # @user.update_attributes(user_params)
    # if @user.save!
    #   render_data({}, I18n.t('user.password_reset_success') )
    # end
    # rescue Exception => e
    #   error_valid_data(e)
  end

  def change
    @user = User.find_by id: @current_user
    check_required_params params, [:password, :password_confirmation]
    if @user.change_password params[:password], params[:password_confirmation]
      render_success message: I18n.t('password.changed')
    else
      render_unprocessable_entity message: @user.errors.full_messages.join(', ')
    end
  end

  # private
  # def get_user
  #    @user = User.find_by(reset_digest: params[:token])
  # end

  # def user_params
  #      params.require(:user).permit(:password, :password_confirmation)
  #  end

  # # Checks expiration of reset token.
  # def check_expiration
  #   if @user.password_reset_expired?
  #     error_data( I18n.t('user.reset_email_token_expired') )
  #   end
  # end
end
