require 'rails_helper'
require 'jwt'

RSpec.describe Api::V4::UserUnitsController , type: :controller do

  before(:each) do
    @unit = FactoryBot.create(:unit)
  end

  describe '#enable units' do

    context '/v4/user_units/enable' do
      it 'should enable unit for that bookings' do
        token = JsonWebToken.encode(user_id: @unit.user.id) if @unit
        get :enable, params: {access_token: token, start_date: "2017-12-19", end_date: "2017-12-21", number_of_subunits: 0, id: @unit.id, format: :json}

        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status 200

      end

    end
  end

  describe '#list disable units' do

    context '/v4/user_units/list_disable_units' do
      it 'should list of units' do
        token = JsonWebToken.encode(user_id: @unit.user.id) if @unit
        get :list_disable_units, params: {access_token: token, id: @unit.id, format: :json}

        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status 200

      end

    end
  end

end
