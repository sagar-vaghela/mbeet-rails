class CreateUserBookingCancelRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :user_booking_cancel_requests do |t|
      t.references :user, foreign_key: true, null: false
      t.references :unit, foreign_key: true, null: false
      
      t.boolean :reason, null: false
      t.text :message, null: false

      t.timestamps
    end
  end
end
