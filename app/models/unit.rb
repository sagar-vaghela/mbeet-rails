# == Schema Information
#
# Table name: units
#
#  id                 :integer          not null, primary key
#  user_id            :integer          not null
#  title_translations :jsonb            not null
#  body_translations  :jsonb            not null
#  number_of_guests   :integer          default(0), not null
#  number_of_rooms    :integer          default(1), not null
#  number_of_beds     :integer          default(0), not null
#  number_of_baths    :integer          default(0), not null
#  available_as       :integer          default(1), not null
#  address            :text             not null
#  price              :decimal(10, 2)   default(0.0), not null
#  service_charge     :decimal(6, 2)    default(0.0), not null
#  latitude           :decimal(20, 8)
#  longitude          :decimal(20, 8)
#  unit_class         :string           default("a"), not null
#  soft_delete        :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  room_type          :integer          default(1), not null
#  city_id            :integer          default(1), not null
#  unit_type          :integer          default(1), not null
#  total_area         :decimal(20, 3)   default(0.0), not null
#  unit_enabled       :boolean          default(FALSE)
#  unit_status        :boolean          default(FALSE)
#  mbeet_percentage   :integer          default(10)
#  bed_type           :integer
#  number_of_subunits :integer          default(0)
#

class Unit < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  # ------------------------ Sunspot search------------------ #
  searchable do

    # ---------------------- Same Model Fields ----------------- #
    integer :id, multiple: true
    integer :number_of_rooms
    integer :number_of_beds
    integer :number_of_guests
    integer :number_of_baths
    integer :room_type
    integer :unit_type
    integer :bed_type
    string  :unit_class
    integer :available_as
    integer :number_of_subunits
    double  :price
    # text    :city
    integer :city_id
    text    :address
    boolean :soft_delete
    time    :created_at
    boolean :unit_status
    boolean :unit_enabled


    # To set up condition for HMBTM
    integer :amenities, multiple: true do
      amenities.map(&:id)
    end
    integer :amenity_ids, multiple: true

    integer :user_likes, multiple: true do
      user_likes.map { |user_like| user_like.unit_id }
    end


    text :city do
      city.name
    end


    # ------------------ added with admin v3 ---------------------- #

    text :address, :title_translations, :body_translations, :price, :id
    # ------------------------------------------------------------- #
    before_save :downcase_unit_class

    # Association to Bookings table

    # join(:check_in, :target => Booking, :type => :time, :join => { :from => :unit_id, :to => :id })
    # join(:check_out, :target => Booking, :type => :time, :join => { :from => :unit_id, :to => :id })
  end

  # ------------------------ Associations --------------------- #
  include Units::Associations

  # ------------------------ Mobile Api Response -------------- #
  include V1::UnitResponder
  include V2::UnitResponder
  # user units responder
  include V3::UnitResponder
  include V4::UnitResponder
  include V5::UnitResponder
  include V6::UnitResponder

  # ------------------------ Admin Api Response --------------- #
  include V1::Admin::UnitResponder
  include V2::Admin::UnitResponder
  include V3::Admin::UnitResponder
  include V4::Admin::UnitResponder
  include V5::Admin::UnitResponder
  include V6::Admin::UnitResponder

  # ------------------------- callbacks ----------------------- #
  validate :check_if_any_subunit_are_booked
  # after_update :unit_attributes_changed

  # ------------------------ Api Methods ---------------------- #
  def avg_star
    # To return average value with precision 2 use sprintf("%0.02f")
    val = self.feedbacks.average(:star)
    val.present? ? sprintf( "%0.02f", val) : 0
  end

  def feedback_count
    self.feedbacks.size
  end

  def default_image
    CONSTANT[:cdn_cloudy_url] + CONSTANT[:default_flat_image]
  end


  def send_admin_email_by_user_flat_changes
    self.update(unit_status: false)
    save!
    Resque.enqueue(OwnerUpdateFlatEmailWorker, self.id)
  end

  # enable all disabled unit if the user or admin change number of subunits
  # def unit_attributes_changed
  #   unless self.number_of_subunits_was == self.number_of_subunits
  #     if self.number_of_subunits_was > self.number_of_subunits
  #       self.unit_disabled_dates
  #       .where("number_of_disabled_units >= '#{self.number_of_subunits}'")
  #       .update_all(number_of_disabled_units: self.number_of_subunits)
  #     end
  #   end
  # end

  # check if any subunits are booked or not
  def check_if_any_subunit_are_booked
    # debugger
    booked_days_any = self.booked_subunits.where("day_booked >= '#{Time.now.to_date}'")
    disabled_days_any = self.unit_disabled_dates.where("day_disabled >= '#{Time.now.to_date}'")

    if  booked_days_any.all?{ |booking| booking.count <= self.number_of_subunits}
      subunits_count =  self.number_of_subunits_was - self.number_of_subunits
      if self.number_of_subunits_was > self.number_of_subunits
        if self.number_of_subunits == 0
          self.unit_disabled_dates.destroy_all
        else
          self.unit_disabled_dates.each do |disabled|
            current_count = disabled.number_of_disabled_units
            new_count = current_count - subunits_count < 0 ? 0 : current_count - subunits_count
            disabled.update(number_of_disabled_units: new_count)
          end
        end
      end
    else
      errors.add(:base, :not_available)  if booked_days_any.present?
    end
  end

  def downcase_unit_class
    self.unit_class.downcase!
  end
end
