class CreateRequestToListUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :request_to_list_units do |t|
      t.references :user, foreign_key: true, null: false
      t.string :name, null: false
      t.string :email, null: false
      t.string :phone, null: false
      t.text :message, null: false

      t.timestamps
    end
  end
end
