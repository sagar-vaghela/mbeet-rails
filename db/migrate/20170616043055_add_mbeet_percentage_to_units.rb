class AddMbeetPercentageToUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :mbeet_percentage, :integer, default: 10
  end
end
