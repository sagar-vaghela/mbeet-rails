module V4::BookingResponder
	extend ActiveSupport::Concern

	included do
		api_accessible :v4_my_orders do |t|
	    t.add :id
	    t.add :check_in
	    t.add :check_out
	    t.add :payment_status
	    t.add :lease_status
	    t.add :total_amount
			t.add :advance_payment
			t.add lambda{|booking| (booking.payment_status == 3 && booking.check_out < Time.now) ?  1 : 0 } , :as => :hide_cancel
	    t.add :booking_cancelled
	    t.add :created_at
	    t.add :feedback
	  end

	  api_accessible :v4_unit_with_bookings, extend: :v4_my_orders do |t|
	    t.add :unit, :template => :unit_with_pictures_for_myorders
	  end

	  api_accessible :v4_my_order_detail, extend: :v4_my_orders do |t|
	    t.add :guests
	    t.add :for_days, as: :rented_for_days
	    t.add :sub_total_amount
			t.add lambda{|booking| booking.unit.user_id} , :as => :owner_id
	  end

	  api_accessible :v4_unit_with_bookings_extra, extend: :v4_my_order_detail do |t|
	    t.add :unit, :template => :unit_with_pictures_for_myorders
	  end

	  api_accessible :v4_booking_with_payment_txns, extend: :v4_unit_with_bookings_extra do |t|
	    t.add :payment_txn, :template => :payment_transaction
	  end

		api_accessible :v4_booking_with_customer_info, extend: :v4_booking_with_payment_txns do |t|
	    t.add :user, :template => :customer_phone_no_name
	  end
	end
end
