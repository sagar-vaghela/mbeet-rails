class Api::Admin::V6::TopDestinationsController < Api::Admin::V6::BaseController
  before_action :admin_login_required
  before_action :find_top_destination, only: [:update, :show, :destroy]
  before_action :get_picture_ids, only: [:update]
  before_action :find_current_user

  # GET: '/admin/v6/top_destinations'
  def index
    impressionist(@user, request.path) if @user

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = TopDestination.search do
      data_accessor_for(TopDestination).include = [:city]
      fulltext params[:search]

      order_by(:created_at, :desc)
      paginate(:offset => params[:offset], :per_page => limit)
    end

    render_data({ total: TopDestination.all.size, result_total: search.total, top_destination: search.results.as_api_response(:v6_top_destination_pictures_admin)}, I18n.t('top_destination.top_destination'))
  end

  # GET: '/admin/v6/top_destinations/top_destin_cities'
  def top_destin_cities
    impressionist(@user, request.path) if @user

    city_ids = []
    city_ids = TopDestination.pluck(:city_id)
    remaining_cities = city_ids.present? ? City.where.not('id IN(?)', city_ids) : City.all

    render_data({ total: remaining_cities.size, result_total: remaining_cities.size, cities: remaining_cities.as_api_response(:city)}, I18n.t('city.remaining_cities'))
  end

  # POST: 'admin/v6/top_destinations/'
  def create
    impressionist(@user, request.path) if @user

    check_required_params params, [:description_en, :description_ar]
    top_destination = TopDestination.new top_destination_params
    top_destination.description_translations = {en: top_destination.description_en, ar: top_destination.description_ar}
    top_destination.city_id = params[:city_id] if params[:city_id].present?
    if top_destination.save
        if params[:pictures_attributes][0][:image_en].present?
        image = Picture.new
        image.name = params[:pictures_attributes][0][:image_en] if params[:pictures_attributes].present?
        image.imageable_type = 'TopDestination'
        image.imageable_id = top_destination.id
        image.save
      end
      if params[:pictures_attributes][0][:image_ar].present?
        image = Picture.new
        image.name = params[:pictures_attributes][0][:image_ar] if params[:pictures_attributes].present?
        image.imageable_type = 'TopDestination'
        image.imageable_id = top_destination.id
        image.locale = true
        image.save
      end

     render_created ({
       message: I18n.t('top_destination.created'),
       data: { top_destination: top_destination.as_api_response(:v6_top_destination_pictures_admin) }
     })
    else
     render_unprocessable_entity( message: top_destination.errors.full_messages.join(', '))
    end
  end

  # GET: 'admin/v6/top_destinations/'
  def show
    impressionist(@user, request.path) if @user

    render_data({ top_destination: @top_destination.as_api_response(:v6_top_destination_pictures_admin) }, I18n.t('top_destination.detail')) if @top_destination
  end

  # PUT: '/admin/v6/top_destinations/:id'
  def update
    impressionist(@user, request.path) if @user

    if (@ar_picture.present? && @en_picture.present? && params[:pictures_attributes].present?)
      @ar_picture.update(name: params[:pictures_attributes][0][:image_ar]) if params[:pictures_attributes][0][:image_ar]
      @en_picture.update(name: params[:pictures_attributes][0][:image_en]) if params[:pictures_attributes][0][:image_en]
    end

    if @top_destination.update top_destination_params
      render_updated ({
        message: I18n.t('top_destination.updated'),
        data: { top_destination: @top_destination.as_api_response(:v6_top_destination_pictures_admin) }
        })
    else
      render_unprocessable_entity( message: @top_destination.errors.full_messages.join(', '))
    end
  end

  # DELETE: '/admin/v6/top_destinations/:id'
  def destroy
    impressionist(@user, request.path) if @user

    render_data({ top_destination: {} }, I18n.t('top_destination.deleted')) if @top_destination.destroy!
  end

  private

  def find_top_destination
    @top_destination = TopDestination.find(params[:id])
  end

  def top_destination_params
    params.permit(:description_en, :description_ar, :city_id)
  end

  def get_picture_ids
    @ar_picture = Picture.find(params[:imageAr_id])
    @en_picture = Picture.find(params[:imageEn_id])
  end

  def find_current_user
    @user = User.find_by(id: @current_user, role_id: [1, 2])
  end
end
