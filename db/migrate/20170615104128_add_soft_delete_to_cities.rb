class AddSoftDeleteToCities < ActiveRecord::Migration[5.0]
  def change
    add_column :cities, :soft_delete, :boolean, default: false
  end
end
