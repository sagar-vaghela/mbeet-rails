class Api::V4::MyOrdersController < Api::V3::MyOrdersController
  before_action :login_required
  before_action :check_my_orders, only: :index

  def index
    render_data({ total: Booking.where(user_id: params[:user_id]).count, my_orders: @my_order_list.as_api_response(:v4_unit_with_bookings) }, I18n.t('user.my_orders') )
  end

  def show
    @my_order_detail = Booking.find(params[:id])
    render_data({ my_orders: @my_order_detail.as_api_response(:v4_booking_with_payment_txns) }, I18n.t('user.my_order_detail')) if @my_order_detail.present?
  end

  private
  def check_my_orders
    @my_order_list = Booking.where(user_id: params[:user_id])
                            .order(id: :desc)
                            .limit(params[:limit].to_i == -1 ? 100 : params[:limit])
                            .offset(params[:offset])
    render_data({}, I18n.t('no_data')) unless @my_order_list.present?
  end
end
