class Api::V3::ListMyUnitController < Api::V3::BaseController
	before_action :login_required #, :already_requested

	def create
		@requested = RequestToListUnit.create!(list_my_unit_params)
		render_data({ request_to_list: {} }, I18n.t('user.request.success') ) if @requested
	rescue StandardError => e
		error_valid_data(e)
	end

	private
	def list_my_unit_params
		# params.permit(:user_id, :unit_id, :name, :email, :phone, :message)
		params.permit(:user_id, :name, :email, :phone, :message)
	end

	# def already_requested
	# 	requested = RequestToListUnit.find_by(user_id: params[:user_id], unit_id: params[:unit_id], name: params[:name],
	# 		                        email: params[:email], phone: params[:phone], message: params[:message])
	# 	error_valid_data(I18n.t('user.request.already')) if requested.present?
	# end
end
