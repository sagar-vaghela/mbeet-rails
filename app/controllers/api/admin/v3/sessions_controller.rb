class Api::Admin::V3::SessionsController < Api::Admin::V3::BaseController
	before_action :check_null_password

	# POST /admin/v3/login
	def create
		if @user && @user.authenticate(params[:password])
			auth_token = JsonWebToken.encode({user_id: @user.id})
			render_data( { auth_token: auth_token, users: @user.as_api_response(:v3_user_only) }, I18n.t('admin.user.success') )
		else
			error_data( I18n.t('admin.user.error') )
		end
	end

	private
	def check_null_password
		check_required_params params, [:email, :password]
		return error_valid_data( I18n.t('admin.user.error') ) if params[:email].blank? || params[:password].blank?
		@user = User.where(soft_delete: false).find_by(email: params[:email].downcase)
		error_valid_data( I18n.t('admin.user.wrong') ) if (@user.present?) && @user["role_id"] != 1
	end

end
