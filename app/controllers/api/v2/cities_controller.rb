class Api::V2::CitiesController < Api::V2::BaseController
	def index
		@cities = City.order(get_order).limit(params[:limit].to_i == -1 ? 100 : params[:limit]).offset(params[:offset])
		render_data({ cities: @cities.as_api_response(:cities_list) }, I18n.t('user.my_orders') ) if @cities.present?
	end

	private
	def get_order
		"name_translations->>'#{I18n.locale}' ASC"
	end
end
