class Api::Admin::V6::UsersController < Api::Admin::V5::UsersController
  before_action :admin_login_required
  before_action :check_user, except: [:index, :create, :owners, :dashbord_users , :getCurrentUserPermission]
  after_action  :rescue_response
  before_action :find_current_user, except: :getCurrentUserPermission

  # GET
  def index
    impressionist(@cur_user, request.path) if @cur_user

    limit = params[:limit].to_i == -1 ? 100 : params[:limit]
    search = User.search do
      fulltext params[:search]

      if params[:soft_delete].present?
        status = params[:soft_delete] == 'true' ? true : false
        with(:soft_delete, status)
      end

      if params[:is_owner].present?
        status = params[:is_owner] == 'true' ? true : false
        with(:is_owner, status)
        with(:soft_delete, false)
      end

      if params[:email_verified].present?
        status = params[:email_verified] == 'true' ? true : false
        with(:email_verified, status)
        with(:soft_delete, false)
      end

      if params[:phone_verified].present?
        status = params[:phone_verified] == 'true' ? true : false
        with(:phone_verified, status)
        with(:soft_delete, false)
      end

      # with(:role,   params[:role]) if params[:role].present?
      if params[:role].present?
        with(:role,params[:role])
        with(:soft_delete, false)
      end
      if params[:sort_by].present? && params[:sort_direction].present?
        order_by(params[:sort_by].downcase.to_sym, params[:sort_direction].downcase.to_sym)
      else
        order_by(:created_at, :desc)
      end
      paginate(:offset => params[:offset], :per_page => limit)
    end
    # @users = User.order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
    render_data({ total: User.where(soft_delete: false).size, result_total: search.total ,users: search.results.as_api_response(:v4_admin_user_only)}, I18n.t('user.list'))
  end

	# GET
	def show
    impressionist(@cur_user, request.path) if @cur_user
		render_data({ users: @user.as_api_response(:v4_admin_user_only) }, I18n.t('user.detail'))
	end

	# POST
  def create
    impressionist(@cur_user, request.path) if @cur_user
    @user = User.new(user_params)
    @user.role_id_exist = params[:role_id].empty?
    @user.skip_password_validation = true
    @user.is_owner = true if @user.role_id == 2

    if @user.save!
      # Uncomment this to send email to user
      @user.send_login_detail_to_user
      render_data({ users: @user.as_api_response(:v4_admin_user_only) }, I18n.t('admin.user.created'))
    end
    rescue ActiveModel::StrictValidationFailed => e
      error_valid_data(e)
    rescue ActiveRecord::RecordNotUnique => e
      err = @user.errors.full_messages.try(:first).present? ? @user.errors.full_messages.try(:first) : e
      error_valid_data(err)
    rescue ActiveRecord::RecordInvalid => e
      err = @user.errors.full_messages.try(:first).present? ? @user.errors.full_messages.try(:first) : e
      error_valid_data(err)
  end

	# PUT
	def update
    impressionist(@cur_user, request.path) if @cur_user

		@user.skip_password_validation = true
		@user.update(user_params)

		if @user.role_id != 2
			@user.is_owner = false
		else
			@user.is_owner = true
		end

		render_data({ users: @user.as_api_response(:v4_admin_user_only) }, I18n.t('admin.user.updated')) if @user.save!
	end

	# DELETE
	def destroy
    impressionist(@cur_user, request.path) if @cur_user

		# all these conditions will check if the user has some bookings or some related units to avoid deleting
		# a user with some bookings or some related units
		@user.skip_password_validation = true
		if Unit.where(user_id: @user.id).any?
			if Unit.where(user_id: @user.id).all?{ |unit| unit.soft_delete == true }
				if find_bookings.any?
					render_unprocessable_entity( message: I18n.t('admin.user.has_some_bookings'))
				else
          if  @user.update(soft_delete: true)
						render_data({ users: {} }, I18n.t('admin.user.deleted'))
	        else
	          render_unprocessable_entity( message: @user.errors.full_messages.join(', '))
	        end
		    end
			else
				render_unprocessable_entity( message: I18n.t('admin.user.has_some_units'))
			end
		else
			if find_bookings.any?
				render_unprocessable_entity( message: I18n.t('admin.user.has_some_bookings'))
			else
        if  @user.update(soft_delete: true)
				  render_data({ users: {} }, I18n.t('admin.user.deleted'))
				else
				  render_unprocessable_entity( message: @user.errors.full_messages.join(', '))
				end
	    end
		end
	end

	# GET
	def owners
    impressionist(@cur_user, request.path) if @cur_user

		@owners = User.owners.order(name: :asc)
		render_data({ owners: @owners.as_api_response(:owners)}, I18n.t('admin.user.owners')) if @owners.present?
	end

	def units
    impressionist(@cur_user, request.path) if @cur_user

		@units = Unit.where(user_id: params[:id]).order(id: :desc).limit(params[:limit] == -1 ? 100 : params[:limit]).offset(params[:offset])
		if @units.present?
			render_data({ total: @units.size, units: @units.as_api_response(:v4_admin_unit_only)}, I18n.t('unit.list'))
		else
			render_no_data
		end
	end

	def units_interested_on
    impressionist(@cur_user, request.path) if @cur_user

		units_interested_on_ids = UserLike.where(user_id: params[:id]).pluck(:unit_id)
		@units = Unit.find(units_interested_on_ids)
		# render json:{untis: @units}
		render_data({ total: @units.size, units: @units.as_api_response(:v4_admin_unit_only)}, I18n.t('unit.list'))
	end

  def dashbord_users
    impressionist(@cur_user, request.path) if @cur_user

    if params[:date].present?
      date = Date.parse(params[:date])
      render json: {
        message: I18n.t('user.today_registred'),
        active:  User.where('DATE(created_at) = ? AND soft_delete = ?', date, false).count,
        deleted: User.where('DATE(created_at) = ? AND soft_delete = ?', date, true).count}
    else
      render json: {
        message: I18n.t('user.total_registred'),
        active: User.where(soft_delete: false).count,
        deleted: User.where(soft_delete: true).count}
    end
  end

  def getCurrentUserPermission
    @user = User.find(@current_user)
    render_data({ user_permissions: @user.as_api_response(:user_permissions) }, "user permission") if @user
  end

  private
  def find_current_user
    @cur_user = User.find_by(id: @current_user, role_id: [1, 2])
  end

end
