class SaveUserNotificationsWorker
	@queue = "save_notifications"

	def self.perform data, push_message, push_type
		Notification.create(user_id: data["user_id"], booking_id: data["id"], body: JSON.parse(push_message), push_type: push_type)
	end
end