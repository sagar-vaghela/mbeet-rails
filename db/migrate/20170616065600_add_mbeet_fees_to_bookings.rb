class AddMbeetFeesToBookings < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :mbeet_fees, :decimal, precision: 10, scale: 2
  end
end
