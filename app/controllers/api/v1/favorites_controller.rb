class Api::V1::FavoritesController < Api::V1::BaseController
	before_action :login_required, :check_favourites #,:validate_limit_offset

	# GET: /v2/favourites
  def index
  	render_data({ favourites: @favourites.as_api_response(:favourites_list) }, I18n.t('favourite.list'))
  end

  private
  def check_favourites
  	@favourites = UserLike.where(user_id: params[:user_id])
  	                      .order(created_at: :desc)
  	                      .limit(params[:limit] == -1 ? 100 : params[:limit])
  	                      .offset(params[:offset])
  	render_data({}, I18n.t('no_data')) unless @favourites
  end
end
