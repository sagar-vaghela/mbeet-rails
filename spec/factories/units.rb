FactoryBot.define do
  factory :unit do
    association :user, factory: :owner_user
    association :city
    title_translations "4bhk home available"
    body_translations "Lorem ipsum Lorem ipsum Lorem ipsum"
    number_of_guests 5
    number_of_rooms 2
    number_of_beds 2
    number_of_baths 2
    available_as 3
    address "california street"
    service_charge 200
    latitude 0.45e2
    longitude 0.5636521456e2
    unit_class 'a'
    soft_delete false
    created_at "Sat 16 Dec 2017"
    unit_status false
    unit_enabled false
  end
end
