class CreateCoupons < ActiveRecord::Migration[5.0]
  def change
    create_table :coupons do |t|
      t.references :creator, references: :user, null: false
      t.integer :value, null: false
      t.datetime :starting_date, null: false
      t.datetime :expiration_date, null: false
      t.integer :used_coupon_count, null: false, default: 0
      t.decimal :total_charged_amount, precision: 10, scale: 2, null: false, default: '0'
      t.decimal :total_discounted_amount, precision: 10, scale: 2, null: false, default: '0'
      t.decimal :average_charged_amount, precision: 10, scale: 2, null: false, default: '0'
      t.decimal :average_discount_amount, precision: 10, scale: 2, null: false, default: '0'
      t.integer :status

      t.timestamps
    end
  end
end
