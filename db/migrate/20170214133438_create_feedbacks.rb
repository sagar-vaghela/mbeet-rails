class CreateFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|
      t.references :user, foreign_key: true, null: false
      t.references :unit, foreign_key: true, null: false

      t.decimal :star, precision: 3, scale: 1, null: false, default: 0.0
      t.text :feedback, null: false

      t.timestamps
    end
  end
end
