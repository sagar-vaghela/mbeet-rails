class OrderEmailWorker
	@queue = 'user_order_email'

	def self.perform data, push_type
		user = User.where(id: data["user_id"]).pluck(:email, :name).first
		invoice_order_id = PaymentTxn.where(booking_id: data["id"]).pluck(:invoice_unique_id).first

		# Send email to user and admins related to his booking order
		OrderMailer.invoice(user.first, user.second, invoice_order_id).deliver
		OrderMailer.invoice_to_admin(user.first, user.second, invoice_order_id).deliver_now
	end
end
