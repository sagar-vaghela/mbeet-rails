require 'rails_helper'

RSpec.describe Api::Admin::V4::CouponsController, type: :controller do

  before(:each) do
    @coupon = FactoryBot.create(:coupon)
    @coupons = Coupon.all
    @creator = @coupon.user
    @token = JsonWebToken.encode(user_id: @creator.id) if @creator
  end

  describe '#index' do
    context 'when coupons are present' do
      it 'should show coupons' do
        expect(response).to have_http_status 200
        expect(@coupons.count).to eq(1)
      end

    end
  end

  describe '#show' do

    context 'should show coupon details' do
      it '/admin/v4/coupons/:id' do
        get :show, params: {access_token: @token, id: @coupon.id, format: :json}
        expect(response.content_type).to eq "application/json"
      end
    end
  end

  describe '#create' do
    context 'create coupon' do
      let(:params) { {
        creator_id: @coupon.user.id,
        value: 12,
        starting_date: "Fri, 01 Dec 2017",
        expiration_date: "Mon, 01 Jan 2018",
        status: "enable"
      } }

      it '/admin/v4/coupons' do
        post :create, params: {access_token: @token, coupon: params, format: :json}
        expect(response.content_type).to eq "application/json"
      end

    end
  end

  describe '#update' do
    context 'update old coupon' do

      it '/admin/v4/coupons/:id' do
        put :update, params: {access_token: @token, id: @coupon.id, format: :json}
        expect(response.content_type).to eq "application/json"
      end

    end
  end

  describe '#delete' do
    context 'delete a coupon' do

      it '/admin/v4/coupons/:id' do
        delete :destroy, params: {access_token: @token, id: @coupon.id, format: :json}
        expect(response.content_type).to eq "application/json"
      end

    end
  end

end
