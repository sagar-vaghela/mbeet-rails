# == Schema Information
#
# Table name: pictures
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  imageable_type :string           not null
#  imageable_id   :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Picture < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
  # ------------------------- Associations ----------------------- #
  belongs_to :imageable, polymorphic: true

  # ------------------------ Admin Api Response --------------- #
  include V6::Admin::PictureResponder

  # ------------------------- Mobile Api Response ---------------- #
  api_accessible :images do |t|
    t.add :id
    t.add :url
  end

  api_accessible :user_image do |t|
    t.add :url
  end

  api_accessible :admin_images do |t|
    t.add :id
    t.add :url, as: "name"
  end

  def url
    CONSTANT[:cdn_cloudy_url] + self.name
  end

end
