# == Schema Information
#
# Table name: rules
#
#  id                :integer          not null, primary key
#  name_translations :jsonb            not null
#  status            :boolean          default(TRUE), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Rule < ApplicationRecord

  # maintain user visited urls
  is_impressionable
  
	# ------------------- Associations --------------------- #
  has_and_belongs_to_many :units

	# Without this you can't access jsonb column correctly with locale
  translates :name

	default_scope { where(status: true) }

	# ------------------ Mobile & Admin Api Response --------------- #
  api_accessible :rules_and_units do |t|
  	t.add :id
  end

  api_accessible :rule_list do |t|
  	t.add :id
    t.add :name
  end
end
